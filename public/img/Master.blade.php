<!DOCTYPE html>
<html lang="tr">
    <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('Title')</title>

	<meta name="description" content="@yield('MetaDescription')" />
	<meta name="keywords" content="@yield('MetaTag')"/>
	<meta name="robots" content="index, follow"> 
	<link rel="icon" href="{{url('images/favicon.ico')}}" type="image/x-icon">
	
	<!-- >> CSS ============================================================================== -->
	<!-- Bootstrap -->
	<link href="{{url('plugins/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
	<!-- /Bootstrap -->
	<!-- Google Web Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
	<!-- /google web fonts -->
	<!-- owl carousel -->
	<link href="{{url('plugins/owl.carousel/owl-carousel/owl.carousel.css')}}" rel="stylesheet">
	<!-- /owl carousel -->
	<!-- Font Awesome -->
	<link rel="stylesheet" href="{{url('plugins/font-awesome/css/font-awesome.css')}}">
	<!-- /Font Awesome -->
	<link rel="stylesheet" href="{{url('css/style.css')}}">
	<link rel="stylesheet" href="{{url('css/responsive.css')}}">
        <link href="{{url('css/sistem.css')}}" rel="stylesheet" />
	<!-- Add fancyBox -->
	<link rel="stylesheet" href="{{url('plugins/fancybox/source/jquery.fancybox.css?v=2.1.5')}}" type="text/css" media="screen" />
        <link href="{{url('plugins/menu/jquery.smartmenus.bootstrap.css')}}" rel="stylesheet" />
        <style>
            #top-link-block.affix-top {
                position: absolute; /* allows it to "slide" up into view */
                bottom: -82px; /* negative of the offset - height of link element */
                right: 10px; /* padding from the left side of the window */
            }
            #top-link-block.affix {
                position: fixed; /* keeps it on the bottom once in view */
                bottom: 18px; /* height of link element */
                right: 10px; /* padding from the left side of the window */
            }
            .btn-circle {
                width: 49px;
                height: 49px;
                text-align: center;
                padding: 5px 0;
                font-size: 20px;
                line-height: 2.00;
                border-radius: 30px;
                background-color: #07A7E3;
                opacity: 0.5;
            }
        </style>
    @yield('css')
    
</head>
<body>

<div class="header arkarenk">
	<div class="container">
		<div class="col-xs-12 col-sm-4 ustbilgisol hidden-xs">
			<p><i class="fa fa-phone"></i> {{ str_replace(['-','('],[' ',' ('],$subeler->Telefon) }}</p>
		</div>
			<div class="col-xs-12 col-sm-4 ustbilgiorta">
				<div class="mid-logo">
		<a href="{{ url($dil.'/') }}">
					<img src="{{url('images/uploads/'.$ayarlar->Logo)}}">
		</a>
				</div>
			</div>
		<div class="col-xs-12 col-sm-4 ustbilgisag hidden-xs">
			<p><i class="fa fa-home"></i> {{$subeler->Adres }}</p>
		</div>

		<div class="col-xs-12 ustbilgisol hidden-sm hidden-md hidden-lg xs-text-center">
			<p><i class="fa fa-phone"></i> {{ str_replace(['-','('],[' ',' ('],$subeler->Telefon) }}</p>
			<a style='text-decoration: none;' href="{{ url($dil.'/iletisim') }}"><p><i class="fa fa-calendar"></i> {{ Fnk::Ceviri('Randevu') }}</p></a>
		</div>
	</div>
</div>
@if(empty(Request::segment(2)))
{!!Fnk::Slider('',false)!!}
@endif
<div class="clearfix"></div>
<div class="arkarenk">
  <div class="container">
    	{!!Fnk::Menuler()!!}
  </div>
</div>
<div class="clearfix"></div>
<div class="navigasyon container">
	<div class="col-xs-12">
		@yield('navigasyon')
	</div>	
</div>
<div class="main">
	@yield('content')
</div>

<span id="top-link-block" class="hidden">
    <a href="#top" class="btn btn-lg btn-circle" style='color: #CCC' onclick="$('html,body').animate({scrollTop:0},'slow');return false;">
        <i class="glyphicon glyphicon-chevron-up"></i>
    </a>
</span><!-- /top-link-block -->

<footer class="footer arkarenk">
	<div class="container">
		<div class="col-xs-12 col-sm-6 col-md-3">
			<p id="footerlogo"><img src="{{url('images/uploads/'.$ayarlar->Logo)}}" width="120" height="75"></p>
			{!!Fnk::SosyalAglar()!!}
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6">
			<p><i class="fa fa-location-arrow mr20"></i>{{$subeler->Adres}}</p>
			<p><i class="fa fa-phone mr20"></i>{{str_replace(['-','('],[' ',' ('],$subeler->Telefon)}}</p>
			@if($subeler->Eposta!='')
				<p><i class="fa fa-envelope-o mr20"></i>{{$subeler->Eposta}}</p>
			@endif
		</div>
			{!!Fnk::AltMenuler()!!}
		<div class="col-xs-12 col-sm-6 col-md-3 pull-right">
			<a href="{{ url('http://www.portakalyazilim.com.tr/tr/eren-elektronik-web-sayfasi/') }}" class="pull-right" title="Portakal Yazılım" target="_blank"><img src="{{ url('images/portakalLogoNormal.png') }}"></a>
		</div>
	</div>
</footer>
<!-- >> JAVASCRIPT
============================================================================== -->

<script src="{{url('plugins/vendor/jquery.min.js')}}"></script>
<script src="{{url('plugins/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{url('plugins/fancybox/source/jquery.fancybox.pack.js?v=2.1.5')}}"></script>
<!--[if lte IE 8]> <script type="text/javascript" src="http://lab.momm.com.br/atualizzr/chromelab.js"></script> <![endif]-->

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="{{url('plugins/vendor/html5shiv.js')}}"></script>
	<script src="{{url('plugins/vendor/respond.min.js')}}"></script>
<![endif]-->

<!-- >> /JAVASCRIPT
============================================================================= -->
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "8d92a2e3-c95b-4026-b162-55d697a2b1ba", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
<script src="{{url('plugins/menu/jquery.smartmenus.js')}}"></script>
<script src="{{url('plugins/menu/jquery.smartmenus.bootstrap.js')}}"></script>
<script type="text/javascript">
	function printMain() {
        var printContents = document.getElementsByClassName('main')[0].innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }
$(function() {
  $('#main-menu').smartmenus();
});
/*	$('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
	    // Avoid following the href location when clicking
	    event.preventDefault(); 
	    // Avoid having the menu to close when clicking
	    event.stopPropagation(); 
	    // If a menu is already open we close it
	    //$('ul.dropdown-menu [data-toggle=dropdown]').parent().removeClass('open');
	    // opening the one you clicked on
	    $(this).parent().addClass('open');

	    var menu = $(this).parent().find("ul");
	    var menupos = menu.offset();
	  
	    if ((menupos.left + menu.width()) + 30 > $(window).width()) {
	        var newpos = - menu.width();      
	    } else {
	        var newpos = $(this).parent().width();
	    }
	    menu.css({ left:newpos });

	}); */
    
// Only enable if the document has a long scroll bar
// Note the window height + offset
if ( ($(window).height() + 100) < $(document).height() ) {
    $('#top-link-block').removeClass('hidden').affix({
        // how far to scroll down before link "slides" into view
        offset: {top:100}
    });
}

  $(document).ready(function() {

   var docHeight = $(window).height();
   var footerHeight = $('.footer').height();
   var footerTop = $('.footer').position().top + footerHeight;

   if (footerTop < docHeight) {
    $('.footer').css('margin-top', (docHeight - (footerTop+3)) + 'px');
   }
  });
</script>
@yield('js')
{!! $ayarlar->Analytics !!}
<style>
	.breadcrumb{
		background-color: transparent !important;
	}
</style>
</body>
</html>
