@extends('Site.Layout.Master')

@section('Title',$menu->MetaTitle)
@section('MetaTag')
    {!! @implode(', ', json_decode(@$ayarlar->MetaTag ,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES)) !!}
@endsection
@section('MetaDescription',$menu->MetaDescription)

@section('content')


<div role="main" class="main">
    <section class="page-header page-header-custom-background parallax" style="background-color: rgba(57, 74, 9, 0.85);" data-plugin-parallax data-plugin-options='{"speed": 1.5}' data-image-src="{{asset('img/parallax.jpg')}}">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                <ul class="breadcrumb breadcrumb-valign-mid">
                    <li><a href="{{url($dil)}}">{{\App\Http\Fnk::Ceviri('anasayfa')}}</a> </li>
                    <li><a href="{{url($dil.'/'.\App\Http\Fnk::Ceviri('urunler-url').'/'.@$urun->Urun_UstKat)}}">{{\App\Http\Fnk::Ceviri('urunler')}}</a> </li>
                    <li class="active">{{$urun->Adi}}</li>
                </ul>
                <h1>{{$urun->Adi}}</h1>
            </div>
        </div>
    </div>
</section>
<div id="main" class="page">
    <div class="container">
        <div class="row">
            
            <article class="col-sm-12 col-md-12 col-xs-12 content product-page pull-right">
                @if($urun->Gosterim==2)
                    <div class="col-sm-12 col-md-6">
                        <div class="image-box">
                            @if(!empty(json_decode($urun->Resim)))
                            @foreach(json_decode($urun->Resim) as $index => $element)
                            @if($index==0)
                            <div class="col-md-4 col-xs-12">
                                <a class="img-thumbnail img-thumbnail-hover-icon lightbox" href="{{url('/images/uploads/Urun/resim/'.$element)}}" data-plugin-options='{"type":"image"}'>
                                    <img class="img-responsive" src="{{url('/images/uploads/Urun/resim/'.$element)}}" alt="Project Image" style="height: 250px">
                                </a>
                            </div>
                            @elseif($index==1)
                            <div class="col-md-4 col-xs-12">
                                <a class="img-thumbnail img-thumbnail-hover-icon lightbox" href="{{url('/images/uploads/Urun/resim/'.$element)}}" data-plugin-options='{"type":"image"}'>
                                    <img class="img-responsive" src="{{url('/images/uploads/Urun/resim/'.$element)}}" alt="Project Image" style="height: 250px">
                                </a>
                            </div> 
                            @elseif($index==2)
                            <div class="col-md-4 col-xs-12">
                                <a class="img-thumbnail img-thumbnail-hover-icon lightbox" href="{{url('/images/uploads/Urun/resim/'.$element)}}" data-plugin-options='{"type":"image"}'>
                                    <img class="img-responsive" src="{{url('/images/uploads/Urun/resim/'.$element)}}" alt="Project Image" style="height: 250px">
                                </a>
                            </div>    
                            @endif
                            @endforeach
                            @endif
                           
                               
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-xs-12">
                       <div class="toggle toggle-quaternary" data-plugin-toggle>
                                <section class="toggle active">
                                    <label>Boyutları</label>
                                    <div class="toggle-content">
                                        <p>{{ $urun->Icerik }}</p>
                                    </div>
                                </section>
                                <section class="toggle">
                                    <label>Ana Maddesi</label>
                                    <div class="toggle-content">
                                        <p>{{ $urun->Icerik2 }}</p>
                                    </div>
                                </section>
                                <section class="toggle">
                                    <label>Renkleri</label>
                                    <div class="toggle-content col-md-12 col-xs-12">
                                      <div class="lightbox mb-lg" data-plugin-options='{"delegate": "a", "type": "image", "gallery": {"enabled": true}}'>
                                     @if(!empty(json_decode($urun->Resim3)))
                                        @foreach(json_decode($urun->Resim3) as $index => $element)
                                             

                                        <div class="col-md-3 col-xs-3">
                                            <a class="img-thumbnail img-thumbnail-hover-icon mb-xs mr-xs" href="{{url('/images/uploads/urun/resim/renk/'.$element)}}" data-plugin-options='{"type":"image"}'>
                                                <img class="img-responsive" src="{{url('/images/uploads/urun/resim/renk/'.$element)}}" style="width: 50!important;height: 50px!important" alt="Project Image" >
                                            </a>
                                        </div>          

                                        
                                        @endforeach
                                        @endif
                                        
                                            


                                    </div>
                                </section>
                            </div>
                    @endif 

                    @if($urun->Gosterim==1)  

                        <div class="col-sm-12 col-md-12 col-xs-12">
                        
                            <div class="lightbox mb-lg" data-plugin-options='{"delegate": "a", "type": "image", "gallery": {"enabled": true}}'>

                            @if(!empty(json_decode($urun->Resim2)))
                            @foreach(json_decode($urun->Resim2) as $index => $element)
                            

                            <div class="col-md-3">
                                
                                <a class="img-thumbnail img-thumbnail-hover-icon mb-xs mr-xs" href="{{url('/images/uploads/urun/resim/'.$element)}}" data-plugin-options='{"type":"image"}'>
                                    <img class="img-responsive" src="{{url('/images/uploads/urun/resim/'.$element)}}" alt="Project Image" style="height: 250px;width: 250px">
                                </a>
                            </div>

                            
                            @endforeach
                            @endif
                            </div>
                           
                               
                        
                    </div>


                    @endif     
            </article>

        </div>
    </div>
</div><!-- #main -->
@endsection

