@extends('Site.Layout.Master')

@section('Title',$menu->MetaTitle)
@section('MetaTag')
    {!! @implode(', ', json_decode(@$ayarlar->MetaTag ,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES)) !!}
@endsection
@section('MetaDescription',$menu->MetaDescription)

@section('content')
<a href="{{ url($dil) }}" class="pull-right " style=" margin-top: -20px;"><i style="color:#91120f;padding-right: 20px;
" class="fa fa-close fa-2x" aria-hidden="true"></i></a>
<div role="main" class="main" >
<div class="container">
<div class="col-md-12 ">
<h1 style="margin-left: 30%;font-size: 30px;color: #880e0a;">{{ $menu->Adi }}</h1></br>
<div class="col-md-12" > 
@if(@$urunler)
@foreach($urunler as $key => $urun)

@if($key == 0)
    <div class="col-md-6">
        <div class="sari_daire" >
        <span>
            {{ $urun->daire_yazi }}
        </span>
        </div>
        <div class="sari_dikdortgen">
        <span>
            {{ $urun->diktortgen_yazi }}
        </span>
            </br></br>
            <span>
            {{ $urun->diktortgen_yazi2 }}
        </span>
        </div>  

        <div class="sari_daire_alt_yazi">
        <span>
            {{ $urun->daire_alti_yazi }}
        </span>
        </div>
    </div>

@elseif($key == 1)

    <div class="col-md-3">
        <div class="sari_daireler ilk">
        <span>
            {{ $urun->daire_yazi }}
        </span>
        </div>      

        <div class="sari_daireler_alt_yazi ilk">
        <span>
            {{ $urun->daire_alti_yazi }}
        </span>
        </div>
    </div>
    <div class="col-md-12" >
   

@elseif($key%3 == 1)
    <div class="col-md-3">
        <div class="sari_daireler">
        <span>
            {{ $urun->daire_yazi }}
        </span>
        </div>      

        <div class="sari_daireler_alt_yazi">
        <span>
            {{ $urun->daire_alti_yazi }}
        </span>
        </div>
    </div>
    </div>
@elseif($key%3 == 2)
    <div class="col-md-12" >
     <div class="col-md-3">
        <div class="sari_daireler">
        <span>
            {{ $urun->daire_yazi }}
        </span>
        </div>      

        <div class="sari_daireler_alt_yazi">
        <span>
            {{ $urun->daire_alti_yazi }}
        </span>
        </div>
    </div>
@else
     <div class="col-md-3">
        <div class="sari_daireler">
        <span>
            {{ $urun->daire_yazi }}
        </span>
        </div>      

        <div class="sari_daireler_alt_yazi">
        <span>
            {{ $urun->daire_alti_yazi }}
        </span>
        </div>
    </div>
@endif



@endforeach
@endif
</div>
</div>
</div>
</div>
</div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function() {
        $('.sari_daireler span').each(function(index, el) {
            var yazi_uzunluk = $(this).text().length;
            var span_height = $(this).height();
            var div_height = $(this).parent().height();
            $(this).parent().css('padding-top',(div_height - span_height) / 1.5);
            if(yazi_uzunluk > 58){
                $(this).css({
                    'font-size' : '22px',
                    'padding-top' : (div_height - span_height + 10),
                });
            }

        });
        $('.sari_daire span').each(function(index, el) {
            var yazi_uzunluk = $(this).text().length;
            var span_height = $(this).height();
            var div_height = $(this).parent().height();
            $(this).parent().css('padding-top',(div_height - span_height) / 1.5);
            if(yazi_uzunluk > 58){
                $(this).css({
                    'font-size' : '22px',
                    'padding-top' : (div_height - span_height + 10),
                });
            }

        });
        $('.sari_dikdortgen span').each(function(index, el) {
            var yazi_uzunluk = $(this).text().length;
            var span_height = $(this).height();
            var div_height = $(this).parent().height();
            $(this).parent().css('padding-top',(div_height - span_height) / 1.5);
            if(yazi_uzunluk > 58){
                $(this).css({
                    'font-size' : '22px',
                    'padding-top' : (div_height - span_height + 10),
                });
            }

        });

       
    });
</script>
@stop



