@extends('Site.Layout.Master')

@section('Title',$menu->MetaTitle)
@section('MetaTag')
    {!! @implode(', ', json_decode(@$ayarlar->MetaTag ,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES)) !!}
@endsection
@section('MetaDescription',$menu->MetaDescription)




@section('content')


   
<div role="main" class="main">
    

</div>

<section class="section section-no-border section-light " style="width: 100%" >

<div class="" style="margin-left: 10%;margin-right: 10%">
    <div class="row">
                 
                    <iframe
                            style="width:100%; height:230px; min-width:240px; border:0;"
                            frameborder="0"
                            src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCRN_S28IxohKWqLlB5pbJ6KX2sXD9TqPc&q={{ $subeler->first()->Adres }}" allowfullscreen>
                    </iframe>
                    </br></br>
                @foreach($subeler as $sube)
                
                    <div class="custom-location">
                        
                            <ul class="list list-icons list-primary">
                                <li class="appear-animation animated fadeInUp appear-animation-visible" @if(empty($sube->Telefon)) style="display: none" @endif style="height: 30px"  data-appear-animation="fadeInUp" data-appear-animation-delay="0">
                                    <img style="float: left;height : 17px;object-fit: contain" src="{{ url("icon1.png") }}">
                                        <p style="margin-left:29px" align="justify">
                                        <a style="color:#2f2e2d " href="tel:+{{  $sube->Telefon }}" class="text-color-dark">{{  $sube->Telefon }}
                                        </a>
                                        </p>
                                </li>
                                <li class="appear-animation animated fadeInUp appear-animation-visible" @if(empty($sube->Fax)) style="display: none" @endif style="height: 30px"  data-appear-animation="fadeInUp" data-appear-animation-delay="0">
                                    <img style="float: left;height : 17px;object-fit: contain" src="{{ url("icon2.png") }}">
                                        <p style="margin-left:29px" align="justify">
                                         <a style="color:#2f2e2d " href="tel:+{{  $sube->Fax }}" class="text-color-dark">{{  $sube->Fax }}
                                         </a>
                                        </p>
                                </li>
                                <li class="appear-animation animated fadeInUp appear-animation-visible" @if(empty($sube->Adres)) style="display: none" @endif  style="height: 30px" data-appear-animation="fadeInUp" data-appear-animation-delay="0">
                                    <img style="float: left;height : 17px;object-fit: contain" src="{{ url("icon3.png") }}">
                                        <p style="margin-left:29px" align="justify">
                                        {{  $sube->Adres }}
                                        </p>
                                </li>
                                
                            </ul>
                           
                    </div>
               
                @endforeach
                 </br>
                    <form id="contactform" class="form-box register-form contact-form" method="POST" action="{{url($dil.'/'.\App\Http\Fnk::Ceviri('iletisim-url'))}}" id="form">
                    <input type='hidden' name='_token' value='{{csrf_token()}}' ></input>
                    <input type="hidden" class="form-control" id="dilIlet" required name="dilIlet" value="{{ $dil }}"></input>

                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="AdSoyad" id="AdSoyad" placeholder="{{\App\Http\Fnk::Ceviri('ad')}}" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="email" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="Eposta" id="Eposta" placeholder="{{\App\Http\Fnk::Ceviri('eposta')}}" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="text" value="" data-msg-required="Please enter your phone." maxlength="100" class="form-control" name="Konu" id="Konu" placeholder="{{\App\Http\Fnk::Ceviri('konu')}}" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input rows="3" type="text" value="" data-msg-required="Please enter your phone." maxlength="100" class="form-control" name="Mesaj" id="Mesaj" placeholder="{{\App\Http\Fnk::Ceviri('mesaj')}}" required>
                                </div>
                            </div>
                        </div>
                        <center>
                        {{  Fnk::captchaSet() }}
                        {!! app('captcha')->display(); !!}
                        <div class="row">
                            <div class="col-md-12">
                                <input type="submit" value="{{\App\Http\Fnk::Ceviri('gonder')}}" class="btn btn-danger custom-btn-style-2 font-weight-bold" data-loading-text="Loading...">
                            </div>
                        </div>
                        </center>
                    </form>
    </div>
</div>   
</section>                    
<!-- Google Maps - Go to the bottom of the page to change settings and map location. -->

@stop
@section('js')

@endsection

