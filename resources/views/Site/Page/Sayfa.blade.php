@extends('Site.Layout.Master')

@section('Title',$menu->MetaTitle)
@section('MetaTag')
	{!! @implode(', ', json_decode(@$ayarlar->MetaTag ,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES)) !!}
@endsection
@section('MetaDescription',$menu->MetaDescription)


@section('content')

<div role="main" class="main" >

	<center><h1 style="margin-left:8%;margin-right: 8%;font-size: 28px;color: #880e0a;">{{ $menu->Adi }}</h1></center></br>
	<div class="row col-md-12"> 
		<div class="col-md-12"> 
		{!!$menu->Icerik!!}
		
		</div>
	</div>	


</div>



@stop