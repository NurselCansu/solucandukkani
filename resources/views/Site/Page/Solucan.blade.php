@extends('Site.Layout.Master')

@section('Title',Fnk::Ceviri('solucan'))
@section('MetaTag')
	@foreach($solucanlar as $solucan)
    {!! @implode(', ', json_decode(@$solucan->MetaTag ,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES)) !!}, 
    @endforeach
@endsection
@section('MetaDescription',Fnk::Ceviri('solucan'))


@section('content')
<div role="main" class="main" >
	<center><h1 style="font-size: 30px;color: #880e0a;">{{ Fnk::Ceviri('solucan') }}</h1></center></br>
<div class="">    
<div class="row col-md-12 "> 
@foreach($solucanlar as $solucan)
<a href="{{ url($dil.'/'.Fnk::Ceviri('menu-url').'/'.$solucan->link) }}" style="color: #555;">
	<div class="col-md-3 kirmizi_halka">
		<center >{{ $solucan->Adi }}</center>
	</div>
</a>
@endforeach


</div>
</div>
</div>
@stop
@section('js')
<script>
    $(document).ready(function() {
        $('.kirmizi_halka center').each(function(index, el) {
            var yazi_uzunluk = $(this).text().length;
            var span_height = $(this).height();
            var div_height = $(this).parent().height();
            $(this).parent().css('padding-top',(div_height - span_height) / 2);
            if(yazi_uzunluk > 58){
                $(this).css({
                    'font-size' : '15px',
                    'padding-top' : (div_height - span_height - 10),
                });
            }

        });
       
       
    });
</script>
@stop