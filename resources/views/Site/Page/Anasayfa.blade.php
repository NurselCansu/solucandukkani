@extends('Site.Layout.Master')

@section('Title',@$ayarlar->MetaTitle)
@section('MetaTag')
	{!! @implode(', ', json_decode(@$ayarlar->MetaTag ,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES)) !!}
@endsection
@section('MetaDescription',@$ayarlar->MetaDescription)



@section('content')			
	
@stop
@section('css')
<style type="text/css">	

	.sweet-alert {margin: auto; transform: translateX(-50%);background-size: inherit; }
	.sweet-alert { 
		width: {{ trim(@$popup->Genislik)}}px;
	 	background-image: url("{{ url('images/uploads/Popup/resim/kapak/'.json_decode(@$popup->ResimKapak)[0] ) }}") 
	 	
	 }

</style>
@stop
@section('js')
<script type="text/javascript">
@if(@$popup)
	jQuery(document).ready(function($) {
		swal({
  			  title: '<span style="color:{{ @$popup->BaslikRenk }}!important" >{{ @$popup->Baslik}}</span>',
			  text: '<span style="color:{{ @$popup->IcerikRenk }}!important" >{{ @$popup->Icerik }}</span>',
			  timer: {{ @$popup->Sure }}*1000,
			  animation: "slide-from-top",
			  allowEscapeKey:true,
              allowOutsideClick:true,
			  showConfirmButton: false,
			  imageUrl: "{{ url('images/uploads/Popup/resim/'.json_decode(@$popup->Resim)[0] ) }}",
			  imageSize:"{{ @$popup->FotoGenislik}}x{{ @$popup->FotoUzunluk }}",
			  html: true,
		});
	});
@endif
</script>
@stop