@extends('Site.Layout.Master')

@section('Title',$menu->MetaTitle)
@section('MetaTag')
	{!! @implode(', ', json_decode(@$ayarlar->MetaTag ,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES)) !!}
@endsection
@section('MetaDescription',$menu->MetaDescription)


@section('content')


<div role="main" class="main">
<section class="page-header page-header-light page-header-reverse page-header-more-padding">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb breadcrumb-valign-mid">
					<li><a href="{{ url('/'.$dil) }}">{{\App\Http\Fnk::Ceviri('anasayfa')}}</a></li>
					<li class="active">{{$haber->Adi}}</li>
				</ul>
				<h1>{{$haber->Adi}}</h1>
			</div>
		</div>
	</div>
</section>
	<div class="row center mt-xl">
            	<h3 class="heading-primary">{{  $haber->Adi }}</h3>
            	<div class="divider divider-primary">
					<i class="fa fa-chevron-down"></i>
				</div>
	</div>			


	<div class="container">
		<div class="row "> 

		<div class="col-md-5">
			 <img alt="" class="img-responsive img-rounded" src="{{asset('images/uploads/Haberler/'.$haber->Resim)}}">

		</div>
		<div class="col-md-7">
			{!! $haber->Icerik !!}
		</div>	
		</div>	
	</div>

</div>



@stop