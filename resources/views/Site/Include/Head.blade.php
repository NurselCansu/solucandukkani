
	<meta charset="utf-8">
	<title>@yield('Title')</title>
	<meta name="keywords" content="@yield('MetaTag')"/>
	<meta name="description" content="@yield('MetaDescription')" /> 
	<meta name="robots" content="index, follow"> 
    <meta name="author" content="Portakal Yazılım - www.portakalyazilim.com.tr">
	<meta class="viewport" name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">


	<!-- Favicon -->
		<link rel="shortcut icon" href="{{ url('images/uploads/'.$ayarlar->Favicon) }}" type="image/x-icon" />
		<link rel="apple-touch-icon" href="{{ url('images/uploads/'.$ayarlar->Favicon) }}">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('style.css') }}">
		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="{{ asset('css/custom.css') }}">
		<link rel="stylesheet" href="{{ asset('dist/sweetalert.css') }}">
		



		

		