
@section('header')

<div class="bodybg">
<div class="row siteHeader center-block" id="siteHeader">
    <div class="col-md-8 logo_div">
        <img alt=". . ." class="logo"  src="{{ url('/images/uploads/'.$ayarlar->Logo) }}" style="margin-top: 3%;">
    </div>  
    <div class="col-md-4">
        <img src="{{ url('tarim.png') }}" class="tarim_logo" alt="" style="height: 50px ; object-fit: contain;margin-left: 25%"/></br>
        <img class="telNo" src="{{ url('image/telNo.png') }}"/>
    </div>
    <div class="navbar-wrapper">
    <div class="">
    <nav class="navbar  menu" id="menu">
        <div class="">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed pull-left" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav menu_ayar">
                    {!! Fnk::Menuler() !!}
                    @foreach($tumdiller as $element)
                        @if($tumdiller->count() > 1 )
                        <li class="dropdown-full-color dropdown-primary " style="margin-left : -10px!important">
                            <a href="{{url($element->KisaAd.'/cevir/'.@$segment2.'/'.@$segment3.'/'.@$segment4.'/'.@$segment5)}}">
                                <img class="x" style="height: 25px;border-radius: 100%" src="{{url('/flags/flags/'.$element->Resim)}}">                   
                                </img>
                            </a>
                        </li>
                         @endif
                    @endforeach
                </ul>
            </div><!--/.nav-collapse -->
            <hr class="pull-left hr_menu">
        </div>
    </nav>
</div>
</div>
    <div class="icerik pull-left" style="border: 4px solid #9a1812;display: none" >
    <a href="{{ url($dil) }}" class="pull-right icerik_close" style=" margin-top: -20px;" ><i style="color:#91120f" class="fa fa-close fa-2x" aria-hidden="true"></i></a>
        <div class="row">
            @yield('content') 
            <a><center class="geri_don" style="display:none"><i style="color:#91120f" class="fa fa-undo fa-3x" aria-hidden="true"></i></center></a>
           
        </div>
    </div>
</div>

<div class="solucanContainer">
    <img src="{{ url('image/solucan.gif') }}">
</div>
<div class="traktor">
    <img src="{{ url('image/yavaş--traktör.gif') }}">
</div>
<div class="degirmen">
    <img src="{{ url('image/değirmen-yavaş.gif') }}">
</div>

</div>
@yield("footer")

@endsection
