<!-- Google Analytics Ayarlaması -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', '{{ @$google_analytics->Analytics }}', 'auto');
  ga('send', 'pageview');

</script>


<!-- Vendor -->
        
        <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('vendor/jquery.validation/jquery.validation.min.js') }}"></script>
        <script src="{{ asset('vendor/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
      
        
       
        <script src="{{ asset('dist/sweetalert.min.js') }}"></script>
        <script src="{{ asset('dist/jquery-bigtext.js') }}"></script>
        

        <!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        
            ga('create', 'UA-12345678-1', 'auto');
            ga('send', 'pageview');
        </script>
         -->
<script>

    $( document ).ready(function () {

        var logo_uzunluk = $('.logo_div img').width();
        var navbar_uzunluk = $('.menu_ayar').width();
        if(navbar_uzunluk > logo_uzunluk){
            $('.logo_div').css('padding-left', ((navbar_uzunluk - logo_uzunluk) / 2 + 5) + 'px');
        }
        $('.hr_menu').css('width',navbar_uzunluk+"px");
      

        @if(Request::segment(2) == Fnk::Ceviri("solucan-url"))
            $('.icerik').css("width","63%");
        @elseif(Request::segment(2) == Fnk::Ceviri("menu-url"))
            $('.geri_don').show();
        @elseif(Request::segment(2) == "")
            $('.footer').css("z-index","16");

        @elseif(Request::segment(2) == Fnk::Ceviri("urunler-url"))
            $('.icerik_close').hide();
        @elseif(Request::segment(2) == Fnk::Ceviri("iletisim-url"))
            $('.icerik').css({
                'width' : '35%',
                'border' : '4px solid white'
            });
            $('.icerik_close').hide();
        @else
            $('.footer').css('z-index','1');
        @endif
        $('.geri_don').click(function(event) {
            window.history.go(-1);
        });

         $('.kirmizi_halka_menu center').each(function(index, el) {
            var yazi_uzunluk = $(this).text().length;
            var span_height = $(this).height();
            var div_height = $(this).parent().height();
            $(this).parent().css('padding-top',(div_height - span_height) / 2.8);
            if(yazi_uzunluk > 58){
                $(this).css({
                    'font-size' : '14px',
                    'padding-top' : (div_height - span_height - 10),
                });
            }

        });
        var viewportWidth = $(window).width();
        var viewportHeight = $(window).height();
        if(viewportWidth < 550){
            $('.tarim_logo').css('margin-left','23%');
            $('#menu').addClass('navbar-fixed-top');
            $('#menu').addClass('menuSmall');
            $('.icerik').css("width","110%");
            $('.bodybg').css('min-height',viewportHeight+"px");
            $('.hr_menu').hide();
            var logo_uzunluk = $('.logo_div img').width();
            var telNo_uzunluk = $('.telNo').width();
            $('.telNo').css('padding-left',"-5px");
        }
        else if (viewportWidth <= 1000 || viewportHeight >= 1050) {
            
            $('#menu').addClass('navbar-fixed-top');
            $('#menu').addClass('menuSmall');
            $('.icerik').css("width","100%");
            $('.icerik').css('margin-top', '-2%');
            $('.bodybg').css('min-height',viewportHeight+"px");
            $('.hr_menu').hide();
            $('.tarim').hide();
            $('.portakal').css({
                'bottom':0,
                'margin-left':'-7%',
            });
            $('.footer').css("z-index","16");
            $('.logo_div').css('padding-left', 0);
            var logo_uzunluk = $('.logo_div img').width();
            var telNo_uzunluk = $('.telNo').width();
            $('.telNo').css('padding-left',((logo_uzunluk - telNo_uzunluk) / 2.32) + "px");
            if(viewportWidth > 930){
                $('.telNo').css('padding-left',((logo_uzunluk - telNo_uzunluk) / 1.3) + "px");
            }
            
            $('.tarim_logo').css('margin-left','30%');

        } 
        else
        {
            $('#menu').removeClass('navbar-fixed-top'); 
            $('#menu').removeClass('menuSmall');
        }

        if (viewportWidth <= 991) {          
            $('#siteHeader').removeClass('siteHeader');
            $('#siteHeader').addClass('siteHeaderSmall');
        }
        else{ 
            $('#siteHeader').removeClass('siteHeaderSmall');
            $('#siteHeader').addClass('siteHeader');
        }
        
        //Landspace Portrait kontrol
        if($(window).innerWidth() <= 991){    
            if($(window).innerWidth() > $(window).innerHeight()){
                //landscape
                $('.tarim_logo').hide();
                $('.telNo').hide();
                $('.icerik').css({
                    "width":"70%",
                    "margin-left":"2%"
                });

                if($(window).innerWidth() > 950){
                    $('.icerik').css('width', '80%');
                    $('.tarim_logo').show();
                    $('.telNo').show();
                }
            }
        }
       
       
    });
    $( window ).resize(function () {
        var viewportWidth = $(window).width();
        if (viewportWidth <= 900) {
          
            $('#menu').addClass('navbar-fixed-top');
            $('#menu').addClass('menuSmall');

        }
        else
        {
            $('#menu').removeClass('navbar-fixed-top');
            
            $('#menu').removeClass('menuSmall');
        }
        if (viewportWidth <= 991)
        {
            $('#siteHeader').removeClass('siteHeader');
            $('#siteHeader').addClass('siteHeaderSmall');
        }
        else
        {
            $('#siteHeader').removeClass('siteHeaderSmall');
            $('#siteHeader').addClass('siteHeader');
        }
    });
</script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
    @if($segment2)
           $('.icerik').css("background","rgba(250,250,250,0.7)");
           $('.icerik').show( "slow" );
    @else
           $('.icerik').hide("slow");
    @endif
////////////////////Mesaj İletimin başlangıcı///////////////////////
        $("#contactform").submit(function(event) {
            swal({
                title: "{{\App\Http\Fnk::Ceviri('durum')}}" ,
                text: "{{\App\Http\Fnk::Ceviri('bekle')}}",
                timer: 2000,
                showConfirmButton: false,
                html: true
                });
        });
///////////////BU Kısım Mesajın İletim durumunu döndürür belirtir//////////////////////                
        @if(!empty(session('status')))
                @if(session('status')=='TRUE')
                    swal("{{\App\Http\Fnk::Ceviri('gonderildi')}}", "", "success")
                @else
                    swal("{{\App\Http\Fnk::Ceviri('gonderilmedi')}}", "", "error")
                @endif
        @endif
//////////////////////////////////////////////////////////////////////////////////////
        
    });
</script>
<script>
    function modalAc(){
         $('.modal-backdrop').css("z-index","0!important");
    }
</script>






