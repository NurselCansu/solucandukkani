<!DOCTYPE html>
<html lang="tr" class="photography-demo-2">
    <head>
        {{ Fnk::googleDogrulamaKodu() }}
        @include('Site.Include.Head')
        @yield('css')
    </head>
   <body>
         
            @include('Site.Include.Header')

            @yield('header')

            

    @include('Site.Include.Footer')

    @yield('footer')

    @include('Site.Include.Scripts')
    
    @yield('js')
    {!! @$tawk->Script !!}
</body>
</html>
