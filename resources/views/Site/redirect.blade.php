<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"
></script>
<script type="text/javascript">
	var userLang = (navigator.language || navigator.userLanguage).substring(0,2); 
	var control = false;
    @if( $diller->count() > 1)
		@foreach($diller as $dil)
		
			if( userLang == "{{ $dil->KisaAd }}" ){
				
				window.location = '{{ url('/') }}/'+userLang;
				control = true;
			}
			else{
				
				window.location = '{{ url('/'.$dil->first()->KisaAd) }}';
			}
		@endforeach	
		if(control){
			window.location = '{{ url('/') }}/'+userLang;		
		}

	@else	
		
		window.location = '{{ url('/'.$diller->first()->KisaAd) }}';
	@endif
</script>