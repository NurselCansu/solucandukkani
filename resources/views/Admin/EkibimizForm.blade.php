@extends('Admin.Layout.Form')

@section('baslik', 'Ekibimiz')

@section('form')

<div class="form-group" id="Tipalani">
    <label for="Tip" class="col-sm-2 control-label">Tip</label>
    <div class="col-sm-10">
        <select class="form-control" id="Tip" required name="Tip" onchange="TipiDegistir()">
            <option value="">Lütfen Tip Seçiniz</option>
            <option @if( @$veri->Tip == 'I' ) selected @endif value="I">Kurucu İçerik</option>
            <option @if( @$veri->Tip == 'K' ) selected @endif value="K">Kurucu</option>
            <option @if( @$veri->Tip == 'M' ) selected @endif value="M">Müdür</option>
            <option @if( @$veri->Tip == 'P' ) selected @endif value="P">Personel</option>
        </select>
    </div>
</div>

<div class="form-group" id="Adi_soyadialani" @if(@$veri->Tip == 'I' ) style="display:none" @endif>
    <label for="Adi_soyadi" class="col-sm-2 control-label">Adı Soyadı</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="{{@$veri->Adi_soyadi}}" name="Adi_soyadi" id="Adi_soyadi" placeholder="Adı Soyad">
    </div>
</div>
<div class="form-group" id="Telefonalani" @if(@$veri->Tip == 'I' ) style="display:none" @endif>
    <label for="Telefon" class="col-sm-2 control-label">Telefon</label>
    <div class="col-sm-10">
        <input type="text" class="form-control"  value="{{@$veri->Telefon}}" name="Telefon" id="Telefon" placeholder="Telefon">
    </div>
</div>
<div class="form-group" id="Emailalani" @if(@$veri->Tip == 'I' ) style="display:none" @endif>
    <label for="Email" class="col-sm-2 control-label">E-mail</label>
    <div class="col-sm-10">
        <input type="text" class="form-control"  value="{{@$veri->Email}}" name="Email" id="Email" placeholder="E-mail">
    </div>
</div>
<div class="form-group" id="Icerikalani" @if(@$veri->Tip != 'I' ) style="display:none" @endif>
    <label for="Icerik" class="col-sm-2 control-label">İçerik</label>
    <div class="col-sm-10">
        <textarea required id="Icerik" name="Icerik" class="ck">{{@$veri->Icerik}}</textarea>

    </div>
</div>

<div class="form-group" id="Pozisyonalani" @if(@$veri->Tip == 'I' ) style="display:none" @endif>
    <label for="Pozisyon" class="col-sm-2 control-label">Pozisyon</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="{{@$veri->Pozisyon}}" name="Pozisyon" id="Pozisyon" placeholder="Pozisyon">
    </div>
</div>

<div class="form-group" id="Fotografalani" @if(@$veri->Tip == 'I' ) style="display:none" @endif>
    <label for="Fotograf" class="col-sm-2 control-label">Fotoğraf</label>
    <div class="col-sm-10">
      <input type="file" class="form-control" name="Fotograf[]" id="Fotograf"  placeholder="Kapak Resim">
    </div>
    @if( @$veri->Fotograf )
        <div class="col-md-10 col-md-offset-2 container container-fluid" >
            <div class="checkbox">
                @foreach( json_decode(@$veri->Fotograf)  as $key=>$value)
                <a>
                    <img  src="{{ url('images/uploads/ekibimiz/'.$value ) }}" width="150" height="150" alt="">
                    <input type="checkbox"  name="secili_belge_resim[]"   @if( @$value ) checked value="{{$value}}" @else value="0" @endif>
                </a>
                @endforeach
            </div>
        </div>
    @endif
</div>


@stop

@section('js')
<script type="text/javascript">
    function TipiDegistir() {
        var Tip = $('#Tip').val();
        console.log(Tip);
        if (Tip == "I") {
            $('#Adi_soyadialani').hide('slow');
            $('#Pozisyonalani').hide('slow');
            $('#Fotografalani').hide('slow');
            $('#Telefonalani').hide('slow');
            $('#Emailalani').hide('slow');
            $('#Icerikalani').show('slow');
            $('#MetaTagalani ').show('slow');
            $('#MetaTitlealani').show('slow');
            $('#MetaDescriptionalani').show('slow');

        } else if (Tip == "K" || Tip == "M" || Tip == "P") {
            $('#Adi_soyadialani').show('slow');
            $('#Telefonalani').show('slow');
            $('#Emailalani').show('slow');
            $('#Pozisyonalani').show('slow');
            $('#Fotografalani').show('slow');
            $('#Icerikalani').hide('slow');
            $('#MetaTagalani ').hide('slow');
            $('#MetaTitlealani').hide('slow');
            $('#MetaDescriptionalani').hide('slow');

        }
    }
    $( document ).ready(function() {
        var Tip = '{{@$veri->Tip}}';
     
            if (Tip == "I") {
                $('#Adi_soyadialani').hide('slow');
                $('#Pozisyonalani').hide('slow');
                $('#Fotografalani').hide('slow');
                $('#Icerikalani').show('slow');
                $('#MetaTagalani ').show('slow');
                $('#MetaTitlealani').show('slow');
                $('#MetaDescriptionalani').show('slow');

            } else if (Tip == "K" || Tip == "M" || Tip == "P") {
                $('#Adi_soyadialani').show('slow');
                $('#Pozisyonalani').show('slow');
                $('#Fotografalani').show('slow');
                $('#Icerikalani').hide('slow');
                $('#MetaTagalani ').hide('slow');
                $('#MetaTitlealani').hide('slow');
                $('#MetaDescriptionalani').hide('slow');
            }
        
 
    });



</script>
@stop