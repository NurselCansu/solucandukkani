@extends('Admin.Layout.Form')
@section('baslik'){{$Title}}@endsection
@section('form')
<input type="hidden" name="trid" value="{{@$trveri->id}}">

<div class="form-group">
    <label for="Fotograf" class="col-sm-2 control-label">Video Galeri Sayfa Fotoğrafı</label>
    <div class="col-sm-10">
      <input type="file" class="form-control" name="Fotograf[]" id="Fotograf"  placeholder="Fotograf">
    </div>
    @if( @$veri->Fotograf )
        <div class="col-md-10 col-md-offset-2" >
            <a href="{{ url('images/uploads/FotoGaleri/sayfa/'.@$veri->Fotograf ) }}">
                <div class="checkbox">
                    <img class="replace-2x" src="{{ url('images/uploads/FotoGaleri/sayfa/'.@$veri->Fotograf)  }}" width="150" height="150" alt="">
                </div>
            </a>
        </div>
    @endif
</div>

@stop

@section('js')

@stop