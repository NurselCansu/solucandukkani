@extends('Admin.Layout.Master')

@section('title', 'Video Detay')

@section('content')
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">@yield('title')</div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
				
				<tr>
					<th style="width:200px;">Dil</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>@if( $veri->DilId == 1) Türkçe 
						@elseif( $veri->DilId == 2) İngilizce 
						@elseif( $veri->DilId == 3) İspanyolca 
						@elseif( $veri->DilId == 4) Rusça
						@elseif( $veri->DilId == 5) Fransızca
						@endif
					</td>
				</tr>
				<tr>
					<th style="width:200px;">Fotoğraf</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><img width='150' height='150' src="{{url('images/uploads/FotoGaleri/fsayfa/'.$veri->Fotograf)}}" style="max-width: 100%;"></td>
				</tr>
				
				<tr>
					<th style="width:200px;">Durum</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{($veri->is_active==1)?'Aktif':'Pasif'}}</td>
				</tr>


				<tr>
					<th style="width:200px;">Oluşturulma Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{Fnk::TarihDuzenle($veri->created_at)}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Güncelleme Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{Fnk::TarihDuzenle($veri->updated_at)}}</td>
				</tr>
				<tr>
					<td colspan="3"><a href="{{URL::previous()}}" class="btn btn-primary pull-right">Geri Dön</a></td>
				</tr>
			</table>
		</div>
	</div>
</div>
@stop