@extends('Admin.Layout.Master')

@section('title', 'Bayilik Detay')

@section('content')
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">@yield('title')</div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
				<tr>
					<th style="width:200px;">Dil</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->DilId}}</td>
				</tr>

				<tr>
					<th style="width:200px;">Bayilik Faaliyet Alanı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{($veri->bayilik_faliyet_alani==1)?'Yurtiçi':'Yurtdışı'}}</td>
				</tr>

				<tr>
					<th style="width:200px;">Bayilik Adı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{ $veri->BayilikAdi }}</td>
				</tr>

				<tr>
					<th style="width:200px;">Bayilik Adres</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{ $veri->Adres }}</td>
				</tr>

				<tr>
					<th style="width:200px;">Bayilik Telefon</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{ $veri->Telefon }}</td>
				</tr>

				<tr>
					<th style="width:200px;">Bayilik Fax</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{ $veri->Fax }}</td>
				</tr>

				<tr>
					<th style="width:200px;">Bayilik Eposta</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{ $veri->Eposta }}</td>
				</tr>

				<tr>
					<th style="width:200px;">Bayilik Web Adres</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{ $veri->Web }}</td>
				</tr>

				<tr>
					<th style="width:200px;">Bayilik Koordinatları</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{ $veri->Kordinat }}</td>
				</tr>
				
				<tr>
					<th style="width:200px;">Durum</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{($veri->is_active==1)?'Aktif':'Pasif'}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Oluşturulma Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{Fnk::TarihDuzenle($veri->created_at)}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Güncelleme Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{Fnk::TarihDuzenle($veri->updated_at)}}</td>
				</tr>
				<tr>
					<td colspan="3"><a href="{{URL::previous()}}" class="btn btn-primary pull-right">Geri Dön</a></td>
				</tr>
			</table>
		</div>
	</div>
</div>
@stop