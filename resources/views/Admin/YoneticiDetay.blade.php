@extends('Admin.Layout.Master')

@section('title', 'Yönetici Detay')

@section('content')
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">@yield('title')</div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
				<tr>
					<th style="width:200px;">Adı Soyadı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->name}}</td>
				</tr>
				<tr>
					<th style="width:200px;">E-Posta Adresi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->email}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Oluşturulma Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->created_at->format('d.m.Y H:i')}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Güncelleme Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->updated_at->format('d.m.Y H:i')}}</td>
				</tr>
				<tr>
					<td colspan="3"><a href="{{URL::previous()}}" class="btn btn-primary pull-right">Geri Dön</a></td>
				</tr>
			</table>
		</div>
	</div>
</div>
@stop