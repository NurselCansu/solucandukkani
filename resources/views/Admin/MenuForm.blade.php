@extends('Admin.Layout.Form')
@section('baslik'){{$Title}}@endsection
@section('form')


<input type="hidden" name="id" value="{{@$veri->id}}">
<div class="form-group">
    <label for="UstKatId" class="col-sm-2 control-label">Üst Menü</label>
    <div class="col-sm-10">
    	<select class="form-control" id="UstKatId" required name="UstKatId">
    		<option value="0">Üst Menüsü Yok</option>
    				{{ 
                        Fnk::ChildrenOption($menuler,@$veri->UstKatId,@$veri->id,'Adi',0,false)
                    }}
    	</select>
   </div>
</div>
<div class="form-group">

    <label class="col-md-2 control-label">Menu Durum</label>
    <div class="col-md-10">
        <div class="checkbox">
            <label>
                <input type="checkbox" id="Tip" name="Tip"  @if(@$veri->Tip == 1) checked value="1" @else value="0" @endif >Menulerde Yer almasın
            </label>
        </div>
    </div>
</div>
<div class="form-group">
    <label for="Adi" class="col-sm-2 control-label">Menü Adı</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="{{@$veri->Adi}}" name="Adi" id="Adi" placeholder="Menü Adı">
    </div>
</div>
<div class="form-group">
    <label for="MenuTipi" class="col-sm-2 control-label">Menü Tipi</label>
    <div class="col-sm-10">
        <select name="MenuTipi" class="form-control" required="" onchange="MenuTipiDegistir()" id="MenuTipi">
            <option value="">Lütfen Seçiniz</option>
            <option  value="icerik" @if(isset($veri) && $veri->MenuTipi=='icerik') selected @endif>İçerik</option>
            <option  value="link"  @if(isset($veri) && $veri->MenuTipi=='link') selected @endif  >Link</option>
            <option   value="modul" @if(isset($veri) && $veri->MenuTipi=='modul') selected @endif>Modül</option>
        </select>
    </div>
</div>
<div class="form-group">
    <label for="LinkAcilisTipi" class="col-sm-2 control-label">Link Açılış Tipi</label>
    <div class="col-sm-10">
        <div class="radio">
            <label>
                <input type="radio" name="LinkAcilisTipi" value="" @if(isset($veri) && $veri->LinkAcilisTipi=='') checked @endif @if(!isset($veri)) checked @endif >Aynı Pencere</label>
    	</div>
	    <div class="radio">
	        <label><input type="radio" name="LinkAcilisTipi" @if(isset($veri) && $veri->LinkAcilisTipi=='target="_blank"') checked @endif value='target="_blank"'>Yeni Pencere</label>
		</div>
	</div>
</div>
<div class="form-group" id="Linkalani" @if(!isset($veri) || $veri->MenuTipi!='link') style="display:none" @endif >
	<label for="Link" class="col-sm-2 control-label">Menü Linki</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="Link" name="Link" value="{{@$veri->Link}}">
    </div>
</div>
<div class="form-group" id="Icerikalani" @if(!isset($veri) || $veri->MenuTipi!='icerik') style="display:none" @endif >
     <label for="Icerik" class="col-sm-2 control-label">Menü İçeriği</label>
    <div class="col-sm-10">
        <textarea required id="Icerik" name="Icerik" class="ck">{{@$veri->Icerik}}</textarea>
    </div>
</div>
<div class="form-group" id="Modulalani" @if(!isset($veri) || $veri->MenuTipi!='modul') style="display:none" @endif >
    <label for="Modul" class="col-sm-2 control-label">Modül</label>
    <div class="col-sm-10">
        <select name="Modul" class="form-control" id="Modul" required>
            <option value="">Lütfen Modül Seçiniz</option>
            <option value="urunler-url" @if(@$veri->Link and $veri->MenuTipi=='modul') @if( substr(@$veri->Link, 4)  == \App\Http\Fnk::Ceviriadmin('urunler-url',substr(@$veri->Link,1,2)) ) selected @endif @endif >Ürünler Modülü</option>
            <option value="iletisim-url" @if(@$veri->Link and $veri->MenuTipi=='modul') @if(substr(@$veri->Link, 4) == \App\Http\Fnk::Ceviriadmin('iletisim-url',substr(@$veri->Link,1,2))) selected @endif @endif>İletişim Modülü</option>
             <option value="solucan-url" @if(@$veri->Link and $veri->MenuTipi=='modul') @if(substr(@$veri->Link, 4) == \App\Http\Fnk::Ceviriadmin('solucan-url',substr(@$veri->Link,1,2))) selected @endif @endif>Solucan Dükkanı Modülü</option>
           
            
           
        </select>
    </div>
</div>

@stop

@section('js')
<script type="text/javascript">
    function MenuTipiDegistir() {
        var MenuTip = $('#MenuTipi').val();
        if (MenuTip == "link") {
            $('#Linkalani').show('slow');
            $('#Icerikalani').hide('slow');
            $('#Modulalani').hide('slow');
        } else if (MenuTip == "icerik") {
            $('#Icerikalani').show('slow');
            $('#Linkalani').hide('slow');
            $('#Modulalani').hide('slow');
        } else if (MenuTip == "modul") {
            $('#Modulalani').show('slow');
            $('#Icerikalani').hide('slow');
            $('#Linkalani').hide('slow');
        }
    }

jQuery(document).ready(function($) {
         $('#Tip').change(function() {
            if($(this).is(":checked")) {
               $(this).val("1");
              
            }
            else{
               $(this).val("0");
              
            }
        });
});
</script>
@stop