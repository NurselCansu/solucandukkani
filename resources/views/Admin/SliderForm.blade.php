@extends('Admin.Layout.Form')

@section('baslik', 'Slider')

@section('form')
<div class="form-group">
  <label for="name" class="col-sm-2 control-label">Resim Adı</label>
  <div class="col-sm-10">
    <input type="text" class="form-control" required value="{{@$veri->Adi}}" name="Adi" id="name" placeholder="Resim Adı">
  </div>
</div>
<div class="form-group">
    <label for="Resim" class="col-sm-2 control-label">Slider Resmi</label>
    <div class="col-sm-10">
    @if($veri->Resim)
        <img src="{{ url('images/uploads/Slider/'.$veri->Resim) }}" style="height: 100px;object-fit: contain;display: block;">
    @endif

        <input type="file" class="form-control" name="Resim" id="Resim" placeholder="Resim">
    </div>
</div>



<div class="form-group">
    <label class="col-sm-2 control-label">İçerik 1</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="Icerik" id="Icerik" value="{{@$veri->Icerik}}"></input>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">İçerik 2</label>
    <div class="col-sm-10">
        <input type="text" name="Icerik2" class="form-control" value="{{ @$veri->Icerik2 }}"></input>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">İçerik 3</label>
    <div class="col-sm-10">
        <input type="text" name="Icerik3" class="form-control" value="{{ @$veri->Icerik3 }}"></input>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Link Adı</label>
    <div class="col-sm-10">
        <input type="text" name="LinkAdi" class="form-control" value="{{ @$veri->LinkAdi }}"></input>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Link</label>
    <div class="col-sm-10">
        <input type="text" name="Link" class="form-control" value="{{ @$veri->Link }}"></input>
    </div>
</div>

<div class="form-group">
    <label for="trbaslangic" class="col-sm-2 control-label">Başlangıç Tarih</label>
    <div class="col-sm-4">
        <div class="input-group datetimepicker2">
            <input type="text" name="baslangic" class="form-control" required="" value="@if(!empty($veri->baslangic)){{ date('d.m.Y H:i', strtotime($veri->baslangic)) }}@else{{ date('d.m.Y H:i') }}@endif" aria-required="true" aria-invalid="false">
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
    </div>
</div>
<div class="form-group">
    <label for="trbitis" class="col-sm-2 control-label">Bitiş Tarih</label>
    <div class="col-sm-4">
        <div class="input-group datetimepicker3">
            <input type="text" name="bitis" class="form-control" required="" value="@if(!empty($veri->bitis)){{ date('d.m.Y H:i', strtotime($veri->bitis)) }}@else{{ date('d.m.Y H:i') }}@endif" aria-required="true" aria-invalid="false">
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
$(document).ready(function(){
    $('.datetimepicker2').datetimepicker();
    $('.datetimepicker3').datetimepicker();
});    
    function SliderTipiDegistir() {
        var SliderTip = $('#SliderTipi').val();
        if(SliderTip == "link") {
            $('#Linkalani').show('slow');
            $('#Icerikalani').hide('slow');
        }else if(SliderTip == "icerik"){
            $('#Icerikalani').show('slow');
            $('#Linkalani').hide('slow');
        }
    }

   

</script>
@endsection