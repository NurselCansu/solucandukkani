@extends('Admin.Layout.Form')

@section('baslik', 'Ihracat')

@section('form')

<div class="form-group">
    <label for="AfrikaKulAdUnvanTelEmail" class="col-sm-2 control-label">Afrika<br>Kullanıcı Adi Soyadi, Unvan, Telefon, Email </label>
    <div class="col-sm-10">
        <br>
        <input type="text" class="form-control" required value="{{@$veri->AfrikaKulAdUnvanTelEmail}}" name="AfrikaKulAdUnvanTelEmail" id="AfrikaKulAdUnvanTelEmail" placeholder="Adi Soyadi, Unvan, Telefon, Email">
    </div>
</div>

<div class="form-group">
    <label for="AsiaKulAdUnvanTelEmail" class="col-sm-2 control-label">Asya<br>Kullanıcı Adi Soyadi, Unvan, Telefon, Email </label>
    <div class="col-sm-10">
        <br>
        <input type="text" class="form-control" required value="{{@$veri->AsiaKulAdUnvanTelEmail}}" name="AsiaKulAdUnvanTelEmail" id="AsiaKulAdUnvanTelEmail" placeholder="Adi Soyadi, Unvan, Telefon, Email">
    </div>
</div>

<div class="form-group">
    <label for="AustraliaKulAdUnvanTelEmail" class="col-sm-2 control-label">Avustralya<br>Kullanıcı Adi Soyadi, Unvan, Telefon, Email </label>
    <div class="col-sm-10">
        <br>
        <input type="text" class="form-control" required value="{{@$veri->AustraliaKulAdUnvanTelEmail}}" name="AustraliaKulAdUnvanTelEmail" id="AustraliaKulAdUnvanTelEmail" placeholder="Adi Soyadi, Unvan, Telefon, Email">
    </div>
</div>

<div class="form-group">
    <label for="SAmericaKulAdUnvanTelEmail" class="col-sm-2 control-label">Güney Amerika<br>Kullanıcı Adi Soyadi, Unvan, Telefon, Email </label>
    <div class="col-sm-10">
        <br>
        <input type="text" class="form-control" required value="{{@$veri->SAmericaKulAdUnvanTelEmail}}" name="SAmericaKulAdUnvanTelEmail" id="SAmericaKulAdUnvanTelEmail" placeholder="Adi Soyadi, Unvan, Telefon, Email">
    </div>
</div>

<div class="form-group">
    <label for="EuropeKulAdUnvanTelEmail" class="col-sm-2 control-label">Avrupa<br>Kullanıcı Adi Soyadi, Unvan, Telefon, Email </label>
    <div class="col-sm-10">
        <br>
        <input type="text" class="form-control" required value="{{@$veri->EuropeKulAdUnvanTelEmail}}" name="EuropeKulAdUnvanTelEmail" id="EuropeKulAdUnvanTelEmail" placeholder="Adi Soyadi, Unvan, Telefon, Email">
    </div>
</div>

<div class="form-group">
    <label for="NAmericaKulAdUnvanTelEmail" class="col-sm-2 control-label">Orta Doğu<br>Kullanıcı Adi Soyadi, Unvan, Telefon, Email </label>
    <div class="col-sm-10">
        <br>
        <input type="text" class="form-control" required value="{{@$veri->NAmericaKulAdUnvanTelEmail}}" name="NAmericaKulAdUnvanTelEmail" id="NAmericaKulAdUnvanTelEmail" placeholder="Adi Soyadi, Unvan, Telefon, Email">
    </div>
</div>

<div class="form-group">
    <label for="BuyukResim" class="col-sm-2 control-label"> Büyük Resim</label>
    <div class="col-sm-10">
      <input type="file" class="form-control" name="BuyukResim" id="BuyukResim"  >
    </div>
    @if( @$veri->BuyukResim )
        <div class="col-md-10 col-md-offset-2" >
            <a href="{{ url('images/uploads/ihracat/'.@$veri->BuyukResim)}}">
                <div class="checkbox">
                    <img src="{{ url('images/uploads/ihracat/'.@$veri->BuyukResim) }}" width="150" height="150" alt="">
                </div>
            </a>
        </div>
    @endif
</div>

<div class="form-group">
    <label for="BelgeResim" class="col-sm-2 control-label">Belge Resimler</label>
    <div class="col-sm-10">
        <input type="file" class="form-control" name="BelgeResim[]" id="BelgeResim"  multiple >
    </div>
    @if( @$veri->BelgeResim )
        <div class="col-md-10 col-md-offset-2 container container-fluid" >
            <div class="checkbox">
                @foreach( json_decode(@$veri->BelgeResim)  as $key=>$value)
                <a>
                    <img src="{{ url('images/uploads/ihracat/belgeler/'.$value ) }}" width="150" height="150" alt="">
                    <input type="checkbox"  name="secili_belge_resim[]"   @if( @$value ) checked value="{{$value}}" @else value="0" @endif>
                </a>
                @endforeach
            </div>
        </div>
    @endif
</div>

<div class="form-group">
    <label for="IhracatIcerik" class="col-sm-2 control-label">İçerik</label>
    <div class="col-sm-10">
        <textarea name="IhracatIcerik" class="ck">{{ @$veri->IhracatIcerik }}</textarea>
    </div>
</div>

<script type="text/javascript">


</script>
@stop