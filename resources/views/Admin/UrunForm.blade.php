@extends('Admin.Layout.Form')
@section('baslik','Ürün')
@section('form')


<?php 
use App\Http\Models\Menu as MN; 
$menu = MN::where('is_active',1)->where('MenuTipi', 'modul')->where('Link', 'LIKE','/tr/urunler-url%')->get(); 
 
 ?> 

@section('form')
<div class="form-group">
    <label for="Urun_UstKat" class="col-sm-2 control-label">Üst Kategori</label>
    <div class="col-sm-10">
        <select class="form-control" id="Urun_UstKat" required name="Urun_UstKat">
            <option value="">Üst Kategori Seçiniz</option>
            @foreach(@$menu as $td)
                <option value="{{$td->id}}" @if(@$td->id == @$veri->Urun_UstKat) selected @endif>{{$td->Adi}}</option>
            @endforeach
        </select>
    </div>
</div>


<div class="form-group">

    <label class="col-md-2 control-label">Ürün Şekli Daire + Diktörtgen</label>
    <div class="col-md-10">
        <div class="checkbox">
            <label>
                <input type="checkbox" id="Tip" name="Tip"  @if(@$veri->Tip == 1) checked value="1" @else value="0" @endif >Daire ve Diktörtgen olsun
            </label>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Daire İçi Yazı</label>
    <div class="col-sm-10">
        <input type="text" value="{{ @$veri->daire_yazi }}" class="form-control" name="daire_yazi">
    </div>
</div>

<div class="form-group diktortgen" style="display: none">
    <label class="col-sm-2 control-label">Dikdörtgen İçi Yazı 1</label>
    <div class="col-sm-10">
        <input type="text" value="{{ @$veri->diktortgen_yazi }}" class="form-control" name="diktortgen_yazi">
    </div>
</div>

<div class="form-group diktortgen" style="display: none">
    <label class="col-sm-2 control-label">Dikdörtgen İçi Yazı 2</label>
    <div class="col-sm-10">
        <input type="text" value="{{ @$veri->diktortgen_yazi2 }}" class="form-control" name="diktortgen_yazi2">
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Daire Altı Yazı</label>
    <div class="col-sm-10">
        <input type="text" value="{{ @$veri->daire_alti_yazi }}" class="form-control" name="daire_alti_yazi">
    </div>
</div>



@stop
@section('js')
<script type="text/javascript">
    
jQuery(document).ready(function($) {
       $('#Tip').change(function() {
            if($(this).is(":checked")) {
               $(this).val("1");
               $('.diktortgen').show("slow");
            }
            else{
               $(this).val("0");
               $('.diktortgen').hide("slow");
            }
        });

    
});
</script>
@stop
