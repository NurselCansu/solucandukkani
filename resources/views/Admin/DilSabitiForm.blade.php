@extends('Admin.Layout.Form')
@section('baslik'){{$Title}}@endsection
@section('form')
<input type="hidden" name="id" value="{{@$veri->id}}">
<div class="form-group">
    <label for="SabitAdi" class="col-sm-2 control-label">Sabit Adı</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="{{@$veri->SabitAdi}}" name="SabitAdi" id="SabitAdi" placeholder="Sabit Adi">
    </div>
</div>
<div class="form-group">
    <label for="Slug" class="col-sm-2 control-label">Slug</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="{{@$veri->Slug}}" name="Slug" id="Slug" placeholder="Slug">
    </div>
</div>

@stop

@section('js')
<script type="text/javascript">
    var slug = function(str) {
    var charMap = {Ç:'c',Ö:'o',Ş:'s',İ:'i',I:'i',Ü:'u',Ğ:'g',ç:'c',ö:'o',ş:'s',ı:'i',ü:'u',ğ:'g'};
    str_array = str.split('');

        for(var i=0, len = str_array.length; i < len; i++) {
            str_array[i] = charMap[ str_array[i] ] || str_array[i];
        }
    str = str_array.join('');
    var $slug = '';
    var trimmed = $.trim(str);
    $slug = trimmed.replace(/[^a-z0-9-]/gi, '-').
    replace(/-+/g, '-').
    replace(/^-|-$/g, '');
//    return $slug.toLowerCase();
    return $slug;
}


$( "#SabitAdi" ).keyup(function() {
  $( "#Slug" ).val(slug($(this).val()));
});
</script>
@stop