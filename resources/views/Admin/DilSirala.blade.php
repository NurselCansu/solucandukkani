@extends('Admin.Layout.Master')

<?php
    $url = Request::url();
    $link = explode('Sirala',$url);
    $link = $link[0].'Sirala'
?>
@section('title') {{$baslik}} Sıralama @endsection

@section('content')
<div class="panel panel-default">
  <div class="panel-heading">
  <span class="pull-left">@yield('title')</span>
              
                
                <div style="clear: both;"></div>
            </div>
  <div class="panel-body">
    <p class="alert alert-info">
        @yield('title') İşlemini Sürükle - Bırak Yaparak Sitenizdeki Görünüm Sırasını Değiştirebilirsiniz.
    </p>
    
    <ul id="sortable" class="list-group">
    <?php
        foreach($veri as $v){
            
            echo '<li class="list-group-item" id="id-'.$v->id.'">
                 '.$v->$vtcolumnname.'
        </li>';
        }
    ?>
    </ul>
  </div>
</div>
@stop

@section('js')
<script src="{{asset('plugins/jquery-ui/jquery-ui.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function () {
    $('#sortable').sortable({
        axis: 'y',
        stop: function (event, ui) {
            var oData = $(this).sortable('serialize');

            $.ajax({
                data: oData+'&_token={{{csrf_token()}}}',
                type: 'POST',
                dataType: 'json',
                url: '{{$link}}'
                
            }).done(function(data){
                if (data.status) {
                    new PNotify({
                        title: 'Sıralama',
                        text: 'İşleminiz Başarıyla Yapılmıştır.',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                } else {
                    new PNotify({
                        title: 'Hata',
                        text: 'Hata var ulAN.',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                }
            });
    }
    });
    $('li').click(function(){
        

    });

});

 </script>

@stop
