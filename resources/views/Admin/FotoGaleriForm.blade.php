@extends('Admin.Layout.Form')
@section('baslik'){{$Title}}@endsection
@section('form')
<input type="hidden" name="trid" value="{{@$trveri->id}}">
<div class="form-group">

    <label for="Adi" class="col-sm-2 control-label">Foto Galeri Adı</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="{{@$veri->Adi}}" name="Adi" id="Adi" placeholder="Türkçe, İngilizce, İspanyolca, Rusça, Fransızca galeri adları olucak şekilde yazınız ">
    </div>
</div>

<div class="form-group">

    <label for="KapakFotografi" class="col-sm-2 control-label">Galeri Kapak Fotoğrafı</label>
    <div class="col-sm-10">
        <input type="file" class="form-control" name="KapakFotografi[]" id="KapakFotografi"  placeholder="KapakFotografi">
    </div>
    @if( @$veri->KapakFotografi )
        <div class="col-md-10 col-md-offset-2" >
            <a href="{{ url('images/uploads/FotoGaleri/kapak/'.@$veri->KapakFotografi ) }}">
                <div class="checkbox">
                    <img class="replace-2x" src="{{ url('images/uploads/FotoGaleri/kapak/'.@$veri->KapakFotografi)  }}" width="150" height="150" alt="">
                </div>
            </a>
        </div>
    @endif

</div>

@stop

@section('js')

@stop