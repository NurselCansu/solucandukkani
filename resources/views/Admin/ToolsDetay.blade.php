@extends('Admin.Layout.Master')

@section('title', 'Ürün Detay')

@section('content')
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">@yield('title')</div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
				<tr>
					<th style="width:200px;">Adı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->Adi}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Resim</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><img width='150' height='150' src="{{url('images/uploads/Tools/KapakResim/'.$veri->KapakResim)}}" style="max-width: 100%;"></td>
				</tr>

				<tr>
					<th style="width:200px;">Pdf</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><img width='150' height='150' src="{{url('images/uploads/Tools/Pdf/'.$veri->Pdf)}}" style="max-width: 100%;"></td>
				</tr>
				
				<tr>
					<th style="width:200px;">Meta Keywords</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->MetaTag}}</td>
				</tr>
                                <tr>
					<th style="width:200px;">Meta Title</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->MetaTitle}}</td>
				</tr>
                                <tr>
					<th style="width:200px;">Meta Description</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->MetaDescription}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Durum</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{($veri->is_active==1)?'Aktif':'Pasif'}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Oluşturulma Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{Fnk::TarihDuzenle($veri->created_at)}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Güncelleme Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{Fnk::TarihDuzenle($veri->updated_at)}}</td>
				</tr>
				<tr>
					<td colspan="3"><a href="{{URL::previous()}}" class="btn btn-primary pull-right">Geri Dön</a></td>
				</tr>
			</table>
		</div>
	</div>
</div>
@stop