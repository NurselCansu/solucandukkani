@extends('Admin.Layout.Master')

@section('title', 'Menü Detay')

@section('content')
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">@yield('title')</div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
				<tr>
					<th style="width:200px;">Üst Menü Adı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$ustkat}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Menü Adı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->Adi}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Link Açılış Tipi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>@if($veri->LinkAcilisTipi=='') Aynı Pencere @else Yeni Pencere @endif</td>
				</tr>
				<tr>
					<th style="width:200px;">Menü Tipi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->MenuTipi}}</td>
				</tr>
				@if($veri->MenuTipi=='link' || $veri->MenuTipi=='modul')
				<tr>
					<th style="width:200px;">Menü Linki</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->Link}}</td>
				</tr>
				@endif
				@if($veri->MenuTipi=='icerik')
				<tr>
					<th style="width:200px;">Menü İçeriği</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{!!$veri->Icerik!!}</td>
				</tr>
				@endif
				<tr>
					<th style="width:200px;">Durum</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{($veri->is_active==1)?'Aktif':'Pasif'}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Oluşturulma Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{Fnk::TarihDuzenle($veri->created_at)}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Güncelleme Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{Fnk::TarihDuzenle($veri->updated_at)}}</td>
				</tr>
				<tr>
					<td colspan="3"><a href="{{URL::previous()}}" class="btn btn-primary pull-right">Geri Dön</a></td>
				</tr>
			</table>
		</div>
	</div>
</div>
@stop