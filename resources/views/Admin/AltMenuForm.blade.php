@extends('Admin.Layout.Form')
@section('baslik'){{$Title}}@endsection
@section('form')
<input type="hidden" name="id" value="{{@$veri->id}}">
<div class="form-group">
	<label for="UstKatId" class="col-sm-2 control-label">Üst Menü</label>
	<div class="col-sm-10">
		<select class="form-control" id="UstKatId" required name="UstKatId">
			<option value="0">Üst Menüsü Yok</option>
				<?php
					Fnk::ChildrenOption($menuler,@$veri->UstKatId,@$veri->id,'Adi',0,false);
				?>
		</select>
	</div>
</div>
<div class="form-group">
    <label for="Adi" class="col-sm-2 control-label">Menü Adı</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="{{@$veri->Adi}}" name="Adi" id="Adi" placeholder="Menü Adı">
    </div>
</div>
<div class="form-group">
    <label for="MenuTipi" class="col-sm-2 control-label">Menü Tipi</label>
    <div class="col-sm-10">
        <select name="MenuTipi" class="form-control" required="" onchange="MenuTipiDegistir()" id="MenuTipi">
            <option value="">Lütfen Seçiniz</option>
            <option @if(isset($veri) && $veri->MenuTipi=='icerik') selected @endif value="icerik">İçerik</option>
            <option @if(isset($veri) && $veri->MenuTipi=='link') selected @endif value="link">Link</option>
            <option @if(isset($veri) && $veri->MenuTipi=='modul') selected @endif value="modul">Modül</option>
        </select>
    </div>
</div>
<div class="form-group">
    <label for="LinkAcilisTipi" class="col-sm-2 control-label">Link Açılış Tipi</label>
    <div class="col-sm-10">
        <div class="radio">
            <label>
                <input type="radio" name="LinkAcilisTipi" value="" @if(isset($veri) && $veri->LinkAcilisTipi=='') checked @endif @if(!isset($veri)) checked @endif >Aynı Pencere</label>
    	</div>
	    <div class="radio">
	        <label><input type="radio" name="LinkAcilisTipi" @if(isset($veri) && $veri->LinkAcilisTipi=='target="_blank"') checked @endif value='target="_blank"'>Yeni Pencere</label>
		</div>
	</div>
</div>
<div class="form-group" id="Linkalani" @if(!isset($veri) || $veri->MenuTipi!='link') style="display:none" @endif >
	<label for="Link" class="col-sm-2 control-label">Menü Linki</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="Link" name="Link" value="{{@$veri->Link}}">
    </div>
</div>
<div class="form-group" id="Icerikalani" @if(!isset($veri) || $veri->MenuTipi!='icerik') style="display:none" @endif >
     <label for="Icerik" class="col-sm-2 control-label">Menü İçeriği</label>
    <div class="col-sm-10">
        <textarea required id="Icerik" name="Icerik" class="ck">{{@$veri->Icerik}}</textarea>
    </div>
</div>
<div class="form-group" id="Modulalani" @if(!isset($veri) || $veri->MenuTipi!='modul') style="display:none" @endif >
     <label for="Modul" class="col-sm-2 control-label">Modül</label>
    <div class="col-sm-10">
        <select name="Modul" class="form-control" id="Modul" required>
            <option value="">Lütfen Modül Seçiniz</option>
            <option value="iletisim" @if(@$veri->Link==url('iletisim')) selected @endif>İletişim Modülü</option>
        </select>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript">
    function MenuTipiDegistir() {
        var MenuTip = $('#MenuTipi').val();
        if (MenuTip == "link") {
            $('#Linkalani').show('slow');
            $('#Icerikalani').hide('slow');
            $('#Modulalani').hide('slow');
        } else if (MenuTip == "icerik") {
            $('#Icerikalani').show('slow');
            $('#Linkalani').hide('slow');
            $('#Modulalani').hide('slow');
        } else if (MenuTip == "modul") {
            $('#Modulalani').show('slow');
            $('#Icerikalani').hide('slow');
            $('#Linkalani').hide('slow');
        }
    }

</script>
@stop