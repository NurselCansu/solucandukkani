@extends('Admin.Layout.Master')

@section('title', 'Ana Sayfa')

@section('content')
@if(Session::has('Message'))
<div class="alert alert-danger" role="alert">
        {{ Session::get('Message') }}
    </div>
@endif
Hoşgeldiniz.
@endsection
