@extends('Admin.Layout.Form')

@section('baslik', 'Ayarlar')

@section('form')

<div class="form-group">
    <label for="FirmaAdi" class="col-sm-2 control-label">Firma Adı</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="{{@$veri->FirmaAdi}}" name="FirmaAdi" id="FirmaAdi" placeholder="Firma Adı">
    </div>
</div>
<div class="form-group">
    <label for="Logo" class="col-sm-2 control-label">Logo</label>
    <div class="col-sm-10">
        <img src="{{url('images/uploads/'.@$veri->Logo)}}" style="max-height: 70px;margin-bottom: 10px;" id="anlikresim" class="img-rounded" />
      <input type="file" class="form-control" name="Logo" id="Logo" placeholder="Logo">
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Favicon</label>
    <div class="col-sm-10">
        <img src="{{url('images/uploads/'.@$veri->Favicon)}}" style="max-height: 70px;margin-bottom: 10px;"  class="img-rounded" />
        <input type="file" name="Favicon" id="Favicon" class="form-control">
    </div>
</div>

<div class="form-group">
    <label for="Adi" class="col-sm-2 control-label">Ana Sayfa Yazısı Başlık</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="{{@$veri->AnaSayfaBaslik}}" name="AnaSayfaBaslik" id="Adi" placeholder="Ana Sayfa Yazısı Başlığı">
    </div>
</div>
<div class="form-group">
    <label  class="col-sm-2 control-label">Anasayfa Sloganı 1</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="AnaSayfaIcerikSutun1" value="{{ @$veri->AnaSayfaIcerikSutun1 }}"></textarea>
    </div>
</div>
<div class="form-group">
    <label  class="col-sm-2 control-label">Anasayfa Sloganı 2</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="AnaSayfaIcerikSutun2" value="{{ @$veri->AnaSayfaIcerikSutun2 }}" ></textarea>
    </div>
</div>
<div class="form-group">
    <label  class="col-sm-2 control-label">Anasayfa Sloganı 3</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="AnaSayfaIcerikSutun3" value="{{ @$veri->AnaSayfaIcerikSutun3 }}"></textarea>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Slogan Link</label>
    <div class="col-sm-10">
        <input type="text" name="slogan_link" class="form-control" value="{{  @$veri->slogan_link }}">
    </div>
</div>

<div class="form-group">
    <label  class="col-sm-2 control-label">Ana Sayfa Tanıtım Yazısı Baslık</label>
    <div class="col-sm-10">
        <textarea rows="3" type="text" name="AnaSayfaAltIcerikSutun1" class="form-control">{{ @$veri->AnaSayfaAltIcerikSutun1 }}</textarea>
    </div>
</div>
<div class="form-group">
    <label  class="col-sm-2 control-label">Ana Sayfa Tanıtım Yazısı İçerik</label>
    <div class="col-sm-10">
        <textarea rows="3" type="text" name="AnaSayfaAltIcerikSutun2" class="form-control">{{ @$veri->AnaSayfaAltIcerikSutun2 }}</textarea>
    </div>
</div>
<div class="form-group">
    <label  class="col-sm-2 control-label">Ana Sayfa Tanıtım Yazısı Yönlenecek Link</label>
    <div class="col-sm-10">
        <textarea rows="3" type="text" name="AnaSayfaAltIcerikSutun3" class="form-control">{{ @$veri->AnaSayfaAltIcerikSutun3 }}</textarea>
    </div>
</div>

<div class="form-group">
    <label  class="col-sm-2 control-label">Değerlerimiz</label>
    <div class="col-sm-10">
        <textarea rows="3" type="text" name="AnaSayfaAltSliderSutun1" class="form-control">{{ @$veri->AnaSayfaAltSliderSutun1 }}</textarea>
    </div>
</div>

<div class="form-group">
    <label  class="col-sm-2 control-label">Misyonumuz</label>
    <div class="col-sm-10">
        <textarea rows="3" type="text" name="AnaSayfaAltSliderSutun2" class="form-control">{{ @$veri->AnaSayfaAltSliderSutun2 }}</textarea>
    </div>
</div>





<div class="form-group key2" >
    <label class="col-sm-2 control-label">Meta Keywords</label>
    <div class="col-sm-10">
        <select type="text" class="form-control" style="width: 100%" required multiple value="{{@$veri->MetaTag}}" name="MetaTag[]" id="MetaTag" placeholder="Meta Keywords">
            @if(!empty(@$veri->MetaTag))
                @foreach (json_decode(@$veri->MetaTag) as $tag)
                   <option value="{{ $tag }}" selected="selected">{{ $tag }}</option>
                @endforeach
            @endif    
        </select>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Meta Title</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="{{@$veri->MetaTitle}}" name="MetaTitle" id="MetaDescription" placeholder="Meta Description">
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Meta Description</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="{{@$veri->MetaDescription}}" name="MetaDescription" id="MetaDescription" placeholder="Meta Description">
    </div>
</div>

@stop
@section('js')
<script type="text/javascript">
    jQuery(document).ready(function($) {

        $('#MetaTag').select2({
            tags: true,
            multiple: true,
            tokenSeparators: [',',' '],
            
        });
        

        
    });
</script>
@stop