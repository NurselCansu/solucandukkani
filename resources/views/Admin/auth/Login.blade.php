<!DOCTYPE html>
<html lang="tr">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Portakal Yazılım CMS Yönetim Paneli">
    <meta name="author" content="Portakal Yazılım">

    <title>Admin - Giriş Formu</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('plugins/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{asset('plugins/metisMenu/dist/metisMenu.min.css')}}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{asset('css/sb-admin-2.css')}}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Lütfen Giriş Yapınız</h3>
                    </div>
                    <div class="panel-body">
                    @if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>HATA!</strong> Giriş İşleminde Sorun Oluştu.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
                                        
					
					<form role="form" method="POST" action="{{ url('Admin/login') }}">
						{!! csrf_field() !!}
						<fieldset>
						<div class="form-group">
								<input type="email" placeholder="E-Posta Adresi" class="form-control email" required name="email" value="{{ old('email') }}" autofocus>
						</div>

						<div class="form-group">
								<input type="password" placeholder="Parola" required class="form-control" name="password">
						</div>

						<div class="form-group">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="remember"> Beni Hatırla
									</label>
								</div>
						</div>
						<div class="form-group">
								<button type="submit" class="btn btn-lg btn-primary btn-block">Giriş</button>
							<!--	<br><a class="btn btn-lg btn-danger btn-block" href="{{ url('password/email') }}">Şifremi Unuttum</a> -->
						</div>
						</fieldset>
					</form>
                    </div>
                </div>
            </div>
        </div>
    </div>



<div class="hidden-xs" style="position: absolute;bottom: 20px;right: 20px;">
    <a href="http://www.portakalyazilim.com.tr" target="_blank">Portakal Yazılım CMS Yönetim Paneli</a>
</div>

<div class="hidden-sm hidden-md hidden-lg" style="text-align: center">
    <a href="http://www.portakalyazilim.com.tr" target="_blank">Portakal Yazılım CMS Yönetim Paneli</a>
</div>


  <!-- jQuery -->
    <script src="{{asset('plugins/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('plugins/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{asset('plugins/metisMenu/dist/metisMenu.min.js')}}"></script>
    <!-- Custom Theme JavaScript -->
    <script src="{{asset('js/sb-admin-2.js')}}"></script>

</body>

</html>