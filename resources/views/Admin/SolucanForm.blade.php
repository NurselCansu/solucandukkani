@extends('Admin.Layout.Form')
@section('baslik','Solucan')
@section('form')


<input type="hidden" name="id" value="{{@$veri->id}}">


<div class="form-group">
<label class="col-sm-2 control-label">Ana Görsel Yazısı</label>
<div class="col-sm-10">
<input type="text" name="Adi" value="{{ @$veri->Adi }}" class="form-control">
</div>
</div>

<div class="form-group " >
    <label class="col-sm-2 control-label">Ana Görsel Link Verme</label>
    <div class="col-sm-10">
       <select name="link" class="form-control" required>
        
           @foreach($menuler as $menu)
                <option value="{{ $menu->Slug }}" @if(($menu->Sluq) == (@$veri->link)) selected @endif>{{ $menu->Adi }}</option>
           @endforeach
       </select>
    </div>
</div>

@stop

@section('js')

@stop

