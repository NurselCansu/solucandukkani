@extends('Admin.Layout.Master')

@section('title', 'Yönetici Listesi')

@section('content')
<?php
$url = Request::url();
$link = str_replace(array ('/Listele', '/Bekleyenler'), '', $url);
$alanadlari = ['Adı', 'E-Posta Adresi'];
$degeradlari = ['name', 'email'];
?>
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            @yield('title')
            <div class="pull-right" style="margin:-5px;">
                <a href="{{$link}}/Ekle" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Yeni Ekle</a>
            </div>
        </div>
        <div class="panel-body">
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover" id="dataTables">
                    <thead>
                        <tr>
                            <?php
                            foreach ($alanadlari as $value){
                                echo '<th style="min-width:100px">' . $value . '</th>';
                            }
                            ?>
                            <th style="width:250px">İşlem</th>
                        </tr>
                    </thead>       
                    <tbody>
                        @foreach($veriler as $veri)
                            <tr>
                                @foreach($degeradlari as $value)
                                    @if($value=='Resim')
                                        <td><img src="{{url('images/uploads')}}/{{$veri->$value}}" class="img-rounded" height="50"></td>
                                    @else
                                        <td>{{$veri->$value}}</td>
                                    @endif
                                @endforeach

                                <td style="text-align: center">
                                    <a class="btn btn-default btn-sm" href="{{$link}}/Detay/{{$veri->id}}">Detay</a>
                                    <a href="{{$link}}/Duzenle/{{$veri->id}}" class="btn btn-sm btn-info">Düzenle</a>
                                    <a href="{{$link}}/Sil/{{$veri->id}}" class="btn btn-sm btn-danger">Sil</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <?php
                                foreach ($alanadlari as $value){
                                    echo '<th>' . $value . '</th>';
                                }
                                ?>
                                <th>İşlem</th>
                            </tr>
                        </tfoot>
                </table>
            </div>

        </div>
    </div>
</div>
@stop

@section('css')
<link href="{{asset('plugins/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/datatables-responsive/css/dataTables.responsive.css')}}" rel="stylesheet">
        @stop

        @section('js')
        <script src="{{asset('plugins/datatables/media/js/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('plugins/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')}}"></script>
        <script type="text/javascript">
                                                                                                                                            $(document).ready(function() {
                                                                                                                                    $('#dataTables').DataTable({
                                                                                                                                    responsive: true,
                                                                                                                                            "language": {
                                                                                                                                            "lengthMenu": "Sayfada Gösterilecek Veri Adeti _MENU_",
                                                                                                                                                    "zeroRecords": "Arama Sonucu Bulunamadı.",
                                                                                                                                                    "info": "Gösterilen Sayfa : _PAGE_. Toplam Sayfa : _PAGES_",
                                                                                                                                                    "infoEmpty": "Gösterilecek Veri Bulunamadı",
                                                                                                                                                    "infoFiltered": "(_MAX_ Kayıt Filtrelendi)",
                                                                                                                                                    "search": "Arama : ",
                                                                                                                                                    "oPaginate": {
                                                                                                                                                    "sNext": "İleri",
                                                                                                                                                            "sPrevious": "Geri"
                                                                                                                                                    }
                                                                                                                                            }
                                                                                                                                    });
                                                                                                                                    });
        </script>

        @stop