@extends('Admin.Layout.Form')

@section('baslik', 'Popup')

@section('form')

<div class="form-group">
    <label for="Resim" class="col-sm-2 control-label">Popup Resim Kapak</label>
    <div class="col-sm-10">
      <input type="file" class="form-control" name="Resim" id="Resim"  onchange="loadImageKapak(this);"  placeholder="Resim">
    </div>
    @if( @$veri->Resim )
        <div class="col-md-10 col-md-offset-2" >
            <a href="{{ url('images/uploads/'.@$veri->Resim)  }}">
                <div class="checkbox">
                    <img class="replace-2x" src="{{ url('images/uploads/'.@$veri->Resim)  }}" width="150" height="150" alt="">
                </div>
            </a>
        </div>
    @endif
</div>

<div class="form-group foto" @unless(@$veri->Resim) style="display: none" @endunless>
    <label for="FotoGenislik" class="col-sm-2 control-label">Popup Fotograf Genislik</label>
    <div class="col-sm-4">
        <input type="text" class="form-control"  value="@if(!empty(@$veri->FotoGenislik)){{trim(@$veri->FotoGenislik)}}@else{{trim(80)}} @endif" name="FotoGenislik"  id="FotoGenislik" >
    </div>
    <label for="FotoUzunluk" class="col-sm-2 control-label">Popup Fotograf Uzunluk</label>
    <div class="col-sm-4">
        <input type="text" class="form-control"  value="@if(!empty(@$veri->FotoUzunluk)){{trim(@$veri->FotoUzunluk)}}@else{{trim(80)}} @endif" name="FotoUzunluk"  id="FotoUzunluk" >
    </div>
</div>
<div class="form-group">
    <label for="Resim" class="col-sm-2 control-label">Popup Resim Arkaplan</label>
    <div class="col-sm-10">
      <input type="file" class="form-control" name="ResimKapak" id="ResimKapak"  onchange="loadImageArka(this);" placeholder="Resim Kapak">
    </div>
    @if( @$veri->ResimKapak )
        <div class="col-md-10 col-md-offset-2" >
            <a href="{{ url('images/uploads/'.@$veri->ResimKapak) }}">
                <div class="checkbox">
                    <img class="replace-2x" src="{{ url('images/uploads/'.@$veri->ResimKapak) }}" width="150" height="150" alt="">
                </div>
            </a>
        </div>
    @endif
</div>
<div class="form-group">
    <label for="Genisligi" class="col-sm-2 control-label">Popup Genişliği</label>
    <div class="col-sm-10">
        <input type="text" class="form-control"  value="@if(!empty(@$veri->Genislik)){{@$veri->Genislik}}@else 487 @endif" name="Genislik"  id="Genislik" >
    </div>
</div>

<div class="form-group">
    <label for="Baslik" class="col-sm-2 control-label">Popup Başlığı</label>
    <div class="col-sm-9">
        <input type="text" class="form-control" required value="{{@$veri->Baslik}}" name="Baslik" id="Baslik" >
    </div>
    <div class="col-sm-1">
        <input type="color" id="BaslikRenk" name="BaslikRenk" value="{{ @$veri->BaslikRenk }}" style="width:85%;">
    </div>
</div>

<div class="form-group">
    <label for="Icerik" class="col-sm-2 control-label">Popup İçeriği</label>
    <div class="col-sm-9">
        <input type="text" class="form-control" required value="{{@$veri->Icerik}}" name="Icerik" id="Icerik" >
    </div>
    <div class="col-sm-1">
        <input type="color" id="IcerikRenk" name="IcerikRenk" value="{{ @$veri->IcerikRenk }}" style="width:85%;">
    </div>
</div>

<div class="form-group">
    <label for="Sure" class="col-sm-2 control-label">Popup Süresi</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="{{@$veri->Sure}}" name="Sure" id="Sure" placeholder="sn olarak giriniz" >
    </div>
</div>
<div class="form-group">
    <label for="trbaslangic" class="col-sm-2 control-label">Başlangıç Tarih</label>
    <div class="col-sm-4">
        <div class="input-group datetimepicker2">
            <input type="text" name="baslangic" class="form-control" required="" value="@if(!empty($veri->baslangic)){{ date('d.m.Y H:i', strtotime($veri->baslangic)) }}@else{{ date('d.m.Y H:i') }}@endif" aria-required="true" aria-invalid="false">
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
    </div>
</div>
<div class="form-group">
    <label for="trbitis" class="col-sm-2 control-label">Bitiş Tarih</label>
    <div class="col-sm-4">
        <div class="input-group datetimepicker3">
            <input type="text" name="bitis" class="form-control" required="" value="@if(!empty($veri->bitis)){{ date('d.m.Y H:i', strtotime($veri->bitis)) }}@else{{ date('d.m.Y H:i') }}@endif" aria-required="true" aria-invalid="false">
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
    </div>
    <div class="col-sm-6">
        @if(@$veri->bitis < date('Y-m-d H:i:s'))
           <div class="alert alert-danger" role="alert">Popup Süresi Bittiği için Anasayfada Gözükmez. . .</div>
        @endif
    </div>
</div>

<img id="kapak" style="display: none" />
<img id="arkaplan" style="display: none" />


@stop

@section('css')
<style type="text/css"> 

    .sweet-alert {margin: auto; transform: translateX(-50%); background-size: inherit; }
    @if(!empty(@$veri->ResimKapak))
        .sweet-alert{
            background-image: url("{{ url('images/uploads/'.@$veri->ResimKapak) }}") 
        }
    @endif

</style>
@stop

@section('js')
    
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('.onizleme').show();
         
      });
    var control=true;
    function loadImageKapak(input) {
        control=false;
         $('.foto').show('slow');  
         if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#kapak') .attr('src', e.target.result);  
                };
                reader.readAsDataURL(input.files[0]);
        }
    }
    function loadImageArka(input){
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#arkaplan').attr('src', e.target.result);
                 
            };
            reader.readAsDataURL(input.files[0]);
        }
        onizleme();
        swal.close();
    }
    function onizleme(){
        var genislik = $.trim($('#Genislik').val());
        var baslikRengi = $.trim($('#BaslikRenk').val());
        var baslikIcerik = $.trim($('#Baslik').val());
        var icerikRengi = $.trim($('#IcerikRenk').val());
        var icerik = $.trim($('#Icerik').val());
        var sure = parseInt($('#Sure').val())*1000;
        var kapakFotoGenislik = $.trim($('#FotoGenislik').val());
        var kapakFotoUzunluk = $.trim($('#FotoUzunluk').val());

        $('.sweet-alert').css('width',genislik);
        
        $('.sweet-alert').css('background-image','url('+ $.trim($('#arkaplan').attr('src')) + ')');   
        swal({
              title: '<span style="color:'+baslikRengi+'!important" >'+baslikIcerik+'</span>',
              text: '<span style="color:'+icerikRengi+'!important" >'+icerik+'</span>',
              timer: sure,
              showConfirmButton: false,
              allowEscapeKey:true,
              allowOutsideClick:true,
              animation: "slide-from-top",
              imageUrl: 
                  @if(!empty(@$veri->Resim))
                        "{{ url('images/uploads/'.@$veri->Resim) }}" 
                  @else 
                        $('#kapak').attr('src') 
                  @endif,
              imageSize: kapakFotoGenislik+"x"+kapakFotoUzunluk,
              html: true,
        });

    }


 
</script>

@stop

