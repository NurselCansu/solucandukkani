<?php $__env->startSection('footer'); ?>


<footer id="footer" class="light">
	<div class="container">
		<div class="row">
			<div class="footer-ribbon">
				<span>OZB</span>
			</div>
				<div class="col-md-2">
					<h4><?php echo e(\App\Http\Fnk::Ceviri('sosyal-medya-takip')); ?></h4>
					<ul class="social-icons">
						<?php foreach($sosyal as $element): ?>
                            <li class="social-icons-<?php echo e($element->Adi); ?>"><a <?php echo e($element->LinkAcilisTipi); ?> href="<?php echo e(url($element->Link)); ?>"><i class="fa fa-<?php echo e($element->Adi); ?>"></i></a>
                        <?php endforeach; ?>
					</ul>
				</div>
				<div class="col-md-3">
					<h4><?php echo e(\App\Http\Fnk::Ceviri('footer-sayfalar')); ?></h4>
						<?php foreach($altmenuler as $element): ?>
                   			<a <?php echo e($element->LinkAcilisTipi); ?> 
                       			<?php if($element->MenuTipi=='icerik'): ?>
                        			href="/<?php echo e($dil); ?>/AltMenu/<?php echo e($element->Slug); ?>"
                        		<?php else: ?>
                        			href="<?php echo e(url($element->Link)); ?>"
                        		<?php endif; ?>
                        		><?php echo e($element->Adi); ?>

                        	</a>
                        	<br>
                    	<?php endforeach; ?>
				</div>
				<div class="col-md-4">
					<div class="contact-details">
						<h4><?php echo e(\App\Http\Fnk::Ceviri('footer-iletisim')); ?></h4>
						<ul class="contact">
							<li><p><i class="fa fa-map-marker heading-primary"></i> <strong><?php echo e(\App\Http\Fnk::Ceviri('footer-adres')); ?>:</strong> <?php echo e($iletisimbilgileri->Adres); ?></p></li>
							<li><p><i class="fa fa-phone heading-primary"></i> <strong><?php echo e(\App\Http\Fnk::Ceviri('footer-telefon')); ?>:</strong> <?php echo e($iletisimbilgileri->Telefon); ?></p></li>
							<li><p><i class="fa fa-envelope heading-primary"></i> <strong><?php echo e(\App\Http\Fnk::Ceviri('footer-eposta')); ?>:</strong> <a href="mailto:<?php echo e($iletisimbilgileri->Eposta); ?>"><?php echo e($iletisimbilgileri->Eposta); ?></a></p></li>
						</ul>
					</div>
				</div>
				<div class="col-md-3">
					<div id="googlemapsMarkers" class="google-map mt-none mb-lg" style="height: 230px;"></div>
				</div>
						
		</div>
	</div>

	<div class="footer-bottom">
        <div class="container">
            <div class="row col-sm-12">
                <div class="copyright col-xs-12 col-sm-6 col-md-6">
                    Copyright © <?php echo e(date('Y')); ?> <?php echo e($ayarlar->FirmaAdi); ?><br>
                   <a href="#" data-toggle="modal" data-target=".bd-example-modal-sm" style="color:#848484"> Web Tasarım ve Programlama : <span style="font-size: 10px">PORTAKAL YAZILIM</span>
                    </a> 
                    <div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content" align="center" style="padding:7px;padding-top:10px;  ">
                                <p>OZB web sayfası tasarım ve kodlaması Portakal Yazılım tarafından, 
                                günümüzün en son teknoloji standartlarına uygun olarak yapılmıştır.
                                Bu standartlar uygulanırken arama motorlarında sıralama, her türlü platformdan ve cihazdan erişebilirlik, 
                                responsive uyumluluğu, hızlı yüklenme, güncelleme ve bakım işlemlerinin yönetilebilir ve kolay olması, 
                                hukuksal sorumluluk gibi değerler gözetilmiştir. Sayfa tasarlanırken ve kodlanırken uygulanan teknolojilerden 
                                bazıları şunlardır: W3C, HTML5, Ajax, Jquery, Php, Laravel, MVC, MySQL.<br/>
                                Web sunucusu hizmeti Portakal Yazılım'ın Türkiye lokasyonlu kendi sunucuları üzerinden sağlanmaktadır.
                                Proje hakkında detaylı bilgi için <a href="http://www.portakalyazilim.com.tr/tr" target="_blank" rel="follow">tıklayınız</a>.</p>
                                <a href="http://www.portakalyazilim.com.tr/tr" target="_blank" rel="follow"><img src="<?php echo e(url('images/portakal.png')); ?>" style="width:50%; height:50%;"></a>
                            </div>
                        </div>
                    </div>
                </div>
							<div class="col-md-4">
								<nav id="sub-menu">
									<ul>

							<!--Burada dğişiklik yapılacak-->
									
									</ul>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</footer>
	<script src="<?php echo e(asset('vendor/jquery/jquery.min.js')); ?>"></script>		
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQKVYMlwil4C_gTM-yE5VdAbRGeqt-MBo"></script>
	<script type="text/javascript">
	

		$(document).ready(function() {
			// Markers
			$("#googlemapsMarkers").gMap({
				controls: {
					draggable: (($.browser.mobile) ? false : true),
					panControl: true,
					zoomControl: true,
					mapTypeControl: true,
					scaleControl: true,
					streetViewControl: true,
					overviewMapControl: true
				},

				scrollwheel: false,
				markers: [

				//Bu alan dile gore adres çekmeli!!!!
				<?php foreach($subeler as $sube): ?>
					<?php if($sube["iletisimde_goster"] == 1 and \App\Http\Models\Dil::select('id')->where('KisaAd',$dil)->first()->id == $sube["DilId"]): ?>
					{
					address: "<?php echo e($sube->Adres); ?>",
					html: "<strong><?php echo e($sube->SubeAdi); ?></strong><br/><?php echo e($sube->Telefon); ?>",
					icon: {
						animation: google.maps.Animation.BOUNCE,
						image: "<?php echo e(url('img/pointer.png')); ?>",
						iconsize: [36, 45],
						iconanchor: [12, 46]
					}
				},
					<?php endif; ?>
				<?php endforeach; ?>
				],
				
				
				zoom: 4
			});
		});
		
	</script>


<?php $__env->stopSection(); ?>




