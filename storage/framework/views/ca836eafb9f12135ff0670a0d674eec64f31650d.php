	<meta charset="utf-8">
	<title><?php echo $__env->yieldContent('Title'); ?></title>
	<meta name="keywords" content="<?php echo $__env->yieldContent('MetaTag'); ?>">
	<meta name="description" content="<?php echo $__env->yieldContent('MetaDescription'); ?>" /> 
	<meta name="robots" content="index, follow"> 
	<meta class="viewport" name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!-- Favicon -->
	<!-- Favicon -->
		<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="img/apple-touch-icon.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo e(asset('vendor/bootstrap/css/bootstrap.min.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('vendor/font-awesome/css/font-awesome.min.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('vendor/animate/animate.min.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('vendor/simple-line-icons/css/simple-line-icons.min.css')); ?>">
		
		<link rel="stylesheet" href="<?php echo e(asset('vendor/magnific-popup/magnific-popup.min.css')); ?>">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo e(asset('css/theme.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('css/theme-elements.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('css/theme-blog.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('css/theme-shop.css')); ?>">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="<?php echo e(asset('vendor/rs-plugin/css/settings.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('vendor/rs-plugin/css/layers.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('vendor/rs-plugin/css/navigation.css')); ?>">
		

		

	    <!-- Owl Carousel Assets -->
	    <link href="<?php echo e(asset('/plugins/owl.carousel/owl-carousel/owl.carousel.css')); ?>" rel="stylesheet">
	    <link href="<?php echo e(asset('/plugins/owl.carousel/owl-carousel/owl.theme.css')); ?>" rel="stylesheet">
		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?php echo e(asset('css/skins/default.css')); ?>">

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo e(asset('css/custom.css')); ?>">

		<!-- Head Libs -->
		<script src="<?php echo e(asset('vendor/modernizr/modernizr.min.js')); ?>"></script>

		