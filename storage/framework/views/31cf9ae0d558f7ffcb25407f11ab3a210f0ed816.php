

<?php $__env->startSection('Title',$fotogaleri->MetaTitle); ?>
<?php $__env->startSection('MetaTag',$fotogaleri->MetaTag); ?>
<?php $__env->startSection('MetaDescription',$fotogaleri->MetaDescription); ?>
<?php $__env->startSection('content'); ?>
<div class="breadcrumb-box">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="/<?php echo e($dil); ?>/"><?php echo e(\App\Http\Fnk::Ceviri('anasayfa')); ?></a> </li>
            <li><a href="/<?php echo e($dil); ?>/<?php echo e(\App\Http\Fnk::Ceviri('foto-galeri-url')); ?>"><?php echo e(\App\Http\Fnk::Ceviri('fotograf-galerileri')); ?></a> </li>
            <li class="active"><?php echo e(\App\Http\Fnk::Ceviri('fotograf-galerileri')); ?></li>
        </ul>	
    </div>
</div><!-- .breadcrumb-box -->
<section id="main" style="padding-top: 10px">
    <header class="page-header">
        <div class="container">
            <h6 class="title">
                 <?php if( $dil == "tr" ): ?>
                    <?php echo e(@explode(',',$fotogaleri->Adi)[0]); ?>

                <?php elseif( $dil == "en" ): ?>
                    <?php echo e(@explode(',',$fotogaleri->Adi)[1]); ?>

                <?php elseif( $dil == "es" ): ?>
                    <?php echo e(@explode(',',$fotogaleri->Adi)[2]); ?>

                <?php elseif( $dil == "ru" ): ?>
                    <?php echo e(@explode(',',$fotogaleri->Adi)[3]); ?>

                <?php elseif( $dil == "fr" ): ?>
                    <?php echo e(@explode(',',$fotogaleri->Adi)[4]); ?>

                <?php endif; ?>
            </h6>
        </div>	
    </header>

    <div class="container">
        <div class="row">
            <div class="content gallery col-sm-12 col-md-12">
                <div id="gallery-modern" class="row">
                    <div class="grid-sizer col-xs-12 col-sm-3 col-md-3"></div>
                    <?php foreach($fotograflar as $index => $element): ?>

                    <div class="images-box col-sm-3 col-md-3">    
                        
                        <a class="gallery-images" rel="fancybox" href="<?php echo e(url('images/uploads/FotoGaleri/'.$element->Resim)); ?>">
                            <img class="" src="<?php echo e(url('images/uploads/FotoGaleri/'.$element->Resim)); ?>" width="100%" height="197" alt="">
                                <span class="bg-images"><i class="fa fa-search"></i></span>
                        </a>
                    </div><!-- .images-box -->
                    <?php endforeach; ?>

                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <?php echo $fotograflar->render(); ?>

                    </div>
                </div>
            </div><!-- .content -->
        </div>
    </div><!-- .container -->
</section><!-- #main -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>