<?php $__env->startSection('title'); ?> <?php echo e($Title); ?> Listesi <?php $__env->stopSection(); ?>

<?php
    $url = Request::url();
    $link = str_replace(array('/Listele', '/Bekleyenler'), '', $url);
    use Illuminate\Support\Str;
?>

<?php $__env->startSection('content'); ?>
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading"><?php echo $__env->yieldContent('title'); ?>
        <?php if(Request::segment(2) == "Menu"): ?>
        <select id="table-filter" onchange="DilFiltrele(this)" class="form-control"  style=" width: 20%!important; margin-left:5%; display: inline!important;" >
            <option value="">Tüm Diller</option>
            <?php foreach($tumdiller as $dil): ?>
                <option><?php echo e($dil->UzunAd); ?></option>
            <?php endforeach; ?>
        </select>
        <?php endif; ?>
            <?php
            if ($EkleButonu) {
                echo '<div class="pull-right" style="margin:-5px;">
                        <a href="' . $link . '/Ekle" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Yeni Ekle</a>
                    </div>';
            }

            if ($SiralamaButonu) {
                echo '<div class="pull-right" style="margin-top:-5px;margin-right:10px;">
                        <a href="' . $link . '/Sirala/0" class="btn btn-danger btn-sm"><i class="fa fa-bars"></i> Sırala</a>
                </div>';
            }
            
            if ($GaleriSiralaButonu) {
                echo '<div class="pull-right" style="margin-top:-5px;margin-right:10px;">
                        <a href="' . $link . '/Sirala" class="btn btn-danger btn-sm"><i class="fa fa-bars"></i> Sırala</a>
                </div>';
            }

            if ($TumunuSilButonu) {
                echo '<div class="pull-right" style="margin-top:-5px;margin-right:10px;">
                        <a href="' . $link . '/TumunuSil" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Tümünü Sil</a>
                </div>';
            }
            ?>
        </div>
        <div class="panel-body">
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover" id="dataTables">
                    <thead>
                        <tr>
                            <th>id</th>
                            <?php
                            foreach ($ListelemeBasliklari as $value) {
                                echo '<th style="min-width:100px">' . $value . '</th>';
                            }
                            ?>
                            <th style="width:250px">İşlem</th>
                        </tr>
                    </thead>       
                        <tfoot>
                            <tr>
                                <th>id</th>
                                <?php
                                foreach ($ListelemeBasliklari as $value) {
                                    echo '<th>' . $value . '</th>';
                                }
                                ?>
                                <th>İşlem</th>
                            </tr>
                        </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<?php if($SilmeMesaji!=''): ?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Kapat</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo e($Title); ?> Silme İşlemi</h4>
            </div>
            <div class="modal-body">
                Silmek İstediğiniz <?php echo e($SilmeMesaji); ?> Bulunuyorsa Onlarda Silinecektir. <br>Devam etmek istediğinizden Emin misiniz?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Vazgeç</button>
                <a id="sillink" href="#" class="btn btn-danger">Sil</a>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('plugins/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('plugins/datatables-responsive/css/dataTables.responsive.css')); ?>" rel="stylesheet">
        <?php $__env->stopSection(); ?>

        <?php $__env->startSection('js'); ?>
        <script>
                    function uyariveri(id){
                    $('#sillink').attr('href', '<?php echo e($link); ?>/Sil/' + id);
                            $('#myModal').modal('show');
                    }
        </script>

        <?php if($KopyalaButonu): ?>
        <script type="text/javascript" src="<?php echo e(asset('plugins/ZeroClipboard/ZeroClipboard.js')); ?>"></script>
        <script type="text/javascript">
            function copy(){
                $('.kopyala').each(function(){
                    var clip = new ZeroClipboard(this);
                        clip.on("ready", function() {
                            this.on("aftercopy", function(event) {
                                new PNotify({
                                    title: '<?php echo e($Title); ?>',
                                    text: '<?php echo e($Title); ?> Linki Kopyalama İşleminiz Başarıyla Yapılmıştır.',
                                    type: 'success',
                                    styling: 'bootstrap3'
                                });
                                //  console.log("Copied text to clipboard: " + event.data["text/plain"]);
                            });
                        });
                        clip.on("error", function(event) {
                            new PNotify({
                            title: 'HATA',
                                text: '<?php echo e($Title); ?> Linki Kopyalanamadı.',
                                type: 'error',
                                styling: 'bootstrap3'
                            });
                            $(".demo-area").hide();
                            console.log('error[name="' + event.name + '"]: ' + event.message);
                            ZeroClipboard.destroy();
                        });
                });
            }
        </script>
        <?php endif; ?>

        <script src="<?php echo e(asset('plugins/datatables/media/js/jquery.dataTables.js')); ?>"></script>
        <script src="<?php echo e(asset('plugins/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')); ?>"></script>

        <script type="text/javascript">
                  
                var datatable = $('#dataTables').DataTable({

                    responsive: true,
                    "columnDefs": [
                        {
                            "targets": [0],
                            "visible": false,
                        }
                    ],
                    "order": [[ <?php echo e($ListelemeSirala); ?>, "desc" ]],
                    "processing": true,
                    "serverSide": true,

                    <?php if($KopyalaButonu): ?>
                    "fnDrawCallback":function(oSettings) {
                        copy();
                    },
                    <?php endif; ?>
                    "ajax": "<?php echo e($link); ?>/Getir",
                    "language": {
                        "lengthMenu": "Sayfada Gösterilecek Veri Adeti _MENU_",
                        "zeroRecords": "Arama Sonucu Bulunamadı.",
                        "info": "Toplam Kayıt Sayısı : _MAX_ (Gösterilen Sayfa : _PAGE_. Toplam Sayfa : _PAGES_)",
                        "infoEmpty": "Gösterilecek Veri Bulunamadı",
                        "infoFiltered": "(_MAX_ Kayıt Filtrelendi)",
                        "search": "Arama  ",
                        "oPaginate": {
                            "sNext": "İleri",
                            "sPrevious": "Geri"
                        }
                    },
                
                });

  
                if (typeof (copy) === 'function'){
                    copy();
                }

                function DilFiltrele(e) {
                    datatable.data().search($(e).val()).draw();
                }

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>