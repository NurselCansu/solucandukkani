<ul class="nav" id="side-menu">
	<li>
		<a href="<?php echo e(url('Admin')); ?>"><i class="fa fa-bar-chart-o fa-fw"></i> Anasayfa</a>
	</li>
	<?php if( in_array($admin->role, [0]) ): ?>
		<li <?php if(strpos(Request::url(),'Menu')!==false): ?> class="active" <?php endif; ?> >
			<a href="#"><i class="fa fa-bars fa-fw"></i> Menü Yönetimi<span class="fa arrow"></span></a>
			<ul class="nav nav-second-level">
				<li>
					<a href="<?php echo e(url('Admin/Menu/Ekle')); ?>"> Menü Ekle</a>
				</li>
				<li>
					<a href="<?php echo e(url('Admin/Menu/Listele')); ?>"> Menü Listele</a>
				</li>
				<li>
					<a href="<?php echo e(url('Admin/AltMenu/Ekle')); ?>"> Footer Menü Ekle</a>
				</li>
				<li>
					<a href="<?php echo e(url('Admin/AltMenu/Listele')); ?>"> Footer Menü Listele</a>
				</li>

			</ul>
		</li>
		<!-- <li <?php if(strpos(Request::url(),'Basvurular')!==false): ?> class="active" <?php endif; ?> >
			<a href="#"><i class="fa fa-users"></i> Başvuru Yönetimi<span class="fa arrow"></span></a>
			<ul class="nav nav-second-level">
				
				<li>
					<a href="<?php echo e(url('Admin/Basvurular/Listele')); ?>"> Başvuru Listele</a>
				</li>
				<li>
					<a href="<?php echo e(url('Admin/AltMenu/Ekle')); ?>"> Footer Menü Ekle</a>
				</li>
				<li>
					<a href="<?php echo e(url('Admin/AltMenu/Listele')); ?>"> Footer Menü Listele</a>
				</li>

			</ul>
		</li> -->


	
		
		<li <?php if(strpos(Request::url(),'Urun')!==false): ?> class="active" <?php endif; ?> >
			<a href="#"><i class="fa fa-cubes fa-fw"></i> Ürün Yönetimi<span class="fa arrow"></span></a>
			<ul class="nav nav-second-level">
				<li>
					<a href="<?php echo e(url('Admin/Kategori/Ekle')); ?>"> Kategori Ekle</a>
				</li>
				<li>
					<a href="<?php echo e(url('Admin/Kategori/Listele')); ?>"> Kategori Listele</a>
				</li>
				<li>
					<a href="<?php echo e(url('Admin/Urun/Ekle')); ?>"> Ürün Ekle</a>
				</li>
				<li>
					<a href="<?php echo e(url('Admin/Urun/Listele')); ?>"> Ürün Listele</a>
				</li>
				<li>
					<a href="<?php echo e(url('Admin/Tools/Ekle')); ?>"> Tools Ekle</a>
				</li>
				<li>
					<a href="<?php echo e(url('Admin/Tools/Listele')); ?>"> Tools Listele</a>
				</li>
			</ul>
		</li>
		<?php /* <li <?php if(strpos(Request::url(),'Haber')!==false): ?> class="active" <?php endif; ?> >
			<a href="#"><i class="fa fa-newspaper-o"></i> Haber Yönetimi<span class="fa arrow"></span></a>
			<ul class="nav nav-second-level">
				<li>
					<a href="<?php echo e(url('Admin/Haber/Ekle')); ?>"> Haber Ekle</a>
				</li>
				<li>
					<a href="<?php echo e(url('Admin/Haber/Listele')); ?>"> Haber Listele</a>
				</li>
			</ul>
		</li>
		<li <?php if(strpos(Request::url(),'Bayilik')!==false): ?> class="active" <?php endif; ?> >
			<a href="#"><i class="fa fa-cubes"></i> Bayilik Yönetimi<span class="fa arrow"></span></a>
			<ul class="nav nav-second-level">
				<li>
					<a href="<?php echo e(url('Admin/Bayilik/Ekle')); ?>"> Bayilik Ekle</a>
				</li>
				<li>
					<a href="<?php echo e(url('Admin/Bayilik/Listele')); ?>"> Bayilik Listele</a>
				</li>
			</ul>
		</li> */ ?>
		<li <?php if(strpos(Request::url(),'Slider')!==false): ?> class="active" <?php endif; ?> >
			<a href="#"><i class="fa fa-file-image-o fa-fw"></i> Slider Yönetimi<span class="fa arrow"></span></a>
			<ul class="nav nav-second-level">
				<li>
					<a href="<?php echo e(url('Admin/Slider/Ekle')); ?>"> Slider Ekle</a>
				</li>
				<li>
					<a href="<?php echo e(url('Admin/Slider/Listele')); ?>"> Slider Listele</a>
				</li>
			</ul>
		</li>
		<li <?php if(strpos(Request::url(),'FotoGaleri')!==false): ?> class="active" <?php endif; ?> >
			<a href="#"><i class="fa fa-camera-retro fa-fw"></i> FotoGaleri Yönetimi<span class="fa arrow"></span></a>
			<ul class="nav nav-second-level">
				<li>
					<a href="<?php echo e(url('Admin/FotoGaleriAyar/Ekle')); ?>"> Foto Galeri Sayfa Ayar Ekle</a>
				</li>
				
				<li>
					<a href="<?php echo e(url('Admin/FotoGaleriAyar/Listele')); ?>"> Foto Galeri Sayfa Ayar Listele</a>
				</li>
				<li>
					<a href="<?php echo e(url('Admin/FotoGaleri/Ekle')); ?>"> Foto Galeri Ekle</a>
				</li>
				<li>
					<a href="<?php echo e(url('Admin/FotoGaleri/Listele')); ?>"> Foto Galeri Listele</a>
				</li>
			</ul>
		</li>
		<?php /* <li <?php if(strpos(Request::url(),'VideoGaleri')!==false): ?> class="active" <?php endif; ?> >
			<a href="#"><i class="fa fa-youtube-play fa-fw"></i> Video Galeri Yönetimi<span class="fa arrow"></span></a>
			<ul class="nav nav-second-level">
				<li>
					<a href="<?php echo e(url('Admin/VideoGaleriAyar/Ekle')); ?>"> Video Galeri Sayfa Ayar Ekle</a>
				</li>
				
				<li>
					<a href="<?php echo e(url('Admin/VideoGaleriAyar/Listele')); ?>"> Video Galeri Sayfa Ayar Listele</a>
				</li>

				<li>
					<a href="<?php echo e(url('Admin/VideoGaleri/Ekle')); ?>"> Video Galeri Ekle</a>
				</li>
				<li>
					<a href="<?php echo e(url('Admin/VideoGaleri/Listele')); ?>"> Video Galeri Listele</a>
				</li>
			</ul>
		</li> */ ?>
		<li <?php if(strpos(Request::url(),'SosyalMedya')!==false): ?> class="active" <?php endif; ?> >
			<a href="#"><i class="fa fa-youtube-play fa-fw"></i> Sosyal Medya Yönetimi<span class="fa arrow"></span></a>
			<ul class="nav nav-second-level">
				<li>
					<a href="<?php echo e(url('Admin/SosyalMedya/Ekle')); ?>"> Sosyal Medya Ekle</a>
				</li>
				<li>
					<a href="<?php echo e(url('Admin/SosyalMedya/Listele')); ?>"> Sosyal Medya Listele</a>
				</li>
			</ul>
		</li>
		
		<!-- <li <?php if(strpos(Request::url(),'IletisimBilgileri')!==false || strpos(Request::url(),'IletisimIstekleri')!==false): ?> class="active" <?php endif; ?> >
			<a href="#"><i class="fa fa-users"></i> Ekibimiz Yönetimi<span class="fa arrow"></span></a>
			<ul class="nav nav-second-level">

				<li>
					<a href="<?php echo e(url('Admin/Ekibimiz/Ekle')); ?>"> Ekip Üyesi Ekle</a>
				</li>
				<li>
					<a href="<?php echo e(url('Admin/Ekibimiz/Listele')); ?>"> Ekip Üyesi Listele</a>
				</li>
			</ul>
		</li> -->

		<li <?php if(strpos(Request::url(),'IletisimBilgileri')!==false || strpos(Request::url(),'IletisimIstekleri')!==false): ?> class="active" <?php endif; ?> >
			<a href="#"><i class="fa fa-map-marker fa-fw"></i> İletişim Yönetimi<span class="fa arrow"></span></a>
			<ul class="nav nav-second-level">
				<!-- <li>
					<a href="<?php echo e(url('Admin/IletisimIstekleri/Listele')); ?>"> İletişim İstekleri</a>
				</li> -->
				<li>
					<a href="<?php echo e(url('Admin/IletisimBilgileri/Ekle')); ?>"> Şube Ekle</a>
				</li>
				<li>
					<a href="<?php echo e(url('Admin/IletisimBilgileri/Listele')); ?>"> Şube Listele</a>
				</li>
			</ul>
		</li>
		<li <?php if(strpos(Request::url(),'Yonetici')!==false || strpos(Request::url(),'Kullanici')!==false): ?> class="active" <?php endif; ?> >
			<a href="#"><i class="fa fa-users fa-fw"></i> Kullanıcı Yönetimi<span class="fa arrow"></span></a>
			<ul class="nav nav-second-level">
				<li>
					<a href="<?php echo e(url('Admin/Yonetici/Ekle')); ?>"> Yönetici Ekle</a>
				</li>
				<li>
					<a href="<?php echo e(url('Admin/Yonetici/Listele')); ?>"> Yönetici Listele</a>
				</li>
			</ul>
		</li>
		<!-- <?php if( in_array($admin->role, [0, 1]) ): ?>
		<li <?php if(strpos(Request::url(),'InsanKaynaklari')!==false || strpos(Request::url(),'InsanKaynaklari')!==false): ?> class="active" <?php endif; ?> >
			<a href="#"><i class="glyphicon glyphicon-user fa-fw"></i> Başvuru Formları <span class="fa arrow"></span></a>
			<ul class="nav nav-second-level">
				<li>
					<a href="<?php echo e(url('Admin/InsanKaynaklari/Listele')); ?>"> Başvuru Formları Listele</a>
				</li>
			</ul>
		</li>
		<?php endif; ?>
                <li <?php if(strpos(Request::url(),'OperatorKart')!==false || strpos(Request::url(),'OperatorKart')!==false): ?> class="active" <?php endif; ?> >
			<a href="#"><i class="fa fa-credit-card fa-fw"></i> Operator Kart <span class="fa arrow"></span></a>
			<ul class="nav nav-second-level">
				<li>
					<a href="<?php echo e(url('Admin/OperatorKart/Listele')); ?>">  Operator Kayıtları Listele</a>
				</li>
				<li>
					<a href="<?php echo e(url('Admin/OperatorArayanFirma/Listele')); ?>">  Operator Arayan Firmaları Listele</a>
				</li>
			</ul>
		</li> -->
		<li <?php if(strpos(Request::url(),'Popup')!==false || strpos(Request::url(),'Popup')!==false): ?> class="active" <?php endif; ?> >
			<a href="#"><i class="fa fa-comments fa-fw"></i> Popup Yönetimi<span class="fa arrow"></span></a>
			<ul class="nav nav-second-level">
			
				</li>
				<li>
					<a href="<?php echo e(url('Admin/Popup/Ekle')); ?>"> Popup Ayar Ekle</a>
				</li>
				<li>
					<a href="<?php echo e(url('Admin/Popup/Listele')); ?>"> Popup Ayar Listele</a>
				</li>
                        
			</ul>
		</li>
		
		<li <?php if(strpos(Request::url(),'Ayarlar')!==false || strpos(Request::url(),'Kullanici')!==false): ?> class="active" <?php endif; ?> >
			<a href="#"><i class="fa fa-cogs fa-fw"></i> Ayar Yönetimi<span class="fa arrow"></span></a>
			<ul class="nav nav-second-level">
				<li>
					<a href="<?php echo e(url('Admin/Keywords/Ekle')); ?>"> Keywords Ekle</a>
				</li>
				<li>
					<a href="<?php echo e(url('Admin/Keywords/Listele')); ?>"> Keywords Listele</a>
				</li>
				<li>
					<a href="<?php echo e(url('Admin/Ayarlar/Ekle')); ?>"> Ayar Ekle</a>
				</li>
				<li>
					<a href="<?php echo e(url('Admin/Ayarlar/Listele')); ?>"> Ayar Listele</a>
				</li>
				<li>
					<a href="#"><i class="fa fa-cog fa-fw"></i> Genel Ayarlar<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
					   
						<li style="margin-left:8%">
							<a href="<?php echo e(url('Admin/Captcha/Ekle')); ?>"><i class="fa fa-check-square-o" aria-hidden="true"> ReCaptcha Ekle</i></a>
						</li>
						<li style="margin-left:8%">
							<a href="<?php echo e(url('Admin/Captcha/Listele')); ?>"><i class="fa fa-check-square-o" aria-hidden="true"> ReCaptcha Görüntüle</i></a>
						</li>
						<li style="margin-left:8%">
							<a href="<?php echo e(url('Admin/Google/Ekle')); ?>"><i class="fa fa-google" aria-hidden="true">  Google Site Doğrulama Kodu Ekle</i></a>
						</li>
						<li style="margin-left:8%">
							<a href="<?php echo e(url('Admin/Google/Listele')); ?>"><i class="fa fa-google" aria-hidden="true">  Google Site Doğrulama Kodu Görüntüle</i></a>
						</li>
						<li style="margin-left:8%">
							<a href="<?php echo e(url('Admin/GoogleAnalytics/Ekle')); ?>"><i class="fa fa-google" aria-hidden="true">  Google Analytics Kodu Ekle</i></a>
						</li>
						<li style="margin-left:8%">
							<a href="<?php echo e(url('Admin/GoogleAnalytics/Listele')); ?>"> <i class="fa fa-google" aria-hidden="true"> Google Analytics Kodu Görüntüle</i></a>
						</li>
					</ul>
				</li>
				<li <?php if(strpos(Request::url(),'Mail')!==false || strpos(Request::url(),'Mail')!==false): ?> class="active" <?php endif; ?> >
					<a href="#"><i class="fa fa-envelope fa-fw"></i> E-posta Ayarları<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
					
						</li>
						<li style="margin-left:8%">
							<a href="<?php echo e(url('Admin/MailKullanici/Ekle')); ?>">İletişim Maili Gönderilecek Ekle</a>
						</li>
						<li style="margin-left:8%">
							<a href="<?php echo e(url('Admin/MailKullanici/Listele')); ?>">İletişim Maili Gönderilecek Listele</a>
						</li>
						<li style="margin-left:8%">
							<a href="<?php echo e(url('Admin/Mail/Ekle')); ?>"> E-posta Ayar Ekle</a>
						</li>
						<li style="margin-left:8%">
							<a href="<?php echo e(url('Admin/Mail/Listele')); ?>"> E-posta Ayar Listele</a>
						</li>
		                        
					</ul>
				</li>
				<li <?php if(strpos(Request::url(),'Dil')!==false || strpos(Request::url(),'DilSabiti')!==false): ?> class="active" <?php endif; ?> >
				<a href="#"><i class="fa fa-language fa-fw"></i> Dil Yönetimi<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						<li style="margin-left: 8%">
							<a href="<?php echo e(url('Admin/Dil/Ekle')); ?>"> Dil Ekle</a>
						</li>
						<li style="margin-left: 8%">
							<a href="<?php echo e(url('Admin/Dil/Listele')); ?>"> Dil Listele</a>
						</li>
						<li style="margin-left: 8%">
							<a href="<?php echo e(url('Admin/DilSabiti/Ekle')); ?>"> Dil Sabiti Ekle</a>
						</li>
						<li style="margin-left: 8%">
							<a href="<?php echo e(url('Admin/DilSabiti/Listele')); ?>"> Dil Sabiti Listele</a>
						</li>
					</ul>
				</li>
                          
                            
			</ul>
		</li>

		
                

	<?php endif; ?>


</ul>