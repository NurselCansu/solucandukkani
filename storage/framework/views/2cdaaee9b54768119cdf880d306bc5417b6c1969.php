
<?php $__env->startSection('baslik'); ?><?php echo e($Title); ?><?php $__env->stopSection(); ?>
<?php $__env->startSection('form'); ?>
<input type="hidden" name="trid" value="<?php echo e(@$trveri->id); ?>">

<div class="form-group">
    <label for="Fotograf" class="col-sm-2 control-label">Video Galeri Sayfa Fotoğrafı</label>
    <div class="col-sm-10">
      <input type="file" class="form-control" name="Fotograf[]" id="Fotograf"  placeholder="Fotograf">
    </div>
    <?php if( @$veri->Fotograf ): ?>
        <div class="col-md-10 col-md-offset-2" >
            <a href="<?php echo e(url('images/uploads/FotoGaleri/fsayfa/'.@$veri->Fotograf )); ?>">
                <div class="checkbox">
                    <img class="replace-2x" src="<?php echo e(url('images/uploads/FotoGaleri/fsayfa/'.@$veri->Fotograf)); ?>" width="150" height="150" alt="">
                </div>
            </a>
        </div>
    <?php endif; ?>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>