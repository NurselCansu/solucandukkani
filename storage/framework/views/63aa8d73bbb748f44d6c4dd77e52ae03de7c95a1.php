<?php echo $ayarlar->Analytics; ?>

<!--[if (!IE)|(gt IE 8)]><!-->


<?php if( urldecode(request::segment(3)) != \App\Http\Fnk::Ceviri('katalog-url') ): ?>

<script src="<?php echo e(asset('js/jquery-3.0.0.min.js')); ?>"></script>

<?php endif; ?>
<!-- Vendor -->
        <script src="<?php echo e(asset('vendor/jquery/jquery.min.js')); ?>"></script>
        <script src="<?php echo e(asset('vendor/jquery.appear/jquery.appear.min.js')); ?>"></script>
        <script src="<?php echo e(asset('vendor/jquery.easing/jquery.easing.min.js')); ?>"></script>
        <script src="<?php echo e(asset('vendor/jquery-cookie/jquery-cookie.min.js')); ?>"></script>

        <script src="<?php echo e(asset('vendor/bootstrap/js/bootstrap.min.js')); ?>"></script>
        <script src="<?php echo e(asset('vendor/common/common.min.js')); ?>"></script>
        <script src="<?php echo e(asset('vendor/jquery.validation/jquery.validation.min.js')); ?>"></script>
        <script src="<?php echo e(asset('vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js')); ?>"></script>
        <script src="<?php echo e(asset('vendor/jquery.gmap/jquery.gmap.min.js')); ?>"></script>
        <script src="<?php echo e(asset('vendor/jquery.lazyload/jquery.lazyload.min.js')); ?>"></script>
        <script src="<?php echo e(asset('vendor/isotope/jquery.isotope.min.js')); ?>"></script>
        <script src="<?php echo e(asset('vendor/magnific-popup/jquery.magnific-popup.min.js')); ?>"></script>
        <script src="<?php echo e(asset('vendor/vide/vide.min.js')); ?>"></script>
       
        
        <!--Owl-Carousel için-->
        <script src="<?php echo e(asset('/plugins/owl.carousel/assets/js/bootstrap-collapse.js')); ?>"></script>
        <script src="<?php echo e(asset('/plugins/owl.carousel/assets/js/bootstrap-transition.js')); ?>"></script>
        <script src="<?php echo e(asset('/plugins/owl.carousel/assets/js/bootstrap-tab.js')); ?>"></script>
        <script src="<?php echo e(asset('/plugins/owl.carousel/owl-carousel/owl.carousel.js')); ?>"></script>

        <!-- Theme Base, Components and Settings -->
        <script src="<?php echo e(asset('js/theme.js')); ?>"></script>
        
        <!-- Current Page Vendor and Views -->
        <script src="<?php echo e(asset('vendor/rs-plugin/js/jquery.themepunch.tools.min.js')); ?>"></script>
        <script src="<?php echo e(asset('vendor/rs-plugin/js/jquery.themepunch.revolution.min.js')); ?>"></script>
        <script src="<?php echo e(asset('vendor/circle-flip-slideshow/js/jquery.flipshow.min.js')); ?>"></script>
        <script src="<?php echo e(asset('js/views/view.home.js')); ?>"></script>
        
        <!-- Theme Custom -->
        <script src="<?php echo e(asset('js/custom.js')); ?>"></script>

        <!-- Current Page Vendor and Views -->
        <script src="<?php echo e(asset('vendor/circle-flip-slideshow/js/jquery.flipshow.min.js')); ?>"></script>
        <script src="<?php echo e(asset('vendor/nivo-slider/jquery.nivo.slider.min.js')); ?>"></script>
        <script src="<?php echo e(asset('js/views/view.home.js')); ?>"></script>

        
        <!-- Theme Initialization Files -->
        <script src="<?php echo e(asset('js/theme.init.js')); ?>"></script>

        <!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        
            ga('create', 'UA-12345678-1', 'auto');
            ga('send', 'pageview');
        </script>
         -->

<script type="text/javascript">
    
 

 
  var time = 7; // time in seconds
 
  var $progressBar,
      $bar, 
      $elem, 
      isPause, 
      tick,
      percentTime;
 
    //Init the carousel
    $("#owl-demo").owlCarousel({
      slideSpeed : 500,
      paginationSpeed : 500,
      singleItem : true,
      afterInit : progressBar,
      afterMove : moved,
      startDragging : pauseOnDragging
    });
 
    //Init progressBar where elem is $("#owl-demo")
    function progressBar(elem){
      $elem = elem;
      //build progress bar elements
      buildProgressBar();
      //start counting
      start();
    }
 
    //create div#progressBar and div#bar then prepend to $("#owl-demo")
    function buildProgressBar(){
      $progressBar = $("<div>",{
        id:"progressBar"
      });
      $bar = $("<div>",{
        id:"bar"
      });
      $progressBar.append($bar).prependTo($elem);
    }
 
    function start() {
      //reset timer
      percentTime = 0;
      isPause = false;
      //run interval every 0.01 second
      tick = setInterval(interval, 10);
    };
 
    function interval() {
      if(isPause === false){
        percentTime += 1 / time;
        $bar.css({
           width: percentTime+"%"
         });
        //if percentTime is equal or greater than 100
        if(percentTime >= 100){
          //slide to next item 
          $elem.trigger('owl.next')
        }
      }
    }
 
    //pause while dragging 
    function pauseOnDragging(){
      isPause = true;
    }
 
    //moved callback
    function moved(){
      //clear interval
      clearTimeout(tick);
      //start again
      start();
    }
 
    //uncomment this to make pause on mouseover 
    // $elem.on('mouseover',function(){
    //   isPause = true;
    // })
    // $elem.on('mouseout',function(){
    //   isPause = false;
    // })
 


///////////////////////////////////////////////////////////

        
     $(".owl-ref").owlCarousel({

            autoPlayTimeout: 2000,
            item: 2,
            autoPlay: true

      });

    


</script>
<script>
/*	$("#search").autocomplete({
    source: "<?php echo e(url($dil.'/Ara/')); ?>",
    minLength: 2,
    select: function (event, ui) {
        window.location = ui.item.slug;
    }
});
$("#searchproduct").autocomplete({
    source: "<?php echo e(url($dil.'/Ara/')); ?>",
    minLength: 2,
    select: function (event, ui) {
        window.location = ui.item.slug;
    }
});*/


$(document).ready(function () {

        setTimeout(function () {
            $("#renklilogo").css("display", "block");
        }, 2000);
        $("#portakallogo").hover(function () {
            $("#renklilogo").css("display", "none");
            $("#siyahlogo").css("display", "block");
        }, function () {
            $("#renklilogo").css("display", "block");
            $("#siyahlogo").css("display", "none");
        });

        if ($.browser.safari)
        {
            $("#renklilogo").css("display", "none");
            $("#siyahlogo").css("display", "none");
        }


    });
    var is_safari = navigator.userAgent.indexOf('Safari') != -1 || navigator.userAgent.indexOf('Chrome') != -1 && navigator.userAgent.indexOf('Android') == -1;
    if (is_safari)
    {
        var portakalhtml = $("#portakallogo").html();
        $("#portakallogo").hide();
        $("#plogo").html(portakalhtml);
        $("#plogo").css("position", "absolute");
        $("#plogo").css("margin-right", "-60px");
        $("#plogo").css("right", "50%");
        $("#plogo").css("margin-left", "-60px");
        $("#plogo").css("tect-align", "center");
        $("#plogo").css("background", "white");
        $("#plogo").css("border-top-width", "0px");
        $("#plogo > #renklilogo").css("display", "block");
        $("#plogo > #siyahlogo").css("display", "none");
    }

    

    $('.urunler-menu ul li.active').parents('li').addClass('active');

	
</script>




