<?php $__env->startSection('title', 'Yönetici Listesi'); ?>

<?php $__env->startSection('content'); ?>
<?php
$url = Request::url();
$link = str_replace(array ('/Listele', '/Bekleyenler'), '', $url);
$alanadlari = ['Adı', 'E-Posta Adresi'];
$degeradlari = ['name', 'email'];
?>
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <?php echo $__env->yieldContent('title'); ?>
            <div class="pull-right" style="margin:-5px;">
                <a href="<?php echo e($link); ?>/Ekle" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Yeni Ekle</a>
            </div>
        </div>
        <div class="panel-body">
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover" id="dataTables">
                    <thead>
                        <tr>
                            <?php
                            foreach ($alanadlari as $value){
                                echo '<th style="min-width:100px">' . $value . '</th>';
                            }
                            ?>
                            <th style="width:250px">İşlem</th>
                        </tr>
                    </thead>       
                    <tbody>
                        <?php foreach($veriler as $veri): ?>
                            <tr>
                                <?php foreach($degeradlari as $value): ?>
                                    <?php if($value=='Resim'): ?>
                                        <td><img src="<?php echo e(url('images/uploads')); ?>/<?php echo e($veri->$value); ?>" class="img-rounded" height="50"></td>
                                    <?php else: ?>
                                        <td><?php echo e($veri->$value); ?></td>
                                    <?php endif; ?>
                                <?php endforeach; ?>

                                <td style="text-align: center">
                                    <a class="btn btn-default btn-sm" href="<?php echo e($link); ?>/Detay/<?php echo e($veri->id); ?>">Detay</a>
                                    <a href="<?php echo e($link); ?>/Duzenle/<?php echo e($veri->id); ?>" class="btn btn-sm btn-info">Düzenle</a>
                                    <a href="<?php echo e($link); ?>/Sil/<?php echo e($veri->id); ?>" class="btn btn-sm btn-danger">Sil</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <?php
                                foreach ($alanadlari as $value){
                                    echo '<th>' . $value . '</th>';
                                }
                                ?>
                                <th>İşlem</th>
                            </tr>
                        </tfoot>
                </table>
            </div>

        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('plugins/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('plugins/datatables-responsive/css/dataTables.responsive.css')); ?>" rel="stylesheet">
        <?php $__env->stopSection(); ?>

        <?php $__env->startSection('js'); ?>
        <script src="<?php echo e(asset('plugins/datatables/media/js/jquery.dataTables.min.js')); ?>"></script>
        <script src="<?php echo e(asset('plugins/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')); ?>"></script>
        <script type="text/javascript">
                                                                                                                                            $(document).ready(function() {
                                                                                                                                    $('#dataTables').DataTable({
                                                                                                                                    responsive: true,
                                                                                                                                            "language": {
                                                                                                                                            "lengthMenu": "Sayfada Gösterilecek Veri Adeti _MENU_",
                                                                                                                                                    "zeroRecords": "Arama Sonucu Bulunamadı.",
                                                                                                                                                    "info": "Gösterilen Sayfa : _PAGE_. Toplam Sayfa : _PAGES_",
                                                                                                                                                    "infoEmpty": "Gösterilecek Veri Bulunamadı",
                                                                                                                                                    "infoFiltered": "(_MAX_ Kayıt Filtrelendi)",
                                                                                                                                                    "search": "Arama : ",
                                                                                                                                                    "oPaginate": {
                                                                                                                                                    "sNext": "İleri",
                                                                                                                                                            "sPrevious": "Geri"
                                                                                                                                                    }
                                                                                                                                            }
                                                                                                                                    });
                                                                                                                                    });
        </script>

        <?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>