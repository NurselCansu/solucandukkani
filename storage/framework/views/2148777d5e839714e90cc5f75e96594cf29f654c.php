<?php $__env->startSection('Title',$menu->MetaTitle); ?>
<?php $__env->startSection('MetaTag'); ?>
    <?php echo @implode(', ', json_decode(@$ayarlar->MetaTag ,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES)); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('MetaDescription',$menu->MetaDescription); ?>

<?php $__env->startSection('content'); ?>
<a href="<?php echo e(url($dil)); ?>" class="pull-right " style=" margin-top: -20px;"><i style="color:#91120f;padding-right: 20px;
" class="fa fa-close fa-2x" aria-hidden="true"></i></a>
<div role="main" class="main" >
<div class="container">
<div class="col-md-12 ">
<h1 style="margin-left: 30%;font-size: 30px;color: #880e0a;"><?php echo e($menu->Adi); ?></h1></br>
<div class="col-md-12" > 
<?php if(@$urunler): ?>
<?php foreach($urunler as $key => $urun): ?>

<?php if($key == 0): ?>
    <div class="col-md-6">
        <div class="sari_daire" >
        <span>
            <?php echo e($urun->daire_yazi); ?>

        </span>
        </div>
        <div class="sari_dikdortgen">
        <span>
            <?php echo e($urun->diktortgen_yazi); ?>

        </span>
            </br></br>
            <span>
            <?php echo e($urun->diktortgen_yazi2); ?>

        </span>
        </div>  

        <div class="sari_daire_alt_yazi">
        <span>
            <?php echo e($urun->daire_alti_yazi); ?>

        </span>
        </div>
    </div>

<?php elseif($key == 1): ?>

    <div class="col-md-3">
        <div class="sari_daireler ilk">
        <span>
            <?php echo e($urun->daire_yazi); ?>

        </span>
        </div>      

        <div class="sari_daireler_alt_yazi ilk">
        <span>
            <?php echo e($urun->daire_alti_yazi); ?>

        </span>
        </div>
    </div>
    <div class="col-md-12" >
   

<?php elseif($key%3 == 1): ?>
    <div class="col-md-3">
        <div class="sari_daireler">
        <span>
            <?php echo e($urun->daire_yazi); ?>

        </span>
        </div>      

        <div class="sari_daireler_alt_yazi">
        <span>
            <?php echo e($urun->daire_alti_yazi); ?>

        </span>
        </div>
    </div>
    </div>
<?php elseif($key%3 == 2): ?>
    <div class="col-md-12" >
     <div class="col-md-3">
        <div class="sari_daireler">
        <span>
            <?php echo e($urun->daire_yazi); ?>

        </span>
        </div>      

        <div class="sari_daireler_alt_yazi">
        <span>
            <?php echo e($urun->daire_alti_yazi); ?>

        </span>
        </div>
    </div>
<?php else: ?>
     <div class="col-md-3">
        <div class="sari_daireler">
        <span>
            <?php echo e($urun->daire_yazi); ?>

        </span>
        </div>      

        <div class="sari_daireler_alt_yazi">
        <span>
            <?php echo e($urun->daire_alti_yazi); ?>

        </span>
        </div>
    </div>
<?php endif; ?>



<?php endforeach; ?>
<?php endif; ?>
</div>
</div>
</div>
</div>
</div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script>
    $(document).ready(function() {
        $('.sari_daireler span').each(function(index, el) {
            var yazi_uzunluk = $(this).text().length;
            var span_height = $(this).height();
            var div_height = $(this).parent().height();
            $(this).parent().css('padding-top',(div_height - span_height) / 1.5);
            if(yazi_uzunluk > 58){
                $(this).css({
                    'font-size' : '22px',
                    'padding-top' : (div_height - span_height + 10),
                });
            }

        });
        $('.sari_daire span').each(function(index, el) {
            var yazi_uzunluk = $(this).text().length;
            var span_height = $(this).height();
            var div_height = $(this).parent().height();
            $(this).parent().css('padding-top',(div_height - span_height) / 1.5);
            if(yazi_uzunluk > 58){
                $(this).css({
                    'font-size' : '22px',
                    'padding-top' : (div_height - span_height + 10),
                });
            }

        });
        $('.sari_dikdortgen span').each(function(index, el) {
            var yazi_uzunluk = $(this).text().length;
            var span_height = $(this).height();
            var div_height = $(this).parent().height();
            $(this).parent().css('padding-top',(div_height - span_height) / 1.5);
            if(yazi_uzunluk > 58){
                $(this).css({
                    'font-size' : '22px',
                    'padding-top' : (div_height - span_height + 10),
                });
            }

        });

       
    });
</script>
<?php $__env->stopSection(); ?>




<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>