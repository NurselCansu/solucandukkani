<?php $__env->startSection('baslik', 'Ayarlar'); ?>

<?php $__env->startSection('form'); ?>

<?php
use App\Http\Models\Keywords;

$keywords = Keywords::where('is_active',1)->get();

?>

<div class="form-group">
    <label for="FirmaAdi" class="col-sm-2 control-label">Firma Adı</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->FirmaAdi); ?>" name="FirmaAdi" id="FirmaAdi" placeholder="Firma Adı">
    </div>
</div>
<div class="form-group">
    <label for="Logo" class="col-sm-2 control-label">Logo</label>
    <div class="col-sm-10">
        <img src="<?php echo e(url('images/uploads/'.$ayarlar->Logo)); ?>" style="max-height: 70px;margin-bottom: 10px;" id="anlikresim" class="img-rounded" />
      <input type="file" class="form-control" name="Logo" id="Logo" placeholder="Logo">
    </div>
</div>


<!-- <div class="form-group">
    <label for="is_Kampanyali_Urunler" class="col-sm-2 control-label">Anasayfada Gösterilecek Ürün Tipi</label>
    <div class="col-sm-10">
        <select class="form-control" id="is_Kampanyali_Urunler" required name="is_Kampanyali_Urunler">
            <option <?php if(@$veri->is_Kampanyali_Urunler == 0 ): ?> selected <?php endif; ?> value="0" >Başlıca Ürünleri Göster</option>
            <option <?php if(@$veri->is_Kampanyali_Urunler == 1 ): ?> selected <?php endif; ?> value="1" >Kampanyalı Ürünleri Göster</option>
        </select>
    </div>
</div>
 -->

<div class="form-group">
    <label for="Adi" class="col-sm-2 control-label">Ana Sayfa Yazısı Başlık</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->AnaSayfaBaslik); ?>" name="AnaSayfaBaslik" id="Adi" placeholder="Ana Sayfa Yazısı Başlığı">
    </div>
</div>
<div class="form-group">
    <label  class="col-sm-2 control-label">Dönüşüm Metni 1</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="AnaSayfaIcerikSutun1" value="<?php echo e(@$veri->AnaSayfaIcerikSutun1); ?>"></textarea>
    </div>
</div>
<div class="form-group">
    <label  class="col-sm-2 control-label">Dönüşüm Metni 2</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="AnaSayfaIcerikSutun2" value="<?php echo e(@$veri->AnaSayfaIcerikSutun2); ?>" ></textarea>
    </div>
</div>
<div class="form-group">
    <label  class="col-sm-2 control-label">Dönüşüm Metni Alt Metin</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="AnaSayfaIcerikSutun3" value="<?php echo e(@$veri->AnaSayfaIcerikSutun3); ?>"></textarea>
    </div>
</div>
<div class="form-group">
    <label  class="col-sm-2 control-label">Ana Sayfa Alt İçerik Sütun 1</label>
    <div class="col-sm-10">
        <textarea rows="3" type="text" name="AnaSayfaAltIcerikSutun1" class="form-control"><?php echo e(@$veri->AnaSayfaAltIcerikSutun1); ?></textarea>
    </div>
</div>
<div class="form-group">
    <label  class="col-sm-2 control-label">Ana Sayfa Alt İçerik Sütun 2</label>
    <div class="col-sm-10">
        <textarea rows="3" type="text" name="AnaSayfaAltIcerikSutun2" class="form-control"><?php echo e(@$veri->AnaSayfaAltIcerikSutun2); ?></textarea>
    </div>
</div>
<div class="form-group">
    <label  class="col-sm-2 control-label">Ana Sayfa Alt İçerik Sütun 3</label>
    <div class="col-sm-10">
        <textarea rows="3" type="text" name="AnaSayfaAltIcerikSutun3" class="form-control"><?php echo e(@$veri->AnaSayfaAltIcerikSutun3); ?></textarea>
    </div>
</div>


<!-- <div class="form-group">
    <label  class="col-sm-2 control-label">Ana Sayfa Alt Slider Başlık </label>
    <div class="col-sm-10">
        <textarea name="AnaSayfaAltSliderBaslik" class="ck"><?php echo e(@$veri->AnaSayfaAltSliderBaslik); ?></textarea>
    </div>
</div>
<div class="form-group">
    <label  class="col-sm-2 control-label">Ana Sayfa Alt Slider Sütun 1</label>
    <div class="col-sm-10">
        <textarea name="AnaSayfaAltSliderSutun1" class="ck"><?php echo e(@$veri->AnaSayfaAltSliderSutun1); ?></textarea>
    </div>
</div>
<div class="form-group">
    <label  class="col-sm-2 control-label">Ana Sayfa Alt Slider Sütun 2</label>
    <div class="col-sm-10">
        <textarea name="AnaSayfaAltSliderSutun2" class="ck"><?php echo e(@$veri->AnaSayfaAltSliderSutun2); ?></textarea>
    </div>
</div>
<div class="form-group">
    <label  class="col-sm-2 control-label">Ana Sayfa Alt Slider Sütun 3</label>
    <div class="col-sm-10">
        <textarea name="AnaSayfaAltSliderSutun3" class="ck"><?php echo e(@$veri->AnaSayfaAltSliderSutun3); ?></textarea>
    </div>
</div> -->

<div class="form-group">
    <label class="col-sm-2 control-label">Meta Alanı Seçim</label>
    <div class="col-sm-10">
        <select class="form-control" id="Secim" name="Secim">
            <option value="0">Var olan Metadan Çek</option>
            <option value="1">Kendi metamı gireyim</option>
        </select>
    </div>
</div>
<div class="form-group key1" style="display: none" >
    <label class="col-sm-2 control-label">Meta Keywords Çek</label>
    <div class="col-sm-10">
        <select type="text" class="form-control" style="width: 100%" required multiple value="<?php echo e(@$keywords->Keywords); ?>" name="MetaTag1[]" id="MetaTag2"  placeholder="Meta Keywords">
            <?php if(!empty($keywords)): ?>
                <?php foreach($keywords as $key=>$value): ?>
                   <option value="<?php echo e($value->id); ?>"><?php echo e($value->Adi); ?></option>
                <?php endforeach; ?>
            <?php endif; ?>
        </select>
    </div>
</div>
<div class="form-group key2" style="display: none" >
    <label class="col-sm-2 control-label">Meta Keywords</label>
    <div class="col-sm-10">
        <select type="text" class="form-control" style="width: 100%" required multiple value="<?php echo e(@$veri->MetaTag); ?>" name="MetaTag[]" id="MetaTag" placeholder="Meta Keywords">
            <?php if(!empty(@$veri->MetaTag)): ?>
                <?php foreach(json_decode(@$veri->MetaTag) as $tag): ?>
                   <option value="<?php echo e($tag); ?>" selected="selected"><?php echo e($tag); ?></option>
                <?php endforeach; ?>
            <?php endif; ?>    
        </select>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Meta Title</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->MetaTitle); ?>" name="MetaTitle" id="MetaDescription" placeholder="Meta Description">
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Meta Description</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->MetaDescription); ?>" name="MetaDescription" id="MetaDescription" placeholder="Meta Description">
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<script type="text/javascript">
    jQuery(document).ready(function($) {

        $('#MetaTag').select2({
            tags: true,
            multiple: true,
            tokenSeparators: [',',' '],
            
        });
        $('#MetaTag2').select2({
            multiple: true,
        });
        $('#Secim').on('change', function() {
            var secim = parseInt(this.value);
            
            if(secim == 0){
                $('.key1').show('slow');
                $('.key2').hide('slow');
            }
            else{
                $('.key2').show('slow');
                $('.key1').hide('slow');
            }
        });

        
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>