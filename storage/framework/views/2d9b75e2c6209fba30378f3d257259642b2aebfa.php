<?php $__env->startSection('Title',$menu->MetaTitle); ?>
<?php $__env->startSection('MetaTag',$menu->MetaTag); ?>
<?php $__env->startSection('MetaDescription',$menu->MetaDescription); ?>

<?php $__env->startSection('content'); ?>

<section class="page-header page-header-light page-header-reverse page-header-more-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb breadcrumb-valign-mid">
                    <li><a href="<?php echo e(url('/'.$dil)); ?>"><?php echo e(\App\Http\Fnk::Ceviri('anasayfa')); ?></a></li>
                    <li class="active"><?php echo e(\App\Http\Fnk::Ceviri($menu->Slug)); ?></li>
                </ul>
                <h1><?php echo e(\App\Http\Fnk::Ceviri($menu->Slug)); ?></h1>
            </div>
        </div>
    </div>
</section>

<div class="clearfix"></div>
<div class="container">
    <div  class="row">
        <form name="search-form" class="col-sm-12 col-md-6 col-md-offset-4 search-form">
            <input class="search-string form-control ui-autocomplete-input" type="search" placeholder="<?php echo e(\App\Http\Fnk::Ceviri('hizli-urun-arama')); ?>" name="search-string" id="searchproduct" autocomplete="off">
        </form>
    </div>
</div>

<div class="clearfix"></div>    
<br/>          
<div class="clearfix"></div>           
        <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                           <div id="sidebar" class="sidebar col-sm-12 col-md-12">
                                <aside class="widget menu">
                                        <header>
                                            <h3 class="heading-primary"><?php echo e(\App\Http\Fnk::Ceviri('urun-kategorileri')); ?></h3>
                                        </header>
                                        
                                            <?php echo \App\Http\Fnk::Kategoriler(); ?>

                                        
                                </aside><!-- .menu-->
                            </div><!-- #sidebar -->
                        </div>
            <div class="col-md-9">
                <?php if(!empty($kategori->Icerik)): ?>
                    <div  class="urun urun-aciklama col-sm-12 col-md-12 col-xs-12">
                        <?php echo $kategori->Icerik; ?>

                    </div>
                <?php endif; ?>
<div class="clearfix"></div>
<div class="clearfix"></div>
            <?php foreach($urunler as $element): ?>
                <div  class="urun col-sm-4 col-md-4 col-xs-6">
                    <a href="<?php echo e(url($dil.'/'.\App\Http\Fnk::Ceviri('urun-url').'/'.$element->Slug)); ?>">
                        <span class="thumb-info">
                                <span class="thumb-info-wrapper">
                                    <img src="<?php echo e(url('/images/uploads/Urun/resim/'.json_decode($element->Resim)[0])); ?>" class="img-responsive" alt="" style="height: 250px; width:150% ; padding: 20px" >
                                    <span class="thumb-info-title">
                                        <span class="thumb-info-inner"><?php echo e($element->Adi); ?></span>
                                        <span class="thumb-info-type">İncele</span>
                                    </span>
                                    <span class="thumb-info-action">
                                        <span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
                                    </span>
                                </span>
                            </span>
                        </a> 
                    </div>
                    <?php endforeach; ?>
                </div>
            <div style="float: right;">
                <?php echo $urunler->render(); ?>

            </div>
        </div>
    </div>
</div>
</div><!-- .container -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>

<script type="text/javascript">
    
    $(document).ready(function() {
        var i=0;

        $('.nav ul li a').click(function(event){

            if(i%2==0){

                if ($(this).next('ul.sub').children().length !== 0) {     
                event.preventDefault();
                }
                $(this).siblings('.sub').show('slow');

            }

            else{

                if ($(this).next('ul.sub').children().length !== 0) {     
                event.preventDefault();
                }
                $(this).siblings('.sub').hide('slow');

            }

            i++;

        });

         

        
        
        
        

    });

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>