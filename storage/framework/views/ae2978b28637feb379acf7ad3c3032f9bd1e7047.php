<?php $__env->startSection('baslik', 'Sosyal Medya'); ?>

<?php $__env->startSection('form'); ?>
<div class="form-group">
    <label for="Adi" class="col-sm-2 control-label">Sosyal Medya Seçiniz</label>
    <div class="col-sm-10">
    <select name="Adi" class="form-control" id="Adi" required>
        <option value="">Sosyal Medya Seçiniz</option>
        <option value="facebook" <?php if(@$veri->Adi=='facebook'): ?> selected <?php endif; ?>>Facebook</option>
        <option value="twitter" <?php if(@$veri->Adi=='twitter'): ?> selected <?php endif; ?>>Twitter</option>
        <option value="youtube-play" <?php if(@$veri->Adi=='youtube'): ?> selected <?php endif; ?>>Youtube</option>
        <option value="google" <?php if(@$veri->Adi=='google'): ?> selected <?php endif; ?>>Google+</option>
        <option value="instagram" <?php if(@$veri->Adi=='instagram'): ?> selected <?php endif; ?>>İnstagram</option>
        <option value="linkedin" <?php if(@$veri->Adi=='linkedin'): ?> selected <?php endif; ?>>Linkedin</option>
        <option value="foursquare" <?php if(@$veri->Adi=='foursquare'): ?> selected <?php endif; ?>>Foursquare</option>
    </select>
    </div>
</div>
<div class="form-group">
    <label for="Link" class="col-sm-2 control-label">Link</label>
    <div class="col-sm-10">
        <input type="text" class="form-control"  value="<?php echo e(@$veri->Link); ?>" name="Link" placeholder=" Linki">
    </div>
</div>
<div class="form-group">
    <label for="LinkAcilisTipi" class="col-sm-2 control-label">Link Açılış Tipi</label>
    <div class="col-sm-10">
        <div class="radio">
            <label>
                <input type="radio" name="LinkAcilisTipi" value="" <?php if(isset($veri) && $veri->LinkAcilisTipi==''): ?> checked <?php endif; ?> <?php if(!isset($veri)): ?> checked <?php endif; ?> >Aynı Pencere</label>
        </div>
        <div class="radio">
            <label><input type="radio" name="LinkAcilisTipi" <?php if(isset($veri) && $veri->LinkAcilisTipi=='target="_blank"'): ?> checked <?php endif; ?> value='target="_blank"'>Yeni Pencere</label>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>