<?php $__env->startSection('baslik','Ürün'); ?>
<?php $__env->startSection('form'); ?>


<?php 
use App\Http\Models\Menu as MN; 
$menu = MN::where('is_active',1)->where('MenuTipi', 'modul')->where('Link', 'LIKE','/tr/urunler-url%')->get(); 
 
 ?> 

<?php $__env->startSection('form'); ?>
<div class="form-group">
    <label for="Urun_UstKat" class="col-sm-2 control-label">Üst Kategori</label>
    <div class="col-sm-10">
        <select class="form-control" id="Urun_UstKat" required name="Urun_UstKat">
            <option value="">Üst Kategori Seçiniz</option>
            <?php foreach(@$menu as $td): ?>
                <option value="<?php echo e($td->id); ?>" <?php if(@$td->id == @$veri->Urun_UstKat): ?> selected <?php endif; ?>><?php echo e($td->Adi); ?></option>
            <?php endforeach; ?>
        </select>
    </div>
</div>


<div class="form-group">

    <label class="col-md-2 control-label">Ürün Şekli Daire + Diktörtgen</label>
    <div class="col-md-10">
        <div class="checkbox">
            <label>
                <input type="checkbox" id="Tip" name="Tip"  <?php if(@$veri->Tip == 1): ?> checked value="1" <?php else: ?> value="0" <?php endif; ?> >Daire ve Diktörtgen olsun
            </label>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Daire İçi Yazı</label>
    <div class="col-sm-10">
        <input type="text" value="<?php echo e(@$veri->daire_yazi); ?>" class="form-control" name="daire_yazi">
    </div>
</div>

<div class="form-group diktortgen" style="display: none">
    <label class="col-sm-2 control-label">Dikdörtgen İçi Yazı 1</label>
    <div class="col-sm-10">
        <input type="text" value="<?php echo e(@$veri->diktortgen_yazi); ?>" class="form-control" name="diktortgen_yazi">
    </div>
</div>

<div class="form-group diktortgen" style="display: none">
    <label class="col-sm-2 control-label">Dikdörtgen İçi Yazı 2</label>
    <div class="col-sm-10">
        <input type="text" value="<?php echo e(@$veri->diktortgen_yazi2); ?>" class="form-control" name="diktortgen_yazi2">
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Daire Altı Yazı</label>
    <div class="col-sm-10">
        <input type="text" value="<?php echo e(@$veri->daire_alti_yazi); ?>" class="form-control" name="daire_alti_yazi">
    </div>
</div>



<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<script type="text/javascript">
    
jQuery(document).ready(function($) {
       $('#Tip').change(function() {
            if($(this).is(":checked")) {
               $(this).val("1");
               $('.diktortgen').show("slow");
            }
            else{
               $(this).val("0");
               $('.diktortgen').hide("slow");
            }
        });

    
});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>