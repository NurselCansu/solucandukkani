

<?php $__env->startSection('Title',$ayarlar->MetaTitle); ?>
<?php $__env->startSection('MetaTag',$ayarlar->MetaTag); ?>
<?php $__env->startSection('MetaDescription',$ayarlar->MetaDescription); ?>

<?php $__env->startSection('content'); ?>

<div class="breadcrumb-box">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="/<?php echo e($dil); ?>/"><?php echo e(\App\Http\Fnk::Ceviri('anasayfa')); ?></a> </li>
            <li class="active"><?php echo e(\App\Http\Fnk::Ceviri('operator-kart')); ?></li>
        </ul>	
    </div>
</div><!-- .breadcrumb-box -->
<section id="main" style="background-image:url('<?php echo e(url('images/bluroperator.jpg')); ?>') ; background-size: cover;">
    <div class="container">
        <article class="content">
            <div class="row">

                <div class="col-sm-6 col-md-6">
                    <div class="form-content" id="1" >
                        <form class="row form-box login-form form-validator"  method="POST" action="<?php echo e(url($dil.'/'.\App\Http\Fnk::Ceviri('operator-kart-url-'.$ref))); ?>">
                            <input type='hidden' name='_token' value='<?php echo e(csrf_token()); ?>' ></input>
                            
                           <h3 class="title text-center"><div id="operatork">
                                <?php echo e(\App\Http\Fnk::Ceviri('operator-kart')); ?></div></h3>

                            <div class=" col-md-12 form-group">
                                <div class=" col-md-6 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('adiniz-soyadiniz')); ?><span class="required">*</span></label>
                                    <input class="form-control" type="text" name="Adi" required>
                                </div>

                                <div class="col-md-6 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('makine-marka-model')); ?></label>
                                    <input class="form-control" type="text" name="MakineMarka" >
                                </div>    
                            </div>
                            
                            <div class="col-md-12">
                                <div class="col-md-6 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('telefon-numaraniz')); ?><span class="required">*</span></label>
                                    <input class="form-control" type="phone" name="Telefon" required>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('calisma-alani')); ?><span class="required">*</span></label>
                                    <select id="yer_alti_mi" name="yer_alti_mi" class="form-control" >
                                        <option value="0" <?php if($ref == 0): ?> selected <?php endif; ?>><?php echo e(\App\Http\Fnk::Ceviri('yer-alti')); ?></option>
                                        <option value="1" <?php if($ref == 1): ?> selected <?php endif; ?>><?php echo e(\App\Http\Fnk::Ceviri('yer-ustu')); ?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-6 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('e-posta-adresiniz')); ?><span ></span></label>
                                    <input class="form-control" type="mail" name="Eposta" >
                                </div>
                                <div class="col-md-6 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('simdiki-firma')); ?></label>
                                    <input class="form-control" type="text" name="SimdikiFirma" id="SimdikiFirma" >
                                </div>

                            </div>

                            <div class="col-md-12">
                                
                                <div class="col-md-6 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('ikamet-adresiniz')); ?></label>
                                    <input class="form-control" type="text" name="IkametAdresi" >
                                </div>
                                <div class="col-md-6 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('onceki-firma')); ?></label>
                                    <input class="form-control" type="text" name="OncekiFirma" id="OncekiFirma" >
                                </div>
                            </div>                       

                            <div class="col-md-12">
                                
                                <div class="col-md-6 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('Operator-is-ilani')); ?></label>
                                    <input class="form-control" type="text" name="CalismaDurumu" id="CalismaDurumu" >
                                    <!-- <select id="CalismaDurumu" name="CalismaDurumu" class="form-control" >
                                        <option>Seçiniz</option>
                                        <option value="1" ><?php echo e(\App\Http\Fnk::Ceviri('calisiyor')); ?></option>
                                        <option value="0" ><?php echo e(\App\Http\Fnk::Ceviri('calismiyor')); ?></option>
                                    </select> -->
                                </div>
                                <div class="col-md-6 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('elbise-ayakkabi-beden')); ?></label>
                                    <input class="form-control" type="text" name="Beden" >
                                </div>
                            </div>    

                            <div class="col-md-12 form-group" style="padding-left:28px;padding-right:28px">
                                <label><?php echo e(\App\Http\Fnk::Ceviri('yedek-parca-talep')); ?></label>
                                <textarea class="form-control" rows="2" type="text" name="YedekParcaTalep" ></textarea>
                                <label><p><small><em><?php echo e(\App\Http\Fnk::Ceviri('Not-Adresinize-hediye')); ?></em></small></p></label>
                            </div>
                            
                            <div class="col-md-12 form-group">
                                <div class="col-sm-8 ">
                                    <?php echo app('captcha')->display();; ?>

                                </div>
                                <div class="col-md-4 ">
                                    <button type="submit" class=" btn btn-default"><?php echo e(\App\Http\Fnk::Ceviri('gonder')); ?></button>
                                </div>
                            </div>    
                        </form><!-- .form-box -->
                    </div>
                </div>

                <div class="col-sm-6 col-md-6">
                    <div class="form-content row form-box" id="2">
                        <div class="logo" >
                            <a href="<?php echo e(url($dil)); ?>">
                                <img id="l" src="<?php echo e(url('/images/uploads/'.$ayarlar->Logo)); ?>" class="logo-img" alt="">
                            </a>
                        </div>

                        <p class="lead">
                            <?php echo e(\App\Http\Fnk::Ceviri('operator-kart-aciklama')); ?>

                        </p>
                        <p class="lead">   
                            <?php echo e(\App\Http\Fnk::Ceviri('operator-kart-aciklama2')); ?>

                        </p>
                        <p class="lead">
                            <?php echo e(\App\Http\Fnk::Ceviri('operator-kart-aciklama3')); ?>

                        </p>
                        <p class="lead">
                            <?php echo e(\App\Http\Fnk::Ceviri('operator-kart-aciklama4')); ?>                                   
                        </p>
                        <p></p>
                    </div>
                </div>
            </div>
        </article><!-- .content -->
    </div>
</section><!-- #main -->

<style type="text/css">
[class*="frame-shadow"] {
    background: #f2f2f2;
}
   .form-group {
        margin-bottom: -5px;
    }
    #operatork{
        border-bottom-width: 3px;
        border-bottom-style: solid; 
        border-bottom-color: #00aeef; 
        margin-left: 25%;
        margin-right: 25%;
    }

    @media  only screen and (max-width: 600px){
         #operatork{
        width: 80%!important;
        margin-left: 10%;
        margin-right: 10%;
        }
    }
  

    
     @media  only screen and (min-width: 600px max-width: 1100px){
         #operatork{
        width: 100%!important;
        margin-left: 0;
        margin-right: 0;
    }

    }
    /* 
     .form-content>div#test{
        min-height: 600px;
        padding-bottom: 83px;
        background-color:#f2f2f2;
    }*/
   /* .selectBox, .form-control{
        background: rgba(255, 255, 255, 0.63);
    }*/
</style>

<script>
    $("#CalismaDurumu").change(function(){
        var calismadurumu = $("#CalismaDurumu").val();
        if(calismadurumu == 1){
            $("#SimdikiFirma").prop('required',true);
            $("#OncekiFirma").prop('required',false);

        }else if(calismadurumu == 0){
            $("#OncekiFirma").prop('required',true);
            $("#SimdikiFirma").prop('required',false);
        }
    });

    $( document ).ready(function() {
        $('#form').validate({
            ignore: ':hidden:not([class~=selectized]),:hidden > .selectized, .selectize-control .selectize-input input',
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block validateerror',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
             submitHandler: function(form) {
                return true;
             }
        });
        
        var bar = $('#uploadbar');
        $('#form').ajaxForm({
            url:'<?php echo e(url($dil.'/'.\App\Http\Fnk::Ceviri('operator-kart-url-'.$ref))); ?>',
            beforeSend: function() {
                bar.parent('div').show();
                var percentVal = '0%';
                bar.width(percentVal);
                bar.html(percentVal);
                bar.attr('aria-valuenow',0);
                $('#btn').attr('disabled',true);
            },
            uploadProgress: function(event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                bar.attr('aria-valuenow',percentComplete);
                bar.width(percentVal);
                bar.html(percentVal);
            },
            success: function() {
                var percentVal = '100%';
                bar.attr('aria-valuenow',100);
                bar.width(percentVal);
                bar.html(percentVal);
                $('#btn').removeAttr('disabled');
            },
            complete: function(xhr) {
         
                var sonuc = JSON.parse(xhr.responseText);
                console.log(sonuc);
                if(sonuc.islem){
                    
                    alert("İşleminiz Başarıyla Yapılmıştır.");
                    
                    if (typeof(submitafter) === "function") {
                        submitafter(sonuc);
                     }
                    
                    $("#form")[0].reset();
                    
                    if($('.selectize').length>0){
                        $( ".selectize" ).each(function( index ) {
                            if(index%3==0)this.selectize.clear()
                        });
                    }               
                //  jQuery("a[data-toggle=collapse][href=#adim1]").click();
                }else{
                    $.each(sonuc.error, function(index, value) {
                       new PNotify({
                            title: 'HATA',
                            text: value,
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    });
                    $('#btn').removeAttr('disabled');
                }
                jQuery(document).ready(function () {
                   setTimeout(uploadbartemizle(),1000 );
                });
            }
        });
    });
</script>

<!-- <script type="text/javascript">
    

    $(document).ready(function(){


        if($(window).height()<600){

            $('#2').css('height','auto!important');
            
        }
        else{

        var y=$('#1').height();
        console.log(h);

        $('#2').css('height',y+'px!important');
        }

        


    });
</script> -->


<?php $__env->stopSection(); ?>

<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>