<?php $__env->startSection('baslik', 'Ürün'); ?>

<?php 
use App\Http\Models\Menu as MN; 
$menu = MN::where('is_active',1)->where('MenuTipi', 'modul')->where('Link', 'LIKE','/tr/urunler%')->get(); 
 
 ?> 

<?php $__env->startSection('form'); ?>
<div class="form-group">
    <label for="Urun_UstKat" class="col-sm-2 control-label">Üst Kategori</label>
    <div class="col-sm-10">
        <select class="form-control" id="Urun_UstKat" required name="Urun_UstKat">
            <option value="">Üst Kategori Seçiniz</option>
            <?php foreach(@$menu as $td): ?>
                <option value="<?php echo e($td->id); ?>" <?php if(@$td->id == @$veri->Urun_UstKat): ?> selected <?php endif; ?>><?php echo e($td->Adi); ?></option>
            <?php endforeach; ?>
        </select>
    </div>
</div>

<div class="form-group">
    <label for="Adi" class="col-sm-2 control-label">Ürün Adı</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->Adi); ?>" name="Adi" id="Adi" placeholder="Ürün Adı">
    </div>
</div>
<div class="form-group">
    <label for="SeriNo" class="col-sm-2 control-label">Seri No</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->SeriNo); ?>" name="SeriNo" id="SeriNo" placeholder="Seri Numarası">
    </div>
</div>
<div class="form-group">
    <label for="UreticiSeriNo" class="col-sm-2 control-label">Üretici Seri No</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->UreticiSeriNo); ?>" name="UreticiSeriNo" id="UreticiSeriNo" placeholder="Üretici Seri Numarası">
    </div>
</div>

<div class="form-group">
    <label for="UreticiSeriNo" class="col-sm-2 control-label">Delkom Seri No</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->DelkomNo); ?>" name="DelkomNo" id="DelkomNo" placeholder="Delkom Seri Numarası">
    </div>
</div>
<!-- Drifter sayfası tabanca yedek parçaları gösterimi için hazırlandı-->
<div class="form-group">
    <label class="col-md-2 control-label">Ürün Gösterimi </label>
    <div class="col-md-10">
        <div class="checkbox">
            <label>
                <input type="checkbox" id="Urun_Gosterimi" name="Urun_Gosterimi"  <?php if(@$veri->Urun_Gosterimi == 1): ?> checked <?php endif; ?> >Altında ürünler olacak 
            </label>
        </div>
    </div>
</div>
<div class="form-group gidilecek" <?php if( @$veri->Urun_Gosterimi == 0): ?> style="display:none" <?php endif; ?>>
    <label for="GidilecekKategori" class="col-sm-2 control-label">Hangi kategoriye gidecek</label>
    <div class="col-sm-10">
        <select class="form-control" id="GidilecekKategori"  name="GidilecekKategori" >
            <option value="">Gidilecek Kategoriyi Seçiniz</option>
            <?php foreach(@$kategori as $td): ?>
                <option value="<?php echo e($td->id); ?>" <?php if( @$td->id == @$veri->GidilecekKategori ): ?> selected <?php endif; ?>><?php echo e($td->adi); ?></option>
            <?php endforeach; ?>
        </select>
    </div>
</div>
<div class="form-group">
    <label for="KategoriId" class="col-sm-2 control-label">Kategori</label>
    <div class="col-sm-10">
        <select class="form-control" id="KategoriId" multiple required name="KategoriId[]">
            <option value="">Kategori Seçiniz</option>
            <?php foreach(@$kategori as $td): ?>
                <option value="<?php echo e($td->id); ?>" <?php if( \App\Http\Models\UrunKategori::where('urun_id', @$veri->id)->where('kategori_id', $td->id)->first() ): ?> selected <?php endif; ?>><?php echo e($td->adi); ?></option>
            <?php endforeach; ?>
        </select>
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label">Ürün Öne Çıkarılsın</label>
    <div class="col-md-10">
        <div class="checkbox">
            <label>
                <input type="checkbox" id="OneCikarilmis" name="OneCikarilmis" <?php if(@$veri->OneCikarilmis == 1): ?> checked <?php endif; ?> >Ürün Anasayfada İlk Satırda Öne Çıkarılsın
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" id="OneCikarilmis2" name="OneCikarilmis2" <?php if(@$veri->OneCikarilmis2 == 1): ?> checked <?php endif; ?> >Ürün Anasayfada İkinci Satırda Öne Çıkarılsın
            </label>
        </div>
    </div>
</div>


<div class="form-group">
    <label class="col-md-2 control-label">Kampanyalı Ürün</label>
    <div class="col-md-10">
        <div class="checkbox">
            <label>
                <input type="checkbox" id="Kampanya" name="Kampanya"  <?php if(@$veri->Kampanya == 1): ?> checked <?php endif; ?> >Ürün Kampanyalı Ürünler Sliderında Gösterilsin
            </label>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-md-2 control-label">Ürünler Sayfasında </label>
    <div class="col-md-10">
        <div class="checkbox">
            <label>
                <input type="checkbox" id="UrunGizli" name="UrunGizli"  <?php if(@$veri->UrunGizli == 1): ?> checked <?php endif; ?> >Gösterilmesin
            </label>
        </div>
    </div>
</div>


<div class="form-group">
    <label for="Resim" class="col-sm-2 control-label">Resim</label>
    <div class="col-sm-10">
      <input type="file" class="form-control" name="Resim[]" id="Resim"  placeholder="Resim">
    </div>
    <?php if( @$veri->Resim ): ?>
        <div class="col-md-10 col-md-offset-2" >
            <a href="<?php echo e(url('images/uploads/Urun/resim/'.json_decode(@$veri->Resim)[0] )); ?>">
                <div class="checkbox">
                    <img class="replace-2x" src="<?php echo e(url('images/uploads/Urun/resim/'.json_decode(@$veri->Resim)[0])); ?>" width="150" height="150" alt="">
                </div>
            </a>
        </div>
    <?php endif; ?>
</div>
<?php /* <div class="form-group">
    <label for="Resim360" class="col-sm-2 control-label">360 Resim</label>
    <div class="col-sm-10">
      <input type="file" class="form-control" name="Resim360[]" id="Resim360"  multiple placeholder="360 Derece Resim360lerini Seçiniz" >
    </div>
</div> */ ?>
<div class="form-group">
    <label for="KisaAciklama" class="col-sm-2 control-label">Kısa Açıklama</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="KisaAciklama" id="KisaAciklama" >
    </div>
</div>
 <?php if(isset($Dil) && $Dil): ?>
            <div class="form-group">
                <label for="DilYazi" class="col-sm-2 control-label">Yazı Dili</label>
                <div class="col-sm-10">
                    <select class="form-control" id="DilYazi"  onchange="DilDegistir()">
                        <option value="">Lütfen Dil Seçiniz</option>
                        <?php foreach($tumdiller as $td): ?>
                            <option <?php if($Dil && (@$veri->DilId == $td->id)): ?> selected <?php endif; ?> value="<?php echo e($td->id); ?>"><?php echo e($td->UzunAd); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
<?php endif; ?>
<div class="form-group" id="T" <?php if(!isset($veri) || $veri->DilId!='1'): ?> style="display:none" <?php endif; ?> >
    <label for="Icerik" class="col-sm-2 control-label">Türkçe İçerik</label>
    <div class="col-sm-10">
        <textarea name="Icerik" class="ck"><?php echo e(@$veri->Icerik); ?></textarea>
    </div>
</div>
<div class="form-group" id="I" <?php if(!isset($veri) || $veri->DilId!='2'): ?> style="display:none" <?php endif; ?> >
    <label for="Icerik2" class="col-sm-2 control-label">İngilizce İçerik</label>
    <div class="col-sm-10">
        <textarea name="Icerik2" class="ck"><?php echo e(@$veri->Icerik2); ?></textarea>
    </div>
</div>
<div class="form-group" id="Is" <?php if(!isset($veri) || $veri->DilId!='3'): ?> style="display:none" <?php endif; ?> >
    <label for="Icerik3" class="col-sm-2 control-label">İspanyolca İçerik</label>
    <div class="col-sm-10">
        <textarea name="Icerik3" class="ck"><?php echo e(@$veri->Icerik3); ?></textarea>
    </div>
</div>
<div class="form-group" id="R" <?php if(!isset($veri) || $veri->DilId!='4'): ?> style="display:none" <?php endif; ?> >
    <label for="Icerik4" class="col-sm-2 control-label">Rusça İçerik</label>
    <div class="col-sm-10">
        <textarea name="Icerik4" class="ck"><?php echo e(@$veri->Icerik4); ?></textarea>
    </div>
</div>
<div class="form-group" id="F" <?php if(!isset($veri) || $veri->DilId!='5'): ?> style="display:none" <?php endif; ?> >
    <label for="Icerik5" class="col-sm-2 control-label">Fransızca İçerik</label>
    <div class="col-sm-10">
        <textarea name="Icerik5" class="ck"><?php echo e(@$veri->Icerik5); ?></textarea>
    </div>
</div>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('select>option:eq(1)').attr('selected', true);
        $('.gizli').hide();

         //Yedek parça checkbox durum kontrol
        
        $('#Urun_Gosterimi').change(function() {
            if($(this).is(":checked")) {
               $('.gidilecek').show('slow');
               $('#GidilecekKategori').addClass('required');
            }
            else{
                $('.gidilecek').hide('slow');
                $('#GidilecekKategori').removeClass('required');
            }
        });

    });
    function DilDegistir() {
        var DilTip = $('#DilYazi').val();

        if (DilTip == "1") {//Türkçe
            $('#T').show('slow');
            $('#I').hide('slow');
            $('#Is').hide('slow');
            $('#R').hide('slow');
            $('#F').hide('slow');
        } 
        else if (DilTip == "2") {//İngilizce
            $('#T').hide('slow');
            $('#I').show('slow');
            $('#Is').hide('slow');
            $('#R').hide('slow');
            $('#F').hide('slow');
           
        }
        else if (DilTip == "3") {//ispanyolca
            $('#T').hide('slow');
            $('#I').hide('slow');
            $('#Is').show('slow');
            $('#R').hide('slow');
            $('#F').hide('slow');
            
        }
        else if (DilTip == "4") {//Rusça
            $('#T').hide('slow');
            $('#I').hide('slow');
            $('#Is').hide('slow');
            $('#R').show('slow');
            $('#F').hide('slow');
            
        }
         else if (DilTip == "5") {//Fransızca
            $('#T').hide('slow');
            $('#I').hide('slow');
            $('#Is').hide('slow');
            $('#R').hide('slow');
            $('#F').show('slow');
            
        }
    }

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>