<?php $__env->startSection('baslik'); ?><?php echo e($Title); ?><?php $__env->stopSection(); ?>
<?php $__env->startSection('form'); ?>
<input type="hidden" name="id" value="<?php echo e(@$veri->id); ?>">
<div class="form-group">
	<label for="UstKatId" class="col-sm-2 control-label">Üst Menü</label>
	<div class="col-sm-10">
		<select class="form-control" id="UstKatId" required name="UstKatId">
			<option value="0">Üst Menüsü Yok</option>
				<?php
					Fnk::ChildrenOption($menuler,@$veri->UstKatId,@$veri->id,'Adi',0,false);
				?>
		</select>
	</div>
</div>
<div class="form-group">
    <label for="Adi" class="col-sm-2 control-label">Menü Adı</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->Adi); ?>" name="Adi" id="Adi" placeholder="Menü Adı">
    </div>
</div>
<div class="form-group">
    <label for="MenuTipi" class="col-sm-2 control-label">Menü Tipi</label>
    <div class="col-sm-10">
        <select name="MenuTipi" class="form-control" required="" onchange="MenuTipiDegistir()" id="MenuTipi">
            <option value="">Lütfen Seçiniz</option>
            <option <?php if(isset($veri) && $veri->MenuTipi=='icerik'): ?> selected <?php endif; ?> value="icerik">İçerik</option>
            <option <?php if(isset($veri) && $veri->MenuTipi=='link'): ?> selected <?php endif; ?> value="link">Link</option>
            <option <?php if(isset($veri) && $veri->MenuTipi=='modul'): ?> selected <?php endif; ?> value="modul">Modül</option>
        </select>
    </div>
</div>
<div class="form-group">
    <label for="LinkAcilisTipi" class="col-sm-2 control-label">Link Açılış Tipi</label>
    <div class="col-sm-10">
        <div class="radio">
            <label>
                <input type="radio" name="LinkAcilisTipi" value="" <?php if(isset($veri) && $veri->LinkAcilisTipi==''): ?> checked <?php endif; ?> <?php if(!isset($veri)): ?> checked <?php endif; ?> >Aynı Pencere</label>
    	</div>
	    <div class="radio">
	        <label><input type="radio" name="LinkAcilisTipi" <?php if(isset($veri) && $veri->LinkAcilisTipi=='target="_blank"'): ?> checked <?php endif; ?> value='target="_blank"'>Yeni Pencere</label>
		</div>
	</div>
</div>
<div class="form-group" id="Linkalani" <?php if(!isset($veri) || $veri->MenuTipi!='link'): ?> style="display:none" <?php endif; ?> >
	<label for="Link" class="col-sm-2 control-label">Menü Linki</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="Link" name="Link" value="<?php echo e(@$veri->Link); ?>">
    </div>
</div>
<div class="form-group" id="Icerikalani" <?php if(!isset($veri) || $veri->MenuTipi!='icerik'): ?> style="display:none" <?php endif; ?> >
     <label for="Icerik" class="col-sm-2 control-label">Menü İçeriği</label>
    <div class="col-sm-10">
        <textarea required id="Icerik" name="Icerik" class="ck"><?php echo e(@$veri->Icerik); ?></textarea>
    </div>
</div>
<div class="form-group" id="Modulalani" <?php if(!isset($veri) || $veri->MenuTipi!='modul'): ?> style="display:none" <?php endif; ?> >
     <label for="Modul" class="col-sm-2 control-label">Modül</label>
    <div class="col-sm-10">
        <select name="Modul" class="form-control" id="Modul" required>
            <option value="">Lütfen Modül Seçiniz</option>
            <option value="iletisim" <?php if(@$veri->Link==url('iletisim')): ?> selected <?php endif; ?>>İletişim Modülü</option>
        </select>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script type="text/javascript">
    function MenuTipiDegistir() {
        var MenuTip = $('#MenuTipi').val();
        if (MenuTip == "link") {
            $('#Linkalani').show('slow');
            $('#Icerikalani').hide('slow');
            $('#Modulalani').hide('slow');
        } else if (MenuTip == "icerik") {
            $('#Icerikalani').show('slow');
            $('#Linkalani').hide('slow');
            $('#Modulalani').hide('slow');
        } else if (MenuTip == "modul") {
            $('#Modulalani').show('slow');
            $('#Icerikalani').hide('slow');
            $('#Linkalani').hide('slow');
        }
    }

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>