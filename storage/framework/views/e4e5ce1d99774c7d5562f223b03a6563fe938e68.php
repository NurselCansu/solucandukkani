<?php $__env->startSection('baslik', 'Tawk.To'); ?>

<?php $__env->startSection('form'); ?>

<div class="form-group">
    <label for="Script" class="col-sm-2 control-label">Script</label>
    <div class="col-sm-10">
        <textarea type="text" class="form-control" required  name="Script" id="Script" placeholder="Script"><?php echo e(@$veri->Script); ?></textarea>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>

<script type="text/javascript">
    
    jQuery(document).ready(function($) {
        $('.gizli').hide();
        $('.is_active').hide();
    });

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>