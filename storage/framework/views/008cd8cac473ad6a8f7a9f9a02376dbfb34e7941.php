<?php $__env->startSection('Title',$haber->MetaTitle); ?>
<?php $__env->startSection('MetaTag',$haber->MetaTag); ?>
<?php $__env->startSection('MetaDescription',$haber->MetaDescription); ?>

<?php $__env->startSection('content'); ?>


<div role="main" class="main">
<section class="page-header page-header-light page-header-reverse page-header-more-padding">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb breadcrumb-valign-mid">
					<li><a href="<?php echo e(url('/'.$dil)); ?>"><?php echo e(\App\Http\Fnk::Ceviri('anasayfa')); ?></a></li>
					<li class="active"><?php echo e($haber->Adi); ?></li>
				</ul>
				<h1><?php echo e($haber->Adi); ?></h1>
			</div>
		</div>
	</div>
</section>
	<div class="row center mt-xl">
            	<h3 class="heading-primary"><?php echo e($haber->Adi); ?></h3>
            	<div class="divider divider-primary">
					<i class="fa fa-chevron-down"></i>
				</div>
	</div>			


	<div class="container">
		<div class="row "> 

		<div class="col-md-5">
			 <img alt="" class="img-responsive img-rounded" src="<?php echo e(asset('images/uploads/Haberler/'.$haber->Resim)); ?>">

		</div>
		<div class="col-md-7">
			<?php echo $haber->Icerik; ?>

		</div>	
		</div>	
	</div>

</div>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>