
<?php $__env->startSection('header'); ?>

   <header id="header" class="header-narrow header-semi-transparent" data-plugin-options='{"stickyEnabled": true, "stickyEnableOnBoxed": true, "stickyEnableOnMobile": true, "stickyStartAt": 1, "stickySetTop": "0"}'>


                <div class="header-body">
                    <div class="header-container container">
                        <div class="header-row">
                            <div class="header-column">
                                <div class="header-logo">
                                   <a href="<?php echo e(url('tr')); ?>">
                                        <img alt="Porto"  height="39" src="<?php echo e(url('/images/uploads/'.$ayarlar->Logo)); ?>">
                                    </a>
                                </div>
                            </div>
                            <div class="header-column">
                                <div class="header-row">
                                    <div class="header-nav header-nav-stripe">
                                        <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">
                                            <i class="fa fa-bars"></i>
                                        </button>
                                        

                                        <div class="header-nav-main header-nav-main-square header-nav-main-effect-2 header-nav-main-sub-effect-1 collapse">
                                            <nav>
                                                <ul class="nav nav-pills" id="mainNav">
                                                    <?php echo Fnk::Menuler(); ?>

                                                    <?php foreach($tumdiller as $element): ?>
                                                        <?php if($tumdiller->count() > 1 ): ?>
                                                         <li class="dropdown-full-color dropdown-primary " style="margin-left : -10px!important"><a href="<?php echo e(url($element->KisaAd.'/cevir/'.@$segment2.'/'.@$segment3.'/'.@$segment4.'/'.@$segment5)); ?>"><img class="x" style="height: 25px;border-radius: 100%" src="<?php echo e(url('/flags/flags/'.$element->Resim)); ?>"></img></a></li>
                                                         <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </ul>  
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>



<?php $__env->stopSection(); ?>
