
<?php $__env->startSection('baslik'); ?><?php echo e($Title); ?><?php $__env->stopSection(); ?>
<?php $__env->startSection('form'); ?>
<input type="hidden" name="trid" value="<?php echo e(@$trveri->id); ?>">
<div class="form-group">

    <label for="Adi" class="col-sm-2 control-label">Foto Galeri Adı</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->Adi); ?>" name="Adi" id="Adi" placeholder="Türkçe, İngilizce, İspanyolca, Rusça, Fransızca galeri adları olucak şekilde yazınız ">
    </div>
</div>

<div class="form-group">

    <label for="KapakFotografi" class="col-sm-2 control-label">Galeri Kapak Fotoğrafı</label>
    <div class="col-sm-10">
        <input type="file" class="form-control" name="KapakFotografi[]" id="KapakFotografi"  placeholder="KapakFotografi">
    </div>
    <?php if( @$veri->KapakFotografi ): ?>
        <div class="col-md-10 col-md-offset-2" >
            <a href="<?php echo e(url('images/uploads/FotoGaleri/kapak/'.@$veri->KapakFotografi )); ?>">
                <div class="checkbox">
                    <img class="replace-2x" src="<?php echo e(url('images/uploads/FotoGaleri/kapak/'.@$veri->KapakFotografi)); ?>" width="150" height="150" alt="">
                </div>
            </a>
        </div>
    <?php endif; ?>

</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>