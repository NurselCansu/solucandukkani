<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Yönetim Paneli">
    <meta name="author" content="Portakal Yazılım - www.portakalyazilim.com.tr">
    
    <title>Admin - <?php echo $__env->yieldContent('title'); ?></title>

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo e(asset('plugins/bootstrap/dist/css/bootstrap.min.css')); ?>" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo e(asset('plugins/metisMenu/dist/metisMenu.min.css')); ?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo e(asset('css/sb-admin-2.css')); ?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo e(asset('plugins/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo e(asset('css/pnotify.custom.min.css')); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo e(asset('/plugins/cropper-master/dist/cropper.css')); ?>" rel="stylesheet" type="text/css">

    <!--SWEET ALERT DİALOG CSS AND JS-->
    <script src="<?php echo e(url('dist/sweetalert.min.js')); ?>"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('dist/sweetalert.css')); ?>">
    <style type="text/css">
        li > a [ href="#" ]{
            font-size: 15px!important;
        }  
        .nav-second-level > li {
            font-size: 12px!important;
        }
    </style>
    
    <?php echo $__env->yieldContent('css'); ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo e(url('Admin')); ?>"> <?php echo e($ayarlar->FirmaAdi); ?></a>
            </div>
            <!-- /.navbar-header -->
          <ul class="nav navbar-top-links navbar-right">
            
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li>
                            <a href="<?php echo e(url('Admin/Yonetici/Duzenle/'.$admin->id)); ?>"><i class="fa fa-user fa-fw"></i> Profil Düzenle</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?php echo e(url('Admin/logout')); ?>"><i class="fa fa-sign-out fa-fw"></i> Çıkış Yap</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <?php echo $__env->make('Admin.Layout.Menuler', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
                
                <div style="padding:10px;text-align: center;margin-top: 25px;" class="hidden-xs">
                <a href="http://www.portakalyazilim.com.tr" target="_blank"><img src="<?php echo e(url('images/portakal.png')); ?>" width="100px"></a>
                </div>
                
                
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $__env->yieldContent('title'); ?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
              <?php echo $__env->yieldContent('content'); ?>
            </div>
      
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<div style="padding:10px;text-align: center;" class="visiable-xs hidden-sm hidden-md hidden-lg">
                <a href="http://www.portakalyazilim.com.tr" target="_blank"><img src="<?php echo e(url('images/portakal.png')); ?>" width="100px"></a>
			</div>
    <!-- jQuery -->
    <script src="<?php echo e(asset('plugins/jquery/dist/jquery.min.js')); ?>"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo e(asset('plugins/bootstrap/dist/js/bootstrap.min.js')); ?>"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo e(asset('plugins/metisMenu/dist/metisMenu.min.js')); ?>"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo e(asset('js/sb-admin-2.js')); ?>"></script>
    <script src="<?php echo e(asset('js/pnotify.custom.min.js')); ?>"></script>
    <script src="<?php echo e(asset('plugins/cropper-master/dist/cropper.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('plugins/jquery-ui/jquery-ui.js')); ?>"></script>
	
	<script>
		function AjaxPost(url,post,callback){
			post+='&_token=<?php echo e(csrf_token()); ?>'
			jQuery.post("<?php echo e(url('Admin')); ?>/"+url, post, function (data) {
				if(callback && typeof(callback) === "function") {
					callback(data);
				}
			}, "json");
		}

        <?php if(session('islem')): ?>
            alert("<?php echo e(session('islem')); ?>");
        <?php endif; ?>
	</script>
    
	
	<?php echo $__env->yieldContent('jsform'); ?>
	<?php echo $__env->yieldContent('js'); ?>
	
</body>

</html>
