


<?php $__env->startSection('Title',$tool->MetaTitle); ?>
<?php $__env->startSection('MetaTag',$tool->MetaTag); ?>
<?php $__env->startSection('MetaDescription',$tool->MetaDescription); ?>


<?php $__env->startSection('content'); ?>
<div class="breadcrumb-box">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="/<?php echo e($dil); ?>/"><?php echo e(\App\Http\Fnk::Ceviri('anasayfa')); ?></a> </li>
            <li class="active">Tools</li>
        </ul>   
    </div>
</div><!-- .breadcrumb-box -->
<section id="main" style="padding-top:30px; ">
    <header class="page-header">
        <div class="container">
            <h5 class="title">MİTSUBİSHİ</h5>
        </div>  
    </header>
    <div class="container">
        <div class="row">
            <article class="bottom-padding content col-sm-12 col-md-12" style="margin-bottom:0px;">
                <?php if( $pdf == "mitsubishi_en"): ?>
                <div id="scroller" style="height: 800px; width: 100%; overflow: auto; ">
                
                    <iframe id="iframe" width="100%" height="100%" src="<?php echo e(url('/pdfjs/web/viewer.html?file='.url('/images/uploads/Tools/mitsubishi_pdf/en_rock_tools.pdf').'#page=1&zoom=page-width')); ?>"></iframe>
                
                </div>
            
                <?php elseif( $pdf == "mitsubishi_es"): ?>
                <div id="scroller" style="height: 800px; width: 100%; overflow: auto; ">
                
                    <iframe id="iframe" width="100%" height="100%" src="<?php echo e(url('/pdfjs/web/viewer.html?file='.url('/images/uploads/Tools/mitsubishi_pdf/es_rock_tools.pdf').'#page=1&zoom=page-width')); ?>"></iframe>
                
                </div>
                
                <?php elseif( $pdf == "mitsubishi_ja"): ?>
                <div id="scroller" style="height: 800px; width: 100%; overflow: auto; ">
                
                    <iframe id="iframe" width="100%" height="100%" src="<?php echo e(url('/pdfjs/web/viewer.html?file='.url('/images/uploads/Tools/mitsubishi_pdf/ja_rock_tools.pdf').'#page=1&zoom=page-width')); ?>"></iframe>
                
                </div>
                <?php elseif( $pdf == "mitsubishi_zh"): ?>
                <div id="scroller" style="height: 800px; width: 100%; overflow: auto; ">
                
                    <iframe id="iframe" width="100%" height="100%" src="<?php echo e(url('/pdfjs/web/viewer.html?file='.url('/images/uploads/Tools/mitsubishi_pdf/zh_rock_tools.pdf').'#page=1&zoom=page-width')); ?>"></iframe>
                
                </div>
                <?php endif; ?>
                <div class="clearfix"></div>
            </article><!-- .content -->
        </div>
    </div><!-- .container -->
</article><!-- .content -->

<?php $__env->stopSection(); ?>
<style type="text/css">
    #main{
        padding-bottom:0px !important;
    }
    .socialheadericon{
        padding-top: 7px;
    }
    .page-box {
        min-height: 40% !important;
        padding-bottom: 20px;
    }
</style>
<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>