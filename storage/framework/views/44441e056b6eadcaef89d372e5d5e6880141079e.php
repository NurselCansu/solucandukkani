<?php $__env->startSection('Title',$urun->MetaTitle); ?>
<?php $__env->startSection('MetaTag',$urun->MetaTag); ?>
<?php $__env->startSection('MetaDescription',$urun->MetaDescription); ?>



<?php $__env->startSection('content'); ?>


<section class="page-header page-header-light page-header-reverse page-header-more-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb breadcrumb-valign-mid">
                    <li><a href="<?php echo e(url($dil)); ?>"><?php echo e(\App\Http\Fnk::Ceviri('anasayfa')); ?></a> </li>
                    <li><a href="<?php echo e(url($dil.'/'.\App\Http\Fnk::Ceviri('urunler-url').'/'.@$urun->Urun_UstKat)); ?>"><?php echo e(\App\Http\Fnk::Ceviri('urunler')); ?></a> </li>
                    <li class="active"><?php echo e($urun->Adi); ?></li>
                </ul>
                <h1><?php echo e($urun->Adi); ?></h1>
            </div>
        </div>
    </div>
</section>
<div id="main" class="page">
    <div class="container">
        <div class="row">
            <article class="col-sm-12 col-md-9 content product-page pull-right">
                <div class="row">
                    <div class="col-sm-12 col-md-8">
                        <div class="image-box">
                            <?php if(!empty(json_decode($urun->Resim))): ?>
                            <?php foreach(json_decode($urun->Resim) as $index => $element): ?>
                            <?php if($index==0): ?>

                            <div class="col-md-7">
                                
                                <a class="img-thumbnail img-thumbnail-hover-icon lightbox" href="<?php echo e(url('/images/uploads/Urun/resim/'.$element)); ?>" data-plugin-options='{"type":"image"}'>
                                    <img class="img-responsive" src="<?php echo e(url('/images/uploads/Urun/resim/'.$element)); ?>" alt="Project Image" style="height: 250px">
                                </a>
                            </div>

                            <?php endif; ?>
                            <?php endforeach; ?>
                            <?php endif; ?>
                           
                               
                    </div>
                    <div class="col-sm-6 col-md-4">
                       
                        <div class="title-box">
                            <h3 style="padding:0 !important"class="title"><?php echo e(\App\Http\Fnk::Ceviri('aciklama')); ?></h3>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-6 bottom-padding">

<!--Bu kısım Ürünlere farklı dilde yazı girilebilmesi için urun veritabanından farklı içerik bölümlerini çekiyor-->
                            <?php
                            $dil=Request::segment(1);
                            ?>
                                
                        <?php if($dil=="en"): ?>
                                <?php echo $urun->Icerik2; ?>

                        <?php elseif($dil=="es"): ?>
                                <?php echo $urun->Icerik3; ?>

                        <?php elseif($dil=="ru"): ?>
                                <?php echo $urun->Icerik4; ?>

                        <?php elseif($dil=="fr"): ?>
                                <?php echo $urun->Icerik5; ?>

                        <?php else: ?>        
                                <?php echo $urun->Icerik; ?>

                        
                            
                       
                    
                        <?php endif; ?>


                                
                            </div>
                        </div>
                       
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>

               <div class="row">
                        <div class="col-md-12">
                            <div class="tabs">
                                <ul class="nav nav-tabs nav-justified">
                                    <li class="active">
                                        <a href="#brosur" data-toggle="tab" class="text-center"><i class="fa fa-star"></i> Broşür</a>
                                    </li>
                                    <li>
                                        <a href="#manuel" data-toggle="tab" class="text-center">Manuel</a>
                                    </li>
                                    <li>
                                        <a href="#soru" data-toggle="tab" class="text-center">Soru Formu</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div id="brosur" class="tab-pane active" style="height: 600px">
                                        
                                    <iframe id="iframe" width="100%" height="100%" src="<?php echo e(url('/pdfjs/web/viewer.html?file='.url('/nem.pdf').'#page=1&zoom=page-width')); ?>"></iframe>

                                    </div>
                                    <div id="manuel" class="tab-pane">
                                        


                                    </div>
                                    <div id="soru" class="tab-pane">
                                        


                                    </div>
                                </div>
                            </div>
                        </div>
 
            </article><!-- .content -->
            <aside class="sidebar col-sm-12 col-md-3" id="sidebar" data-plugin-sticky data-plugin-options='{"minWidth": 991, "containerSelector": ".container", "padding": {"top": 160}}'>


           
            <h4 class="heading-primary"><strong><?php echo e(\App\Http\Fnk::Ceviri('urun-kategorileri')); ?></strong></h4>
                <div class="toggle toggle-primary" data-plugin-toggle="">            
                    <?php echo \App\Http\Fnk::Kategoriler(); ?>

                </div>                
        </aside><!--Sidebar close-->       
        </div>
    </div>
</div><!-- #main -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>