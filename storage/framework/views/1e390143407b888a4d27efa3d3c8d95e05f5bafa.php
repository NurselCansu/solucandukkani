<?php $__env->startSection('baslik'); ?><?php echo e($Title); ?><?php $__env->stopSection(); ?>
<?php $__env->startSection('form'); ?>
<input type="hidden" name="id" value="<?php echo e(@$veri->id); ?>">
<div class="form-group">
    <label for="KisaAd" class="col-sm-2 control-label">Dili Eklenecek Ülkeyi Seçin</label>
    <div class="col-sm-10">
       <select class="form-control" id="DilEkle" required name="DilEkle">
           <option data-bayrak="flag.png">Lütfen Dil Ekleyiniz</option>
            <?php foreach(Fnk::Ulkeler() as $key => $ulke): ?>
                <option data-bayrak="<?php echo e(@$key); ?>.svg" <?php if(@$veri->KisaAd == $key ): ?> selected <?php endif; ?> value="<?php echo e($key); ?>"><?php echo e($ulke); ?> </option>
            <?php endforeach; ?>
        </select>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script type="text/javascript">
    jQuery(document).ready(function($) {

        function formatState (state) {
            if (!state.id) { return state.text; }
            var $state = $(
                '<span><img src="<?php echo e(url('/flags/flags')); ?>/'+$(state.element).data('bayrak')+ '" class="img-flag" style="height:30px ; border-radius: 100%; " /> ' + state.text +' - '+ state.element.value +'</span>'
            );
            return $state;
        };


        $('#DilEkle').select2({
             templateResult: formatState
        });
        
    });
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>