

<?php $__env->startSection('header'); ?>
<?php 

use App\Http\Models\Dil as VD;

$dil=Request::segment(1);

$dilOzellik=VD::where('KisaAd',$dil)->where('is_active',1)->first();


?>

<header id="header" data-plugin-options='{"stickyEnabled": true, "stickyEnableOnBoxed": true, "stickyEnableOnMobile": true, "stickyStartAt": 37, "stickySetTop": "-7px", "stickyChangeLogo": false}'>
                <div class="header-body">
                    <div class="header-container container">
                        <div class="header-row">
                            <div class="header-column">
                                <div class="header-logo" >
                                    <a >
                                        <img alt="OZB" height="100"  data-sticky-height="70" data-sticky-top="-10" src="<?php echo e(url('images/uploads/'.$ayarlar->Logo)); ?>">
                                    </a>
                                </div>
                            </div>
                            <div class="header-column">
                                <div class="header-row">
                                    <div class="header-search hidden-xs">
                                        <form id="searchForm" action="page-search-results.html" method="get">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="q" id="q" placeholder="Arama..." required>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                                </span>
                                            </div>
                                        </form>
                                    </div>
                                    <nav class="header-nav-top">
                                        <ul class="nav nav-pills hidden-xs">
                                       
                                                <?php foreach($sosyal as $element): ?>
                                                    <li class="social-icons-<?php echo e($element->Adi); ?>"><a <?php echo e($element->LinkAcilisTipi); ?> href="<?php echo e(url($element->Link)); ?>"><i class="fa fa-<?php echo e($element->Adi); ?>"></i></a>
                                                <?php endforeach; ?>
                                            
                                        </ul>
                                    </nav>
                                </div>
                                <div class="header-row">
                                    <div class="header-nav">
                                        <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">
                                            <i class="fa fa-bars"></i>
                                        </button>
                                        <ul class="header-social-icons social-icons ">

                                        <?php foreach($tumdiller as $element): ?>
                                                                <li><a href="<?php echo e(url($element->KisaAd.'/cevir/'.@$segment2.'/'.@$segment3.'/'.@$segment4.'/'.@$segment5)); ?>"><img class="x" style="height: 30px" src="<?php echo e(url($element->Resim)); ?>"></img></a></li>
                                        <?php endforeach; ?>
                                              
                                            
                                        </ul>
                                        <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 collapse">
                                            <nav>
                                                <ul class="nav nav-pills" id="mainNav">
                                                    <?php echo Fnk::Menuler(); ?>

                                                </ul>  
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>



<?php $__env->stopSection(); ?>
