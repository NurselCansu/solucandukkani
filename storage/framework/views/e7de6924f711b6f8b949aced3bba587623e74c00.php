

<?php $__env->startSection('title', 'İletişim İstek Detay'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading"><?php echo $__env->yieldContent('title'); ?></div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
				<tr>
					<th style="width:200px;">Adı Soyadı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->AdSoyad); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">E-Posta Adresi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->Eposta); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Telefon</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->Telefon); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Konu</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->Konu); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Mesaj</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo $veri->Mesaj; ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Oluşturulma Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->created_at->format('d.m.Y H:i')); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Güncelleme Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->updated_at->format('d.m.Y H:i')); ?></td>
				</tr>
				<tr>
					<td colspan="3">
					<a href="<?php echo e(URL::previous()); ?>" class="btn btn-primary pull-right">Geri Dön</a>
					<a href="<?php echo e(url('Admin/IletisimIstekleri/Cevapla/'.$veri->id)); ?>" class="btn btn-success pull-right" style="margin-right:10px">Cevapla</a>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>