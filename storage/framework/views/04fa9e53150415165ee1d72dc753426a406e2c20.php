

<?php $__env->startSection('baslik', 'Ürün'); ?>

<?php 
use App\Http\Models\Menu as MN; 
$menu = MN::where('is_active',1)->where('MenuTipi', 'modul')->where('Link', 'LIKE','/tr/urunler%')->get(); 
 
 ?> 

<?php $__env->startSection('form'); ?>
<div class="form-group">
    <label for="Urun_UstKat" class="col-sm-2 control-label">Üst Kategori</label>
    <div class="col-sm-10">
        <select class="form-control" id="Urun_UstKat" required name="Urun_UstKat">
            <option value="">Üst Kategori Seçiniz</option>
            <?php foreach(@$menu as $td): ?>
                <option value="<?php echo e($td->id); ?>" <?php if(@$td->id == @$veri->Urun_UstKat): ?> selected <?php endif; ?>><?php echo e($td->Adi); ?></option>
            <?php endforeach; ?>
        </select>
    </div>
</div>

<div class="form-group">
    <label for="Adi" class="col-sm-2 control-label">Ürün Adı</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->Adi); ?>" name="Adi" id="Adi" placeholder="Ürün Adı">
    </div>
</div>
<div class="form-group">
    <label for="SeriNo" class="col-sm-2 control-label">Seri No</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->SeriNo); ?>" name="SeriNo" id="SeriNo" placeholder="Seri Numarası">
    </div>
</div>
<div class="form-group">
    <label for="UreticiSeriNo" class="col-sm-2 control-label">Üretici Seri No</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->UreticiSeriNo); ?>" name="UreticiSeriNo" id="UreticiSeriNo" placeholder="Üretici Seri Numarası">
    </div>
</div>

<div class="form-group">
    <label for="UreticiSeriNo" class="col-sm-2 control-label">Delkom Seri No</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->DelkomNo); ?>" name="DelkomNo" id="DelkomNo" placeholder="Delkom Seri Numarası">
    </div>
</div>

<div class="form-group">
    <label for="KategoriId" class="col-sm-2 control-label">Kategori</label>
    <div class="col-sm-10">
        <select class="form-control" id="KategoriId" multiple required name="KategoriId[]">
            <option value="">Kategori Seçiniz</option>
            <?php foreach(@$kategori as $td): ?>
                <option value="<?php echo e($td->id); ?>" <?php if( \App\Http\Models\UrunKategori::where('urun_id', @$veri->id)->where('kategori_id', $td->id)->first() ): ?> selected <?php endif; ?>><?php echo e($td->adi); ?></option>
            <?php endforeach; ?>
        </select>
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label">Ürün Öne Çıkarılsın</label>
    <div class="col-md-10">
        <div class="checkbox">
            <label>
                <input type="checkbox" id="OneCikarilmis" name="OneCikarilmis" <?php if(@$veri->OneCikarilmis == 1): ?> checked <?php endif; ?> >Ürün Anasayfada İlk Satırda Öne Çıkarılsın
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" id="OneCikarilmis2" name="OneCikarilmis2" <?php if(@$veri->OneCikarilmis2 == 1): ?> checked <?php endif; ?> >Ürün Anasayfada İkinci Satırda Öne Çıkarılsın
            </label>
        </div>
    </div>
</div>


<div class="form-group">
    <label class="col-md-2 control-label">Kampanyalı Ürün</label>
    <div class="col-md-10">
        <div class="checkbox">
            <label>
                <input type="checkbox" id="Kampanya" name="Kampanya"  <?php if(@$veri->Kampanya == 1): ?> checked <?php endif; ?> >Ürün Kampanyalı Ürünler Sliderında Gösterilsin
            </label>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-md-2 control-label">Ürünler Sayfasında </label>
    <div class="col-md-10">
        <div class="checkbox">
            <label>
                <input type="checkbox" id="UrunGizli" name="UrunGizli"  <?php if(@$veri->UrunGizli == 1): ?> checked <?php endif; ?> >Gösterilmesin
            </label>
        </div>
    </div>
</div>


<div class="form-group">
    <label for="Resim" class="col-sm-2 control-label">Resim</label>
    <div class="col-sm-10">
      <input type="file" class="form-control" name="Resim[]" id="Resim"  placeholder="Resim">
    </div>
    <?php if( @$veri->Resim ): ?>
        <div class="col-md-10 col-md-offset-2" >
            <a href="<?php echo e(url('images/uploads/Urun/resim/'.json_decode(@$veri->Resim)[0] )); ?>">
                <div class="checkbox">
                    <img class="replace-2x" src="<?php echo e(url('images/uploads/Urun/resim/'.json_decode(@$veri->Resim)[0])); ?>" width="150" height="150" alt="">
                </div>
            </a>
        </div>
    <?php endif; ?>
</div>
<?php /* <div class="form-group">
    <label for="Resim360" class="col-sm-2 control-label">360 Resim</label>
    <div class="col-sm-10">
      <input type="file" class="form-control" name="Resim360[]" id="Resim360"  multiple placeholder="360 Derece Resim360lerini Seçiniz" >
    </div>
</div> */ ?>
<div class="form-group">
    <label for="KisaAciklama" class="col-sm-2 control-label">Kısa Açıklama</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="KisaAciklama" id="KisaAciklama" >
    </div>
</div>
<div class="form-group">
    <label for="Icerik" class="col-sm-2 control-label">İçerik</label>
    <div class="col-sm-10">
        <textarea name="Icerik" class="ck"><?php echo e(@$veri->Icerik); ?></textarea>
    </div>
</div>

<script type="text/javascript">


</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>