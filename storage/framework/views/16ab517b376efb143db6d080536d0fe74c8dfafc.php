<?php $__env->startSection('title', 'Ayarlar Detay'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading"><?php echo $__env->yieldContent('title'); ?></div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
				<tr>
					<th style="width:200px;">Dil</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->DilId); ?></td>
				</tr>

				<tr>
					<th style="width:200px;">Firma Adı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo $veri->FirmaAdi; ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Anasayfa Alt Slider Resim</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><img width='150' height='150' src="<?php echo e(url('images/uploads/'.$veri->AnasayfaResim)); ?>" style="max-width: 100%;"></td>
				</tr>
				<tr>
					<th style="width:200px;">Logo</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><img width='150' height='150' src="<?php echo e(url('images/uploads/'.$veri->Logo)); ?>" style="max-width: 100%;"></td>
				</tr>

				<tr>
					<th style="width:200px;">Anasayfa Slogan</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo $veri->AnaSayfaBaslik; ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Anasayfa İçerik Sütun 1</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo $veri->AnaSayfaIcerikSutun1; ?></td>
				</tr>
 				<tr>
					<th style="width:200px;">Anasayfa İçerik Sütun 2</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo $veri->AnaSayfaIcerikSutun2; ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Anasayfa Alt İçerik Sütun 1</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo $veri->AnaSayfaAltIcerikSutun1; ?></td>
				</tr>
 				<tr>
					<th style="width:200px;">Anasayfa Alt İçerik Sütun 2</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo $veri->AnaSayfaAltIcerikSutun2; ?></td>
				</tr>
	 			<!--22.12.2016 günü anasayfadaki en alt kısma sadece fotoğraf eklenmek istendiği için iptal edilmiştir -->
	 			<!-- <tr>
					<th style="width:200px;">Anasayfa Alt Slider Başlık</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo $veri->AnaSayfaAltSliderBaslik; ?></td>
				</tr>
	 			<tr>
					<th style="width:200px;">Anasayfa Alt Slider İçerik Sütun 1</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo $veri->AnaSayfaAltSliderSutun1; ?></td>
				</tr>
	 			
	 			<tr>
					<th style="width:200px;">Anasayfa Alt Slider İçerik Sütun 2</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo $veri->AnaSayfaAltSliderSutun2; ?></td>
				</tr>
	 			
	 			<tr>
					<th style="width:200px;">Anasayfa Alt Slider İçerik Sütun 3</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo $veri->AnaSayfaAltSliderSutun3; ?></td>
				</tr> -->
				
				<tr>
					<th style="width:200px;">Analytics</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo $veri->Analytics; ?></td>
				</tr>

				<tr>
					<th style="width:200px;">Google Analytics Id</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo $veri->AnalyticsAccountId; ?></td>
				</tr>

				<tr>
					<th style="width:200px;">Durum</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e(($veri->is_active==1)?'Aktif':'Pasif'); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Oluşturulma Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e(Fnk::TarihDuzenle($veri->created_at)); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Güncelleme Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e(Fnk::TarihDuzenle($veri->updated_at)); ?></td>
				</tr>
				<tr>
					<td colspan="3"><a href="<?php echo e(URL::previous()); ?>" class="btn btn-primary pull-right">Geri Dön</a></td>
				</tr>
			</table>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>