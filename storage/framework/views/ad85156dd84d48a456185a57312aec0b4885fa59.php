<?php $__env->startSection('baslik', 'Ekibimiz'); ?>

<?php $__env->startSection('form'); ?>

<div class="form-group" id="Tipalani">
    <label for="Tip" class="col-sm-2 control-label">Tip</label>
    <div class="col-sm-10">
        <select class="form-control" id="Tip" required name="Tip" onchange="TipiDegistir()">
            <option value="">Lütfen Tip Seçiniz</option>
            <option <?php if( @$veri->Tip == 'I' ): ?> selected <?php endif; ?> value="I">Kurucu İçerik</option>
            <option <?php if( @$veri->Tip == 'K' ): ?> selected <?php endif; ?> value="K">Kurucu</option>
            <option <?php if( @$veri->Tip == 'M' ): ?> selected <?php endif; ?> value="M">Müdür</option>
            <option <?php if( @$veri->Tip == 'P' ): ?> selected <?php endif; ?> value="P">Personel</option>
        </select>
    </div>
</div>

<div class="form-group" id="Adi_soyadialani" <?php if(@$veri->Tip == 'I' ): ?> style="display:none" <?php endif; ?>>
    <label for="Adi_soyadi" class="col-sm-2 control-label">Adı Soyadı</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->Adi_soyadi); ?>" name="Adi_soyadi" id="Adi_soyadi" placeholder="Adı Soyad">
    </div>
</div>
<div class="form-group" id="Telefonalani" <?php if(@$veri->Tip == 'I' ): ?> style="display:none" <?php endif; ?>>
    <label for="Telefon" class="col-sm-2 control-label">Telefon</label>
    <div class="col-sm-10">
        <input type="text" class="form-control"  value="<?php echo e(@$veri->Telefon); ?>" name="Telefon" id="Telefon" placeholder="Telefon">
    </div>
</div>
<div class="form-group" id="Emailalani" <?php if(@$veri->Tip == 'I' ): ?> style="display:none" <?php endif; ?>>
    <label for="Email" class="col-sm-2 control-label">E-mail</label>
    <div class="col-sm-10">
        <input type="text" class="form-control"  value="<?php echo e(@$veri->Email); ?>" name="Email" id="Email" placeholder="E-mail">
    </div>
</div>
<div class="form-group" id="Icerikalani" <?php if(@$veri->Tip != 'I' ): ?> style="display:none" <?php endif; ?>>
    <label for="Icerik" class="col-sm-2 control-label">İçerik</label>
    <div class="col-sm-10">
        <textarea required id="Icerik" name="Icerik" class="ck"><?php echo e(@$veri->Icerik); ?></textarea>

    </div>
</div>

<div class="form-group" id="Pozisyonalani" <?php if(@$veri->Tip == 'I' ): ?> style="display:none" <?php endif; ?>>
    <label for="Pozisyon" class="col-sm-2 control-label">Pozisyon</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->Pozisyon); ?>" name="Pozisyon" id="Pozisyon" placeholder="Pozisyon">
    </div>
</div>

<div class="form-group" id="Fotografalani" <?php if(@$veri->Tip == 'I' ): ?> style="display:none" <?php endif; ?>>
    <label for="Fotograf" class="col-sm-2 control-label">Fotoğraf</label>
    <div class="col-sm-10">
      <input type="file" class="form-control" name="Fotograf[]" id="Fotograf"  placeholder="Kapak Resim">
    </div>
    <?php if( @$veri->Fotograf ): ?>
        <div class="col-md-10 col-md-offset-2 container container-fluid" >
            <div class="checkbox">
                <?php foreach( json_decode(@$veri->Fotograf)  as $key=>$value): ?>
                <a>
                    <img  src="<?php echo e(url('images/uploads/ekibimiz/'.$value )); ?>" width="150" height="150" alt="">
                    <input type="checkbox"  name="secili_belge_resim[]"   <?php if( @$value ): ?> checked value="<?php echo e($value); ?>" <?php else: ?> value="0" <?php endif; ?>>
                </a>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endif; ?>
</div>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script type="text/javascript">
    function TipiDegistir() {
        var Tip = $('#Tip').val();
        console.log(Tip);
        if (Tip == "I") {
            $('#Adi_soyadialani').hide('slow');
            $('#Pozisyonalani').hide('slow');
            $('#Fotografalani').hide('slow');
            $('#Telefonalani').hide('slow');
            $('#Emailalani').hide('slow');
            $('#Icerikalani').show('slow');
            $('#MetaTagalani ').show('slow');
            $('#MetaTitlealani').show('slow');
            $('#MetaDescriptionalani').show('slow');

        } else if (Tip == "K" || Tip == "M" || Tip == "P") {
            $('#Adi_soyadialani').show('slow');
            $('#Telefonalani').show('slow');
            $('#Emailalani').show('slow');
            $('#Pozisyonalani').show('slow');
            $('#Fotografalani').show('slow');
            $('#Icerikalani').hide('slow');
            $('#MetaTagalani ').hide('slow');
            $('#MetaTitlealani').hide('slow');
            $('#MetaDescriptionalani').hide('slow');

        }
    }
    $( document ).ready(function() {
        var Tip = '<?php echo e(@$veri->Tip); ?>';
     
            if (Tip == "I") {
                $('#Adi_soyadialani').hide('slow');
                $('#Pozisyonalani').hide('slow');
                $('#Fotografalani').hide('slow');
                $('#Icerikalani').show('slow');
                $('#MetaTagalani ').show('slow');
                $('#MetaTitlealani').show('slow');
                $('#MetaDescriptionalani').show('slow');

            } else if (Tip == "K" || Tip == "M" || Tip == "P") {
                $('#Adi_soyadialani').show('slow');
                $('#Pozisyonalani').show('slow');
                $('#Fotografalani').show('slow');
                $('#Icerikalani').hide('slow');
                $('#MetaTagalani ').hide('slow');
                $('#MetaTitlealani').hide('slow');
                $('#MetaDescriptionalani').hide('slow');
            }
        
 
    });



</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>