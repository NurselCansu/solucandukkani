<?php $__env->startSection('content'); ?>

<div id="map-canvas" class="google-map mt-none mb-lg"></div>   

<section class="page-header page-header-light page-header-reverse page-header-more-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb breadcrumb-valign-mid">
                    <li><a href="<?php echo e(url('/'.$dil)); ?>"><?php echo e(\App\Http\Fnk::Ceviri('anasayfa')); ?></a></li>
                    <li class="active"><?php echo e(\App\Http\Fnk::Ceviri('bayiler')); ?></li>
                </ul>
                <h1><?php echo e(\App\Http\Fnk::Ceviri('bayiler')); ?></h1>
            </div>
        </div>
    </div>
</section>

<div role="main" class="main">
	<div class="container">
		<div class="row">
						<div class="col-md-12">
							<h4 class="mb-lg">Bayiliklerimiz</h4>

							<div class="row">
								<div class="col-md-4">
									<div class="tabs tabs-vertical tabs-left tabs-navigation">
										<ul class="nav nav-tabs col-sm-3">
											<li class="active">
												<a href="#tabsNavigation1" data-toggle="tab"><i class="fa fa-group"></i> Yurtiçi</a>
											</li>
											<li>
												<a href="#tabsNavigation2" data-toggle="tab"><i class="fa fa-file"></i> Yurtdışı</a>
											</li>
											
										</ul>
									</div>
								</div>
								<div class="col-md-8">
									<div class="tab-pane tab-pane-navigation active" id="tabsNavigation1">
										<div class="markers"></div>
									</div>
									<div class="tab-pane tab-pane-navigation" id="tabsNavigation2">
										<div class="markers"></div>

									</div>



	</div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>

<script type="text/javascript">
	function initialize() {

    var markers = new Array();

    var mapOptions = {
        zoom: 3,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(39, 32),
        
    };
    
var locations = [
<?php foreach($subeler as $sube): ?>
        [new google.maps.LatLng(<?php echo e($sube->Kordinat); ?>), '<?php echo e($sube->SubeAdi); ?>', '<?php echo e($sube->Adres); ?><br/>asdsa'],
       
<?php endforeach; ?>   
];


    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    var infowindow = new google.maps.InfoWindow();

    for (var i = 0; i < locations.length; i++) {

        // Append a link to the markers DIV for each marker
        $('.markers').append('<a class="marker-link btn btn-primary" data-markerid="' + i + '" href="#">' + locations[i][1] + '</a> ');

        var marker = new google.maps.Marker({
            position: locations[i][0],
            map: map,
            title: locations[i][1],
            icon:"<?php echo e(url('img/pointer.png')); ?>"
        });

        // Register a click event listener on the marker to display the corresponding infowindow content
        google.maps.event.addListener(marker, 'click', (function (marker, i) {

            return function () {
                infowindow.setContent(locations[i][2]);
                infowindow.open(map, marker);
            }

        })(marker, i));

        // Add marker to markers array
        markers.push(marker);
    }

    // Trigger a click event on each marker when the corresponding marker link is clicked
    $('.marker-link').on('click', function () {

        google.maps.event.trigger(markers[$(this).data('markerid')], 'click');
    });
}

initialize();
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>