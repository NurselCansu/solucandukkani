<?php $__env->startSection('baslik', 'Google Analytics'); ?>

<?php $__env->startSection('form'); ?>

<div class="form-group">
    <label class="col-sm-2 control-label">Analytics</label>
    <div class="col-sm-10">
        <textarea class="form-control" name="Analytics" id="Analytics"><?php echo e(@$veri->Analytics); ?></textarea>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Analytics Account Id</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->AnalyticsAccountId); ?>" name="AnalyticsAccountId" id="AnalyticsAccountId" placeholder="Analytics Account Id">
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Açıklama</label>
    <div class="col-sm-10">
        Analytics Account Id alabilmek için google.com/analytics girdikten sonra üst menüden Yönetici > Kullanıcı Yönetimi > "serviceaccount@portakal-yazilim.iam.gserviceaccount.com" Ekle > Ayarları Görüntüle > Görünüm Kimliği buradaki id koplayalıp yapıştırın!
    </div>
</div>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<script type="text/javascript">
    jQuery(document).ready(function($) {
       $('.gizli').hide();
       $('.is_active').hide();
        
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>