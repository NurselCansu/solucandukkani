<?php $__env->startSection('css'); ?>
    <style type="text/css">
        
        <?php if(Request::segment(2)== \App\Http\Fnk::Ceviri('bayilik-url')): ?>
            p{
                margin: 0 0 -10px!important;
            }
        <?php endif; ?>
        
    </style>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>



<div id="map-canvas" class="google-map mt-none mb-lg"></div>   

<section class="page-header page-header-light page-header-reverse page-header-more-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <ul class="breadcrumb breadcrumb-valign-mid">
                    <li><a href="<?php echo e(url('/'.$dil)); ?>"><?php echo e(\App\Http\Fnk::Ceviri('anasayfa')); ?></a></li>
                    <li class="active"><?php echo e(\App\Http\Fnk::Ceviri('bayiler')); ?></li>
                </ul>
                <h1 class="heading-primary"><?php echo e(\App\Http\Fnk::Ceviri('bayiler')); ?></h1>
            </div>
        </div>
    </div>
</section>

<div role="main" class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12">
               <div class="tabs tabs-bottom tabs-center tabs-simple">
                    <ul class="nav nav-tabs">
                        <li class="active" id="yi">
                            <a href="#tabsNavigationSimpleIcons1" data-toggle="tab">
                                <span class="featured-boxes featured-boxes-style-6 p-none m-none">
                                    <span class="featured-box featured-box-primary featured-box-effect-6 p-none m-none">
                                        <span class="box-content p-none m-none">
                                            <i class="icon-featured fa fa-users"></i>
                                        </span>
                                    </span>
                                </span>                                 
                                <p class="mb-none pb-none"><?php echo e(\App\Http\Fnk::Ceviri('yurtici-bayilikler')); ?></p>
                            </a>
                        </li>
                        <li id="yd">
                            <a href="#tabsNavigationSimpleIcons2" data-toggle="tab">
                                <span class="featured-boxes featured-boxes-style-6 p-none m-none">
                                    <span class="featured-box featured-box-primary featured-box-effect-6 p-none m-none">
                                        <span class="box-content p-none m-none">
                                            <i class="icon-featured fa fa-plane"></i>
                                        </span>
                                    </span>
                                </span>                                 
                                <p class="mb-none pb-none"><?php echo e(\App\Http\Fnk::Ceviri('yurtdisi-bayilikler')); ?></p>
                            </a>
                        </li>

                       
                    </ul>
                    <div class="tab-content col-xs-12 center">
                        <div class="tab-pane active" id="tabsNavigationSimpleIcons1">
                            <div class="markers1"></div>
                        </div>
                        <div class="tab-pane col-xs-12 center " id="tabsNavigationSimpleIcons2">
                            <div class="markers2"></div>
                    </div>
            </div>
        </div>    
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<?php
    $dil=Request::segment(1);

?>   



<script type="text/javascript">
var init1=0;
var w = window;
var d = document;
var e = d.documentElement;
var g = d.getElementsByTagName('body')[0];
var x = w.innerWidth || e.clientWidth || g.clientWidth;
var y = w.innerHeight|| e.clientHeight|| g.clientHeight;
var zoom_check=true;

if(x>600){
    zoom_check=false;
}   
//Yurtdışı
function initialize1() {


  
    var markers = new Array();

    var mapOptions = {
        zoom: 6,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(39, 32),
        scrollwheel:  false,
        zoomControl:zoom_check
       
        
    };
  
 
var locations = [
    <?php foreach($bayilerYI as $bayi): ?>

        [new google.maps.LatLng(<?php echo e($bayi->Kordinat); ?>), 
        <?php if($dil=="tr"): ?> 
            '<?php echo @$bayi->BayilikAdi; ?>' 
        <?php else: ?> 
            '<?php echo @$bayi->BayilikAdiDiger; ?>' 
        <?php endif; ?> , 
                '<strong style="color:#07405f"><?php if($dil=="tr"): ?><?php echo trim($bayi->BayilikAdi); ?> <?php else: ?> <?php echo trim($bayi->BayilikAdiDiger); ?>  <?php endif; ?> </strong>' +
                '</br>' +
                '<strong style="color:#07405f;<?php if(empty($bayi->Adres)): ?> display:none; <?php endif; ?>""><?php echo e(\App\Http\Fnk::Ceviri("adres")); ?>:</strong><?php echo e($bayi->Adres); ?>' +
                '<br/>' +
                '<strong style="color:#07405f;"><?php echo e(\App\Http\Fnk::Ceviri("tel")); ?>:</strong><?php echo e($bayi->Telefon); ?>' + 
                '<br/>' + 
                '<strong style="color:#07405f;<?php if(empty($bayi->Fax)): ?> display:none; <?php endif; ?>""><?php echo e(\App\Http\Fnk::Ceviri("fax")); ?>:</strong><?php echo e($bayi->Fax); ?>' +
                '<br/>' +
                '<strong style="color:#07405f;<?php if(empty($bayi->Eposta)): ?> display:none; <?php endif; ?>""><?php echo e(\App\Http\Fnk::Ceviri("eposta")); ?>:</strong><?php echo e($bayi->Eposta); ?>' +
                '<br/>' +
                '<strong style="color:#07405f;<?php if(empty($bayi->Web)): ?> display:none; <?php endif; ?>""><?php echo e(\App\Http\Fnk::Ceviri("web")); ?>:</strong><a target="_blank" href="http://<?php echo e($bayi->Web); ?>"><?php echo e($bayi->Web); ?>' +
                '</a>'
        ],
    <?php endforeach; ?>   
  
];


    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    var infowindow = new google.maps.InfoWindow();

    for (var i = 0; i < locations.length; i++) {

       

            if(init1==0)
            $('.markers1').append('<a style="height:40px" class="marker-link1 btn btn-default col-md-8 col-md-offset-2 col-xs-10" data-markerid="' + i + '" href="#">' + locations[i][1] + '</a><br/> ');
            
        
        var marker = new google.maps.Marker({
            position: locations[i][0],
            map: map,
            title: locations[i][1],
            icon:"<?php echo e(url('img/pointer.png')); ?>",
            optimized: false
        });

        // Register a click event listener on the marker to display the corresponding infowindow content
        google.maps.event.addListener(marker, 'click', (function (marker, i) {

            return function () {
                infowindow.setContent(locations[i][2]);
                infowindow.open(map, marker);
            }

        })(marker, i));

        // Add marker to markers array
        markers.push(marker);
    }

    // Trigger a click event on each marker when the corresponding marker link is clicked
    $('.marker-link1').on('click', function () {

        google.maps.event.trigger(markers[$(this).data('markerid')], 'click');
    });
     $('.marker-link2').on('click', function () {

        google.maps.event.trigger(markers[$(this).data('markerid')], 'click');
    });
    
    
    init1=1;
}

var init2=0;
//Yurtiçi
function initialize2(){

      
    var markers = new Array();

    var mapOptions = {
        zoom: 2,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(39, 32),
        scrollwheel:  false,
        zoomControl:zoom_check
        
        
    };
  
var locations = [
    <?php foreach($bayilerYD as $bayi): ?>

        [new google.maps.LatLng(<?php echo e($bayi->Kordinat); ?>), 
        <?php if($dil=="tr"): ?> 
            '<?php echo @$bayi->BayilikAdi; ?>' 
        <?php else: ?> 
            '<?php echo @$bayi->BayilikAdiDiger; ?>' 
        <?php endif; ?> , 
            '<strong style="color:#07405f"><?php if($dil=="tr"): ?><?php echo trim($bayi->BayilikAdi); ?> <?php else: ?> <?php echo trim($bayi->BayilikAdiDiger); ?>  <?php endif; ?> </strong>' +
            '</br>' +
            '<strong style="color:#07405f"> <?php echo e(\App\Http\Fnk::Ceviri("tel")); ?>:</strong><?php echo e($bayi->Telefon); ?>' +
            '<br/>' +
            '<strong style="color:#07405f;<?php if(empty($bayi->Eposta)): ?> display:none; <?php endif; ?>""><?php echo e(\App\Http\Fnk::Ceviri("eposta")); ?>:</strong><?php echo e($bayi->Eposta); ?>' +
            '<br/>' +
            '<strong style="color:#07405f;<?php if(empty($bayi->Web)): ?> display:none; <?php endif; ?>"><?php echo e(\App\Http\Fnk::Ceviri("web")); ?>:</strong><a target="_blank" href="http://<?php echo e($bayi->Web); ?>"><?php echo e($bayi->Web); ?></a>'
        ],
    <?php endforeach; ?>    
  
];


    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    var infowindow = new google.maps.InfoWindow();

    for (var i = 0; i < locations.length; i++) {

       

            if(init2==0)
            $('.markers2').append('<a style="height:40px" class="marker-link1 btn btn-default col-md-8 col-md-offset-2 col-xs-10" data-markerid="' + i + '" href="#">' + locations[i][1] + '</a><br/> ');
            
        
        var marker = new google.maps.Marker({
            position: locations[i][0],
            map: map,
            title: locations[i][1],
            icon:"<?php echo e(url('img/pointer.png')); ?>",
            optimized: false
        });

        // Register a click event listener on the marker to display the corresponding infowindow content
        google.maps.event.addListener(marker, 'click', (function (marker, i) {

            return function () {
                infowindow.setContent(locations[i][2]);
                infowindow.open(map, marker);
            }

        })(marker, i));

        // Add marker to markers array
        markers.push(marker);
    }

    // Trigger a click event on each marker when the corresponding marker link is clicked
    $('.marker-link1').on('click', function () {

        google.maps.event.trigger(markers[$(this).data('markerid')], 'click');
    });
     $('.marker-link2').on('click', function () {

        google.maps.event.trigger(markers[$(this).data('markerid')], 'click');
    });
    
    init2=1;
}

</script>

<script type="text/javascript">
    
    $(document).ready(function() {
        

        $('#map-canvas p').css('height','0');
        
        $('#yi').click(function() {
                initialize1();
               
                
        });
        $('#yd').click(function() {
                initialize2();
        });
       <?php if(Request::segment(1)!="tr"): ?>

              
             $('#yd').trigger('click');            
             $('#yd').addClass('active');
             $('#yi').removeClass('active');
             $('#tabsNavigationSimpleIcons2').addClass('active');
             $('#tabsNavigationSimpleIcons1').removeClass('active');
             
        <?php else: ?>
             $('#yi').trigger('click');
             $('#yi').addClass('active');
             $('#yd').removeClass('active');
             $('#tabsNavigationSimpleIcons1').addClass('active');
             $('#tabsNavigationSimpleIcons2').removeClass('active');
        <?php endif; ?>  

        });
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>