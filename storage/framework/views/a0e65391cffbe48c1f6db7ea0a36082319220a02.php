

<?php $__env->startSection('baslik', 'Slider'); ?>

<?php $__env->startSection('form'); ?>
<div class="form-group">
  <label for="name" class="col-sm-2 control-label">Resim Adı</label>
  <div class="col-sm-10">
    <input type="text" class="form-control" required value="<?php echo e(@$veri->Adi); ?>" name="Adi" id="name" placeholder="Resim Adı">
  </div>
</div>
<div class="form-group">
    <label for="Resim" class="col-sm-2 control-label">Slider Resmi</label>
    <div class="col-sm-10">
        <input type="file" class="form-control" name="Resim" id="Resim" placeholder="Resim">
    </div>
</div>



<div class="form-group">
    <label class="col-sm-2 control-label">Kısa İçerik</label>
    <div class="col-sm-10">
    	<textarea name="KisaIcerik" id="KisaIcerik"><?php echo e(@$veri->KisaIcerik); ?></textarea>
    </div>
</div>

<div class="form-group">
    <label for="SliderTipi" class="col-sm-2 control-label">Slider Tipi</label>
    <div class="col-sm-10">
        <select name="SliderTipi" class="form-control" required="" onchange="SliderTipiDegistir()" id="SliderTipi">
            <option value="">Lütfen Seçiniz</option>
            <option <?php if(isset($veri) && $veri->SliderTipi=='icerik'): ?> selected <?php endif; ?> value="icerik">İçerik</option>
            <option <?php if(isset($veri) && $veri->SliderTipi=='link'): ?> selected <?php endif; ?> value="link">Link</option>
        </select>
    </div>
</div>
<div class="form-group">
    <label for="LinkAcilisTipi" class="col-sm-2 control-label">Link Açılış Tipi</label>
    <div class="col-sm-10">
        <div class="radio">
            <label>
                <input type="radio" name="LinkAcilisTipi" value="" <?php if(isset($veri) && $veri->LinkAcilisTipi==''): ?> checked <?php endif; ?> <?php if(!isset($veri)): ?> checked <?php endif; ?> >Aynı Pencere
            </label>
    	</div>
	    <div class="radio">
	        <label><input type="radio" name="LinkAcilisTipi" <?php if(isset($veri) && $veri->LinkAcilisTipi=='target="_blank"'): ?> checked <?php endif; ?> value='target="_blank"'>Yeni Pencere</label>
		</div>
	</div>
</div>

<div class="form-group" id="Linkalani" <?php if(!isset($veri) || $veri->SliderTipi!='link'): ?> style="display:none" <?php endif; ?> >
	<label for="Link" class="col-sm-2 control-label">Slider Linki</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="Link" name="Link" value="<?php echo e(@$veri->Link); ?>">
    </div>
</div>
<div class="form-group" id="Icerikalani" <?php if(!isset($veri) || $veri->SliderTipi!='icerik'): ?> style="display:none" <?php endif; ?> >
     <label for="Icerik" class="col-sm-2 control-label">Slider İçeriği</label>
    <div class="col-sm-10">
        <textarea name="Icerik" id="Icerik" class="ck"><?php echo e(@$veri->Icerik); ?></textarea>
    </div>
</div>
<div class="form-group">
	<label for="trbaslangic" class="col-sm-2 control-label">Başlangıç Tarih</label>
	<div class="col-sm-4">
		<div class="input-group datetimepicker2">
			<input type="text" name="baslangic" class="form-control" required="" value="<?php if(!empty($veri->baslangic)): ?><?php echo e(date('d.m.Y H:i', strtotime($veri->baslangic))); ?><?php else: ?><?php echo e(date('d.m.Y H:i')); ?><?php endif; ?>" aria-required="true" aria-invalid="false">
			<span class="input-group-addon">
				<span class="glyphicon glyphicon-calendar"></span>
			</span>
		</div>
	</div>
</div>
<div class="form-group">
	<label for="trbitis" class="col-sm-2 control-label">Bitiş Tarih</label>
	<div class="col-sm-4">
		<div class="input-group datetimepicker3">
			<input type="text" name="bitis" class="form-control" required="" value="<?php if(!empty($veri->bitis)): ?><?php echo e(date('d.m.Y H:i', strtotime($veri->bitis))); ?><?php else: ?><?php echo e(date('d.m.Y H:i')); ?><?php endif; ?>" aria-required="true" aria-invalid="false">
			<span class="input-group-addon">
				<span class="glyphicon glyphicon-calendar"></span>
			</span>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script>
$(document).ready(function(){
	$('.datetimepicker2').datetimepicker();
	$('.datetimepicker3').datetimepicker();
});    
    function SliderTipiDegistir() {
        var SliderTip = $('#SliderTipi').val();
        if(SliderTip == "link") {
            $('#Linkalani').show('slow');
            $('#Icerikalani').hide('slow');
        }else if(SliderTip == "icerik"){
            $('#Icerikalani').show('slow');
            $('#Linkalani').hide('slow');
        }
    }

    window.onload = function() {
    	$( '#KisaIcerik' ).ckeditor({
	        toolbarGroups: [
	            {"name":"basicstyles","groups":["basicstyles"]},
	            {"name":"paragraph","groups":["list","blocks"]},
	            {"name":"colors","groups":["list","blocks"]},
	            {"name":"styles","groups":["styles"]},
	            //{"name":"links","groups":["links"]},
	            //{"name":"document","groups":["mode"]},
	            //{"name":"insert","groups":["insert"]},
	            //{"name":"about","groups":["about"]}
	        ],
    	} );
    };

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>