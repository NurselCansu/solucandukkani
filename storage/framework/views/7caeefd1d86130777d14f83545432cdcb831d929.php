<?php $__env->startSection('baslik'); ?>Fotograf <?php $__env->stopSection(); ?>
<?php $__env->startSection('form'); ?>
<input type="hidden" name="trid" value="<?php echo e(@$trveri->id); ?>">
<div class="form-group">
    <label for="Adi" class="col-sm-2 control-label">Foto Galeri Adı</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" disabled value="<?php echo e($fotogaleri->Adi); ?>" id="Adi">
    </div>
</div>
<div class="form-group">
    <label for="Resim" class="col-sm-2 control-label">Resimler</label>
    <div class="col-sm-10">
      <input type="file" class="form-control" name="Resim[]" id="Resim" placeholder="Resim" multiple>
    </div>
</div>    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>