<?php $__env->startSection('baslik', 'Keywords'); ?>

<?php $__env->startSection('form'); ?>

<div class="form-group">
    <label for="Adi" class="col-sm-2 control-label">Adı</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->Adi); ?>" name="Adi" id="Adi" placeholder="Adi">
    </div>
</div>
<div class="form-group">
    <label for="Keywords" class="col-sm-2 control-label">Keywords</label>
    <div class="col-sm-10">
        <select type="text" class="form-control" required multiple value="<?php echo e(@$veri->Keywords); ?>" name="Keywords[]" id="Keywords" placeholder="Meta Keywords">
            <?php if(!empty(@$veri->Keywords)): ?>
                <?php foreach(json_decode(@$veri->Keywords) as $tag): ?>
                   <option value="<?php echo e($tag); ?>" selected="selected"><?php echo e($tag); ?></option>
                <?php endforeach; ?>
            <?php endif; ?>    
        </select>
    </div>
</div>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<script type="text/javascript">
    jQuery(document).ready(function($) {
       $('.gizli').hide();
        $('#Keywords').select2({
            tags: true,
            multiple: true,
            tokenSeparators: [',',' '],
            
        });
        
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>