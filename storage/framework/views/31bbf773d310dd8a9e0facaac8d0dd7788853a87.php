

<?php $__env->startSection('title', 'Anasayfa'); ?>

<?php $__env->startSection('content'); ?>

<form method="post" enctype="multipart/form-data" class="form-horizontal" action="<?php echo e(url('Admin/excelYap')); ?>" id="form">
<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

<div id="printableArea">
	<div id="chart_ziyaret" style="width: 100%; height: 500px"></div>
	<br>
	<div class="row">
		<div class="aranankelimeler col-sm-5">
			<div class="page-header">
				Aranan Kelimeler
			</div>
			<div id="piechart_kelime" style="width: 100%;height:400px"></div>
		</div>
		<div class="kaynaklar col-sm-6">
			<div class="page-header">
				Kaynaklar
			</div>
			<div id="piechart_kaynak" style="width: 100%;height:400px"></div>
		</div>
		<div class="col-sm-1"></div>
	</div>
	<div class="row">
		<div class="ulke col-sm-4">
		<div class="page-header">
			 Ülkeler
		</div>
				<div id="piechart_ulke" style="width: 100%;height:400px"></div>
		</div>
		<div class="sehir col-sm-4">
			<div class="page-header">
				Şehirler
			</div>
			<div id="piechart_sehir" style="width: 100%;height:400px"></div>
		</div>
		<div class="dil col-sm-4">
			<div class="page-header">
				Diller
			</div>
			<div id="piechart_dil" style="width: 100%;height:400px"></div>
		</div>
	</div>

	<div class="row pull-right">
		<button type="submit" id="btn" name="btn" class="btn btn-success" >Excel Yap</button>
		<a onclick="window.print();" class="btn btn-primary ">Yazdır</a>
		<br>
	</div>
</div>
</form>
			
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


<script type="text/javascript">
			google.charts.load("current", {packages:["corechart"]});
			google.charts.setOnLoadCallback(drawChart0);
			google.charts.setOnLoadCallback(drawChart1);
			google.charts.setOnLoadCallback(drawChart2);
			google.charts.setOnLoadCallback(drawChart3);
			google.charts.setOnLoadCallback(drawChart4);
			google.charts.setOnLoadCallback(drawChart5);
			
			google.charts.load('current', {'packages':['corechart']});

		function drawChart0() {
				
			var d30gun = [];
			d30gun[0] = ['Tarih' , 'Ziyaret'];
		<?php foreach($analytics->where('key', 'Ziyaret') as $element): ?>
			var t = "<?php echo e($element->created_at); ?>".split(/[- :]/);
			var d = new Date(Date.UTC(t[0], t[1]-1, t[2], t[3], t[4], t[5]));
			
			d30gun[d30gun.length] = [ d,<?php echo e($element->value); ?> ];
		<?php endforeach; ?>

				var data0 = google.visualization.arrayToDataTable( d30gun );

				var options0 = {
					title: 'Sayfa Gösterim',
					hAxis: {title: 'Ziyaret',  titleTextStyle: {color: '#333'}},
					vAxis: {minValue: 0}
				};

				var chart = new google.visualization.AreaChart(document.getElementById('chart_ziyaret'));
				chart.draw(data0, options0);
		}


	function drawChart1() {
				var flot_ulke = [];
			flot_ulke[0] = ['Ülkeler' , 'Ziyaret'];
		<?php foreach($analyticsDiger->where('key', 'ZiyaretUlke') as $element): ?>
			flot_ulke[flot_ulke.length] =  [ '<?php echo e(json_decode($element->value)[0]); ?>' , <?php echo e(json_decode($element->value)[1]); ?> ] ;
		<?php endforeach; ?>
				
				var data1 = google.visualization.arrayToDataTable(flot_ulke);
				var options = {
				 
					is3D: true,
				};
				var chart = new google.visualization.PieChart(document.getElementById('piechart_ulke'));
				chart.draw(data1, options);
	}

	function drawChart2() {
		var flot_sehir = [];
			flot_sehir[0] = ['Şehir' , 'Ziyaret'];
		<?php foreach($analyticsDiger->where('key', 'ZiyaretSehir') as $element): ?>
			flot_sehir[flot_sehir.length] = [ '<?php echo e(json_decode($element->value)[0]); ?>' , <?php echo e(json_decode($element->value)[1]); ?> ] ;
		<?php endforeach; ?>
				var data2 = google.visualization.arrayToDataTable(flot_sehir);
				var options = {
				 
					is3D: true,
				};
				var chart = new google.visualization.PieChart(document.getElementById('piechart_sehir'));
				chart.draw(data2, options);
	}

	function drawChart3() {
		var flot_dil = [];
		flot_dil[0] = ['Dil', 'Ziyaret'];
		<?php foreach($analyticsDiger->where('key', 'ZiyaretDil') as $element): ?>
			flot_dil[flot_dil.length] = [ '<?php echo e(json_decode($element->value)[0]); ?>' , <?php echo e(json_decode($element->value)[1]); ?> ] ;
		<?php endforeach; ?>
				var data3 = google.visualization.arrayToDataTable(flot_dil);
				var options = {
				 
					is3D: true,
				};
				var chart = new google.visualization.PieChart(document.getElementById('piechart_dil'));
				chart.draw(data3, options);
	}

	function drawChart4() {
		var flot_kelime = [];
		flot_kelime[0] = ['Kelimeler','Sayı']
		<?php foreach($analyticsDiger->where('key', 'Kelimeler') as $element): ?>
			flot_kelime[flot_kelime.length] = [ '<?php echo e(json_decode($element->value)[0]); ?>', <?php echo e(json_decode($element->value)[1]); ?> ];
		<?php endforeach; ?>
				var data4 = google.visualization.arrayToDataTable(flot_kelime);
				var options = {
				 
					is3D: true,
				};
				var chart = new google.visualization.PieChart(document.getElementById('piechart_kelime'));
				chart.draw(data4, options);
	}

	function drawChart5() {
		var flot_kaynak = [];
		flot_kaynak[0] = ['Kaynak', 'Site'];
		<?php foreach($analyticsDiger->where('key', 'KaynakSite') as $element): ?>
			flot_kaynak[flot_kaynak.length] = [ '<?php echo e(json_decode($element->value)[0]); ?>' , <?php echo e(json_decode($element->value)[1]); ?> ] ;
		<?php endforeach; ?>
				var data5 = google.visualization.arrayToDataTable(flot_kaynak);
				var options = {
				 
					is3D: true,
				};
				var chart = new google.visualization.PieChart(document.getElementById('piechart_kaynak'));
				chart.draw(data5, options);
	}

</script>

<script type="text/javascript">


</script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('css'); ?>
<style>
@media  print {
	.aranankelimeler{
			float:left;
	width:50% !important;
}
.kaynaklar{
		float:left;
		width:50% !important;
}
 .ulke{
			float:left;
	width:33% !important;
}
.sehir{
		float:left;
		width:33% !important;
}
.dil{
		float:left;
		width:33% !important;
}
.page-header{
		display:none
}
.btn{
		display:none;
}
</style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>