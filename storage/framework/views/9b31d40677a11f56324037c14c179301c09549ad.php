<?php $__env->startSection('baslik', 'Yönetici'); ?>

<?php $__env->startSection('form'); ?>
<div class="form-group">
	<label for="name" class="col-sm-2 control-label">Ad</label>
	<div class="col-sm-10">
		<input type="text" class="form-control" required value="<?php echo e(@$veri->name); ?>" name="name" id="name" placeholder="Adı Soyadı">
	</div>
</div>
<div class="form-group">
	<label class="col-md-2 control-label">E-Posta Adresi</label>
	<div class="col-md-10">
		<input type="email" class="form-control" required name="email"  value="<?php echo e(@$veri->email); ?>"  placeholder="E-Posta Adresi">
	</div>
</div>
<div class="form-group">
	<label class="col-md-2 control-label">Yetki</label>
	<div class="col-md-10">
		<select name="role" class="form-control">
			<option value="0" <?php if(@$veri->role == 0): ?> checked <?php endif; ?>>Admin</option>
			<option value="1" <?php if(@$veri->role == 1): ?> checked <?php endif; ?>>İnsan Kaynakları</option>
		</select>
	</div>
</div>
<div class="form-group">
	<label class="col-md-2 control-label">Parola</label>
	<div class="col-md-10">
		<input type="password" class="form-control" <?php if(!isset($veri->password)): ?> required <?php endif; ?> minlength="6" name="password" >
	</div>
</div>
<div class="form-group">
	<label class="col-md-2 control-label">Parola Tekrar</label>
	<div class="col-md-10">
		<input type="password" class="form-control" <?php if(!isset($veri->password)): ?> required <?php endif; ?> minlength="6" name="password_confirmation" >
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>