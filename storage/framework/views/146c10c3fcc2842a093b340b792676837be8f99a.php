<?php
	$url = Request::url();
	$link = explode('Sirala',$url);
	$link = $link[0].'Sirala'
?>
<?php $__env->startSection('title'); ?> <?php echo e($baslik); ?> Sıralama <?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="panel panel-default">
  <div class="panel-heading">
  <span class="pull-left"><?php echo $__env->yieldContent('title'); ?></span>
				<select onChange="UstKatDegistir()" id="UstKatId" class="pull-left">
					<option value="0">Ana Kategoriler</option>
					<?php
						Fnk::ParentOption($anakategoriler,$ustkatid);
					?>
				</select>
				
				<div style="clear: both;"></div>
			</div>
  <div class="panel-body">
    <p class="alert alert-info">
		<?php echo $__env->yieldContent('title'); ?> İşlemini Sürükle - Bırak Yaparak Sitenizdeki Görünüm Sırasını Değiştirebilirsiniz.
	</p>
	
	<ul id="sortable" class="list-group">
	<?php foreach($veri as $v): ?>
			<li class="list-group-item" id="id-<?php echo e($v->id); ?>">
				<?php if(isset($v->Resim)): ?>
					<img src="<?php echo e(url('images/uploads/'.$v->Resim)); ?>" style="height: 50px;object-fit: contain;display: block;" >
					<?php echo e($v->$vtcolumnname); ?>

				<?php else: ?>
					<?php echo e($v->$vtcolumnname); ?>

				<?php endif; ?>	
			</li>
	<?php endforeach; ?>
	
	</ul>
  </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script src="<?php echo e(asset('plugins/jquery-ui/jquery-ui.js')); ?>"></script>
<script type="text/javascript">
  $(document).ready(function () {
    $('#sortable').sortable({
        axis: 'y',
        stop: function (event, ui) {
	        var oData = $(this).sortable('serialize');
	        console.log(oData);
           	$.ajax({
                data: oData+'&_token=<?php echo e(csrf_token()); ?>',
                type: 'POST',
                url: '<?php echo e($link); ?>'
            });
	}
    });
});

 </script>
 <script type="text/javascript">
	function UstKatDegistir(){
		var UstKatId = $('#UstKatId').val();
		window.location = "<?=$link?>/"+UstKatId;
	}
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>