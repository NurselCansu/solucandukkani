<?php
	$url = Request::url();
	$link = explode('Sirala',$url);
	$link = $link[0].'Sirala' ;
?>
<?php $__env->startSection('title'); ?> Foto Galeri Sıralama <?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <span class="pull-left"><?php echo $__env->yieldContent('title'); ?></span>
    	</div>
        <div class="panel-body">
            <p class="alert alert-info">
        		<?php echo $__env->yieldContent('title'); ?> İşlemini Sürükle - Bırak Yaparak Sitenizdeki Görünüm Sırasını Değiştirebilirsiniz.
        	</p>
        	<div id="sortable">
                <?php 
                    foreach( $galeri as $sirasayisi=>$v ){
                        $fotograf = \App\Http\Models\Fotograf::where('GaleriId', $v->id)->where('is_active',1)->orderby('Sira','asc')->first();
                        $sirasayisi++;   
                        echo '
                            <div class="col-xs-2" style="margin-left:10px;margin-bottom:5px;width:150px;height:150px;" id="id-'.$v->id.'">
                                <p>
                                    <img width="100" height="100" src="'.url('images/uploads/FotoGaleri/'.@$fotograf->Resim.'').'" class="img-rounded img-responsive">'.$sirasayisi.'.</img>
                                </p>
                            </div>';
                    }
        	   ?>
        	</div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
    
    <style>
        #sortable > div { float: left; }
        img { padding: 0.5em; }
    </style>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    
    <script src="<?php echo e(asset('plugins/jquery-ui/jquery-ui.js')); ?>"></script>
    
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sortable').sortable({
                axis: 'x,y',
                revert: true,
                stop: function (event, ui) {
                    var oData = $(this).sortable('serialize');
                    console.log(oData);
                   	$.ajax({
                        data: oData+'&_token=<?php echo e(csrf_token()); ?>',
                        type: 'POST',
                        url: '<?php echo e($link); ?>'
                    });
                }
            });
        });
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>