 

<?php $__env->startSection('Title',$tool->MetaTitle); ?>
<?php $__env->startSection('MetaTag',$tool->MetaTag); ?>
<?php $__env->startSection('MetaDescription',$tool->MetaDescription); ?>


<?php $__env->startSection('content'); ?>
<div class="breadcrumb-box">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="/<?php echo e($dil); ?>/"><?php echo e(\App\Http\Fnk::Ceviri('anasayfa')); ?></a> </li>
            <li><a href="/<?php echo e($dil); ?>/<?php echo e(\App\Http\Fnk::Ceviri('Tools-url')); ?>"><?php echo e(\App\Http\Fnk::Ceviri('Tools-url')); ?></a></li>
            <li class="active">Mitsubishi</li>
        </ul>   
    </div>
</div><!-- .breadcrumb-box -->
<section id="main" style="padding-top:30px; ">
    <header class="page-header">
        <div class="container">
            <h5 class="title"><?php echo e($tool->Adi); ?></h5>
        </div> 
    </header>
    
    <div class="container">
        <div class="clearfix ">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" align="center">
                <p><img src="<?php echo e(url('images/mrt_tit.png')); ?>" alt=""></p>
            </div>
            <div class="row row-center wback ">
                <div class="txt-c col-lg-7 col-md-7 col-sm-12 col-xs-12 " style="padding-top: 50px" >
                    <img src="<?php echo e(url('images/global_img.png')); ?>" alt="" align="right">
                </div>
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 list">                
                    <div>
                        <a class="btn" onclick=" window.location = '<?php echo e(url('/'.$dil.'/'.'mitsubishi_en')); ?>' " >    
                            <img src="<?php echo e(url('images/btn_english.png')); ?>" alt="English">
                        </a>
                        <a class="btn" onclick=" window.location = '<?php echo e(url('/'.$dil.'/'.'mitsubishi_ja')); ?>' ">
                            <img src="<?php echo e(url('images/btn_japanese.png')); ?>" alt="Japanese">
                        </a>
                        <a class="btn" onclick=" window.location = '<?php echo e(url('/'.$dil.'/'.'mitsubishi_zh')); ?>' ">
                            <img src="<?php echo e(url('images/btn_chinese.png')); ?>" alt="Chinese">
                        </a>
                        <a class="btn" onclick=" window.location = '<?php echo e(url('/'.$dil.'/'.'mitsubishi_es')); ?>' ">
                            <img src="<?php echo e(url('images/btn_spanish.png')); ?>" alt="Spanish">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php /* <div class="container">
        <div class="row">
            <article class="bottom-padding content col-sm-12 col-md-12" style="margin-bottom:0px;">
                <a href="<?php echo e(url('/images/uploads/Tools/Pdf/'.$tool->Pdf)); ?>">
                    <iframe frameborder="0" height="100%" scrolling="no" src="<?php echo e(url('/images/uploads/Tools/Pdf/'.$tool->Pdf)); ?>" width="100%"></iframe>
                </a>
                <div class="clearfix"></div>
            </article><!-- .content -->
        </div>
    </div><!-- .container --> */ ?>
</article><!-- .content -->

<?php $__env->stopSection(); ?>
<style type="text/css">
    #main{
        padding-bottom:0px !important;
    }
    .socialheadericon{
        padding-top: 7px;
    }
    .page-box {
        min-height: 40% !important;
        padding-bottom: 20px;
    }
    .wback {
        background-image:url('../../images/world_map.png');
        background-repeat: no-repeat;
        background-size:contain;        
        width: 100%;
        height: 90%;
        background-color:rgba(255, 255, 255, 0.2);
        }

</style>
<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>