

<?php $__env->startSection('Title',$ayarlar->MetaTitle); ?>
<?php $__env->startSection('MetaTag',$ayarlar->MetaTag); ?>
<?php $__env->startSection('MetaDescription',$ayarlar->MetaDescription); ?>

<?php $__env->startSection('content'); ?>

<div class="breadcrumb-box">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="/<?php echo e($dil); ?>/"><?php echo e(\App\Http\Fnk::Ceviri('anasayfa')); ?></a> </li>
            <li class="active"><?php echo e(\App\Http\Fnk::Ceviri('Rock-Operator-Arayan-Firmalar')); ?></li>
        </ul>	
    </div>
</div><!-- .breadcrumb-box -->
<section id="main" style="background-image:url('<?php echo e(url('images/bluroperator.jpg')); ?>') ; background-size: cover;">
    <div class="container">
        <article class="content">
            <div class="row">

                <div class="col-sm-12 col-md-12">
                    <div class="form-content" >
                        <form class="row form-box login-form form-validator"  method="POST" action="<?php echo e(url($dil.'/'.\App\Http\Fnk::Ceviri('operator-arayan-url'))); ?>">
                            <input type='hidden' name='_token' value='<?php echo e(csrf_token()); ?>' ></input>
                            
                            <h3 class="title  text-center"><b><?php echo e(\App\Http\Fnk::Ceviri('Rock-Operator-Arayan-Firmalar')); ?></b></h3>


                            <div class="row col-md-12 title" style="background-color: #dcdcdc; font-size: 20px;" >
                                <div class="col-md-6 "><b><?php echo e(\App\Http\Fnk::Ceviri('Size-Nasil-Ulasabiliriz')); ?></b></div>
                                <div class="col-md-6"><b><?php echo e(\App\Http\Fnk::Ceviri('Operator-Alimiyla-ilgili-Yetkili')); ?></b></div>
                            </div> 

                            <div class=" col-md-12 form-group">                                
                                <div class=" col-md-6 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('Firma-Adi')); ?><span class="required">*</span></label>
                                    <input class="form-control" type="text" name="FirmaAdi" required>
                                </div>

                                <div class="col-md-6 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('Firma-Yetkilisi')); ?><span class="required">*</span></label>
                                    <input class="form-control" type="text" name="FirmaYetkili" required >
                                </div>    
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-6 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('Firma-Adresi')); ?><span class="required">*</span></label>
                                    <input class="form-control" type="phone" name="FirmaAdres" required >
                                </div>
                                <div class="col-md-6 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('Yetkilinin-Gorevi')); ?><span class="required">*</span></label>
                                    <input class="form-control" type="phone" name="YetkiliGorevi" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-6 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('Firma-Telefonu')); ?><span class="required">*</span></label>
                                    <input class="form-control" type="mail" name="FirmaTelefon" required >
                                </div>
                                <div class="col-md-6 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('Yetkilinin-Telefonu')); ?><span class="required">*</span></label>
                                    <input class="form-control" type="text" name="YetkiliTelefon" id="SimdikiFirma" required>
                                </div>

                            </div>

                            <div class="col-md-12">

                                <div class="col-md-6 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('Firma-Emaili')); ?><span class="required">*</span></label>
                                    <input class="form-control" type="text" name="FirmaEmail" required>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('Firma-Web-Sitesi')); ?><span class="required">*</span></label>
                                    <input class="form-control" type="text" name="FirmaWebSitesi" id="OncekiFirma" required>
                                </div>
                            </div>  


                            <div class="row col-md-12 title" style="background-color: #dcdcdc; font-size: 20px;">


                                <div class="col-md-6"><b><?php echo e(\App\Http\Fnk::Ceviri('Firmanizin-Faaliyeti-Konusu-Nedir')); ?></b></div>
                                <div class="col-md-6"><b><?php echo e(\App\Http\Fnk::Ceviri('Hangi-Deliciniz-icin-Operator-Ariyorsunuz')); ?></b></div>
                                
                            </div>

                            <div class=" container col-md-6 form-group">                                
                                <div class=" col-md-12 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('calisma-Turu')); ?></label>
                                    <input class="form-control" type="text" name="CalismaTuru" >
                                </div>
                                <div class=" col-md-12 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('calisma-Tipi')); ?></label>
                                    <input class="form-control" type="text" name="CalismaTipi" >
                                </div>
                                <div class=" col-md-12 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('Gunluk-Aylik-calisma-Suresi')); ?></label>
                                    <input class="form-control" type="text" name="GACalismaSuresi" >
                                </div>
                            </div>
                            <div class="container col-md-6 form-group">

                                <div class="col-md-6 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('Delici-Adi')); ?><span class="required">*</span></label>
                                    <input class="form-control" type="text" name="DeliciAdi[0]" required>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('Modeli')); ?><span class="required">*</span></label>
                                    <input class="form-control" type="text" name="Modeli[0]" required>
                                </div>
                                <div class="col-md-6 col-sm-6 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('Delici-Adi')); ?></label>
                                    <input class="form-control" type="text" name="DeliciAdi[1]" >
                                </div> 
                                <div class="col-md-6 col-sm-6  form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('Modeli')); ?></label>
                                    <input class="form-control" type="text" name="Modeli[1]" >
                                </div> 

                                <div class="col-md-6 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('Delici-Adi')); ?></label>
                                    <input class="form-control" type="text" name="DeliciAdi[2]" >
                                </div> 
                                <div class="col-md-6 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('Modeli')); ?></label>
                                    <input class="form-control" type="text" name="Modeli[2]" >
                                </div>    
                            </div>  
                            

                            <div class="row col-md-12 title" style="background-color: #dcdcdc; font-size: 20px;">

                                <div class="col-md-6"><b><?php echo e(\App\Http\Fnk::Ceviri('Operatorun-calismasini-istediginiz-santiye')); ?></b></div>
                                <div class="col-md-6"><b><?php echo e(\App\Http\Fnk::Ceviri('Operatorde-Aradiginiz-Diger-ozellikler-Nelerdir')); ?></b></div>

                            </div>

                            <div class=" container col-md-6 form-group">                                
                                <div class=" col-md-4 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('Ulke')); ?></label>
                                    <input class="form-control" type="text" name="Ulke[0]" >
                                </div>

                                <div class="col-md-4 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('il')); ?></label>
                                    <input class="form-control" type="text" name="Il[0]" >
                                </div> 
                                <div class="col-md-4 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('ilce')); ?></label>
                                    <input class="form-control" type="text" name="Ilce[0]" >
                                </div>
                                  <div class=" col-md-4 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('Ulke')); ?></label>
                                    <input class="form-control" type="text" name="Ulke[1]" >
                                </div>

                                <div class="col-md-4 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('il')); ?></label>
                                    <input class="form-control" type="text" name="Il[1]" >
                                </div> 
                                <div class="col-md-4 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('ilce')); ?></label>
                                    <input class="form-control" type="text" name="Ilce[1]" >
                                </div>
                                <div class=" col-md-4 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('Ulke')); ?></label>
                                    <input class="form-control" type="text" name="Ulke[2]" >
                                </div>

                                <div class="col-md-4 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('il')); ?></label>
                                    <input class="form-control" type="text" name="Il[2]" >
                                </div> 
                                <div class="col-md-4 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('ilce')); ?></label>
                                    <input class="form-control" type="text" name="Ilce[2]" >
                                </div>
                                  
                            </div>
                            <div class=" container col-md-6 form-group">                                
                                <div class=" col-md-6 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('Patlatma-Belgesi')); ?></label>
                                    <input class="form-control" type="text" name="PatlatmaBelgesi" >
                                </div>

                                <div class=" col-md-6 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('Operator-Ehliyeti')); ?></label>
                                    <input class="form-control" type="text" name="OperatorEhliyeti" >
                                </div>     
                                <div class=" col-md-12 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('Yag-Filtre-Bakim-Bilgisi')); ?></label>
                                    <input class="form-control" type="text" name="YagFiltreBakimBilgisi" >
                                </div> 
                                 <div class=" col-md-12 form-group">
                                    <label><?php echo e(\App\Http\Fnk::Ceviri('Delici-Deneyim-Suresi')); ?></label>
                                    <input class="form-control" type="text" name="DeliciDeneyimSuresi" >
                                </div>     
                            </div>

                            <div class="col-md-12 form-group">
                                <div class="col-sm-8 ">
                                    <?php echo app('captcha')->display();; ?>

                                </div>
                                <div class="col-md-4 ">
                                    <button type="submit" class=" btn btn-default"><?php echo e(\App\Http\Fnk::Ceviri('gonder')); ?></button>
                                </div>
                            </div>    
                        </form><!-- .form-box -->
                    </div>
                </div>
            </div>
        </article><!-- .content -->
    </div>
</section><!-- #main -->

<style type="text/css">
    [class*="frame-shadow"] {
        background: #f2f2f2;
    }
    .form-group {
        margin-bottom: -5px;
    }
    /* 
     .form-content>div#test{
        min-height: 600px;
        padding-bottom: 83px;
        background-color:#f2f2f2;
    }*/
   /* .selectBox, .form-control{
        background: rgba(255, 255, 255, 0.63);
    }*/
</style>

<script>
    $("#CalismaDurumu").change(function(){
        var calismadurumu = $("#CalismaDurumu").val();
        if(calismadurumu == 1){
            $("#SimdikiFirma").prop('required',true);
            $("#OncekiFirma").prop('required',false);

        }else if(calismadurumu == 0){
            $("#OncekiFirma").prop('required',true);
            $("#SimdikiFirma").prop('required',false);
        }
    });

    $( document ).ready(function() {
        $('#form').validate({
            ignore: ':hidden:not([class~=selectized]),:hidden > .selectized, .selectize-control .selectize-input input',
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block validateerror',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function(form) {
                return true;
            }
        });
        
        var bar = $('#uploadbar');
        $('#form').ajaxForm({
            url:'<?php echo e(url($dil.'/'.\App\Http\Fnk::Ceviri('operator-kart-url-'.$ref))); ?>',
            beforeSend: function() {
                bar.parent('div').show();
                var percentVal = '0%';
                bar.width(percentVal);
                bar.html(percentVal);
                bar.attr('aria-valuenow',0);
                $('#btn').attr('disabled',true);
            },
            uploadProgress: function(event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                bar.attr('aria-valuenow',percentComplete);
                bar.width(percentVal);
                bar.html(percentVal);
            },
            success: function() {
                var percentVal = '100%';
                bar.attr('aria-valuenow',100);
                bar.width(percentVal);
                bar.html(percentVal);
                $('#btn').removeAttr('disabled');
            },
            complete: function(xhr) {

                var sonuc = JSON.parse(xhr.responseText);
                console.log(sonuc);
                if(sonuc.islem){

                    alert("İşleminiz Başarıyla Yapılmıştır.");
                    
                    if (typeof(submitafter) === "function") {
                        submitafter(sonuc);
                    }
                    
                    $("#form")[0].reset();
                    
                    if($('.selectize').length>0){
                        $( ".selectize" ).each(function( index ) {
                            if(index%3==0)this.selectize.clear()
                        });
                    }               
                //  jQuery("a[data-toggle=collapse][href=#adim1]").click();
            }else{
                $.each(sonuc.error, function(index, value) {
                 new PNotify({
                    title: 'HATA',
                    text: value,
                    type: 'error',
                    styling: 'bootstrap3'
                });
             });
                $('#btn').removeAttr('disabled');
            }
            jQuery(document).ready(function () {
             setTimeout(uploadbartemizle(),1000 );
         });
        }
    });
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>