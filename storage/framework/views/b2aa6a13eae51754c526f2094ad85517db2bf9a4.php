<?php $__env->startSection('title', 'Bayilik Detay'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading"><?php echo $__env->yieldContent('title'); ?></div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
				<tr>
					<th style="width:200px;">Dil</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->DilId); ?></td>
				</tr>

				<tr>
					<th style="width:200px;">Bayilik Faaliyet Alanı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e(($veri->bayilik_faliyet_alani==1)?'Yurtiçi':'Yurtdışı'); ?></td>
				</tr>

				<tr>
					<th style="width:200px;">Bayilik Adı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->BayilikAdi); ?></td>
				</tr>

				<tr>
					<th style="width:200px;">Bayilik Adres</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->Adres); ?></td>
				</tr>

				<tr>
					<th style="width:200px;">Bayilik Telefon</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->Telefon); ?></td>
				</tr>

				<tr>
					<th style="width:200px;">Bayilik Fax</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->Fax); ?></td>
				</tr>

				<tr>
					<th style="width:200px;">Bayilik Eposta</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->Eposta); ?></td>
				</tr>

				<tr>
					<th style="width:200px;">Bayilik Web Adres</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->Web); ?></td>
				</tr>

				<tr>
					<th style="width:200px;">Bayilik Koordinatları</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->Kordinat); ?></td>
				</tr>
				
				<tr>
					<th style="width:200px;">Durum</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e(($veri->is_active==1)?'Aktif':'Pasif'); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Oluşturulma Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e(Fnk::TarihDuzenle($veri->created_at)); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Güncelleme Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e(Fnk::TarihDuzenle($veri->updated_at)); ?></td>
				</tr>
				<tr>
					<td colspan="3"><a href="<?php echo e(URL::previous()); ?>" class="btn btn-primary pull-right">Geri Dön</a></td>
				</tr>
			</table>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>