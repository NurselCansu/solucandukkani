<script type="text/javascript">
	var userLang = (navigator.language || navigator.userLanguage).substring(0,2); 

	if (userLang != "<?php echo e(Request::segment(1)); ?>") {
		if (userLang != "" || $.inArray( userLang, <?php echo $tumdiller->lists('KisaAd'); ?>)) {
			window.location = '<?php echo e(url("/")); ?>/' + userLang;
		} else if( "<?php echo e($varsayilandil->KisaAd); ?>" != "" ) {
			window.location = '<?php echo e(url($varsayilandil->KisaAd)); ?>';
		} else {
			window.location = '<?php echo e(url("tr")); ?>';
		}
	}
</script>