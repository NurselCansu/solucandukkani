<?php $__env->startSection('baslik'); ?>Dil Tanımlama <?php $__env->stopSection(); ?>
<?php $__env->startSection('form'); ?>
<input type="hidden" name="id" value="<?php echo e(@$veri->id); ?>">
<table class="table table-striped table-bordered table-hover">
<tr>
    <th>Slug</th>
    <th>
        <select class="form-control" name="DilId" onchange="window.location.href='<?php echo e(url('Admin/Dil/Tanimla')); ?>/'+this.value+'/<?php echo e($sabitdil->id); ?>'">
            <?php foreach($diller as $dil): ?>
                <option <?php if($dil->id==Request::segment('4')): ?> selected <?php endif; ?> value='<?php echo e($dil->id); ?>'><?php echo e($dil->UzunAd); ?></option>
            <?php endforeach; ?>
        </select>
    </th>
    <th><?php echo e($sabitdil->UzunAd); ?></th>
</tr>
<?php foreach($dilsabitleri as $d): ?>
<tr>
    <td><label class="control-label"><?php echo e($d->Slug); ?></label></td>
    <td><input type="text" class="form-control" value="<?php echo e(@$defaultunceviri[$d->Slug]); ?>" name="" placeholder="<?php if(isset($gecerlidil[$d->Slug])): ?><?php echo e($gecerlidil[$d->Slug]); ?> (geçerli dil)<?php else: ?><?php echo e($d->SabitAdi); ?> (sistem)<?php endif; ?>" readonly /></td>
    <td><input type="text" class="form-control" value="<?php echo e(@$diltanimlamalari[$d->Slug]); ?>" name="Ceviri[<?php echo e($d->id); ?>]" placeholder="<?php if(isset($diltanimlamalari[$d->Slug])): ?> <?php echo e($diltanimlamalari[$d->Slug]); ?> <?php endif; ?>"></td>
</tr>
<?php endforeach; ?>
</table>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>