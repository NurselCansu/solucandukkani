
<?php $__env->startSection('Title','Tools'); ?>
<?php $__env->startSection('MetaTag',$menu->MetaTag); ?>
<?php $__env->startSection('MetaDescription',$menu->MetaDescription); ?>

<?php $__env->startSection('content'); ?>

<div class="breadcrumb-box">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="/<?php echo e($dil); ?>/"><?php echo e(\App\Http\Fnk::Ceviri('anasayfa')); ?></a> </li>
            <li class="active"><?php echo e($menu->Adi); ?></li>
        </ul>	
    </div>
</div><!-- .breadcrumb-box -->

<section style="padding-top: 0px !important;" id="main">


    <div class="container">
        <div style="margin-top:20px;" class="row">
            <form name="search-form" class="col-sm-12 col-md-6 col-md-offset-3 search-form">
                <input class="search-string form-control ui-autocomplete-input" type="search" placeholder="<?php echo e(\App\Http\Fnk::Ceviri('hizli-tool-arama')); ?>" name="search-string" id="searchproduct" autocomplete="off">
            </form>
        </div>
        <div class="row">
           
            <div id="catalog" class="content col-sm-12 col-md-12">
                <div class="products grid row">
                    <?php foreach($tools as $element): ?>
 
                    <div style="margin-bottom:30px;" class="col-sm-4 col-md-3">
                        <div class="serit" style="height: 4px;background-color: #E6E6E6;margin-top: 1px;margin-bottom: 1px;"> </div>
                        
                        <a href=" <?php echo e(url($dil.'/'.\App\Http\Fnk::Ceviri('tool-url').'/'.$element->id)); ?>">
                            <div class="default">
                                <div  class="product-image">
                                    <div  class="hover14" >
                                        <figure >
                                            <div>
                                                <img style="width:100%; "  src="<?php echo e(url('/images/uploads/Tools/KapakResim/'.$element->KapakResim )); ?>"></img>
                                            </div>
                                        </figure>
                                    </div>

                                </div>
                                <div class="product-description">
                                    <div class="serit" > </div>
                                  
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php endforeach; ?>
                </div>
                <?php /* <?php echo $tools->render(); ?> */ ?>
            </div><!-- .content -->


        </div>
    </div><!-- .container -->
</section><!-- #main -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<style>
    .producttitle{
        text-decoration: none !important;
        font-weight: bold !important;
        text-align: left;
        text-indent: 10px;
        background-color: #643234;
        text-decoration: none;
        border-top-right-radius: 15px;
    }
    .producttitle a{
        color:white !important;
        text-decoration: none !important;
    }
    /* Shine */
    .hover14 figure {
        position: relative;
    }
    .hover14 figure::before {
        position: absolute;
        top: 0;
        left: -75%;
        z-index: 2;
        display: block;
        content: '';
        width: 50%;
        height: 100%;
        background: -webkit-linear-gradient(left, rgba(255,255,255,0) 0%, rgba(255,255,255,.3) 100%);
        background: linear-gradient(to right, rgba(255,255,255,0) 0%, rgba(255,255,255,.3) 100%);
        -webkit-transform: skewX(-25deg);
        transform: skewX(-25deg);
    }
    .hover14 figure:hover::before {
        -webkit-animation: shine .75s;
        animation: shine .75s;
    }
    @-webkit-keyframes shine {
        100% {
            left: 125%;
        }
    }
    @keyframes  shine {
        100% {
            left: 125%;
        }
    }
    figure {
        width: 100%;
        margin: 0;
        padding: 0;
        /* background: #d29292; */
        overflow: hidden;
        position: absolute;
    }
    figure:hover+span {
        bottom: -36px;
        opacity: 1;
    }
    .product-description{
        background-color: #f2f2f2;
        color: black;
        margin-top: -6px;
        text-decoration: none;
        padding-left: 10px;
        text-align: left;
    }
    .product-description:hover{
        text-decoration: none !important;
    }
    @media (min-width: 991px)
    {
        .sidebar .menu li a{
            padding: 9px 9px 9px 9px !important;
        }
        span.open-sub {
            margin-left: -40px !important;
        }
        .sidebar .menu li.parent.active > a .open-sub:before, .sidebar .menu li.parent.active > a .open-sub:after {
            background: #0099cc !important;
        }
        .sidebar .menu li.active > a:before {
            background: rgba(255, 255, 255, 0) !important;
        }
        .caroufredsel_wrapper {
            height: 38px !important;
        }
        .banners {
            height: 38px !important;
        }

    }
</style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>