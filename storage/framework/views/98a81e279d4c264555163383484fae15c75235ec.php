<?php $__env->startSection('Title',$menu->MetaTitle); ?>
<?php $__env->startSection('MetaTag',$menu->MetaTag); ?>
<?php $__env->startSection('MetaDescription',$menu->MetaDescription); ?>

<?php $__env->startSection('content'); ?>

<div role="main" class="main">
    <section class="page-header page-header-custom-background parallax" style="background-color: rgba(57, 74, 9, 0.85);" data-plugin-parallax data-plugin-options='{"speed": 1.5}' data-image-src="<?php echo e(asset('img/parallax.jpg')); ?>">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                <ul class="breadcrumb breadcrumb-valign-mid">
                    <li><a href="<?php echo e(url('/'.$dil)); ?>"><?php echo e(\App\Http\Fnk::Ceviri('anasayfa')); ?></a></li>
                    <li class="active"><?php echo e(\App\Http\Fnk::Ceviri($menu->Adi)); ?></li>
                </ul>
                <h1><?php echo e(\App\Http\Fnk::Ceviri($menu->Adi)); ?></h1>
            </div>
        </div>
    </div>
</section>


<div class="clearfix"></div>    
<br/>          
<div class="clearfix"></div>           
    <div class="container">
        <div class="row">
            
            <div class="col-md-12 col-xs-12">
                <?php if(!empty($kategori->Icerik)): ?>
                    <div  class="urun urun-aciklama col-sm-12 col-md-12 col-xs-12">
                        <?php echo $kategori->Icerik; ?>

                    </div>
                <?php endif; ?>
            <div class="clearfix"></div>
            <div class="clearfix"></div>

            <?php foreach($urunler as $element): ?>
            <?php if($element->Gosterim == 3): ?>
                <div  class="urun col-sm-3 col-md-3 ">
                <a href="<?php echo e(url($dil.'/urunler/kategori/'.$element->Urun_AltKat)); ?>">
                <span class="thumb-info">
                    <span class="thumb-info-wrapper">
                        <img src="<?php echo e(url('/images/uploads/Urun/resim/'.json_decode($element->Resim)[0])); ?>" class="img-responsive" alt="" style="height: 250px; width:150% ; padding: 20px" >
                        <span class="thumb-info-title">
                            <span class="thumb-info-inner"><?php echo e($element->Adi); ?></span>
                            <span class="thumb-info-type">İncele</span>
                        </span>
                        <span class="thumb-info-action">
                            <span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
                        </span>
                    </span>
                </span>
                </a> 
                </div>
            <?php else: ?>
                <div  class="urun col-sm-3 col-md-3 ">
                <a href="<?php echo e(url($dil.'/'.\App\Http\Fnk::Ceviri('urun-url').'/'.$element->Slug)); ?>">
                    <span class="thumb-info">
                        <span class="thumb-info-wrapper">
                            <img src="<?php echo e(url('/images/uploads/Urun/resim/'.json_decode($element->Resim)[0])); ?>" class="img-responsive" alt="" style="height: 250px; width:150% ; padding: 20px" >
                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"><?php echo e($element->Adi); ?></span>
                                <span class="thumb-info-type">İncele</span>
                            </span>
                            <span class="thumb-info-action">
                            <span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
                        </span>
                    </span>
                </span>
                </a> 
                </div>
            <?php endif; ?>
            <?php endforeach; ?>
            </div>
                <div style="float: right;">
                    <?php echo $urunler->render(); ?>

                </div>
        </div>
    </div>
</div>
</div><!-- .container -->

<?php $__env->stopSection(); ?>


<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>