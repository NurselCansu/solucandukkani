<?php $__env->startSection('title', 'Operatör Detay'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading"><?php echo $__env->yieldContent('title'); ?></div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
				<tr>
					<th style="width:200px;">Operatör Adı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->Adi); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Makine Marka</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->MakineMarka); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Telefon</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>+9<?php echo e($veri->Telefon); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">E-Posta</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->Eposta); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Adres</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->IkametAdresi); ?></td>
				</tr>

				<tr>
					<th style="width:200px;">Çalışma Alanı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e(($veri->yer_alti_mi==0)?'Yeraltı Makine':'Yerüstü Makine'); ?></td>
				</tr>

				<tr>
					<th style="width:200px;">Şuan Çalıştığı Firma</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->SimdikiFirma); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Önceki Çalıştığı Firma</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->OncekiFirma); ?></td>
				</tr>
				
				<tr>
					<th style="width:200px;">Operatör İş İlanı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->CalismaDurumu); ?></td>
				</tr>
				

                <tr>
					<th style="width:200px;">Beden</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->Beden); ?></td>
				</tr>
                <tr>
					<th style="width:200px;">Yedek Parça Talep</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->YedekParcaTalep); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Durum</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e(($veri->is_active==1)?'Aktif':'Pasif'); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Oluşturulma Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e(Fnk::TarihDuzenle($veri->created_at)); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Güncelleme Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e(Fnk::TarihDuzenle($veri->updated_at)); ?></td>
				</tr>
				<tr>
					<td colspan="3"><a href="<?php echo e(URL::previous()); ?>" class="btn btn-primary pull-right">Geri Dön</a></td>
				</tr>
			</table>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>