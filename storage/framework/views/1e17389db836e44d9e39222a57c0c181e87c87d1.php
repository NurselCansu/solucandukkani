
	<meta charset="utf-8">
	<title><?php echo $__env->yieldContent('Title'); ?></title>
	<meta name="keywords" content="<?php echo $__env->yieldContent('MetaTag'); ?>"/>
	<meta name="description" content="<?php echo $__env->yieldContent('MetaDescription'); ?>" /> 
	<meta name="robots" content="index, follow"> 
    <meta name="author" content="Portakal Yazılım - www.portakalyazilim.com.tr">
	<meta class="viewport" name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">


	<!-- Favicon -->
		<link rel="shortcut icon" href="<?php echo e(url('images/uploads/'.$ayarlar->Favicon)); ?>" type="image/x-icon" />
		<link rel="apple-touch-icon" href="<?php echo e(url('images/uploads/'.$ayarlar->Favicon)); ?>">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo e(asset('vendor/bootstrap/css/bootstrap.min.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('vendor/font-awesome/css/font-awesome.min.css')); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo e(asset('style.css')); ?>">
		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo e(asset('css/custom.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('dist/sweetalert.css')); ?>">
		



		

		