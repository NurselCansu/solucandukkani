<?php $__env->startSection('baslik', 'Google Site Doğrulama Kodu'); ?>

<?php $__env->startSection('form'); ?>

<div class="form-group">
    <label for="content" class="col-sm-2 control-label">Content Doğrulama Kodu</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->content); ?>" name="content" id="content" placeholder="Doğrulama Kodu (content)">
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<script type="text/javascript">
    jQuery(document).ready(function($) {
       $('.gizli').hide();
       $('.is_active').hide();
        
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>