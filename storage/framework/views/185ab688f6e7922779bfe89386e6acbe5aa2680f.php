<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:image="http://www.google.com/schemas/sitemap-image/1.1"
        xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">
    <?php foreach( Cache::get('sitemap') as $url => $params ): ?>
    <url>
        <loc><?php echo e($url); ?></loc>
        <lastmod><?php echo e($params['lastmod']); ?></lastmod>
        <changefreq><?php echo e($params['changefreq']); ?></changefreq>
        <priority><?php echo e($params['priority']); ?></priority>
    </url>
    <?php endforeach; ?>
</urlset>