

<?php $__env->startSection('Title',$menu->MetaTitle); ?>
<?php $__env->startSection('MetaTag',$menu->MetaTag); ?>
<?php $__env->startSection('MetaDescription',$menu->MetaDescription); ?>
<?php $__env->startSection('content'); ?>

<div class="breadcrumb-box"> 
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="/<?php echo e($dil); ?>/"><?php echo e(\App\Http\Fnk::Ceviri('anasayfa')); ?></a> </li>
            <li class="active"><?php echo e(\App\Http\Fnk::Ceviri('fotograf-galerileri')); ?></li>
        </ul>	
    </div>
</div><!-- .breadcrumb-box -->

<section id="main" style="padding-top: 30px">
    <header class="page-header" style="margin-bottom:20px">
        <div class="container">
            <h6 class="title"><?php echo e(\App\Http\Fnk::Ceviri('fotograf-galerileri')); ?></h6>
        </div>  
    </header>
    <div class="container">
        <div class="row" >
            <div class="content gallery col-sm-12 col-md-12">
                <div class="clearfix"></div>
                <div class="rsContent" style="padding-bottom: 40px;">
                    <img  src="<?php echo e(url('/images/uploads/FotoGaleri/fsayfa/'.@$fotograf )); ?>" width="100%" height="300" alt="">
                </div>
                <div id="photo_gallery">
                    <div class="filter-elements">
                        <div class="web-design col-xs-10 col-sm-10 col-md-12"></div>
                        <?php foreach($fotogaleri as $galeri ): ?>
                        <div class="web-design col-xs-10 col-sm-5 col-md-3">
                            <a href="<?php echo e(url($dil.'/'.\App\Http\Fnk::Ceviri('foto-galeri-url').'/'.@$galeri->id)); ?>" class="work">
                                <div id="photo_galleries" style="box-shadow: 0 1px 2px rgba(0,0,0,0.4);
                                     border-radius: 5px;background-position: center;
                                     background-image: url(<?php echo e(url('images/uploads/FotoGaleri/kapak/'.@$galeri->KapakFotografi)); ?>); 
                                     height: 210px; background-repeat: no-repeat;background-size: cover;">     
                                </div>
                            </a>                            
                           <div class="type" align="center">
                                <?php if( $dil == "tr" ): ?>
                                    <?php echo e(@explode(',', $galeri->Adi)[0]); ?>

                                <?php elseif( $dil == "en" ): ?>
                                    <?php echo e(@explode(',', $galeri->Adi)[1]); ?>

                                <?php elseif( $dil == "es" ): ?>
                                    <?php echo e(@explode(',', $galeri->Adi)[2]); ?>

                                <?php elseif( $dil == "ru" ): ?>
                                    <?php echo e(@explode(',', $galeri->Adi)[3]); ?>

                                <?php elseif( $dil == "fr" ): ?>
                                    <?php echo e(@explode(',', $galeri->Adi)[4]); ?>

                                <?php endif; ?>
                            </div>
                         </div>
                        <?php endforeach; ?>
                        <div class="clearfix"></div>
                        <div class="web-design col-xs-10 col-sm-10 col-md-12" style="margin-top: -35px;"></div>
                    </div>
                    <div class="col-sm-12 col-md-12">
                    </div>
                </div>
            </div><!-- .content -->
        </div>
    </div><!-- .container -->
</section><!-- #main -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>