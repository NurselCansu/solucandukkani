<?php $__env->startSection('Title',$menu->MetaTitle); ?>
<?php $__env->startSection('MetaTag',$menu->MetaTag); ?>
<?php $__env->startSection('MetaDescription',$menu->MetaDescription); ?>

<?php $__env->startSection('content'); ?>


<div role="main" class="main">
<section class="page-header page-header-light page-header-reverse page-header-more-padding">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb breadcrumb-valign-mid">
					<li><a href="<?php echo e(url('/'.$dil)); ?>"><?php echo e(\App\Http\Fnk::Ceviri('anasayfa')); ?></a></li>
					<li class="active"><?php echo e($menu->Adi); ?></li>
				</ul>
				<h1><?php echo e($menu->Adi); ?></h1>
			</div>
		</div>
	</div>
</section>

	<div class="container">
		<div class="row "> 
			<?php echo $menu->Icerik; ?>

		</div>	
	</div>

</div>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>