<?php $__env->startSection('footer'); ?>
<?php /* <div id="portakallogo" class="portakal">
    <a style="height:20px;display:none" id="siyahlogo" target="_blank" href="http://www.portakalyazilim.com.tr"><img style="height:25px;" src="<?php echo e(url('images/portakalyazilim.gif')); ?>"></img></a>
    <a style="height:20px;display:none" id="renklilogo" target="_blank" href="http://www.portakalyazilim.com.tr"><img style="height:25px;" src="<?php echo e(url('images/portakalsiyah.png')); ?>"></img></a>
</div> */ ?>

<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row sidebar">
                <aside class="col-xs-12 col-sm-12 col-md-3 widget social">
                    <div class="title-block">
                        <h3 class="title"><?php echo e(\App\Http\Fnk::Ceviri('takip-edin')); ?></h3>
                    </div>
                    <p><?php echo e(\App\Http\Fnk::Ceviri('bizi-sosyal-medyada-takip-edin')); ?></p>
                    <div class="social-list">
                        <?php foreach($sosyal as $element): ?>
                        <a class="icon rounded icon-<?php echo e($element->Adi); ?>" <?php echo e($element->LinkAcilisTipi); ?> href="<?php echo e(url($element->Link)); ?>"><i class="fa fa-<?php echo e($element->Adi); ?>"></i></a>
                        <?php endforeach; ?>
                    </div>
                    <div class="clearfix"></div>
                </aside>
                <aside class="col-xs-12 col-sm-12 col-md-3 widget links">
                    <div class="title-block">
                        <h3 class="title"><?php echo e(\App\Http\Fnk::Ceviri('sayfalar')); ?></h3>
                    </div>
                    <?php foreach($altmenuler as $element): ?>
                    <a <?php echo e($element->LinkAcilisTipi); ?> 
                        <?php if($element->MenuTipi=='icerik'): ?>
                        href="/<?php echo e($dil); ?>/AltMenu/<?php echo e($element->Slug); ?>"
                        <?php else: ?>
                        href="<?php echo e(url($element->Link)); ?>"
                        <?php endif; ?>
                        ><?php echo e($element->Adi); ?></a><br>
                    <?php endforeach; ?>
                </aside>
                <aside class="col-xs-12 col-sm-12 col-md-3 widget">
                    <div class="title-block">
                        <h3 class="title"><?php echo e(\App\Http\Fnk::Ceviri('iletisim')); ?></h3>
                    </div>
                    <div class="phone col-sm-12 col-xs-12">
                        <div class="footer-icon">
                            <i class="verticaltop fa fa-phone" aria-hidden="true"></i>
                        </div><a href="tel:<?php echo e($iletisimbilgileri->Telefon); ?>" class="tel-link" style="color: #8c8c8c "><?php echo e($iletisimbilgileri->Telefon); ?></a>
                    </div>
                    <div class="phone col-sm-12 col-xs-12">
                        <div class="footer-icon">
                            <i class="verticaltop fa fa-fax" aria-hidden="true"></i>
                        </div><?php echo e($iletisimbilgileri->Fax); ?>

                    </div>
                    <div class="phone col-sm-12 col-xs-12">
                        <div class="footer-icon">
                            <i class="verticaltop fa fa-envelope" aria-hidden="true"></i>
                        </div><a href="mailto:<?php echo e($iletisimbilgileri->Eposta); ?>" style="color: #8c8c8c "><?php echo e($iletisimbilgileri->Eposta); ?></a>
                    </div>
                    <div class="phone col-sm-12 col-xs-12">
                        <div class="footer-icon">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                        </div><?php echo e($sube->Adres); ?>

                    </div>
                </aside>

                <aside class="col-xs-12 col-sm-12 col-md-3 widget links">

                    <div class="map-box col-sm-12 col-md-12">
                        <div 
                            style="height: 140px; margin-top:30px;"
                            class="map-canvas"
                            data-zoom="13"
                            data-lat="<?php echo e(@explode(',', $subeler->first()->Kordinat)[0]); ?>"
                            data-lng="<?php echo e(@explode(',', $subeler->first()->Kordinat)[1]); ?>"
                            data-title="<?php echo e($sube->SubeAdi); ?>"
                            data-content="<?php echo e($sube->Adres); ?>"></div>
                    </div>
                </aside>
            </div>
        </div>
    </div><!-- .footer-top -->

    <div class="footer-bottom">
        <div class="container">
            <div class="row col-sm-12">
                <div class="copyright col-xs-12 col-sm-6 col-md-6">
                    Copyright © 1985-<?php echo e(date('Y')); ?> <?php echo e($ayarlar->FirmaAdi); ?><br>
                   <a href="#" data-toggle="modal" data-target=".bd-example-modal-sm" style="color:#848484"> Web Tasarım ve Programlama : <span style="font-size: 10px">PORTAKAL YAZILIM</span>
                    </a> 
                    <div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content" align="center" style="padding:7px;padding-top:10px;  ">
                                <p>Delkom Grup web sayfası tasarım ve kodlaması Portakal Yazılım tarafından, 
                                günümüzün en son teknoloji standartlarına uygun olarak yapılmıştır.
                                Bu standartlar uygulanırken arama motorlarında sıralama, her türlü platformdan ve cihazdan erişebilirlik, 
                                responsive uyumluluğu, hızlı yüklenme, güncelleme ve bakım işlemlerinin yönetilebilir ve kolay olması, 
                                hukuksal sorumluluk gibi değerler gözetilmiştir.Sayfa tasarlanırken ve kodlanırken uygulanan teknolojilerden 
                                bazıları şunlardır: W3C, HTML5, Ajax, Jquery, Php, Laravel, MVC, MySQL.
                                Web sunucusu hizmeti Portakal Yazılım'ın Türkiye lokasyonlu kendi sunucuları üzerinden sağlanmaktadır.
                                Proje hakkında detaylı bilgi için <a href="http://www.portakalyazilim.com.tr/tr/delkom-grup-web-sitesi/" target="_blank" rel="follow">tıklayınız</a>.</p>
                                <a href="http://www.portakalyazilim.com.tr/tr/delkom-grup-web-sitesi/" target="_blank" rel="follow"><img src="<?php echo e(url('images/portakal.png')); ?>" style="width:50%; height:50%;"></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="plogo">
                   
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-sm-offset-6 col-md-offset-6">
                    <a href="#" class="up">
                        <span class="glyphicon glyphicon-arrow-up"></span>
                    </a>
                </div>
            </div>
        </div>
    </div><!-- .footer-bottom -->
</footer>
<script type="text/javascript">
        
        $(document).ready(function(){

                window.oncontextmenu = function () {
                    return false;
                }
        });

        $(document).keydown(function(event){
            if(event.keyCode==123){
                return false;
        }
            else if(event.ctrlKey && event.shiftKey && event.keyCode==73){        
                return false;  //Prevent from ctrl+shift+i
        }
        });

</script>
<?php $__env->stopSection(); ?>


