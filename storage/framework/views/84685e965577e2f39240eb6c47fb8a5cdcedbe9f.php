

<?php $__env->startSection('header'); ?>


<header id="header" class="no-sticky">

            <div id="header-wrap">

                <div class="container clearfix">

                    <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                    <!-- Logo
                    ============================================= -->
                    <div id="logo" class="nobottomborder">
                        <a href="index.html" class="standard-logo" data-dark-logo="<?php echo e(url('images/footer-logo.png')); ?>"><img src="<?php echo e(url('images/footer-logo.png')); ?>" alt="Canvas Logo"></a>
                        <a href="index.html" class="retina-logo" data-dark-logo="<?php echo e(url('images/footer-logo.png')); ?>"><img src="<?php echo e(url('images/footer-logo.png')); ?>" alt="Canvas Logo"></a>
                    </div><!-- #logo end -->

                    <!-- Primary Navigation
                    ============================================= -->
                    <nav id="primary-menu">

                        <?php echo Fnk::Menuler(); ?>

                            

                    </nav><!-- #primary-menu end -->

                    <div class="clearfix visible-md visible-lg">
                        <a href="#" class="social-icon si-small si-borderless si-facebook">
                            <i class="icon-facebook"></i>
                            <i class="icon-facebook"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-borderless si-twitter">
                            <i class="icon-twitter"></i>
                            <i class="icon-twitter"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-borderless si-gplus">
                            <i class="icon-gplus"></i>
                            <i class="icon-gplus"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-borderless si-pinterest">
                            <i class="icon-pinterest"></i>
                            <i class="icon-pinterest"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-borderless si-github">
                            <i class="icon-github"></i>
                            <i class="icon-github"></i>
                        </a>
                    </div>

                </div>

            </div>

        </header><!-- #header end -->


<?php $__env->stopSection(); ?>
