

<?php $__env->startSection('Title',$menu->MetaTitle); ?>
<?php $__env->startSection('MetaTag',$menu->MetaTag); ?>
<?php $__env->startSection('MetaDescription',$menu->MetaDescription); ?>
<?php $__env->startSection('content'); ?>
<div class="breadcrumb-box">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="/<?php echo e($dil); ?>/"><?php echo e(\App\Http\Fnk::Ceviri('anasayfa')); ?></a> </li>
            <li class="active"><?php echo e(\App\Http\Fnk::Ceviri('video-galeriler')); ?></li>
        </ul>	
    </div>
</div><!-- .breadcrumb-box -->

<section id="main" style="padding-top: 30px">
    <header class="page-header" style="margin-bottom: 20px;">
        <div class="container">
            <h6 class="title"><?php echo e(\App\Http\Fnk::Ceviri('video-galeriler')); ?></h6>
        </div>	
    </header>
    <div class="container">
        <div class="row" >
            <div class="content gallery col-sm-12 col-md-12">

                <div class="clearfix"></div>
                
                <div class="rsContent" style="padding-bottom: 40px;">
                    <img  src="<?php echo e(url('/images/uploads/FotoGaleri/sayfa/'.@$fotograf )); ?>" width="1200" height="300" alt="">
                </div>

                <div class="row" id="video_gallery">
                    
                    <div class="row filter-elements">
                         <div class="web-design col-xs-10 col-sm-10 col-md-12"></div>    
                        <?php foreach($videogaleri as $galeri ): ?>
                        <div class="web-design col-xs-10 col-sm-5 col-md-3">
                            <a href="<?php echo e(url('https://www.youtube.com/watch?v='.$galeri->Link)); ?>" class="youtube work">
                                <div style="box-shadow: 0 1px 2px rgba(0,0,0,0.4);
                                     border-radius: 5px;background-position: center;
                                     background-image: url(<?php echo e(url('images/uploads/FotoGaleri/kapak/'.@$galeri->KapakFotografi)); ?>); 
                                     height: 210px; background-repeat: no-repeat;background-size: cover;"></div>
                            </a>
                            <div class="type" align="center"><?php echo e(@$galeri->Adi); ?></div>

                        </div>
                        
                        <?php endforeach; ?>
                         <div class="clearfix"></div>
                        <div class="web-design col-xs-10 col-sm-10 col-md-12" style="margin-top: -35px;"></div>
                    </div>
                    <div class="col-sm-12 col-md-12">
                    </div>
                </div>
            </div><!-- .content -->
        </div>
    </div><!-- .container -->
</section><!-- #main -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(url('plugins/fancyBox/source/jquery.fancybox.css?v=2.1.5')); ?>" media="screen" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script type="text/javascript" src="<?php echo e(url('plugins/fancyBox/source/jquery.fancybox.js?v=2.1.5')); ?>"></script>
<script>
$("a.youtube").click(function() {
    $.fancybox({
            'padding'       : 0,
            'autoScale'     : false,
            'transitionIn'  : 'none',
            'transitionOut' : 'none',
            'title'         : this.title,
            'width'         : 680,
            'height'        : 495,
            'href'          : this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
            'type'          : 'swf',
            'swf'           : {
                 'wmode'        : 'transparent',
                'allowfullscreen'   : 'true'
            }
        });

    return false;
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>