<?php $__env->startSection('baslik', 'Ayarlar'); ?>

<?php $__env->startSection('form'); ?>

<div class="form-group">
    <label for="FirmaAdi" class="col-sm-2 control-label">Firma Adı</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->FirmaAdi); ?>" name="FirmaAdi" id="FirmaAdi" placeholder="Firma Adı">
    </div>
</div>
<div class="form-group">
    <label for="Logo" class="col-sm-2 control-label">Logo</label>
    <div class="col-sm-10">
        <img src="<?php echo e(url('images/uploads/'.$ayarlar->Logo)); ?>" style="max-height: 70px;margin-bottom: 10px;" id="anlikresim" class="img-rounded" />
      <input type="file" class="form-control" name="Logo" id="Logo" placeholder="Logo">
    </div>
</div>


<div class="form-group">
    <label for="is_Kampanyali_Urunler" class="col-sm-2 control-label">Anasayfada Gösterilecek Ürün Tipi</label>
    <div class="col-sm-10">
        <select class="form-control" id="is_Kampanyali_Urunler" required name="is_Kampanyali_Urunler">
            <option <?php if(@$veri->is_Kampanyali_Urunler == 0 ): ?> selected <?php endif; ?> value="0" >Başlıca Ürünleri Göster</option>
            <option <?php if(@$veri->is_Kampanyali_Urunler == 1 ): ?> selected <?php endif; ?> value="1" >Kampanyalı Ürünleri Göster</option>
        </select>
    </div>
</div>


<div class="form-group">
    <label for="Adi" class="col-sm-2 control-label">Ana Sayfa Yazısı Başlık</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->AnaSayfaBaslik); ?>" name="AnaSayfaBaslik" id="Adi" placeholder="Ana Sayfa Yazısı Başlığı">
    </div>
</div>
<div class="form-group">
    <label  class="col-sm-2 control-label">Ana Sayfa İçerik Sütun 1</label>
    <div class="col-sm-10">
        <textarea name="AnaSayfaIcerikSutun1" class="ck"><?php echo e(@$veri->AnaSayfaIcerikSutun1); ?></textarea>
    </div>
</div>
<div class="form-group">
    <label  class="col-sm-2 control-label">Ana Sayfa İçerik Sütun 2</label>
    <div class="col-sm-10">
        <textarea name="AnaSayfaIcerikSutun2" class="ck"><?php echo e(@$veri->AnaSayfaIcerikSutun2); ?></textarea>
    </div>
</div>
<div class="form-group">
    <label  class="col-sm-2 control-label">Ana Sayfa Alt İçerik Sütun 1</label>
    <div class="col-sm-10">
        <textarea name="AnaSayfaAltIcerikSutun1" class="ck"><?php echo e(@$veri->AnaSayfaAltIcerikSutun1); ?></textarea>
    </div>
</div>
<div class="form-group">
    <label  class="col-sm-2 control-label">Ana Sayfa Alt İçerik Sütun 2</label>
    <div class="col-sm-10">
        <textarea name="AnaSayfaAltIcerikSutun2" class="ck"><?php echo e(@$veri->AnaSayfaAltIcerikSutun2); ?></textarea>
    </div>
</div>
<div class="form-group">
    <label  class="col-sm-2 control-label">Ana Sayfa Alt İçerik Sütun 3</label>
    <div class="col-sm-10">
        <textarea name="AnaSayfaAltIcerikSutun3" class="ck"><?php echo e(@$veri->AnaSayfaAltIcerikSutun3); ?></textarea>
    </div>
</div>

<!--22.12.2016 günü anasayfadaki en alt kısma sadece fotoğraf eklenmek istendiği için iptal edilmiştir -->
<!-- <div class="form-group">
    <label  class="col-sm-2 control-label">Ana Sayfa Alt Slider Başlık </label>
    <div class="col-sm-10">
        <textarea name="AnaSayfaAltSliderBaslik" class="ck"><?php echo e(@$veri->AnaSayfaAltSliderBaslik); ?></textarea>
    </div>
</div>
<div class="form-group">
    <label  class="col-sm-2 control-label">Ana Sayfa Alt Slider Sütun 1</label>
    <div class="col-sm-10">
        <textarea name="AnaSayfaAltSliderSutun1" class="ck"><?php echo e(@$veri->AnaSayfaAltSliderSutun1); ?></textarea>
    </div>
</div>
<div class="form-group">
    <label  class="col-sm-2 control-label">Ana Sayfa Alt Slider Sütun 2</label>
    <div class="col-sm-10">
        <textarea name="AnaSayfaAltSliderSutun2" class="ck"><?php echo e(@$veri->AnaSayfaAltSliderSutun2); ?></textarea>
    </div>
</div>
<div class="form-group">
    <label  class="col-sm-2 control-label">Ana Sayfa Alt Slider Sütun 3</label>
    <div class="col-sm-10">
        <textarea name="AnaSayfaAltSliderSutun3" class="ck"><?php echo e(@$veri->AnaSayfaAltSliderSutun3); ?></textarea>
    </div>
</div> -->

<div class="form-group">
    <label  class="col-sm-2 control-label">Alt Resim</label>
    <div class="col-sm-10">
        <?php if(!empty($ayarlar->AnasayfaResim)): ?>
            <img src="<?php echo e(url('images/uploads/'.$ayarlar->AnasayfaResim)); ?>" style="max-height: 70px;margin-bottom: 10px;" id="anlikresim" class="img-rounded" />
        <?php endif; ?>
            <input type="file" class="form-control" name="AnaSayfaResim" id="AnaSayfaResim" placeholder="AnasayfaResim">
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Analytics</label>
    <div class="col-sm-10">
        <textarea class="form-control" name="Analytics" id="Analytics"><?php echo e(@$veri->Analytics); ?></textarea>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Analytics Account Id</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->AnalyticsAccountId); ?>" name="AnalyticsAccountId" id="AnalyticsAccountId" placeholder="Analytics Account Id">
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Açıklama</label>
    <div class="col-sm-10">
        Analytics Account Id alabilmek için google.com/analytics girdikten sonra üst menüden Yönetici > Kullanıcı Yönetimi > "serviceaccount@portakal-yazilim.iam.gserviceaccount.com" Ekle > Ayarları Görüntüle > Görünüm Kimliği buradaki id koplayalıp yapıştırın!
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Meta Keywords</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->MetaTag); ?>" name="MetaTag" id="MetaTag" placeholder="Meta Keywords">
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Meta Title</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->MetaTitle); ?>" name="MetaTitle" id="MetaDescription" placeholder="Meta Description">
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Meta Description</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->MetaDescription); ?>" name="MetaDescription" id="MetaDescription" placeholder="Meta Description">
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>