<?php $__env->startSection('footer'); ?>
<footer id="footer" class="" style="background-color: #1ABC9C">

			<div class="container">

				
			</div>

			<!-- Copyrights
			============================================= -->
			<div id="copyrights" style="background-color: #1ABC9C">

				<div class="container clearfix">

					<div class="col-md-6">
						<div class="col-md-10" style="color: white"> Copyrights &copy; <?php echo e(date('Y')); ?> <?php echo e($ayarlar->FirmaAdi); ?><br>

				<a class="modal_web" href="#myModal2" data-lightbox="inline"  style="color: white" >Web Tasarım ve Programlama : <span style="font-size: 10px">PORTAKAL YAZILIM</span></a>
				<div class="modal1 mfp-hide" id="myModal2">
					<div class="dark divcenter" style=" padding:20px; background-size: cover; max-width: 400px" data-height-lg >
						<div class="modal-content" align="center" style="background: white; border-color: white; padding:7px;padding-top:10px;  ">
							<br>
							<br>
							<p style="color: black;" align="justify">Ambalaj Tasarım web sayfası tasarım ve kodlaması Portakal Yazılım tarafından, 
								günümüzün en son teknoloji standartlarına uygun olarak yapılmıştır.
								Bu standartlar uygulanırken arama motorlarında sıralama, her türlü platformdan ve cihazdan erişebilirlik, 
								responsive uyumluluğu, hızlı yüklenme, güncelleme ve bakım işlemlerinin yönetilebilir ve kolay olması, 
								hukuksal sorumluluk gibi değerler gözetilmiştir.Sayfa tasarlanırken ve kodlanırken uygulanan teknolojilerden 
								bazıları şunlardır: W3C, HTML5, Ajax, Jquery, Php, Laravel, MVC, MySQL.
								Web sunucusu hizmeti Portakal Yazılım'ın Türkiye lokasyonlu kendi sunucuları üzerinden sağlanmaktadır.
								.</p>
								<div ><button style="background-color: #1ABC9C" title="Kapat" type="button" class="mfp-close">×</button></div>

								<a href="http://www.portakalyazilim.com.tr/tr" target="_blank" rel="follow"><img src="<?php echo e(url('/images/portakalyazilim.gif')); ?>" "></a>



								
							</div>
						</div>

			</div><!-- #copyrights end -->

		</footer>
<?php $__env->stopSection(); ?>


