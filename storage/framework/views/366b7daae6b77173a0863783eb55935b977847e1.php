<?php $__env->startSection('Title',$urun->MetaTitle); ?>
<?php $__env->startSection('MetaTag',$urun->MetaTag); ?>
<?php $__env->startSection('MetaDescription',$urun->MetaDescription); ?>



<?php $__env->startSection('content'); ?>


<section class="page-header page-header-light page-header-reverse page-header-more-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb breadcrumb-valign-mid">
                    <li><a href="<?php echo e(url($dil)); ?>"><?php echo e(\App\Http\Fnk::Ceviri('anasayfa')); ?></a> </li>
                    <li><a href="<?php echo e(url($dil.'/'.\App\Http\Fnk::Ceviri('urunler-url').'/'.@$urun->Urun_UstKat)); ?>"><?php echo e(\App\Http\Fnk::Ceviri('urunler')); ?></a> </li>
                    <li class="active"><?php echo e($urun->Adi); ?></li>
                </ul>
                <h1><?php echo e($urun->Adi); ?></h1>
            </div>
        </div>
    </div>
</section>
<div id="main" class="page">
    <div class="container">
        <div class="row">
            <article class="col-sm-12 col-md-9 content product-page pull-right">
                <div class="row">
                    <div class="col-sm-12 col-md-8">
                        <div class="image-box">
                            <?php if(!empty(json_decode($urun->Resim))): ?>
                            <?php foreach(json_decode($urun->Resim) as $index => $element): ?>
                            <?php if($index==0): ?>

                            <div class="col-md-7">
                                
                                <a class="img-thumbnail img-thumbnail-hover-icon lightbox" href="<?php echo e(url('/images/uploads/Urun/resim/'.$element)); ?>" data-plugin-options='{"type":"image"}'>
                                    <img class="img-responsive" src="<?php echo e(url('/images/uploads/Urun/resim/'.$element)); ?>" alt="Project Image" style="height: 250px">
                                </a>
                            </div>

                            <?php endif; ?>
                            <?php endforeach; ?>
                            <?php endif; ?>
                           
                               
                    </div>
                    <div class="col-sm-6 col-md-4">
                       
                        <div class="title-box">
                            <h3 style="padding:0 !important"class="title"><?php echo e(\App\Http\Fnk::Ceviri('aciklama')); ?></h3>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-6 bottom-padding">

<!--Bu kısım Ürünlere farklı dilde yazı girilebilmesi için urun veritabanından farklı içerik bölümlerini çekiyor-->
                            <?php
                            $dil=Request::segment(1);
                            ?>
                                
                        <?php if($dil=="en"): ?>
                                <?php echo $urun->Icerik2; ?>

                        <?php elseif($dil=="de"): ?>
                                <?php echo $urun->Icerik3; ?>

                        <?php elseif($dil=="fr"): ?>
                                <?php echo $urun->Icerik4; ?>

                        <?php else: ?>        
                                <?php echo $urun->Icerik; ?>

                        <?php endif; ?>


                                
                            </div>
                        </div>
                       
                    </div>
                    <div class="clearfix"></div>
                </div>
                
<div class="divider divider-solid">
    <i class="fa fa-chevron-down"></i>
</div>
               <div class="row">
                        <div class="col-md-12">
                            <div class="tabs">
                                <ul class="nav nav-tabs nav-justified">
                                    <li class="active">
                                        <a href="#brosur" data-toggle="tab" class="text-center"><i class="fa fa-star"></i> Broşür</a>
                                    </li>
                                    <li>
                                        <a href="#manuel" data-toggle="tab" class="text-center">Manuel</a>
                                    </li>
                                    <li>
                                        <a href="#soru" data-toggle="tab" class="text-center">Soru Formu</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div id="brosur" class="tab-pane active" >
                                    <p></p>

                                        <?php if ( ! (empty($urun->Brosur))): ?>
                                        <ul class="list list-icons list-primary">
                                        <li id="brosurTık1" class="appear-animation animated fadeInUp appear-animation-visible" data-appear-animation="fadeInUp" data-appear-animation-delay="0"><i class="fa fa-check"></i> inline göstermek için <a href="#" onClick="return false;" id="Btık1">tıklayınız</a></li>
                                        <li id="brosurTık2" class="appear-animation animated fadeInUp appear-animation-visible" data-appear-animation="fadeInUp" data-appear-animation-delay="400"><i class="fa fa-check"></i>
                                        yeni pencerede göstermek için <a id="Btık2" target="_blank" href="<?php echo e(url('/images/uploads/Tools/Brosur/'.$urun->Brosur)); ?>">tıklayınız</a></li>

                                        <li id="brosurTık3" class="appear-animation animated fadeInUp appear-animation-visible" data-appear-animation="fadeInUp" data-appear-animation-delay="800"><i class="fa fa-check"></i>
                                        indirmek için <a id="Btık3" href="<?php echo e(url('/images/uploads/Tools/Brosur/'.$urun->Brosur)); ?>" download>tıklayınız</a></li>  

                                        </ul>
                                        <iframe id="brosurInline" width="100%" style="display:none" height="100%" src="<?php echo e(url('/pdfjs/web/viewer.html?file='.url('/images/uploads/Tools/Brosur/'.$urun->Brosur).'#page=1&zoom=page-width')); ?>"></iframe>
                                        <?php endif; ?>
                                    </div>
                                    <div id="manuel" class="tab-pane" >
                                       <p></p>

                                        <?php if ( ! (empty($urun->Manuel))): ?>
                                        <ul class="list list-icons list-primary">
                                        <li id="manuelTık1" class="appear-animation animated fadeInUp appear-animation-visible" data-appear-animation="fadeInUp" data-appear-animation-delay="0"><i class="fa fa-check"></i> inline göstermek için <a id="Mtık1">tıklayınız</a></li>
                                        <li id="manuelTık2" class="appear-animation animated fadeInUp appear-animation-visible" data-appear-animation="fadeInUp" data-appear-animation-delay="400"><i class="fa fa-check"></i>
                                        yeni pencerede göstermek için <a id="Mtık2" target="_blank" href="<?php echo e(url('/images/uploads/Tools/Manuel/'.$urun->Manuel)); ?>">tıklayınız</a></li>

                                        <li id="manuelTık3" class="appear-animation animated fadeInUp appear-animation-visible" data-appear-animation="fadeInUp" data-appear-animation-delay="800"><i class="fa fa-check"></i>
                                        indirmek için <a id="Mtık3" href="<?php echo e(url('/images/uploads/Tools/Manuel/'.$urun->Manuel)); ?>" download>tıklayınız</a></li>  

                                        </ul>
                                        <iframe id="manuelInline" width="100%" style="display:none" height="100%" src="<?php echo e(url('/pdfjs/web/viewer.html?file='.url('/images/uploads/Tools/Manuel/'.$urun->Manuel).'#page=1&zoom=page-width')); ?>"></iframe>
                                        <?php endif; ?>    
                                    </div>
                                    <div id="soru" class="tab-pane" >
                                        <?php if ( ! (empty($urun->Soru))): ?>
                                        <p></p>
                                        <ul class="list list-icons list-primary">
                                        <li id="soruTık1" class="appear-animation animated fadeInUp appear-animation-visible" data-appear-animation="fadeInUp" data-appear-animation-delay="0"><i class="fa fa-check"></i> indirmek için <a href="<?php echo e(url('/images/uploads/Tools/Soru/'.$urun->Soru)); ?>" id="Stık1">tıklayınız</a></li>

                                        </ul>
                                        
                                        <?php endif; ?>    


                                    </div>
                                </div>
                            </div>
                        </div>
 
            </article><!-- .content -->
<div class="col-md-3">
   <div id="sidebar" class="sidebar col-sm-12 col-md-12">
        <aside class="widget menu">
                <header>
                    <h3 class="heading-primary"><?php echo e(\App\Http\Fnk::Ceviri('urun-kategorileri')); ?></h3>
                </header>
                
                    <?php echo \App\Http\Fnk::Kategoriler(); ?>

                
        </aside><!-- .menu-->
    </div><!-- #sidebar -->
</div> 
        </div>
    </div>
</div><!-- #main -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>

<script type="text/javascript">
    
    $(document).ready(function() {
        $('#Btık1').click(function(event) {
            
            $('#brosurInline').show('slow');
            $('#brosurTık1').hide('slow');
            $('#brosur').css('height','600');

        });
        $('#Btık2').click(function(event) {
            $('#brosur').css('height','auto');
            $('#brosurInline').hide('slow');
            $('#brosurTık1').show('slow');
            $('#brosurTık2').show('slow');

        });
        
        $('#Mtık1').click(function(event) {
            
            $('#manuelInline').show('slow');
            $('#manuelTık1').hide('slow');
            $('#manuel').css('height','600');

        });
        $('#Mtık2').click(function(event) {
            $('#manuel').css('height','auto');
            $('#manuelInline').hide('slow');
            $('#manuelTık1').show('slow');
            $('#manuelTık2').show('slow');

        });
    });


</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>