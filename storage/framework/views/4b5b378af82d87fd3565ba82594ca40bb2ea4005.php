


<?php $__env->startSection('Title',$menu->MetaTitle); ?>
<?php $__env->startSection('MetaTag',$menu->MetaTag); ?>
<?php $__env->startSection('MetaDescription',$menu->MetaDescription); ?>

<?php $__env->startSection('content'); ?>
<section id="main">
	<header class="page-header" style="margin-bottom: 0px;  padding-left: 15px; left: 16px;">
		<div class="container">
			<h5 class="title"><?php echo e($menu->MetaTitle); ?></h5>
		</div>	
	</header>
	
	<div class="container">
		<div class="row">
			<article class="bottom-padding content col-sm-12 col-md-12">
				
				<div class="clearfix"></div>
				<div class="rsContent" style="padding-bottom: 20px;">

					<img  src="<?php echo e(url('/images/uploads/ihracat/'.$ihracat[0]->BuyukResim )); ?>" width="1200" height="625" alt="">

				</div>
				
				<hr style="background-color:#dddddd;  width: 103%; margin-left:-10;"">
				
				<div class="col-sm-6 col-md-6">
					<div class="title-box">
						<h2 class="title">DELKOM</h2>
					</div>
					<p>
						<?php echo $ihracat[0]->IhracatIcerik; ?>

					</p>
					
				</div>
			
				<div class="col-sm-6 col-md-6  overflow">
		 			<div class="manufactures carousel-box load overflow bottom-padding" data-carousel-autoplay="true">
						<div class="title-box">
			  				<a class="next" href="#">
								<svg x="0" y="0" width="9px" height="16px" viewBox="0 0 9 16" enable-background="new 0 0 9 16" xml:space="preserve">
				  					<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#fcfcfc" points="1,0.001 0,1.001 7,8 0,14.999 1,15.999 9,8 "></polygon>
								</svg>
			  				</a>
			  				<a class="prev" href="#">
								<svg x="0" y="0" width="9px" height="16px" viewBox="0 0 9 16" enable-background="new 0 0 9 16" xml:space="preserve">
				  					<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#fcfcfc" points="8,15.999 9,14.999 2,8 9,1.001 8,0.001 0,8 "></polygon>
								</svg>
			  				</a>
			  				<h2 class="title"><?php echo e(\App\Http\Fnk::Ceviri('belgelerimiz')); ?></h2>
						</div>
			
						<div class="clearfix"></div>
			
						<div class="row">
			  				<div class="carousel">
								<?php foreach( $ihracat as $key => $value_): ?> 
									<?php if(  count(json_decode($value_->BelgeResim)) > 0 ): ?>
										<?php foreach( json_decode($value_->BelgeResim) as $value ): ?>
											<div class="make-wrapper">
												<a href="<?php echo e(url('images/uploads/ihracat/belgeler/'.$value)); ?>" rel="fancybox"  class="gallery-images">
													<img  src="<?php echo e(url('images/uploads/ihracat/belgeler/'.$value)); ?>" width="128" height="190" alt="">
												</a>
											</div>
										<?php endforeach; ?>
									<?php endif; ?>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
				
				<a><img src="<?php echo e(url('images/ih_'.$dil.'.jpg')); ?>"></a>
				<div class="clearfix"></div>

				<div class="row" style="padding-top: 20px">
	
					<div class="col-sm-4 col-md-4">
					 	<div class="frame frame-padding">
							<img  src="<?php echo e(url('images/Africa.png')); ?>" width="570" height="320" alt="">
		 				</div>					  
					  	<div class="panel-group multi-collapse" id="accordion2">
							<div class="panel panel-default ">
								<div class="panel-heading">
									<div class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion1" href="#collapse21">
											<?php echo e(\App\Http\Fnk::Ceviri('afrika')); ?>

										</a>
									</div>
						  		</div>
						 		
						 		<div id="collapse21" class="panel-collapse collapse ">
									<div class="panel-body" style="text-align: center;">
										<div class="employee">
											<div class="contact"><?php echo e(explode(',', $ihracat[0]->AfrikaKulAdUnvanTelEmail)[0]); ?> </div>
											<div class="contact"><?php echo e(explode(',', $ihracat[0]->AfrikaKulAdUnvanTelEmail)[1]); ?></div>
											<div>
												<div class="contact"><b><?php echo e(\App\Http\Fnk::Ceviri('e-mail')); ?>: </b><a href="mailto:<?php echo e(explode(',', $ihracat[0]->AfrikaKulAdUnvanTelEmail)[3]); ?>" style="color: #8c8c8c "><?php echo e(explode(',', $ihracat[0]->AfrikaKulAdUnvanTelEmail)[3]); ?></a></div>
												<div class="contact"><b><?php echo e(\App\Http\Fnk::Ceviri('phone')); ?>: </b><?php echo e(explode(',', $ihracat[0]->AfrikaKulAdUnvanTelEmail)[2]); ?></div>
											</div>
										</div>					
									</div>
						  		</div>
							</div>	
						</div>
					</div>
					<div class=" col-sm-4 col-md-4">
					 	<div class="frame frame-padding">
							<img  src="<?php echo e(url('images/Asia.png')); ?>" width="570" height="320" alt="">
		 				</div>					  
					  	<div class="panel-group multi-collapse" id="accordion2">
							<div class="panel panel-default ">
								<div class="panel-heading">
									<div class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion2" href="#collapse22">
											<?php echo e(\App\Http\Fnk::Ceviri('asia')); ?>

										</a>
									</div>
						  		</div>
						 		
						 		<div id="collapse22" class="panel-collapse collapse ">
									<div class="panel-body" style="text-align: center;">
										<div class="employee">
											<div class="contact"><?php echo e(explode(',', $ihracat[0]->AsiaKulAdUnvanTelEmail)[0]); ?> </div>
											<div class="contact"><?php echo e(explode(',', $ihracat[0]->AsiaKulAdUnvanTelEmail)[1]); ?></div>
											<div>
												<div class="contact"><b><?php echo e(\App\Http\Fnk::Ceviri('e-mail')); ?>: </b><a href="mailto:<?php echo e(explode(',', $ihracat[0]->AsiaKulAdUnvanTelEmail)[3]); ?>" style="color: #8c8c8c "><?php echo e(explode(',', $ihracat[0]->AsiaKulAdUnvanTelEmail)[3]); ?></a></div>
												<div class="contact"><b><?php echo e(\App\Http\Fnk::Ceviri('phone')); ?>: </b><?php echo e(explode(',', $ihracat[0]->AsiaKulAdUnvanTelEmail)[2]); ?></div>
											</div>
										</div>										
									</div>
						  		</div>
							</div>	
						</div>
					</div>

					<div class=" col-sm-4 col-md-4">
					 	<div class="frame frame-padding">
							<img  src="<?php echo e(url('images/Australia_Oceania.png')); ?>" width="570" height="320" alt="">
		 				</div>					  
					  	<div class="panel-group multi-collapse" id="accordion2">
							<div class="panel panel-default ">
								<div class="panel-heading">
									<div class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion3" href="#collapse23">
											<?php echo e(\App\Http\Fnk::Ceviri('australia')); ?>

										</a>
									</div>
						  		</div>
						 		
						 		<div id="collapse23" class="panel-collapse collapse ">
									<div class="panel-body" style="text-align: center;">
										<div class="employee">
											<div class="contact"><?php echo e(explode(',', $ihracat[0]->AustraliaKulAdUnvanTelEmail)[0]); ?> </div>
											<div class="contact"><?php echo e(explode(',', $ihracat[0]->AustraliaKulAdUnvanTelEmail)[1]); ?></div>
											<div>
												<div class="contact"><b><?php echo e(\App\Http\Fnk::Ceviri('e-mail')); ?>: </b><a href="mailto:<?php echo e(explode(',', $ihracat[0]->AustraliaKulAdUnvanTelEmail)[3]); ?>" style="color: #8c8c8c "><?php echo e(explode(',', $ihracat[0]->AustraliaKulAdUnvanTelEmail)[3]); ?></a></div>
												<div class="contact"><b><?php echo e(\App\Http\Fnk::Ceviri('phone')); ?>: </b><?php echo e(explode(',', $ihracat[0]->AustraliaKulAdUnvanTelEmail)[2]); ?></div>
											</div>
										</div>	
									</div>
						  		</div>
							</div>	
						</div>
					</div>

					<div class="clearfix"></div>

					<div class=" col-sm-4 col-md-4">
					 	<div class="frame frame-padding">
							<img  src="<?php echo e(url('images/Europe.png')); ?>" width="570" height="320" alt="">
		 				</div>					  
					  	<div class="panel-group multi-collapse" id="accordion2">
							<div class="panel panel-default ">
								<div class="panel-heading">
									<div class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion2" href="#collapse">
											<?php echo e(\App\Http\Fnk::Ceviri('europa')); ?>

										</a>
									</div>
						  		</div>
						 		
						 		<div id="collapse" class="panel-collapse collapse ">
									<div class="panel-body" style="text-align: center;">
										<div class="employee">
											<div class="contact"><?php echo e(explode(',', $ihracat[0]->EuropeKulAdUnvanTelEmail)[0]); ?> </div>
											<div class="contact"><?php echo e(explode(',', $ihracat[0]->EuropeKulAdUnvanTelEmail)[1]); ?></div>
											<div>
												<div class="contact"><b><?php echo e(\App\Http\Fnk::Ceviri('e-mail')); ?>: <a href="mailto:<?php echo e(explode(',', $ihracat[0]->EuropeKulAdUnvanTelEmail)[3]); ?>" style="color: #8c8c8c "></b><?php echo e(explode(',', $ihracat[0]->EuropeKulAdUnvanTelEmail)[3]); ?></a></div>
												<div class="contact"><b><?php echo e(\App\Http\Fnk::Ceviri('phone')); ?>: </b><?php echo e(explode(',', $ihracat[0]->EuropeKulAdUnvanTelEmail)[2]); ?></div>
											</div>
										</div>	

									</div>
						  		</div>
							</div>	
						</div>
					</div>
					
					<div class=" col-sm-4 col-md-4">
					 	<div class="frame frame-padding">
							<img  src="<?php echo e(url('images/uzak_dogu.png')); ?>" width="570" height="320" alt="">
		 				</div>					  
					  	<div class="panel-group multi-collapse" id="accordion2">
							<div class="panel panel-default ">
								<div class="panel-heading">
									<div class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion2" href="#collapse25">
											<?php echo e(\App\Http\Fnk::Ceviri('uzak-dogu')); ?>

										</a>
									</div>
						  		</div>
						 		
						 		<div id="collapse25" class="panel-collapse collapse ">
									<div class="panel-body" style="text-align: center;">
										<div class="employee">
											<div class="contact"><?php echo e(explode(',', $ihracat[0]->NAmericaKulAdUnvanTelEmail)[0]); ?> </div>
											<div class="contact"><?php echo e(explode(',', $ihracat[0]->NAmericaKulAdUnvanTelEmail)[1]); ?></div>
											<div>
												<div class="contact"><b><?php echo e(\App\Http\Fnk::Ceviri('e-mail')); ?>: </b><a href="mailto:<?php echo e(explode(',', $ihracat[0]->NAmericaKulAdUnvanTelEmail)[3]); ?>" style="color: #8c8c8c "><?php echo e(explode(',', $ihracat[0]->NAmericaKulAdUnvanTelEmail)[3]); ?></a></div>
												<div class="contact"><b><?php echo e(\App\Http\Fnk::Ceviri('phone')); ?>: </b><?php echo e(explode(',', $ihracat[0]->NAmericaKulAdUnvanTelEmail)[2]); ?></div>
											</div>
										</div>	
									</div>
						  		</div>
							</div>	
						</div>
					</div>

					<div class=" col-sm-4 col-md-4">
					 	<div class="frame frame-padding">
							<img  src="<?php echo e(url('images/South_America.png')); ?>" width="570" height="320" alt="">
		 				</div>					  
					  	<div class="panel-group multi-collapse" id="accordion2">
							<div class="panel panel-default ">
								<div class="panel-heading">
									<div class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion2" href="#collapse26">
											<?php echo e(\App\Http\Fnk::Ceviri('south-america')); ?>

										</a>
									</div>
						  		</div>
						 		
						 		<div id="collapse26" class="panel-collapse collapse ">
									<div class="panel-body" style="text-align: center;">
										<div class="employee">
											<div class="name"><?php echo e(explode(',', $ihracat[0]->SAmericaKulAdUnvanTelEmail)[0]); ?> </div>
											<div class="contact"><?php echo e(explode(',', $ihracat[0]->SAmericaKulAdUnvanTelEmail)[1]); ?></div>
											<div>
												<div class="contact"><b><?php echo e(\App\Http\Fnk::Ceviri('e-mail')); ?>: </b><a href="mailto:<?php echo e(explode(',', $ihracat[0]->SAmericaKulAdUnvanTelEmail)[3]); ?>" style="color: #8c8c8c "><?php echo e(explode(',', $ihracat[0]->SAmericaKulAdUnvanTelEmail)[3]); ?></a></div>
												<div class="contact"><b><?php echo e(\App\Http\Fnk::Ceviri('phone')); ?>: </b><?php echo e(explode(',', $ihracat[0]->SAmericaKulAdUnvanTelEmail)[2]); ?></div>
											</div>
										</div>
	
									</div>
						  		</div>
							</div>	
						</div>
					</div>
					<div class="clearfix"></div>

	  			</div>

			</article><!-- .content -->
		</div>
	</div><!-- .container -->

</article><!-- .content -->
<style type="text/css">
	#main{
		padding-top: 10px;
		padding-bottom: 0px;
	}

</style>
<?php $__env->stopSection(); ?>





<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>