<?php $__env->startSection('Title',$ayarlar->MetaTitle); ?>
<?php $__env->startSection('MetaTag'); ?>
	<?php echo e(implode(', ', json_decode($ayarlar->MetaTag))); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('MetaDescription',$ayarlar->MetaDescription); ?>



<?php $__env->startSection('content'); ?>			
	<div class="slider-container rev_slider_wrapper" >
		<div id="revolutionSlider" class="slider rev_slider manual">
			<ul>
			<?php foreach( $sliders as $slider): ?>
				<li data-transition="fade">
					<img src="<?php echo e(url('/images/uploads/Slider/'.$slider->Resim)); ?>"  
						alt=""
						data-bgposition="center center" 
						data-bgfit="cover" 
						data-bgrepeat="no-repeat" 
						class="rev-slidebg lazy">
					
					<div class="tp-caption tp-caption-photo-label"
						data-x="['left','left','left','left']"
						data-hoffset="['28','28','28','28']" 
						data-y="['bottom','bottom','bottom','bottom']"
						data-voffset="['28','28','28','28']" 
						data-start="500"
						data-basealign="slide" 
						data-transform_in="y:[-300%];opacity:0;s:500;"><?php echo $slider->KisaIcerik; ?></div>
				</li>
			<?php endforeach; ?>	
			</ul>
		</div>			
	</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('css'); ?>
<style type="text/css">	

	.sweet-alert {margin: auto; transform: translateX(-50%);background-size: inherit; }
	.sweet-alert { 
		width: <?php echo e(trim(@$popup->Genislik)); ?>px;
	 	background-image: url("<?php echo e(url('images/uploads/Popup/resim/kapak/'.json_decode(@$popup->ResimKapak)[0] )); ?>") 
	 	
	 }

</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		swal({
  			  title: '<span style="color:<?php echo e(@$popup->BaslikRenk); ?>!important" ><?php echo e(@$popup->Baslik); ?></span>',
			  text: '<span style="color:<?php echo e(@$popup->IcerikRenk); ?>!important" ><?php echo e(@$popup->Icerik); ?></span>',
			  timer: <?php echo e(@$popup->Sure); ?>*1000,
			  animation: "slide-from-top",
			  allowEscapeKey:true,
              allowOutsideClick:true,
			  showConfirmButton: false,
			  imageUrl: "<?php echo e(url('images/uploads/Popup/resim/'.json_decode(@$popup->Resim)[0] )); ?>",
			  imageSize:"<?php echo e(@$popup->FotoGenislik); ?>x<?php echo e(@$popup->FotoUzunluk); ?>",
			  html: true,
		});
	});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>