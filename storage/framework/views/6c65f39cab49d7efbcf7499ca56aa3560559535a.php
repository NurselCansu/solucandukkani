<!DOCTYPE html>
<html lang="tr">
    <head>
        <?php echo $__env->make('Site.Include.Head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        
        <?php echo $__env->yieldContent('css'); ?>
    </head>
    <body class="fixed-header hidden-top">
    <div class="page-box">
        <div class="page-box-content">
            <?php echo $__env->make('Site.Include.Header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php echo $__env->yieldContent('header'); ?>

            <?php echo $__env->yieldContent('content'); ?>

        </div><!-- .page-box-content -->
        
    </div><!-- .page-box -->

    <?php echo $__env->make('Site.Include.Footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <?php echo $__env->yieldContent('footer'); ?>

    <?php echo $__env->make('Site.Include.Scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    
    <?php echo $__env->yieldContent('js'); ?>

</body>
</html>
