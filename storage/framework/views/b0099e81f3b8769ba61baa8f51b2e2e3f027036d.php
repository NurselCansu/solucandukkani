<?php $__env->startSection('baslik'); ?><?php echo e($Title); ?><?php $__env->stopSection(); ?>
<?php $__env->startSection('form'); ?>
<input type="hidden" name="id" value="<?php echo e(@$veri->id); ?>">
<div class="form-group">
    <label for="SabitAdi" class="col-sm-2 control-label">Sabit Adı</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->SabitAdi); ?>" name="SabitAdi" id="SabitAdi" placeholder="Sabit Adi">
    </div>
</div>
<div class="form-group">
    <label for="Slug" class="col-sm-2 control-label">Slug</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->Slug); ?>" name="Slug" id="Slug" placeholder="Slug">
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script type="text/javascript">
    var slug = function(str) {
    var charMap = {Ç:'c',Ö:'o',Ş:'s',İ:'i',I:'i',Ü:'u',Ğ:'g',ç:'c',ö:'o',ş:'s',ı:'i',ü:'u',ğ:'g'};
    str_array = str.split('');

        for(var i=0, len = str_array.length; i < len; i++) {
            str_array[i] = charMap[ str_array[i] ] || str_array[i];
        }
    str = str_array.join('');
    var $slug = '';
    var trimmed = $.trim(str);
    $slug = trimmed.replace(/[^a-z0-9-]/gi, '-').
    replace(/-+/g, '-').
    replace(/^-|-$/g, '');
//    return $slug.toLowerCase();
    return $slug;
}


$( "#SabitAdi" ).keyup(function() {
  $( "#Slug" ).val(slug($(this).val()));
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>