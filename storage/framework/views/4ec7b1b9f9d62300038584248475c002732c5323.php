<?php $__env->startSection('title', 'Ürün Detay'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading"><?php echo $__env->yieldContent('title'); ?></div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
				<tr>
					<th style="width:200px;">Adı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->Adi); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Resim</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><img width='150' height='150' src="<?php echo e(url('images/uploads/Urun/resim/'.json_decode(@$veri->Resim)[0])); ?>" style="max-width: 100%;"></td>
				</tr>
				<tr>
					<th style="width:200px;">Brosur</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><a href="<?php echo e(url('/images/uploads/Tools/Brosur/'.$veri->Brosur)); ?>"><?php echo e($veri->Brosur); ?></a></td>
				</tr>
				<tr>
					<th style="width:200px;">Manuel</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><a href="<?php echo e(url('/images/uploads/Tools/Manuel/'.$veri->Manuel)); ?>"><?php echo e($veri->Manuel); ?></a></td>
				</tr>
				<tr>
					<th style="width:200px;">Soru Formu</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><a href="<?php echo e(url('/images/uploads/Tools/Soru/'.$veri->Soru)); ?>"><?php echo e($veri->Soru); ?></a></td>
				</tr>

				<tr>
					<th style="width:200px;">Kısa Açıklama</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo $veri->KisaAciklama; ?></td>
				</tr>
                <tr>
					<th style="width:200px;">Meta Keywords</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->MetaTag); ?></td>
				</tr>
                                <tr>
					<th style="width:200px;">Meta Title</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->MetaTitle); ?></td>
				</tr>
                                <tr>
					<th style="width:200px;">Meta Description</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->MetaDescription); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Durum</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e(($veri->is_active==1)?'Aktif':'Pasif'); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Oluşturulma Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e(Fnk::TarihDuzenle($veri->created_at)); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Güncelleme Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e(Fnk::TarihDuzenle($veri->updated_at)); ?></td>
				</tr>
				<tr>
					<td colspan="3"><a href="<?php echo e(URL::previous()); ?>" class="btn btn-primary pull-right">Geri Dön</a></td>
				</tr>
			</table>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>