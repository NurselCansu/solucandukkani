<!-- Google Analytics Ayarlaması -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', '<?php echo e(@$google_analytics->Analytics); ?>', 'auto');
  ga('send', 'pageview');

</script>


<!-- Vendor -->
        
        <script src="<?php echo e(asset('vendor/jquery/jquery.min.js')); ?>"></script>
        <script src="<?php echo e(asset('vendor/bootstrap/js/bootstrap.min.js')); ?>"></script>
        <script src="<?php echo e(asset('vendor/common/common.min.js')); ?>"></script>
        <script src="<?php echo e(asset('vendor/jquery.validation/jquery.validation.min.js')); ?>"></script>
        <script src="<?php echo e(asset('vendor/jquery.gmap/jquery.gmap.min.js')); ?>"></script>
        <script src="<?php echo e(asset('vendor/jquery.lazyload/jquery.lazyload.min.js')); ?>"></script>
        <script src="<?php echo e(asset('vendor/magnific-popup/jquery.magnific-popup.min.js')); ?>"></script>
      
        
        <!--Owl-Carousel için-->
       
        <script src="<?php echo e(asset('/plugins/owl.carousel/owl-carousel/owl.carousel.js')); ?>"></script>

        <!-- Theme Base, Components and Settings -->
        <script src="<?php echo e(asset('js/theme.js')); ?>"></script>
        
        <!-- Current Page Vendor and Views -->
        <script src="<?php echo e(asset('vendor/rs-plugin/js/jquery.themepunch.tools.min.js')); ?>"></script>
        <script src="<?php echo e(asset('vendor/rs-plugin/js/jquery.themepunch.revolution.min.js')); ?>"></script>
        
        <!-- Theme Custom -->
        <script src="<?php echo e(asset('js/custom.js')); ?>"></script>

        <!-- Current Page Vendor and Views -->
     
        <script src="<?php echo e(asset('vendor/nivo-slider/jquery.nivo.slider.min.js')); ?>"></script>
        <script src="<?php echo e(asset('js/views/view.home.js')); ?>"></script>
        <!-- Current Page Vendor and Views -->
       <script src="<?php echo e(asset('js/views/view.contact.js')); ?>"></script>
        <!-- Demo -->
        <script src="<?php echo e(asset('js/demos/demo-photography.js')); ?>"></script>
       
        <!-- Theme Initialization Files -->
        <script src="<?php echo e(asset('js/theme.init.js')); ?>"></script>

        <script src="<?php echo e(asset('dist/sweetalert.min.js')); ?>"></script>

        

        <!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        
            ga('create', 'UA-12345678-1', 'auto');
            ga('send', 'pageview');
        </script>
         -->
 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQKVYMlwil4C_gTM-yE5VdAbRGeqt-MBo"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
////////////////////Mesaj İletimin başlangıcı///////////////////////
        $("#contactform").submit(function(event) {
            swal({
                title: "<?php echo e(\App\Http\Fnk::Ceviri('durum')); ?>" ,
                text: "<?php echo e(\App\Http\Fnk::Ceviri('bekle')); ?>",
                timer: 2000,
                showConfirmButton: false,
                html: true
                });
        });
///////////////BU Kısım Mesajın İletim durumunu döndürür belirtir//////////////////////                
        <?php if(!empty(session('status'))): ?>
                <?php if(session('status')=='TRUE'): ?>
                    swal("<?php echo e(\App\Http\Fnk::Ceviri('gonderildi')); ?>", "", "success")
                <?php else: ?>
                    swal("<?php echo e(\App\Http\Fnk::Ceviri('gonderilmedi')); ?>", "", "error")
                <?php endif; ?>
        <?php endif; ?>
//////////////////////////////////////////////////////////////////////////////////////
        $('.lazy').lazyload();
    });
</script>






