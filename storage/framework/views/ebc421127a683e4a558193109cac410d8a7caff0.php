<?php $__env->startSection('content'); ?>

				
	<div class="slider-container rev_slider_wrapper" >
		<div id="revolutionSlider" class="slider rev_slider manual">
			<ul>

			<?php foreach( $sliders as $slider): ?>
				<li data-transition="fade">
					<img src="<?php echo e(url('/images/uploads/Slider/'.$slider->Resim)); ?>"  
						alt=""
						data-bgposition="center center" 
						data-bgfit="cover" 
						data-bgrepeat="no-repeat" 
						class="rev-slidebg">
					
					<div class="tp-caption tp-caption-photo-label"
						data-x="['left','left','left','left']"
						data-hoffset="['28','28','28','28']" 
						data-y="['bottom','bottom','bottom','bottom']"
						data-voffset="['28','28','28','28']" 
						data-start="500"
						data-basealign="slide" 
						data-transform_in="y:[-300%];opacity:0;s:500;"><?php echo $slider->KisaIcerik; ?></div>
				</li>
			<?php endforeach; ?>	
			</ul>
		</div>			
	</div>
	

<?php $__env->stopSection(); ?>
<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>