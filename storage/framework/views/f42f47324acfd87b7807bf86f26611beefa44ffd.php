<?php $__env->startSection('Title',$urun->MetaTitle); ?>
<?php $__env->startSection('MetaTag',$urun->MetaTag); ?>
<?php $__env->startSection('MetaDescription',$urun->MetaDescription); ?>



<?php $__env->startSection('content'); ?>

<div class="breadcrumb-box">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="<?php echo e(url($dil)); ?>"><?php echo e(\App\Http\Fnk::Ceviri('anasayfa')); ?></a> </li>
            <li><a href="<?php echo e(url($dil.'/urunler')); ?>"><?php echo e(\App\Http\Fnk::Ceviri('urunler')); ?></a> </li>
            <li class="active"><?php echo e($urun->Adi); ?></li>
        </ul>	
    </div>
</div><!-- .breadcrumb-box -->

<div id="main" class="page">
    <header class="page-header">
        <div class="container">
            <h1 style="float:left" class="title"><?php echo e($urun->Adi); ?></h1>
            <blockquote class="blockquote-reverse"  >
                <p ><strong><?php echo e($urun->SeriNo); ?></strong></p>
                <p ><strong><?php echo e(\App\Http\Fnk::Ceviri('delkom-no')); ?> :</strong><?php echo e($urun->DelkomNo); ?></p>
                <p ><strong> <?php echo e(\App\Http\Fnk::EnUstKategori($urun->UrunKategori->first()->kategori_id)); ?> No : </strong><?php echo e($urun->UreticiSeriNo); ?></p>
            </blockquote>
        </div>	
    </header>
    <div class="container">
        <div class="row">
            <article class="col-sm-12 col-md-9 content product-page pull-right">
                <div class="row">
                    <div class="col-sm-12 col-md-8">
                        <div class="image-box">
                            <?php if(!empty(json_decode($urun->Resim))): ?>
                            <?php foreach(json_decode($urun->Resim) as $index => $element): ?>
                            <?php if($index==0): ?>
                            <div class="images-box">
                                <a class="gallery-images" rel="fancybox" href="<?php echo e(url('/images/uploads/Urun/resim/'.$element)); ?>">
                                    <div class="frame frame-padding frame-shadow-lifted">
                                        <img class="" src="<?php echo e(url('/images/uploads/Urun/resim/'.$element)); ?>" width="100%" height="197" alt="<?php echo e($urun->KisaAciklama); ?>"></img>
                                    </div>
                                    <span class="bg-images"><i class="fa fa-search"></i></span>
                                </a>
                            </div>
                            <?php endif; ?>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            <!-- .images-box -->
                            <div class="thumblist-box">
                                <a href="#" class="prev">
                                    <svg x="0" y="0" width="5px" height="16px" viewBox="0 0 9 16" enable-background="new 0 0 9 16" xml:space="preserve">
                                    <polygon fill-rule="evenodd" clip-rule="evenodd" fill="#fcfcfc" points="8,15.999 9,14.999 2,8 9,1.001 8,0.001 0,8 "></polygon>
                                    </svg>
                                </a>
                                <a href="#" class="next">
                                    <svg x="0" y="0" width="5px" height="16px" viewBox="0 0 9 16" enable-background="new 0 0 9 16" xml:space="preserve">
                                    <polygon fill-rule="evenodd" clip-rule="evenodd" fill="#fcfcfc" points="1,0.001 0,1.001 7,8 0,14.999 1,15.999 9,8 "></polygon>
                                    </svg>
                                </a>
                                <div id="thumblist" class="thumblist">
                                    <?php if(!empty(json_decode($urun->Resim))): ?>
                                    <?php foreach(json_decode($urun->Resim) as $index => $element): ?>
                                    <?php if($index!=0): ?>
                                    <a class="gallery-images" rel="fancybox" href="<?php echo e(url('/images/uploads/Urun/resim/'.$element)); ?>">
                                        <img class="" src="<?php echo e(url('/images/uploads/Urun/resim/'.$element)); ?>" width="100%" height="197" alt="<?php echo e($urun->KisaAciklama); ?>"></img>
                                        <span class="bg-images"><i class="fa fa-search"></i></span>
                                    </a>
                                    <?php endif; ?>
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                </div><!-- #thumblist -->
                            </div><!-- .thumblist -->
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <?php if(!empty($urun->Icerik)): ?>
                        <div class="title-box">
                            <h3 style="padding:0 !important"class="title"><?php echo e(\App\Http\Fnk::Ceviri('aciklama')); ?></h3>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-6 bottom-padding">

                            <?php
                            $dil=Request::segment(1);
                            ?>
                                
                        <?php if($dil=="en"): ?>
                                <?php echo $urun->Icerik2; ?>

                        <?php elseif($dil=="es"): ?>
                                <?php echo $urun->Icerik3; ?>

                        <?php elseif($dil=="ru"): ?>
                                <?php echo $urun->Icerik4; ?>

                        <?php elseif($dil=="fr"): ?>
                                <?php echo $urun->Icerik5; ?>

                        <?php else: ?>        
                                <?php echo $urun->Icerik; ?>

                        
                            
                       
                    
                        <?php endif; ?>


                                
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </article><!-- .content -->
            <div id="sidebar" class="sidebar col-sm-12 col-md-2">
                <aside class="widget menu">
                    <header>
                        <h3 class="title"><?php echo e(\App\Http\Fnk::Ceviri('urun-kategorileri')); ?></h3>
                    </header>
                    <nav>
                        <?php echo \App\Http\Fnk::Kategoriler(); ?>

                    </nav>
                </aside><!-- .menu-->
            </div><!-- #sidebar -->
        </div>
    </div>
</div><!-- #main -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<style>
    @media (min-width: 991px)
    {
        .sidebar .menu li a{
            padding: 9px 9px 9px 9px !important;
        }
        span.open-sub {
            margin-left: -40px !important;
        }
        .sidebar .menu li.parent.active > a .open-sub:before, .sidebar .menu li.parent.active > a .open-sub:after {
            background: #0099cc !important;
        }
        .sidebar .menu li.active > a:before {
            background: rgba(255, 255, 255, 0) !important;
        }
        .caroufredsel_wrapper {
            height: 38px !important;
        }
        .banners {
            height: 38px !important;
        }
    }

    blockquote p {
        font-family: 'Times New Roman', Times, serif;
        font-style: normal;
        
    }
</style>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>