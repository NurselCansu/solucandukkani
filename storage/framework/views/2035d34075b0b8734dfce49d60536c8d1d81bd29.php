<?php $__env->startSection('baslik', 'reCaptcha'); ?>

<?php $__env->startSection('form'); ?>

<div class="form-group">
    <label for="site_key" class="col-sm-2 control-label">Site Key</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->site_key); ?>" name="site_key" id="site_key" placeholder="site_key">
    </div>
</div>
<div class="form-group">
    <label for="secret_key" class="col-sm-2 control-label">Secret Key</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->secret_key); ?>" name="secret_key" id="secret_key" placeholder="secret_key">
    </div>
</div>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<script type="text/javascript">
    jQuery(document).ready(function($) {
       $('.gizli').hide();
       $('.is_active').hide();
        
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>