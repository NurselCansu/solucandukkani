<?php $__env->startSection('Title',$menu->MetaTitle); ?>
<?php $__env->startSection('MetaTag',$menu->MetaTag); ?>
<?php $__env->startSection('MetaDescription',$menu->MetaDescription); ?>


<?php $__env->startSection('content'); ?>


    <div id="googlemaps" class="google-map mt-none mb-lg">
        
    </div>     
 
<div role="main" class="main">
    <section class="page-header page-header-light page-header-reverse page-header-more-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumb breadcrumb-valign-mid">
                        <li><a href="<?php echo e(url('/'.$dil)); ?>"><?php echo e(\App\Http\Fnk::Ceviri('anasayfa')); ?></a></li>
                        <li class="active"><?php echo e(\App\Http\Fnk::Ceviri('iletisim')); ?></li>
                    </ul>
                    <h1><?php echo e(\App\Http\Fnk::Ceviri('iletisim')); ?></h1>
                </div>
            </div>
        </div>
    </section>

    <!-- Google Maps - Go to the bottom of the page to change settings and map location. -->
     


    <div class="container">
        <div class="row">
            <div class="content col-sm-12 col-md-12">
                <div class="row">
                    <div class="col-sm-6 col-md-6 contact-info bottom-padding">
                        <?php foreach($subeler as $sube): ?>
                        
                        <?php if($sube["iletisimde_goster"] == 1 and \App\Http\Models\Dil::select('id')->where('KisaAd',$dil)->first()->id == $sube["DilId"]): ?>
                        <div class="row">
                            <div class="col-sm-6 col-md-6" >
                                <address style="margin-bottom:15px">
                                    <div class="title"><?php echo e($sube->SubeAdi); ?></div>
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>  <?php echo e($sube->Adres); ?>

                                </address>
                                <address style="margin-bottom:15px">
                                    <div><i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:<?php echo e($sube->Telefon); ?>" class="tel-link"><?php echo e($sube->Telefon); ?></a></div>
                                </address>
                                <address style="margin-bottom:15px">
                                    <div><i class="fa fa-fax" aria-hidden="true"></i>  <?php echo e($sube->Fax); ?></div>
                                </address>
                                <address style="margin-bottom:15px">
                                    <div><i class="fa fa-envelope" aria-hidden="true"></i>  <a href="mailto:<?php echo e($sube->Eposta); ?>"><?php echo e($sube->Eposta); ?></a></div>
                                </address>
                            </div>
                            <div class="col-sm-6 col-md-6 ">
                                <div class="map-box col-sm-12 col-md-12">
                                    <div
                                        style="height: 200px;"
                                        class="map-canvas"
                                        data-zoom="15"
                                        data-lat="<?php echo e(@explode(',', $sube->Kordinat)[0]); ?>"
                                        data-lng="<?php echo e(@explode(',', $sube->Kordinat)[1]); ?>"
                                        data-title="<?php echo e($sube->SubeAdi); ?>"
                                        data-content="<?php echo e($sube->Adres); ?>"></div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                    <div class="col-sm-6 col-md-6 bottom-padding">
                        <form id="contactform" class="form-box register-form contact-form" method="POST" action="<?php echo e(url($dil.'/'.\App\Http\Fnk::Ceviri('iletisim-url'))); ?>" id="form">
                            <input type='hidden' name='_token' value='<?php echo e(csrf_token()); ?>' ></input>
                            <h3 class="title"><?php echo e(\App\Http\Fnk::Ceviri('mesaj-gonderin')); ?></h3>
                            <div id="success"></div>
                            <label><?php echo e(\App\Http\Fnk::Ceviri('adiniz-soyadiniz')); ?>: <span class="required">*</span></label>
                            <input class="form-control" id="AdSoyad" required name="AdSoyad"></input>
                            <label><?php echo e(\App\Http\Fnk::Ceviri('e-posta-adresiniz')); ?>: <span class="required">*</span></label>
                            <input class="form-control" type="email" id="inputEmail3" name="Eposta" required></input>
                            <label><?php echo e(\App\Http\Fnk::Ceviri('telefon-numaraniz')); ?>: </label>
                            <input class="form-control" type="text" data-inputmask-clearIncomplete="true" data-inputmask="'mask': '0(199)-999-9999'" id="Telefon" name="Telefon"></input>
                            <label><?php echo e(\App\Http\Fnk::Ceviri('konu')); ?>: <span class="required">*</span></label>
                            <input class="form-control" type="text" id="Konu" name="Konu" required></input>
                            <label><?php echo e(\App\Http\Fnk::Ceviri('mesajiniz')); ?>: <span class="required">*</span></label>
                            <textarea class="form-control" id="Mesaj" name="Mesaj" rows="4" required></textarea>
                            <?php echo app('captcha')->display();; ?>

                            <div class="buttons-box clearfix">
                                <button type="submit" class="btn btn-default"><?php echo e(\App\Http\Fnk::Ceviri('gonder')); ?></button>
                                <span class="required"><b>*</b> <?php echo e(\App\Http\Fnk::Ceviri('gerekli-alanlar')); ?></span>
                            </div><!-- .buttons-box -->
                        </form>
                    </div>
                    <div class="map-box col-sm-12 col-md-12">
                        
                        <div class="title-box">
                          
                        </div>
                            <?php     
                                $did = \App\Http\Fnk::DilIdGetir($dil);
                                foreach ($subeler as $key => $value) {
                                    if( $value->DilId == $did and $value->ust_Adres != 0 ){
                                        @$alt_adresler[] = $value;
                                        $alt_idler[] = $value->ust_Adres; 
                                    }elseif( $value->DilId == $did ){
                                        @$ust_adresler[] = $value;
                                    }
                                }
                                ?>
                    

                    <div class="parent-map">
                        <?php foreach($subeler as $key => $sube_): ?>

                           <?php /*  <?php if( $sube_->DilId == \App\Http\Fnk::DilIdGetir($dil) ): ?> */ ?>
                                <div <?php if($key!=0 ): ?> class="location" <?php endif; ?>
                                      <?php if($key==0 ): ?>
                                      style="height: 500px;"
                                      class="map-canvas"
                                      id= "map-canvas"
                                      data-zoom="5"
                                      <?php endif; ?>
                                      data-lat="<?php echo e(@explode(',', $sube_->Kordinat)[0]); ?>"
                                      data-lng='<?php echo e(@explode(',', $sube_->Kordinat)[1]); ?>'
                                      data-title="<?php echo e($sube_->SubeAdi); ?>"
                                      data-content="<?php echo e($sube_->Adres); ?>">
                                </div>
                            <?php /* <?php endif; ?> */ ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- .container -->
</section><!-- #main -->
<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(url('plugins/jquery.inputmask/dist/inputmask/inputmask.js')); ?>"></script>
    <script src="<?php echo e(url('plugins/jquery.inputmask/dist/inputmask/jquery.inputmask.js')); ?>"></script>


    <script>

    $(document).ready(function(){





        $("#googlemaps").gMap({
                controls: {
                    draggable: (($.browser.mobile) ? false : true),
                    panControl: true,
                    zoomControl: true,
                    mapTypeControl: true,
                    scaleControl: true,
                    streetViewControl: true,
                    overviewMapControl: true
                },

                scrollwheel: false,
                markers: [

                //Bu alan dile gore adres çekmeli!!!!
                <?php foreach($subeler as $sube): ?>

                    <?php if($sube["iletisimde_goster"] == 1 and \App\Http\Models\Dil::select('id')->where('KisaAd',$dil)->first()->id == $sube["DilId"]): ?>
                    {
                    address: "<?php echo e($sube->Adres); ?>",
                    html: "<strong><?php echo e($sube->SubeAdi); ?></strong><br/><?php echo e($sube->Telefon); ?>",
                    
                    icon: {
                      
                        image: "<?php echo e(url('img/pointer.png')); ?>",
                        iconsize: [36, 45],
                        iconanchor: [12, 46]
                    }

                },
                    <?php endif; ?>
                <?php endforeach; ?>
                ],
                
                
                zoom: 6
            });

  });
    
    
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>