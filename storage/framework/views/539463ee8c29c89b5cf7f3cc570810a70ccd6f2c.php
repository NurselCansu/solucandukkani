<?php $__env->startSection('content'); ?>
<div role="main" class="main">

		<div class="container">
          <div class="row ">
              
            <section class="call-to-action featured featured-primary mb-xl">
				<div class="call-to-action-content">
				<div class="col-md-5">
					<h2 class="mb-none word-rotator-title mt-lg">
							<strong>
								<span class="word-rotate active" data-plugin-options='{"delay": 3500, "animDelay": 400}'>
								<span class="word-rotate-items" style="width: 450.3438px; top: -42px;">
										<span class="heading-primary"><?php echo $ayarlar->AnaSayfaIcerikSutun1; ?></span>
										<span class="heading-primary"><?php echo $ayarlar->AnaSayfaIcerikSutun2; ?></span>		
								</span>
								</span>
							</strong>	
							<br>
							<p style="color: #0088cc;opacity: 0.67"><?php echo $ayarlar->AnaSayfaIcerikSutun3; ?></p>		
						</h2>
					</div>
					<div class="container">
					<div class="col-md-7">	
					<ul class="list list-icons list-primary">
						<li class="appear-animation" style="<?php if(empty($ayarlar->AnaSayfaAltIcerikSutun1)): ?> display: none <?php endif; ?>" data-appear-animation="fadeInUp" data-appear-animation-delay="0"><i class="fa fa-check"></i><p align="justify"><?php echo $ayarlar->AnaSayfaAltIcerikSutun1; ?></p></li>
						<li class="appear-animation" style="<?php if(empty($ayarlar->AnaSayfaAltIcerikSutun2)): ?> display: none <?php endif; ?>" data-appear-animation="fadeInUp" data-appear-animation-delay="500"><i class="fa fa-check"></i><p align="justify"><?php echo $ayarlar->AnaSayfaAltIcerikSutun2; ?></p></li>
						<li class="appear-animation" style="<?php if(empty($ayarlar->AnaSayfaAltIcerikSutun3)): ?> display: none <?php endif; ?>" data-appear-animation="fadeInUp" data-appear-animation-delay="500"><i class="fa fa-check"></i><p align="justify"><?php echo $ayarlar->AnaSayfaAltIcerikSutun3; ?></p></li>
					</ul>
					</div>
				</div>
				</div>
			</section>

<div class="container col-md-10 col-md-offset-1">
	<div class="row">
		<div class="slider-container light rev_slider_wrapper">
					<div id="revolutionSlider" class="slider rev_slider" style="margin: 0 5% !important;" data-plugin-revolution-slider data-plugin-options='{"delay": 11000, "gridwidth": 970, "gridheight": 300, "disableProgressBar": "on"}'>
						<ul>
						<?php foreach( $sliders as $slider): ?>
							<li data-transition="fade">

								<img src="<?php echo e(url('/images/uploads/Slider/'.$slider->Resim)); ?>"  
									alt=""
									data-bgposition="center center" 
									data-bgfit="cover" 
									data-bgrepeat="no-repeat" 
									data-kenburns="on"
									data-duration="9000"
									data-ease="Linear.easeNone"
									data-scalestart="150"
									data-scaleend="100"
									data-rotatestart="0"
									data-rotateend="0"
									data-offsetstart="0 0"
									data-offsetend="0 0"
									data-bgparallax="0"
									class="rev-slidebg">
							</li>
							<?php endforeach; ?>
						</ul>
			
					</div>
            	</div>
            </div>
        </div>
    </div>     

        	<?php if(count($onecikarilmisurunler)>0): ?>
			<div class="container" style="">	
			    <div class="row">
			    	<div class="row center mt-xl">
            	<h3 class="heading-primary">Ürünlerimiz</h3>
            	<div class="divider divider-primary">
					<i class="fa fa-chevron-down"></i>
				</div>
					<div class="owl-carousel owl-theme owl-ref" data-plugin-options='{"items": 3, "autoplay": true, "autoplayTimeout": 3000}'>
						<?php foreach($onecikarilmisurunler as $element): ?>
							<div>
								 <a href="<?php echo e(url($dil.'/'.\App\Http\Fnk::Ceviri('urun-url').'/'.$element->Slug)); ?>">

                                <span class="thumb-info">
                                        <span class="thumb-info-wrapper">
                                            <img src="<?php if(empty(json_decode($element->Resim)[0])): ?> <?php echo e(url('/images/uploads/'.$ayarlar->Logo)); ?> <?php else: ?><?php echo e(url('/images/uploads/Urun/resim/'.json_decode($element->Resim)[0])); ?><?php endif; ?>" class="img-responsive"  style="height: 200px; width:80% ; padding: 20px " alt="">
                                            <span class="thumb-info-title">
                                                <span class="thumb-info-inner"><?php echo e(\App\Http\Fnk::EnUstKategori($element->UrunKategori->first()->kategori_id)); ?></span>
                                                <span class="thumb-info-type">İncele</span>
                                            </span>
                                            <span class="thumb-info-action">
                                                <span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
                                            </span>
                                        </span>
                                    </span>
                                
                            	</a>
							</div>
						<?php endforeach; ?>		
					</div>		
				</div>
			</div>	
			<?php endif; ?>
        </div>  
    </div>
	<hr>
	<div class="container">
	<div class="row mt-xlg">
        <div class="col-md-12 center">
            <h2 class="mb-none mt-xl font-weight-semibold "  id="Haber">Haberler</h2>
            <!-- <p class="lead mb-xlg">Size en uygun CLR-S ünitesini sunabilmek için </p> -->
            <div class="divider divider-primary divider-small divider-small-center mb-xl">
                <hr>
            </div>

            <div class="owl-carousel owl-theme show-nav-title top-border" data-plugin-options='{"responsive": {"0": {"items": 1}, "479": {"items": 1}, "768": {"items": 2}, "979": {"items": 3}, "1199": {"items": 3}}, "items": 3, "margin": 10, "loop": false, "nav": true, "dots": false}'>
               
                <?php foreach($haber as $element): ?>
                <div>
                    <div class="recent-posts">
                        <article class="post">
                            <div class="owl-carousel owl-theme nav-inside pull-left mr-lg mb-sm" data-plugin-options='{"items": 1, "margin": 10, "animateOut": "fadeOut", "autoplay": true, "autoplayTimeout": 3000}'>
                                <div>
                                    <img alt="" class="img-responsive img-rounded" src="<?php echo e(asset('images/uploads/Haberler/'.$element->Resim)); ?>">
                                </div>
                            </div>
                            <div class="date">
                                <span class="day">15</span>
                                <span class="month">Jan</span>
                            </div>
                            <h4><a href="blog-post.html">SEN BENİM BİTANEMSİNN TAVŞAN! </a></h4>
                            <p><?php echo $element->Icerik; ?><a href="/" class="read-more"> Devamını Oku  <i class="fa fa-angle-right"></i></a></p>
                        </article>
                    </div>
                </div>
                <?php endforeach; ?>
             
            </div>
           
        </div>
    </div>
    </div>
	<hr>
		<section class="video section section-text-light section-video section-center" data-video-path="OZB_Factory.mp4" data-plugin-video-background data-plugin-options='{"posterType": "jpg", "position": "50% 50%", "overlay": true}'>
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<img src="<?php echo e(url('/images/uploads/'.$ayarlar->Logo)); ?>" style="opacity: 0.3">
					</div>
				</div> 
			</div>
		</section>		
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>