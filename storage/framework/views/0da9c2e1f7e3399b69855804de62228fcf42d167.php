<?php $__env->startSection('baslik', 'Kategori'); ?>

<?php $__env->startSection('form'); ?>
<div class="form-group">
    <label for="Adi" class="col-sm-2 control-label">Kategori Adı</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->Adi); ?>" name="Adi" id="Adi" placeholder="Kategori Adı">
    </div>
</div>

<div class="form-group">
    <label for="KategoriSirasi" class="col-sm-2 control-label">Kategori Sırası</label>
    <div class="col-sm-10">
        <input type="number" class="form-control" name="KategoriSirasi" id="KategoriSirasi" value="<?php echo e(@$veri->KategoriSirasi); ?>" placeholder="KategoriSirasi">
    </div>
</div>

<div class="form-group">
    <label for="UstKategoriId" class="col-sm-2 control-label">Üst Kategori</label>
    <div class="col-sm-10">
        <select class="form-control" id="UstKategoriId" required name="UstKategoriId">
            <option value="0" >Lütfen Kategori Seçiniz</option>
            <?php foreach(@$kategoriler as $kategori): ?>
                <option value="<?php echo e($kategori['id']); ?>" <?php if($kategori["id"] == @$veri->UstKategoriId): ?> selected <?php endif; ?>>
                    <?php for($i = 0; $i < $kategori["derinlik"]; $i++): ?>
                    -
                    <?php endfor; ?>
                    <?php echo e($kategori["adi"]); ?></option>
            <?php endforeach; ?>
        </select>
    </div>
</div>

<div class="form-group" hidden>
    <label for="Resim" class="col-sm-2 control-label">Resim</label>
    <div class="col-sm-10">
        <input type="file" class="form-control" name="Resim" id="Resim" placeholder="Resim">
    </div>
</div>
<div class="form-group">
     <label for="Icerik" class="col-sm-2 control-label">Kategori İçeriği</label>
    <div class="col-sm-10">
        <textarea required id="Icerik" name="Icerik" class="ck"><?php echo e(@$veri->Icerik); ?></textarea>
    </div>
</div>
<div class="hide form-group" >
    <label for="Aciklama" class="col-sm-2 control-label" >Açıklama</label>
    <div class="col-sm-10">
        <textarea name="Aciklama" class="ck"><?php echo e(@$veri->Aciklama); ?></textarea>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>

<script type="text/javascript">
    
    $(document).ready(function() {
            $('.gizli').hide();        
    });

</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>