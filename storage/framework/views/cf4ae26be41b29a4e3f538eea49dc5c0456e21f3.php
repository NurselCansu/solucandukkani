<?php $__env->startSection('baslik'); ?><?php echo e($Title); ?><?php $__env->stopSection(); ?>
<?php $__env->startSection('form'); ?>
<input type="hidden" name="id" value="<?php echo e(@$veri->id); ?>">
<div class="form-group">
    <label for="KisaAd" class="col-sm-2 control-label">Kısa Adı</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->KisaAd); ?>" name="KisaAd" id="KisaAd" placeholder="Dil Kısa Adı">
    </div>
</div>
<div class="form-group">
    <label for="UzunAd" class="col-sm-2 control-label">Uzun Adı</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->UzunAd); ?>" name="UzunAd" id="UzunAd" placeholder="Dil Uzun Adı">
    </div>
</div>

<div class="form-group">
    <label for="Resim" class="col-sm-2 control-label">Dil Resmi</label>
    <div class="col-sm-10">
       <?php if(isset($veri) && $veri->Resim!=''): ?> <img src="<?php echo e(url('images/uploads/Dil/'.$veri->Resim)); ?>" style="max-height: 70px;margin-bottom: 10px;" id="anlikresim" class="img-rounded" /> <?php endif; ?>
      <input type="file" class="form-control" name="Resim" id="Resim" placeholder="Resim">
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>