

<?php $__env->startSection('title', 'Slider Detay'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading"><?php echo $__env->yieldContent('title'); ?></div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
				<tr>
					<th style="width:200px;">Dil</th>
					<th style="width: 30px;text-align:center;">:</th>
						<?php
						    $dil = \App\Http\Models\Dil::find($veri->DilId);
						?>
					<td><?php echo e($dil->UzunAd); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Resim Adı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->Adi); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Slug</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->Slug); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Slider</th>
					<th style="width: 30px;text-align:center;">:</th>
                    <td><img width="200" height="200" src="<?php echo e(url('/')); ?>/images/uploads/Slider/<?php echo e($veri->Resim); ?>" /></td>
				</tr>
                <tr>
					<th style="width:200px;">Link Açılış Tipi</th>
					<th style="width: 30px;text-align:center;">:</th>
                    <?php if($veri->LinkAcilisTipi == ''): ?>
						<td>Aynı Pencere</td>
                    <?php else: ?>
                    	<td>Yeni Pencere</td>
                    <?php endif; ?>
				</tr>
                <tr>
					<th style="width:200px;">Slider Tipi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->SliderTipi); ?></td>
				</tr>

				<tr>
					<th style="width:200px;">Kısa İçerik</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->KisaIcerik); ?></td>
				</tr>
                <?php if($veri->SliderTipi == 'link'): ?>
                <tr>
					<th style="width:200px;">Link</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->Link); ?></td>
				</tr>
                <?php else: ?>
                <tr>
					<th style="width:200px;">İçerik</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->Icerik); ?></td>
				</tr>
                <?php endif; ?>
                <tr>
					<th style="width:200px;">Başlangıç Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->baslangic); ?></td>
				</tr>
                <tr>
					<th style="width:200px;">Bitiş Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->bitis); ?></td>
				</tr>
                <tr>
					<th style="width:200px;">Meta Keywords</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->MetaTag); ?></td>
				</tr>
                <tr>
					<th style="width:200px;">Meta Title</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->MetaTitle); ?></td>
				</tr>
                <tr>
					<th style="width:200px;">Meta Description</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->MetaDescription); ?></td>
				</tr>
                <tr>
					<th style="width:200px;">Durum</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e(($veri->is_active==1)?'Aktif':'Pasif'); ?></td>
				</tr>
				
				<tr>
					<td colspan="3"><a href="<?php echo e(URL::previous()); ?>" class="btn btn-primary pull-right">Geri Dön</a></td>
				</tr>
			</table>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>