<?php echo $ayarlar->Analytics; ?>

<!--[if (!IE)|(gt IE 8)]><!-->




<!-- Vendor -->
        
        <script src="<?php echo e(asset('vendor/jquery/jquery.min.js')); ?>"></script>
        <script src="<?php echo e(asset('vendor/bootstrap/js/bootstrap.min.js')); ?>"></script>
        <script src="<?php echo e(asset('vendor/common/common.min.js')); ?>"></script>
        <script src="<?php echo e(asset('vendor/jquery.validation/jquery.validation.min.js')); ?>"></script>
        <script src="<?php echo e(asset('vendor/jquery.gmap/jquery.gmap.min.js')); ?>"></script>
        <script src="<?php echo e(asset('vendor/jquery.lazyload/jquery.lazyload.min.js')); ?>"></script>
        <script src="<?php echo e(asset('vendor/magnific-popup/jquery.magnific-popup.min.js')); ?>"></script>
      
        
        <!--Owl-Carousel için-->
       
        <script src="<?php echo e(asset('/plugins/owl.carousel/owl-carousel/owl.carousel.js')); ?>"></script>

        <!-- Theme Base, Components and Settings -->
        <script src="<?php echo e(asset('js/theme.js')); ?>"></script>
        
        <!-- Current Page Vendor and Views -->
        <script src="<?php echo e(asset('vendor/rs-plugin/js/jquery.themepunch.tools.min.js')); ?>"></script>
        <script src="<?php echo e(asset('vendor/rs-plugin/js/jquery.themepunch.revolution.min.js')); ?>"></script>
        
        <!-- Theme Custom -->
        <script src="<?php echo e(asset('js/custom.js')); ?>"></script>

        <!-- Current Page Vendor and Views -->
     
        <script src="<?php echo e(asset('vendor/nivo-slider/jquery.nivo.slider.min.js')); ?>"></script>
        <script src="<?php echo e(asset('js/views/view.home.js')); ?>"></script>
        <!-- Current Page Vendor and Views -->
       <script src="<?php echo e(asset('js/views/view.contact.js')); ?>"></script>
        <!-- Demo -->
        <script src="<?php echo e(asset('js/demos/demo-photography.js')); ?>"></script>
       
        <!-- Theme Initialization Files -->
        <script src="<?php echo e(asset('js/theme.init.js')); ?>"></script>
        

        <!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        
            ga('create', 'UA-12345678-1', 'auto');
            ga('send', 'pageview');
        </script>
         -->


<script>
$(document).ready(function () {
        var dil="tr";
        if(dil!="<?php echo e(Request::segment(1)); ?>"){
          window.location = '<?php echo e(url("/tr")); ?>';
        }
    });
</script>




