

<?php $__env->startSection('Title',$menu->MetaTitle); ?>
<?php $__env->startSection('MetaTag',$menu->MetaTag); ?>
<?php $__env->startSection('MetaDescription',$menu->MetaDescription); ?>

<?php $__env->startSection('css'); ?>

<style>
    .r {
        border-color: black;
        border-bottom-width: 0.5px;
    }
    .wf {
        width: 100%;
    }
    .bf {
        font-weight: bold;
    }
    #esadi{ display:none;}
    #esyasi{ display:none;}
    #esmeslegiokulu{ display:none;}

    #ehliyettarihi{ display:none;}
    #ehliyetsinifi{ display:none;}
    .hide{display:none;}

</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="breadcrumb-box">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="/<?php echo e($dil); ?>/"><?php echo e(\App\Http\Fnk::Ceviri('anasayfa')); ?></a> </li>
            <li class="active"><?php echo e(\App\Http\Fnk::Ceviri('insan-kaynaklari')); ?></li>
        </ul>	
    </div>
</div><!-- .breadcrumb-box -->
<div id="main" class="page">
    <header class="page-header">
        <div class="container">
            <h6 class="title"><?php echo e(\App\Http\Fnk::Ceviri('is-basvuru-formu')); ?></h6>
        </div>	
    </header>
    <div class="container">
        <div class="row">
            <form class="row no-margin" method="POST" enctype="multipart/form-data" action="<?php echo e(url($dil.'/'.\App\Http\Fnk::Ceviri('insan-kaynaklari-url'))); ?>" id="form">

                <article class="content col-sm-8 col-md-8">

                    <input type='hidden' name='_token' value='<?php echo e(csrf_token()); ?>' ></input>
                    <ul id="checkoutsteps" class="clearfix panel-group">
                        <a href="#step" class="step-title" data-parent="#checkoutsteps" data-toggle="collapse">
                            <h6><?php echo e(\App\Http\Fnk::Ceviri('basvuru-bilgileri')); ?></h6>
                        </a>
                        <div class="step-content">
                            <div class="col-sm-4 col-md-4">
                                <label><?php echo e(\App\Http\Fnk::Ceviri('adi')); ?> <span class="required">*</span></label>
                                <input name="Adi" class="form-control" type="text" ></input>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <label><?php echo e(\App\Http\Fnk::Ceviri('soyadi')); ?> <span class="required">*</span></label>
                                <input name="Soyadi" class="form-control" type="text" ></input>
                            </div>

                            <div class="col-sm-4 col-md-4">
                                <label><?php echo e(\App\Http\Fnk::Ceviri('cinsiyet')); ?> <span class="required">*</span></label>
                                <select name="Cinsiyet" class="form-control">
                                    <option value=""><?php echo e(\App\Http\Fnk::Ceviri('seciniz')); ?></option>
                                    <option value="E" ><?php echo e(\App\Http\Fnk::Ceviri('erkek')); ?></option>
                                    <option value="K" ><?php echo e(\App\Http\Fnk::Ceviri('kadin')); ?></option>
                                </select>
                            </div>

                            <div class="clearfix"></div>
                            
                            <div class="col-sm-4 col-md-4">
                                <label><?php echo e(\App\Http\Fnk::Ceviri('dogum-tarihi')); ?><span class="required">*</span></label>
                                <input name="DogumTarihi" class="form-control" type="date"></input>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <label><?php echo e(\App\Http\Fnk::Ceviri('t-c-kimlik-numarasi')); ?></label>
                                <input name="TcNo" class="form-control" type="number" oninput="validity.valid||(value='');"></input>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <label><?php echo e(\App\Http\Fnk::Ceviri('dogum-yeri-il')); ?> <span class="required">*</span></label>
                                <input name="DogumYeriIl" class="form-control" type="text" ></input>
                                <!-- <select name="DogumYeriIl" class="form-control ">
                                    <option value=""><?php echo e(\App\Http\Fnk::Ceviri('seciniz')); ?></option>
                                    <?php foreach(\App\Http\Fnk::Sehirler() as $kod => $sehir): ?>
                                        <option value="<?php echo e($sehir); ?>"> <?php echo e($sehir); ?> </option>
                                    <?php endforeach; ?>
                                </select> -->
                            </div>

                            <div class="clearfix"></div>
                            
                            <div class="col-sm-4 col-md-4">
                                <label><?php echo e(\App\Http\Fnk::Ceviri('askerlik-durumu')); ?> </label>
                                <select name="AskerlikDurumu" class="form-control">
                                    <option value=""><?php echo e(\App\Http\Fnk::Ceviri('seciniz')); ?></option>
                                    <option value="yapildi" ><?php echo e(\App\Http\Fnk::Ceviri('yapildi')); ?></option>
                                    <option value="yapilmadi" ><?php echo e(\App\Http\Fnk::Ceviri('yapilmadi')); ?></option>
                                    <option value="muaf" ><?php echo e(\App\Http\Fnk::Ceviri('muaf')); ?></option>
                                    <option value="tecilli" ><?php echo e(\App\Http\Fnk::Ceviri('tecilli')); ?></option>
                                </select>
                            </div>

                            <div class="col-sm-4 col-md-4">
                                <label><?php echo e(\App\Http\Fnk::Ceviri('ev-telefonu')); ?></label>
                                <input name="TelefonEv" class="form-control" type="tel" ></input>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <label><?php echo e(\App\Http\Fnk::Ceviri('cep-telefonu')); ?></label>
                                <input name="TelefonCep" class="form-control" type="tel" ></input>
                            </div>

                            <div class="clearfix"></div>

                            <div class="col-sm-4 col-md-4">
                                <label><?php echo e(\App\Http\Fnk::Ceviri('adres')); ?></label>
                                <input name="Adres" class="form-control" rows="1"></input>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <label><?php echo e(\App\Http\Fnk::Ceviri('egitim-durumu')); ?></label>
                                <select name="EgitimSeviyesi" class="form-control">
                                    <option value=""><?php echo e(\App\Http\Fnk::Ceviri('seciniz')); ?></option>
                                    <option value="ilkogretim"><?php echo e(\App\Http\Fnk::Ceviri('ilkogretim')); ?></option>
                                    <option value="lise"><?php echo e(\App\Http\Fnk::Ceviri('lise')); ?></option>
                                    <option value="yuksekokul"><?php echo e(\App\Http\Fnk::Ceviri('yuksekokul')); ?></option>
                                    <option value="lisansustu"><?php echo e(\App\Http\Fnk::Ceviri('lisans-ustu')); ?></option>
                                </select>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <label><?php echo e(\App\Http\Fnk::Ceviri('Mezun-Oldugu-Okul-ve-Bolumu')); ?></label>
                                <input name="YuksekOgretimOkulAdi" class="form-control" type="text">
                            </div>
                            
                            <div class="clearfix"></div>
                          
                            <div class="col-sm-4 col-md-4">
                                <label><?php echo e(\App\Http\Fnk::Ceviri('mezuniyet-durumu-derecesi')); ?> </label>
                                <input name="YuksekOgretimMezuniyetDurumu" class="form-control" type="text">
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <label><?php echo e(\App\Http\Fnk::Ceviri('yabanci-dil')); ?></label>
                                <input name="YabanciDiller[0][Adi]" class="form-control" type="text">
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <label><?php echo e(\App\Http\Fnk::Ceviri('seviye')); ?></label>
                                <select name="YabanciDiller[0][Derecesi]" class="form-control">
                                    <option value=""><?php echo e(\App\Http\Fnk::Ceviri('seciniz')); ?></option>
                                    <option value="baslagic"><?php echo e(\App\Http\Fnk::Ceviri('baslangic')); ?></option>
                                    <option value="orta"><?php echo e(\App\Http\Fnk::Ceviri('orta')); ?></option>
                                    <option value="ileri"><?php echo e(\App\Http\Fnk::Ceviri('ileri')); ?></option>
                                </select>
                            </div>

                            <div class="clearfix"></div>

                            <div id="dileklealani"></div>
                            <div class="col-sm-6 col-md-3 pull-right">
                                <button type="button" id="dilcikar" class=" btn btn-border btn-default btn-block btn-sm"><i class="fa fa-minus" aria-hidden="true"></i> <?php echo e(\App\Http\Fnk::Ceviri('yabanci-dil-cikar')); ?></button>
                            </div>
                            <div class="col-sm-6 col-md-3 pull-right">
                                <button type="button" id="dilekle" class="btn btn-border btn-default btn-block btn-sm"><i class="fa fa-plus" aria-hidden="true"></i> <?php echo e(\App\Http\Fnk::Ceviri('yabanci-dil-ekle')); ?></button>
                            </div>

                            <div class="clearfix"></div>

                            <div class="col-sm-6 col-md-6">
                                <label><?php echo e(\App\Http\Fnk::Ceviri('bilgisayar-kullanma-ve-programlar')); ?></label>
                                <input name="BilgisayarKullanma" class="form-control" type="text">
                            </div>

                            <div class="col-sm-6 col-md-6">
                                <label><?php echo e(\App\Http\Fnk::Ceviri('surucu-belgesi')); ?></label>
                                <select id="ehliyet" name="Ehliyet" class="form-control">
                                    <option value=""><?php echo e(\App\Http\Fnk::Ceviri('seciniz')); ?></option>
                                    <option value="yok"><?php echo e(\App\Http\Fnk::Ceviri('yok')); ?></option>
                                    <option value="var" ><?php echo e(\App\Http\Fnk::Ceviri('var')); ?></option>
                                </select>
                            </div>
                            <div id="ehliyetsinifi" class="col-sm-6 col-md-6">
                                <label><?php echo e(\App\Http\Fnk::Ceviri('sinifi')); ?></label>
                                <select name="EhliyetSinifi" class="form-control">
                                    <option value=""><?php echo e(\App\Http\Fnk::Ceviri('seciniz')); ?></option>
                                    <option value="A2">A2</option>
                                    <option value="B" >B</option>
                                    <option value="C">C</option>
                                    <option value="D" >D</option>
                                    <option value="E">E</option>
                                    <option value="G" >G</option>
                                    <option value="H" >H</option>
                                </select>
                            </div>

                            <div class="clearfix"></div>

                            <div class="col-sm-6 col-md-6">
                                <label><?php echo e(\App\Http\Fnk::Ceviri('bu-isle-ilgili-kullandiginiz-arac-ve-cihazlar')); ?></label>
                                <input name="IsleIlgiliKullanilanAraclar" class="form-control" type="text">
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <label><?php echo e(\App\Http\Fnk::Ceviri('is-basvurusu-yapilan-gorev')); ?></label>
                                <input name="BasvuruYapilanPozisyon" class="form-control" type="text">
                            </div>
                            <div class="clearfix"></div>

                            <div class="col-sm-6 col-md-6">
                                <label><?php echo e(\App\Http\Fnk::Ceviri('referans-adi-soyadi')); ?></label>
                                <input name="Referanslar[0][AdiSoyadi]"  class="form-control" type="text">
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <label><?php echo e(\App\Http\Fnk::Ceviri('telefon-numarasi')); ?></label>
                                <input name="Referanslar[0][Telefon]"  class="form-control" type="text">
                            </div>

                            <div id="referanseklealani"></div>
                            <div class="col-sm-6 col-md-3 pull-right">
                                <button type="button" id="referanscikar" class=" btn btn-border btn-default btn-block btn-sm"><i class="fa fa-minus" aria-hidden="true"></i> <?php echo e(\App\Http\Fnk::Ceviri('referans-cikar')); ?></button>
                            </div>
                            <div class="col-sm-6 col-md-3 pull-right">
                                <button type="button" id="referansekle"  class=" btn btn-border btn-default btn-block btn-sm"><i class="fa fa-plus" aria-hidden="true"> </i> <?php echo e(\App\Http\Fnk::Ceviri('referans-ekle')); ?></button>
                            </div>
                            <div class="clearfix"></div>

                            <div class="col-sm-6 col-md-6">
                            <?php echo app('captcha')->display();; ?>

                            </div>
    
                            <div class="col-sm-6 buttons-box text-right " style="margin-top:33px; margin-left:0px;">
                                <button type="submit" class="btn btn-default"><?php echo e(\App\Http\Fnk::Ceviri('gonder')); ?></button>
                                <span class="required"><b>*</b> <?php echo e(\App\Http\Fnk::Ceviri('doldurulmasi-zorunlu-alanlar')); ?></span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </ul><!-- #checkoutsteps -->

                </article><!-- .content -->
                <article class="content2 col-sm-2 col-md-2">
                    <img class="img-responsive"  src="<?php echo e(url('images/user.jpg')); ?>" id="imagePreview" alt="Preview Image" />
                    <input type="file" name="CvFotograf" id="imageUpload" class="hide"/>
                    <br>
                    <label for="imageUpload" class="col-sm-12 btn btn-default"><?php echo e(\App\Http\Fnk::Ceviri('fotograf-yukleyin')); ?></label>

                    <br>
                    <hr>
                    <h5><?php echo e(\App\Http\Fnk::Ceviri('belge-yukleme')); ?></h5>
                    <input type="file" name="Belge[]" class="hide" id="cvUpload" multiple/>
                    <br>
                    <label for="cvUpload" class="col-sm-12 btn btn-default"><?php echo e(\App\Http\Fnk::Ceviri('cv-belge-yukleyin')); ?></label>
                </article>
            </form>
        </div>
    </div>
</div><!-- #main -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script src="<?php echo e(asset('js/jquery.form.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/jqueryvalidate/jquery.validate.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/jqueryvalidate/messages_tr.js')); ?>"></script>
<script src="<?php echo e(asset('js/pnotify.custom.min.js')); ?>"></script>


<script type="text/javascript">

    function uploadbartemizle(){
        $('#uploadbar').parent('div').hide();
        var bar = $('#uploadbar');
        var percentVal = '0%';
        bar.width(percentVal);
        bar.html(percentVal);
        bar.attr('aria-valuenow',0);
    }
    
    function AjaxPost(url,post,callback){
        post+='&_token=<?php echo e(csrf_token()); ?>'
        jQuery.post("<?php echo e(url('')); ?>/"+url, post, function (data) {
            if(callback && typeof(callback) === "function") {
                callback(data);
            }
        }, "json");
    }
        
    $( document ).ready(function() {
        $('#form').validate({
            ignore: ':hidden:not([class~=selectized]),:hidden > .selectized, .selectize-control .selectize-input input',
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
             submitHandler: function(form) {
                return true;
             }
        });
        
        var bar = $('#uploadbar');
        $('#form').ajaxForm({
            url:'<?php echo e(url($dil.'/insan-kaynaklari')); ?>',
            beforeSend: function() {
                bar.parent('div').show();
                var percentVal = '0%';
                bar.width(percentVal);
                bar.html(percentVal);
                bar.attr('aria-valuenow',0);
                $('#btn').attr('disabled',true);
            },
            uploadProgress: function(event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                bar.attr('aria-valuenow',percentComplete);
                bar.width(percentVal);
                bar.html(percentVal);
            },
            success: function() {
                var percentVal = '100%';
                bar.attr('aria-valuenow',100);
                bar.width(percentVal);
                bar.html(percentVal);
                $('#btn').removeAttr('disabled');
            },
            complete: function(xhr) {
         
                var sonuc = JSON.parse(xhr.responseText);
                console.log(sonuc);
                if(sonuc.islem){
                    
                    alert("İşleminiz Başarıyla Yapılmıştır.");
                    
                    if (typeof(submitafter) === "function") {
                        submitafter(sonuc);
                     }
                    
                    $("#form")[0].reset();
                    
                    if($('.selectize').length>0){
                        $( ".selectize" ).each(function( index ) {
                            if(index%3==0)this.selectize.clear()
                        });
                    }               
                //  jQuery("a[data-toggle=collapse][href=#adim1]").click();
                }else{
                    $.each(sonuc.error, function(index, value) {
                        alert(value);
                    });
                    $('#btn').removeAttr('disabled');
                }
                
                jQuery(document).ready(function () {
                   setTimeout(uploadbartemizle(),3000 );
                });
            }
        });
    });

    $('#imageUpload').change(function () {
        $('#imagePreview').show();
        readImgUrlAndPreview(this);
        function readImgUrlAndPreview(input) {
            if (input.files && input.files[0])
            {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imagePreview').attr('src', e.target.result);
                }
            }
            reader.readAsDataURL(input.files[0]);
        }
    });
    $(document).ready(function () {
        var cocuk = 0;
        var dil = 0;
        var kurs = 0;
        var deneyim = 0;
        var referans = 0;

        $("#dilekle").click(function () {
            dil++;
            $("<div id='diladi" + dil + "' class='col-sm-6 col-md-6'><label><?php echo e(\App\Http\Fnk::Ceviri('Yabanci-Dil')); ?></label>\n\
    <input name='YabanciDiller[" + dil + "][Adi]' class='form-control' type='text'></div>")
                    .insertBefore("#dileklealani");
            $("<div id='dilderecesi" + dil + "' class='col-sm-6 col-md-6'><label><?php echo e(\App\Http\Fnk::Ceviri('Derecesi')); ?></label>\n\
                <select name='YabanciDiller[" + dil + "][Derecesi]' class='form-control'>\n\
                    <option value=''><?php echo e(\App\Http\Fnk::Ceviri('seciniz')); ?></option>\n\
                    <option value='baslangic'><?php echo e(\App\Http\Fnk::Ceviri('Baslangic')); ?></option>\n\
                    <option value='orta' ><?php echo e(\App\Http\Fnk::Ceviri('Orta')); ?></option>\n\
                    <option value='ileri' ><?php echo e(\App\Http\Fnk::Ceviri('ileri')); ?></option>\n\
                </select></div>")
                    .insertBefore("#dileklealani");

        });
        $("#dilcikar").click(function () {
            if (dil > 0) {
                $('#diladi' + dil).remove();
                $('#dilderecesi' + dil).remove();
                dil--;
            }
        });
  
        $("#referansekle").click(function () {
            referans++;
            $("<div id='referansadi" + referans + "' class='col-sm-6 col-md-6'><label><?php echo e(\App\Http\Fnk::Ceviri('Referans-Adi-Soyadi')); ?></label>\n\<input name='Referanslar[" + referans + "][AdiSoyadi]' class='form-control' type='text'></div>")
                    .insertBefore("#referanseklealani");
            $("<div id='referanstelefon" + referans + "' class='col-sm-6 col-md-6'><label><?php echo e(\App\Http\Fnk::Ceviri('Telefon-Numarasi')); ?></label>\n\<input name='Referanslar[" + referans + "][Telefon]' class='form-control' type='text'></div>")
                    .insertBefore("#referanseklealani");
        });
        $("#referanscikar").click(function () {
            if (referans > 0) {
                $('#referansadi' + referans).remove();
                $('#referanstelefon' + referans).remove();
                referans--;
            }
        });
   
        $("#ehliyet").change(function () {
            if ($("#ehliyet").val() == 'var')
            {
                $("#ehliyettarihi").show();
                $("#ehliyetsinifi").show();
            } else
            {
                $("#ehliyettarihi").hide();
                $("#ehliyetsinifi").hide();
            }
        });
    });
</script>


<?php $__env->stopSection(); ?>


<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>