

<?php $__env->startSection('baslik', 'Tools'); ?>

<?php $__env->startSection('form'); ?>
<div class="form-group">
    <label for="Adi" class="col-sm-2 control-label">Adı</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->Adi); ?>" name="Adi" id="Adi" placeholder="Ürün Adı">
    </div>
</div>


<div class="form-group">
    <label for="KapakResim" class="col-sm-2 control-label">Kapak Resim</label>
    <div class="col-sm-10">
      <input type="file" class="form-control" name="KapakResim[]" id="KapakResim" multiple placeholder="Kapak Resim">
    </div>
</div>

<div class="form-group">
    <label for="Pdf" class="col-sm-2 control-label">Pdf</label>
    <div class="col-sm-10">
      <input type="file" class="form-control" name="Pdf[]" id="Pdf"  multiple placeholder="Pdf Ekleyiniz" >
    </div>
</div>



<script type="text/javascript">


</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>