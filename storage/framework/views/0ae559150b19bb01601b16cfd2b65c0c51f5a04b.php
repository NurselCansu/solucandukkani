

<?php
    $url = Request::url();
    $link = explode('Sirala',$url);
    $link = $link[0].'Sirala'
?>
<?php $__env->startSection('title'); ?> <?php echo e($baslik); ?> Sıralama <?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="panel panel-default">
  <div class="panel-heading">
  <span class="pull-left"><?php echo $__env->yieldContent('title'); ?></span>
              
                
                <div style="clear: both;"></div>
            </div>
  <div class="panel-body">
    <p class="alert alert-info">
        <?php echo $__env->yieldContent('title'); ?> İşlemini Sürükle - Bırak Yaparak Sitenizdeki Görünüm Sırasını Değiştirebilirsiniz.
    </p>
    
    <ul id="sortable" class="list-group">
    <?php
        foreach($veri as $v){
            
            echo '<li class="list-group-item" id="id-'.$v->id.'">
                 '.$v->$vtcolumnname.'
        </li>';
        }
    ?>
    </ul>
  </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script src="<?php echo e(asset('plugins/jquery-ui/jquery-ui.js')); ?>"></script>
<script type="text/javascript">
  $(document).ready(function () {
    $('#sortable').sortable({
        axis: 'y',
        stop: function (event, ui) {
            var oData = $(this).sortable('serialize');

            $.ajax({
                data: oData+'&_token=<?php echo e(csrf_token()); ?>',
                type: 'POST',
                dataType: 'json',
                url: '<?php echo e($link); ?>'
                
            }).done(function(data){
                if (data.status) {
                    new PNotify({
                        title: 'Sıralama',
                        text: 'İşleminiz Başarıyla Yapılmıştır.',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                } else {
                    new PNotify({
                        title: 'Hata',
                        text: 'Hata var ulAN.',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                }
            });
    }
    });
    $('li').click(function(){
        

    });

});

 </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('Admin.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>