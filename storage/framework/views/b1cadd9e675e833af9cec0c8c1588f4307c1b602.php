<?php $__env->startSection('Title',$menu->MetaTitle); ?>
<?php $__env->startSection('MetaTag',$menu->MetaTag); ?>
<?php $__env->startSection('MetaDescription',$menu->MetaDescription); ?>


<?php $__env->startSection('content'); ?>

<div role="main" class="main">
    <section class="page-header page-header-custom-background parallax" style="background-color: rgba(57, 74, 9, 0.85);" data-plugin-parallax data-plugin-options='{"speed": 1.5}' data-image-src="<?php echo e(asset('img/parallax.jpg')); ?>">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
				<ul class="breadcrumb breadcrumb-valign-mid">
					<li><a href="<?php echo e(url('/'.$dil)); ?>"><?php echo e(\App\Http\Fnk::Ceviri('anasayfa')); ?></a></li>
					<li class="active"><?php echo e($menu->Adi); ?></li>
				</ul>
				<h1><?php echo e($menu->Adi); ?></h1>
			</div>
		</div>
	</div>	
</section>
<div id="wrapper" >
	<div id="image" >
		<?php foreach( $fotograf as $key=>$foto): ?>
			<div class="image i<?php echo e($key+1); ?>" >
				<a class="img-thumbnail img-thumbnail-hover-icon lightbox sayfa_resim" href="<?php echo e(url('images/uploads/FotoGaleri/'.$foto->Resim)); ?>" data-plugin-options='{"type":"image"}'  >
					<img src="<?php echo e(url('images/uploads/FotoGaleri/'.$foto->Resim)); ?>" style="width: 400px;height: 300px;border-radius: 25px;" >
				</a>	
			</div>
			
		<?php endforeach; ?>	
	</div>
</div>
<br/>
<br/>
<br/>
<br/>

<div class="container">
	<div class="row "> 
		<?php echo $menu->Icerik; ?>

	</div>	
</div>

</div>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>