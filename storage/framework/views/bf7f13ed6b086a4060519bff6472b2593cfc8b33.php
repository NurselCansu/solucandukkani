<?php $__env->startSection('content'); ?>


<div role="main" class="main">

		<div class="container">
          <div class="row ">
              
            <section class="call-to-action featured featured-primary mb-xl">
				<div class="call-to-action-content">
				<div class="col-md-4">
					<h2 class="mb-none word-rotator-title mt-lg">
							<strong>
								<span class="word-rotate active" data-plugin-options='{"delay": 3500, "animDelay": 400}'>
								<span class="word-rotate-items" style="width: 450.3438px; top: -42px;">
										<span class="heading-primary">Teknoloji</span>
										<span class="heading-primary">Tecrübe</span>		
								</span>
								</span>
							</strong>	
							<br>
							<p style="color: #0088cc;opacity: 0.67">ile Gelen Kalite</p>		
						</h2>
					</div>
					<div class="container">
					<div class="col-md-8">	
					<ul class="list list-icons list-primary">
						<li class="appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0"><i class="fa fa-check"></i><p align="justify">Kaliteden ödün vermeyen imalat felsefemiz, robot destekli üretim tesislerimiz ve müşteri memnuniyeti üzerine kurulmuş politikalarımızla müşterilerimize hizmet vermeye devam ediyoruz.</p></li>
						<li class="appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="500"><i class="fa fa-check"></i><p align="justify">Ürünlerimizi başta beton olmak üzere; çimento, alçı, yapı kimyasalları, gıda, ilaç, maden, deterjan ve yem gibi birçok farklı sektörde kullanılmakta ve müşterilerimizce takdir edilmektedir.</p></li>
					</ul>
					</div>
				</div>
				</div>
			</section>

			<div id="owl-demo" class="owl-carousel owl-theme">
				<?php foreach( $sliders as $slider): ?>
				<div class="item">
					<img src="<?php echo e(url('/images/uploads/Slider/'.$slider->Resim)); ?>" />
				</div>
				<?php endforeach; ?>
            	</div>
         

			<div class="container">	
			    <div class="row">
			    	<div class="row center mt-xl">
            	<h3 class="heading-primary">Ürünlerimiz</h3>
            	<div class="divider divider-primary">
					<i class="fa fa-chevron-down"></i>
				</div>
					<div class="owl-carousel owl-theme owl-ref" data-plugin-options='{"items": 3, "autoplay": true, "autoplayTimeout": 3000}'>
						<?php foreach($onecikarilmisurunler as $element): ?>
							<div>
								 <a href="<?php echo e(url($dil.'/'.\App\Http\Fnk::Ceviri('urun-url').'/'.$element->Slug)); ?>">

                                <span class="thumb-info">
                                        <span class="thumb-info-wrapper">
                                            <img src="<?php if(empty(json_decode($element->Resim)[0])): ?> <?php echo e(url('/images/uploads/'.$ayarlar->Logo)); ?> <?php else: ?><?php echo e(url('/images/uploads/Urun/resim/'.json_decode($element->Resim)[0])); ?><?php endif; ?>" class="img-responsive"  style="height: 200px; width:80% ; padding: 20px " alt="">
                                            <span class="thumb-info-title">
                                                <span class="thumb-info-inner"><?php echo e(\App\Http\Fnk::EnUstKategori($element->UrunKategori->first()->kategori_id)); ?></span>
                                                <span class="thumb-info-type">İncele</span>
                                            </span>
                                            <span class="thumb-info-action">
                                                <span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
                                            </span>
                                        </span>
                                    </span>

                                
                            	</a>
							</div>
						<?php endforeach; ?>		
					</div>		
				</div>
			</div>	
			
			<hr>
			<section class="video section section-text-light section-video section-center" data-video-path="OZB_Factory.mp4" data-plugin-video-background data-plugin-options='{"posterType": "jpg", "position": "50% 50%", "overlay": true}'>
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<img src="<?php echo e(url('/images/uploads/'.$ayarlar->Logo)); ?>" style="opacity: 0.3">
							</div>
						</div> 

					</div>
				</section>
			<hr>			

            

          </div>
        </div>
			
</div>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>