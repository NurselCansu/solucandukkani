<?php $__env->startSection('title', 'Menü Detay'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading"><?php echo $__env->yieldContent('title'); ?></div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
				<tr>
					<th style="width:200px;">Üst Menü Adı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($ustkat); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Menü Adı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->Adi); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Link Açılış Tipi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php if($veri->LinkAcilisTipi==''): ?> Aynı Pencere <?php else: ?> Yeni Pencere <?php endif; ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Menü Tipi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->MenuTipi); ?></td>
				</tr>
				<?php if($veri->MenuTipi=='link' || $veri->MenuTipi=='modul'): ?>
				<tr>
					<th style="width:200px;">Menü Linki</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->Link); ?></td>
				</tr>
				<?php endif; ?>
				<?php if($veri->MenuTipi=='icerik'): ?>
				<tr>
					<th style="width:200px;">Menü İçeriği</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo $veri->Icerik; ?></td>
				</tr>
				<?php endif; ?>
                                <tr>
					<th style="width:200px;">Meta Keywords</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->MetaTag); ?></td>
				</tr>
                                <tr>
					<th style="width:200px;">Meta Title</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->MetaTitle); ?></td>
				</tr>
                                <tr>
					<th style="width:200px;">Meta Description</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->MetaDescription); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Durum</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e(($veri->is_active==1)?'Aktif':'Pasif'); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Oluşturulma Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e(Fnk::TarihDuzenle($veri->created_at)); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Güncelleme Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e(Fnk::TarihDuzenle($veri->updated_at)); ?></td>
				</tr>
				<tr>
					<td colspan="3"><a href="<?php echo e(URL::previous()); ?>" class="btn btn-primary pull-right">Geri Dön</a></td>
				</tr>
			</table>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>