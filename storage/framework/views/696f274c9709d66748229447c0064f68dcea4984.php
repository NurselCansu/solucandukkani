<?php $__env->startSection('content'); ?>
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading"></div>
        <div id="printableArea" class="panel-body">
            <table  class="table table-striped table-bordered">
               
                <tr>
                    <th style="width:200px;">Resim</th>
                    <td>
                        <?php if(!empty($veri->Fotograf)): ?>
                        <img width='150' src="<?php echo e(url('images/uploads/ik/fotograf/'.$veri->Fotograf)); ?>" style="max-width: 100%;"></img>
                        <?php endif; ?>
                </td>
                </tr>
                <tr>
                    <th style="width:200px;">Belgeler</th>
                    <td>
                        <?php if(!empty($veri->Belge)): ?>
                        <?php foreach(json_decode($veri->Belge) as $Belge): ?>
                        <a href="<?php echo e(url('images/uploads/ik/belge/'.$Belge)); ?>"><?php echo e($Belge); ?></a><br>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <th style="width:200px;">Ad Soyad</th>
                    <td><?php echo $veri->Adi.' '.$veri->Soyadi; ?></td>
                </tr>
                <tr>
                    <th style="width:200px;">Doğum Yeri </th>
                    <td><?php echo $veri->DogumYeriIl; ?></td>
                </tr>
                <tr>
                    <th style="width:200px;">Doğum Tarihi</th>
                    <td><?php echo $veri->DogumTarihi; ?></td>
                </tr>

                <tr>
                    <th style="width:200px;">Cinsiyet</th>
                    <td><?php echo e($veri->Cinsiyet); ?></td>
                </tr>
                <tr>
                    <th style="width:200px;">Ev Telefonu</th>
                    <td><?php echo e($veri->TelefonEv); ?></td>
                </tr>
                <tr>
                    <th style="width:200px;">Cep Telefonu</th>
                    <td><?php echo e($veri->TelefonCep); ?></td>
                </tr>
                <tr>
                    <th style="width:200px;">Askerlik Durumu</th>
                    <td><?php echo e($veri->AskerlikDurumu); ?></td>
                </tr>

                <tr>
                    <th style="width:200px;">TC Kimlik Numarası</th>
                    <td><?php echo e($veri->TcNo); ?></td>
                </tr>

                <tr>
                    <th style="width:200px;">Adresi</th>
                    <td><?php echo e($veri->Adres); ?></td>
                </tr>
  
                <tr>
                    <th style="width:200px;">Eğitim Seviyesi</th>
                    <td><?php echo e($veri->EgitimSeviyesi); ?></td>
                </tr>
                <tr>
                    <th style="width:200px;">Mezun Olduğu Okul ve Bölümü</th>
                    <td><?php echo e($veri->YuksekOgretim); ?></td>
                </tr>
                <tr>
                    <th style="width:200px;">Yabancı Dil</th>
                    <td>
                        <table class="table table-striped table-bordered">
                            <tr>
                                <th style="width:150px;">Dil</th>
                                <th style="width:50px;"> Seviyesi</th>

                            </tr>
                            <?php if(!empty(json_decode($veri->YabanciDiller))): ?>
                            <?php foreach(json_decode($veri->YabanciDiller) as $x): ?>
                            <tr>
                                <td><?php echo e($x->Adi); ?></td>
                                <td><?php echo e($x->Derecesi); ?></td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>

                        </table>
                    </td>
                </tr>
                <tr>
                    <th style="width:200px;">Bilgisayar Kullanma ve Programlar</th>
                    <td><?php echo e($veri->BilgisayarKullanma); ?></td>
                </tr>
                <tr>
                    <th style="width:200px;">Ehliyet</th>
                    <td><?php echo e($veri->EhliyetTipi); ?></td>
                </tr>
                <tr>
                    <th style="width:200px;">Bu İşle İlgili Kullandığınız Araç ve Cihazlar</th>
                    <td><?php echo e($veri->IsleIlgiliKullanilanAraclar); ?></td>
                </tr>

             
                <tr>
                    <th style="width:200px;">Referanslar</th>
                    <td>
                        <table class="table table-striped table-bordered">
                            <tr>
                                <th style="width:150px;">Adı Soyadı:</th>
                                <th style="width:150px;"> Telefon numarası:</th>

                            </tr>
                            <?php if(!empty(json_decode($veri->Referanslar))): ?>
                            <?php foreach(json_decode($veri->Referanslar) as $x): ?>
                            <tr>
                                <td><?php echo e($x->AdiSoyadi); ?></td>
                                <td><?php echo e($x->Telefon); ?></td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>

                        </table>
                    </td>
                </tr>
                <tr>
                    <th style="width:200px;">İş Başvurusu Yapılan Görev</th>
                    <td><?php echo e($veri->BasvuruYapilanPozisyon); ?></td>
                </tr>

                <tr>
                    <th style="width:200px;">Oluşturulma Tarihi</th>
                    <td><?php echo e(Fnk::TarihDuzenle($veri->created_at)); ?></td>
                </tr>
                <tr>
                    <th style="width:200px;">Güncelleme Tarihi</th>
                    <td><?php echo e(Fnk::TarihDuzenle($veri->updated_at)); ?></td>
                </tr>
                <tr>
                    
                    <td  colspan="3">
                        <div class="pull-right">
                        <a onclick="PrintElem('#printableArea')" class="btn btn-primary ">Yazdır</a>
                        <a href="<?php echo e(URL::previous()); ?>" class="btn btn-primary ">Geri Dön</a>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script>
function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
        var mywindow = window.open('', 'my div', 'height=400,width=600');
        mywindow.document.write('<html><head><title>my div</title>');
        mywindow.document.write('<style>table, td, th {text-align: left; border: 1px solid black;}table {border-collapse: collapse;width: 100%;}th { height: 20px;}</style>');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        //mywindow.document.write('</body></html>');

        mywindow.print();
       // mywindow.close();

        return true;
    }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>