<?php $__env->startSection('baslik', 'İletişim Bilgileri'); ?>

<?php $__env->startSection('form'); ?>
		<div class="form-group">
			<label class="col-md-2 control-label">Şube Adı</label>
			<div class="col-md-10">
				<input class="form-control" type="text" name="SubeAdi" value="<?php echo e(@$veri->SubeAdi); ?>" required />
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label">Adres</label>
			<div class="col-md-10">
				<input type="text" class="form-control" id="HaritaAdres" name="Adres" value="<?php echo e(@$veri->Adres); ?>" required>
				<input type="hidden" id="Kordinat" class="form-control" name="Kordinat" value="<?php echo e(@$veri->Kordinat); ?>">
				 <div class="checkbox">
					<label>
					  <input type="checkbox" checked id="adresguncelle"> Adresim Haritadan Güncellensin
					</label>
				  </div>
			</div>
		</div>

		<div class="form-group">

			<label class="col-md-2 control-label">İletişim Sayfasında Göster </label>

		    <div class="col-md-10">
		        <select id="iletisimde_goster" name="iletisimde_goster" class="form-control" >
		            <option >Seçiniz</option>
		            <option value="1" <?php if(@$veri->iletisimde_goster == 1): ?> selected <?php endif; ?> >Gösterilsin</option>
		            <option value="0" <?php if(@$veri->iletisimde_goster == 0): ?> selected <?php endif; ?> >Gösterilmesin</option>
		        </select>
		    </div>
		</div>
		<?php 
			$ust_adresler['adres'] = \App\Http\Models\IletisimBilgileri::select('id','SubeAdi')->where('is_active','1')->get();
		?>
		<div class="form-group">

			<label class="col-md-2 control-label">Bağlı olduğu adres </label>

		    <div class="col-md-10">
		        <select id="ust_Adres" name="ust_Adres" class="form-control" >
		            <option >Seçiniz</option>
		            <?php foreach($ust_adresler['adres'] as $key=>$adres): ?>
		            	<option value="<?php echo e($adres->id); ?>" <?php if(@$veri->ust_Adres == $adres->id): ?> selected <?php endif; ?> ><?php echo e($adres->SubeAdi); ?></option>
		            <?php endforeach; ?>
		        </select>
		    </div>
		</div>
		
		<div class="form-group">
			<label class="col-md-2 control-label">Harita</label>
			<div class="col-md-10">
				<div id="mapmain">
					<div style="float:left; width:100%; height:300px; clear:both; border:solid 1px #E2DFDA;" id="map"></div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label">Telefon</label>
			<div class="col-md-10">
				<input class="form-control"  type="text" name="Telefon" value="<?php echo e(@$veri->Telefon); ?>" required />
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label">Fax</label>
			<div class="col-md-10">
				<input class="form-control"  type="text" name="Fax" value="<?php echo e(@$veri->Fax); ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label">E-Posta</label>
			<div class="col-md-10">
				<input class="form-control" type="email" name="Eposta" value="<?php echo e(@$veri->Eposta); ?>" />
			</div>
		</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
	<script src='<?php echo e(url('plugins/jquery.inputmask/dist/inputmask/inputmask.js')); ?>'></script>
	<script src='<?php echo e(url('plugins/jquery.inputmask/dist/inputmask/jquery.inputmask.js')); ?>'></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyBq7hs_cVZsxgoD45l-0gubEDYJD5--doA&sensor=false&language=tr"></script>
	<script>
	$(document).ready(function(){
		$(":input").inputmask();
		$("#HaritaAdres").change(function(){
			$.ajax({url: "https://maps.googleapis.com/maps/api/geocode/json?address="+$("#HaritaAdres").val()+"&key=AIzaSyBq7hs_cVZsxgoD45l-0gubEDYJD5--doA", success: function(result){
				var co=new google.maps.LatLng(result.results[0].geometry.location.lat,result.results[0].geometry.location.lng);
				$('#Kordinat').val(co);
				showMap(co);
			}});
		});
		var geocoder = new google.maps.Geocoder();
		
		<?php if(isset($veri)): ?>
			var latLng = new google.maps.adress(<?php echo e($veri->Kordinat); ?>);
		<?php else: ?>
			var latLng = new google.maps.LatLng(39.91365499655554,32.855811906047165);		
		<?php endif; ?>
		
		varImageID = 0;
		
		function showMap(result){
			if(result){
				latLng=result;
			}
			$('#mapmain').show();
			var map = new google.maps.Map(document.getElementById('map'), {
				zoom: 10,
				center: latLng,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			});
			var image = '<?php echo e(url("images/mapsicon.png")); ?>';
			var marker = new google.maps.Marker({
				position: latLng,
				icon: image,
				map: map
			});
			google.maps.event.addListener(map, 'mouseup', function(event) {
				geocoder.geocode({
					latLng: event.latLng
				}, function(responses) {
					
					marker.setPosition(event.latLng);
					if (responses && responses.length > 0) {
						if($('#adresguncelle').is( ":checked" )){
							$('#HaritaAdres').val(responses[0].formatted_address);						
						}
						$('#Kordinat').val(event.latLng);
					} 
				});
			});
		}
		showMap(0);
		
	});
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>