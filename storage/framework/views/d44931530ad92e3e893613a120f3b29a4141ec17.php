



<?php $__env->startSection('Title',$ayarlar->MetaTitle); ?>
<?php $__env->startSection('MetaTag',$ayarlar->MetaTag); ?>
<?php $__env->startSection('MetaDescription',$ayarlar->MetaDescription); ?>


<?php $__env->startSection('content'); ?>

<div class="breadcrumb-box">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="/<?php echo e($dil); ?>/"><?php echo e(\App\Http\Fnk::Ceviri('anasayfa')); ?></a> </li>
        </ul>	
    </div>
</div><!-- .breadcrumb-box -->

<section id="main">
    <header class="page-header">
        <div class="container">
            <h6 class="title"><?php echo e(\App\Http\Fnk::Ceviri('aradiginiz-sayfa-bu-dilde-mevcut-degil')); ?></h6>
        </div>	
    </header>
     <div class="container">
        <div class="row">
            <article class="bottom-padding content col-sm-12 col-md-12">
                <a href="<?php echo e(URL::previous()); ?>"><i class="fa fa-arrow-left" aria-hidden="true"></i> <?php echo e(\App\Http\Fnk::Ceviri('geri-don')); ?></a>
                <div class="clearfix"></div>
            </article><!-- .content -->
        </div>
    </div><!-- .container -->
</article><!-- .content -->

<?php $__env->stopSection(); ?>

<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>