	<meta charset="utf-8">
	<title><?php echo $__env->yieldContent('Title'); ?></title>
	<meta name="keywords" content="<?php echo $__env->yieldContent('MetaTag'); ?>">
	<meta name="description" content="<?php echo $__env->yieldContent('MetaDescription'); ?>" /> 
	<meta name="robots" content="index, follow"> 
	<meta class="viewport" name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!-- Favicon -->
	<link rel="shortcut icon" href="<?php echo e(asset('img/favicon.ico')); ?>"></link>

	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Stylesheets
	============================================= -->
	<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?php echo e(asset('css/bootstrap.css')); ?>" type="text/css" />
	<link rel="stylesheet" href="<?php echo e(asset('style.css')); ?>" type="text/css" />
	<link rel="stylesheet" href="<?php echo e(asset('css/swiper.css')); ?>" type="text/css" />
	<link rel="stylesheet" href="<?php echo e(asset('css/dark.css')); ?>" type="text/css" />
	<link rel="stylesheet" href="<?php echo e(asset('css/font-icons.css')); ?>" type="text/css" />
	<link rel="stylesheet" href="<?php echo e(asset('css/animate.css')); ?>" type="text/css" />
	<link rel="stylesheet" href="<?php echo e(asset('css/magnific-popup.css')); ?>" type="text/css" />

	<link rel="stylesheet" href="<?php echo e(asset('css/responsive.css')); ?>" type="text/css" />
	
