<?php echo $ayarlar->Analytics; ?>

<!--[if (!IE)|(gt IE 8)]><!-->


<?php if( urldecode(request::segment(3)) != \App\Http\Fnk::Ceviri('katalog-url') ): ?>

<script src="<?php echo e(asset('js/jquery-3.0.0.min.js')); ?>"></script>

<?php endif; ?>
<!--<![endif]-->
<!--[if lte IE 8]><script src="<?php echo e(asset('js/jquery-1.9.1.min.js')); ?>"></script><![endif]-->

<script src="<?php echo e(asset('js/jquery-ui.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/price-regulator/jshashtable-2.1_src.js')); ?>"></script>
<script src="<?php echo e(asset('js/price-regulator/jquery.numberformatter-1.2.3.js')); ?>"></script>
<script src="<?php echo e(asset('js/price-regulator/tmpl.js')); ?>"></script>
<script src="<?php echo e(asset('js/price-regulator/jquery.dependClass-0.1.js')); ?>"></script>
<script src="<?php echo e(asset('js/price-regulator/draggable-0.1.js')); ?>"></script>
<script src="<?php echo e(asset('js/price-regulator/jquery.slider.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.carouFredSel-6.2.1-packed.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.touchwipe.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.elevateZoom-3.0.8.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.imagesloaded.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.appear.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.sparkline.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.easypiechart.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.easing.1.3.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.fancybox.pack.js')); ?>"></script>
<script src="<?php echo e(asset('js/isotope.pkgd.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.knob.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.royalslider.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.tubular.1.0.js')); ?>"></script>
<script src="<?php echo e(asset('js/SmoothScroll.js')); ?>"></script>
<script src="<?php echo e(asset('js/country.js')); ?>"></script>
<script src="<?php echo e(asset('js/spin.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/ladda.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/masonry.pkgd.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/morris.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/raphael.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/video.js')); ?>"></script>
<script src="<?php echo e(asset('js/pixastic.custom.js')); ?>"></script>
<script src="<?php echo e(asset('js/livicons-1.4.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/layerslider/greensock.js')); ?>"></script>
<script src="<?php echo e(asset('js/layerslider/layerslider.transitions.js')); ?>"></script>
<script src="<?php echo e(asset('js/layerslider/layerslider.kreaturamedia.jquery.js')); ?>"></script>
<script src="<?php echo e(asset('js/revolution/jquery.themepunch.tools.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/revolution/jquery.themepunch.revolution.min.js')); ?>"></script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
        (Load Extensions only on Local File Systems !
        The following part can be removed on Server for On Demand Loading) -->	
<script src="<?php echo e(asset('js/revolution/extensions/revolution.extension.actions.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/revolution/extensions/revolution.extension.carousel.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/revolution/extensions/revolution.extension.kenburn.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/revolution/extensions/revolution.extension.layeranimation.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/revolution/extensions/revolution.extension.migration.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/revolution/extensions/revolution.extension.navigation.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/revolution/extensions/revolution.extension.parallax.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/revolution/extensions/revolution.extension.slideanims.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/revolution/extensions/revolution.extension.video.min.js')); ?>"></script>
<?php /* <script src="<?php echo e(asset('js/bootstrapValidator.min.js')); ?>"></script>
 */ ?><script src="<?php echo e(asset('js/bootstrap-datepicker.js')); ?>"></script>
<script src="<?php echo e(asset('js/jplayer/jquery.jplayer.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/jplayer/jplayer.playlist.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.scrollbar.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/main.js')); ?>"></script>

<script src="<?php echo e(asset('plugins/jqueryvalidate/jquery.validate.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/jqueryvalidate/messages_tr.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/jquery.inputmask/dist/inputmask/inputmask.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/jquery.inputmask/dist/inputmask/jquery.inputmask.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.form.js')); ?>"></script>
<!--Anasayfa ürün slider kaymasını mouse üzerine gelince durdurma scripti -->
<script type="text/javascript">
    
   
 
       $(document).ready(function() {

        $(".one1").mouseover(function(){
                $(".one1").trigger('pause');
            });

        $('.one1').mouseleave(function(event) {
                    $('.one1').trigger('resume');
                
            });
        $(".one2").mouseover(function(){
                $(".one2").trigger('pause');
            });

        $('.one2').mouseleave(function(event) {
                    $('.one2').trigger('resume');
                
            });
        $(".kamp1").mouseover(function(){
                $(".kamp1").trigger('pause');
            });

        $('.kamp1').mouseleave(function(event) {
                    $('.kamp1').trigger('resume');
                
            });
        
        
    });
        
        
   
</script>
<script>
	$("#search").autocomplete({
    source: "<?php echo e(url($dil.'/Ara/')); ?>",
    minLength: 2,
    select: function (event, ui) {
        window.location = ui.item.slug;
    }
});
$("#searchproduct").autocomplete({
    source: "<?php echo e(url($dil.'/Ara/')); ?>",
    minLength: 2,
    select: function (event, ui) {
        window.location = ui.item.slug;
    }
});

$(document).ready(function () {
        setTimeout(function () {
            $("#renklilogo").css("display", "block");
        }, 2000);
        $("#portakallogo").hover(function () {
            $("#renklilogo").css("display", "none");
            $("#siyahlogo").css("display", "block");
        }, function () {
            $("#renklilogo").css("display", "block");
            $("#siyahlogo").css("display", "none");
        });

        if ($.browser.safari)
        {
            $("#renklilogo").css("display", "none");
            $("#siyahlogo").css("display", "none");
        }


    });
    var is_safari = navigator.userAgent.indexOf('Safari') != -1 || navigator.userAgent.indexOf('Chrome') != -1 && navigator.userAgent.indexOf('Android') == -1;
    if (is_safari)
    {
        var portakalhtml = $("#portakallogo").html();
        $("#portakallogo").hide();
        $("#plogo").html(portakalhtml);
        $("#plogo").css("position", "absolute");
        $("#plogo").css("margin-right", "-60px");
        $("#plogo").css("right", "50%");
        $("#plogo").css("margin-left", "-60px");
        $("#plogo").css("tect-align", "center");
        $("#plogo").css("background", "white");
        $("#plogo").css("border-top-width", "0px");
        $("#plogo > #renklilogo").css("display", "block");
        $("#plogo > #siyahlogo").css("display", "none");
    }

    $(document).ready(function(){
 ////////////////////Mesaj İletimin başlangıcı///////////////////////
        $(".form-box").submit(function(event) {
            swal({
                title: "<?php echo e(\App\Http\Fnk::Ceviri('durum')); ?>" ,
                text: "<?php echo e(\App\Http\Fnk::Ceviri('bekle')); ?>",
                timer: 2000,
                showConfirmButton: false,
                html: true
                });
        });
///////////////BU Kısım Mesajın İletim durumunu döndürür belirtir//////////////////////                
        <?php if(!empty(session('status'))): ?>
                <?php if(session('status')=='TRUE'): ?>
                    swal("<?php echo e(\App\Http\Fnk::Ceviri('gonderildi')); ?>", "", "success")//swal kullanabilmek için sweet alert dialog css ve js gereklidir...Github'a bak
                <?php else: ?>
                    swal("<?php echo e(\App\Http\Fnk::Ceviri('gonderilmedi')); ?>", "", "error")
                <?php endif; ?>
        <?php endif; ?>
//////////////////////////////////////////////////////////////////////////////////////
    });

    $('.urunler-menu ul li.active').parents('li').addClass('active');

	
</script>




