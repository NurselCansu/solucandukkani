<?php $__env->startSection('baslik', 'E-posta'); ?>

<?php $__env->startSection('form'); ?>

<div class="form-group">
    <label for="ayar" class="col-sm-2 control-label">Mail Ayar</label>
    <div class="col-sm-10">
    <select onchange="mailAyariDegisti()" class="form-control" id="ayar" name="ayar">
        <option value="yandex">Yandex</option>
        <option value="gmail">Gmail</option>
        <option value="standart">Manuel</option>
    </select>
    </div>

</div>

<div class="form-group" hidden="true">
    <label for="Driver" class="col-sm-2 control-label">Driver </label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="smtp" name="Driver" id="Driver" placeholder="Driver(Örn smtp)">
    </div>
</div>

<div class="form-group">
    <label for="Host" class="col-sm-2 control-label">Smtp Sunucusu</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->Host); ?>" name="Host" id="Host" placeholder="Host(Örn smtp.yandex.com.tr)">
    </div>
</div>

<div class="form-group">
    <label for="Port" class="col-sm-2 control-label">Port</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->Port); ?>" name="Port" id="Port" placeholder="Port (Örn 587,26)">
    </div>
</div>

<div class="form-group">
    <label for="GonderilecekMailAdres" class="col-sm-2 control-label">Gönderen E-posta</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->GonderilecekMailAdres); ?>" name="GonderilecekMailAdres" id="GonderilecekMailAdres" placeholder="Gönderen E-posta (Örn info@portakalyazilim.com)">
    </div>
</div>

<div class="form-group">
    <label for="GonderilecekMailAdresIsmi" class="col-sm-2 control-label">Gönderen E-posta Açıklaması</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->GonderilecekMailAdresIsmi); ?>" name="GonderilecekMailAdresIsmi" id="GonderilecekMailAdresIsmi" placeholder="Gönderen E-posta Açıklaması (Örn Portakal Yazilim)">
    </div>
</div>

<div class="form-group">
    <label for="Encryption" class="col-sm-2 control-label">Bağlantı Metodu</label>
    <div class="col-sm-10">
        <select id="Encryption" name="Encryption" onchange="baglantiMetodu()" class="form-control">
            <option value="tls" selected="">tls</option>
            <option value="ssl">ssl</option>
        </select>
    </div>
</div>

<div class="form-group">
    <label for="KullaniciAdi" class="col-sm-2 control-label">Kullanici Adi</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->KullaniciAdi); ?>" name="KullaniciAdi" id="KullaniciAdi" placeholder="Kullanıcı Adı (Örn Portakal Yazılım)">
    </div>
</div>

<div class="form-group">
    <label for="Sifre" class="col-sm-2 control-label">Şifre</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->Sifre); ?>" name="Sifre" id="Sifre" placeholder="Şifre (Örn portakal)">
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('.is_active').hide();
            $('#Host').val('smtp.yandex.com.tr');
            $('#Port').val('465');
           
           
        });

        function mailAyariDegisti(){
            if($('[name="ayar"]').val() == 'yandex'){
                $('#Host').val('smtp.yandex.com.tr');
                $('#Port').val('465');

            }else if($('[name="ayar"]').val() == 'gmail'){
                $('#Host').val('smtp.gmail.com');
                $('#Port').val('587');

            }else{
                $('#Host').val('');
                $('#Port').val('');
            }
        }

        function baglantiMetodu(){
            if($('[name="ayar"]').val() == 'gmail' && $('[name="Encryption"]').val()=='tls'){
                $('#Port').val('587');
            }else{
                $('#Port').val('465');
            }

        }
    </script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>