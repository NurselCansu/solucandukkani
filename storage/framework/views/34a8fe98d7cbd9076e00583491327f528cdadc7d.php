

<?php $__env->startSection('Title',$ayarlar->MetaTitle); ?>
<?php $__env->startSection('MetaTag',$ayarlar->MetaTag); ?>
<?php $__env->startSection('MetaDescription',$ayarlar->MetaDescription); ?>

<?php $__env->startSection('content'); ?>
<div class="slider rs-slider load">
	<div class="tp-banner-container">
		
		<div class="rev_slider tp-banner" data-version="5.0.7">
			<ul>
				<?php foreach($sliders as $element): ?>
				<?php if(!empty($element->Katman)): ?>
				<li data-delay="7000" data-transition="fade" data-slotamount="7" data-masterspeed="2000">
					<div class="elements">
						<?php if(!empty($element->Katman)): ?>
						<div class="tp-caption skewtobottom"
						data-x="0"
						data-y="160"
						data-start="500"
						data-transform_in="y:bottom;s:1000;e:Power4.easeOut"
						data-transform_out="y:bottom;s:500;e:Power1.easeIn"
						style="z-index: 3">
						<img class=" sliderkatman" src="<?php echo e(url('/images/uploads/Slider/'.$element->Katman)); ?>" width="400px" alt="">
					</div>
					<?php endif; ?>
					
					<div class="tp-caption lfr skewtoright description cm-description-2"
					data-x="575"
					data-y="100"
					data-start="1000"
					data-transform_in="x:right;s:1000;e:Power4.easeOut"
					data-transform_out="x:right;s:400;e:Power1.easeIn"
					style="max-width: 600px;">
					<?php echo $element->KisaIcerik; ?>

				</div>
				
				<?php if(($element->Link)!='#'): ?>
				<a href="<?php echo e(url($element->Link)); ?>"
					class="btn tp-caption cherry btn-default"
					data-x="590"
					data-y="332"
					data-start="1400"
					data-transform_in="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;s:1200;e:Power3.easeOut"
					data-transform_out="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;s:300;e:Power1.easeIn"
					style="z-index: 5">
					<?php echo e(\App\Http\Fnk::Ceviri('tiklayin')); ?>

				</a>
				<?php endif; ?>
				
			</div>
			<img class="" src="<?php echo e(url('/images/uploads/Slider/'.$element->Resim)); ?>" alt="" data-bgfit="contain" data-bgrepeat="no-repeat">
		</li>
		<?php else: ?>
		<li data-delay="7000" data-transition="fade" data-slotamount="7" data-masterspeed="2000">
			<img class="" src="<?php echo e(url('/images/uploads/Slider/'.$element->Resim)); ?>" alt="" data-bgfit="contain"  data-bgrepeat="no-repeat">
		</li>
		<?php endif; ?>
		<?php endforeach; ?>
	</ul>
	<div class="tp-bannertimer"></div>
</div>

</div>
</div><!-- .rs-slider -->

<section>
	<div class="container">
		
		<div class="title-box">
			<h1 class="title">Delkom</h1>
		</div>

		<div class="row">
			<div class="col-sm-6 col-md-6 bottom-padding">
				<p><?php echo $ayarlar->AnaSayfaIcerikSutun1; ?></p>
			</div>
			<div class="col-sm-6 col-md-6 bottom-padding">
				<p><?php echo $ayarlar->AnaSayfaIcerikSutun2; ?></p>
			</div>		
		</div>   
	</div> 
</section>

<section id="main">
	<article class="content">
		<div class="container">

			<?php if(count($onecikarilmisurunler)>0 && count($onecikarilmisurunler2)>0 ): ?>
			<div class="carousel-box bottom-padding bottom-padding-mini load overflow" data-autoplay-disable="false">
				<div class="title-box no-margin">
					<a href="<?php echo e($dil); ?>/<?php echo e(\App\Http\Fnk::Ceviri('urunler-url')); ?>" style="position: initial !important;float:right  !important;" class="btn btn-default"><?php echo e(\App\Http\Fnk::Ceviri('butun-urunlerimizi-inceleyin')); ?></a>
					<a class="next" href="#" style="position: initial !important; margin-right: 10px;">
						<svg x="0" y="0" width="9px" height="16px" viewBox="0 0 9 16" enable-background="new 0 0 9 16" xml:space="preserve">
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#fcfcfc" points="1,0.001 0,1.001 7,8 0,14.999 1,15.999 9,8 "></polygon>
						</svg>
					</a>
					<a class="prev" href="#" style="position: initial !important;">
						<svg x="0" y="0" width="9px" height="16px" viewBox="0 0 9 16" enable-background="new 0 0 9 16" xml:space="preserve">
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#fcfcfc" points="8,15.999 9,14.999 2,8 9,1.001 8,0.001 0,8 "></polygon>
						</svg>
					</a>
					<h2 class="title"><?php echo e(\App\Http\Fnk::Ceviri('baslica-urunlerimiz')); ?></h2>
            		<img class="drifanasayfa" src="<?php echo e(url('/images/Drifter1.png')); ?>"/>

				</div>

				<div class="clearfix"></div>

				<div style="margin-top:20px" class="row">
					<div class="carousel products">

						<?php foreach($onecikarilmisurunler as $element): ?>
						<div style="margin-bottom:30px;" class="one1 col-sm-4 col-md-3">
							<div class="serit" style="height: 4px;background-color: #643234;margin-top: 1px;margin-bottom: 1px;"> </div>
							<div class="hide trunc producttitle name"><a href="<?php echo e(url($dil.'/'.\App\Http\Fnk::Ceviri('urun-url').'/'.$element->Slug)); ?>"><?php echo e(\App\Http\Fnk::Ceviri('Delkom-No')); ?>: <?php echo e($element->DelkomNo); ?></a></div>
							<a href="<?php echo e(url($dil.'/'.\App\Http\Fnk::Ceviri('urun-url').'/'.$element->Slug)); ?>">
								<div class="default">
									<div  class="product-image">
										<div  class="hover14" >
											<figure >
												<div>
													<img style="width:100%; "src="<?php echo e(url('/images/uploads/Urun/resim/'.json_decode($element->Resim)[0])); ?>"></img>
												</div>
											</figure>
										</div>

									</div>
									<div class="product-description">
										<div class="serit" style="height: 4px;background-color: #643234;margin-top: 1px;margin-bottom: 5px;margin-left: -10px;
										"> </div>
										<?php echo e($element->SeriNo); ?><br>
										<?php echo e(\App\Http\Fnk::Ceviri('delkom-no')); ?>: <?php echo e($element->DelkomNo); ?><br>
										<?php echo e(\App\Http\Fnk::EnUstKategori($element->UrunKategori->first()->kategori_id)); ?> No: <?php echo e($element->UreticiSeriNo); ?> 
									</div>
								</div>
							</a>
						</div>
						<?php endforeach; ?>
					</div>
				</div>
				<div style="margin-top:20px" class="row">
					<div class="carousel products">

						<?php foreach($onecikarilmisurunler2 as $element): ?>
						<div style="margin-bottom:30px;" class="one2 col-sm-4 col-md-3">
							<div class="serit" style="height: 4px;background-color: #643234;margin-top: 1px;margin-bottom: 1px;"> </div>
							<div class="hide trunc producttitle name"><a href="<?php echo e(url($dil.'/'.\App\Http\Fnk::Ceviri('urun-url').'/'.$element->Slug)); ?>"><?php echo e(\App\Http\Fnk::Ceviri('Delkom-No')); ?>: <?php echo e($element->DelkomNo); ?></a></div>
							<a href="<?php echo e(url($dil.'/'.\App\Http\Fnk::Ceviri('urun-url').'/'.$element->Slug)); ?>">
								<div class="default">
									<div  class="product-image">
										<div  class="hover14" >
											<figure >
												<div>
													<img style="width:100%; "src="<?php echo e(url('/images/uploads/Urun/resim/'.json_decode($element->Resim)[0])); ?>"></img>
												</div>
											</figure>
										</div>

									</div>
									<div class="product-description">
										<div class="serit" style="height: 4px;background-color: #643234;margin-top: 1px;margin-bottom: 5px;margin-left: -10px;
										"> </div>
										<?php echo e($element->SeriNo); ?><br>
										<?php echo e(\App\Http\Fnk::Ceviri('delkom-no')); ?>: <?php echo e($element->DelkomNo); ?><br>
										<?php echo e(\App\Http\Fnk::EnUstKategori($element->UrunKategori->first()->kategori_id)); ?> No: <?php echo e($element->UreticiSeriNo); ?> 
									</div>
								</div>
							</a>
						</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
			<?php endif; ?>

			<?php /*  kampanyalı ürünler */ ?>
			<?php if(count($kampanyaliurunler)>0 ): ?>
			<div class="carousel-box bottom-padding bottom-padding-mini load overflow" data-autoplay-disable="false">
				<div class="title-box no-margin">
					<a href="<?php echo e($dil); ?>/<?php echo e(\App\Http\Fnk::Ceviri('urunler-url')); ?>" style="position: initial !important;float:right  !important;" class="btn btn-default"><?php echo e(\App\Http\Fnk::Ceviri('butun-urunlerimizi-inceleyin')); ?></a>
					<a class="next" href="#" style="position: initial !important; margin-right: 10px;">
						<svg x="0" y="0" width="9px" height="16px" viewBox="0 0 9 16" enable-background="new 0 0 9 16" xml:space="preserve">
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#fcfcfc" points="1,0.001 0,1.001 7,8 0,14.999 1,15.999 9,8 "></polygon>
						</svg>
					</a>
					<a class="prev" href="#" style="position: initial !important;">
						<svg x="0" y="0" width="9px" height="16px" viewBox="0 0 9 16" enable-background="new 0 0 9 16" xml:space="preserve">
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#fcfcfc" points="8,15.999 9,14.999 2,8 9,1.001 8,0.001 0,8 "></polygon>
						</svg>
					</a>
					<h2 class="title"><?php echo e(\App\Http\Fnk::Ceviri('kampanyali-urunler')); ?></h2>
				</div>

				<div class="clearfix"></div>
				
				<div style="margin-top:20px" class="row">
					<div class="carousel products">
						<?php foreach($kampanyaliurunler as $element): ?>
						<div style="margin-bottom:30px;" class="kamp1 col-sm-4 col-md-3">
							<div class="serit" style="height: 4px;background-color: #643234;margin-top: 1px;margin-bottom: 1px;"> </div>
							<div class="hide trunc producttitle name"><a href="<?php echo e(url($dil.'/'.\App\Http\Fnk::Ceviri('urun-url').'/'.$element->Slug)); ?>"><?php echo e(\App\Http\Fnk::Ceviri('Delkom-No')); ?>: <?php echo e($element->DelkomNo); ?></a></div>
							<a href="<?php echo e(url($dil.'/'.\App\Http\Fnk::Ceviri('urun-url').'/'.$element->Slug)); ?>">
								<div class="default">
									<div  class="product-image">
										<div  class="hover14" >
											<figure >
												<div>
													<img style="width:100%; "src="<?php echo e(url('/images/uploads/Urun/resim/'.json_decode($element->Resim)[0])); ?>"></img>
												</div>
											</figure>
										</div>

									</div>
									<div class="product-description">
										<div class="serit" style="height: 4px;background-color: #643234;margin-top: 1px;margin-bottom: 5px;margin-left: -10px;
										"> </div>
										<?php echo e($element->SeriNo); ?><br>
										<?php echo e(\App\Http\Fnk::Ceviri('delkom-no')); ?>: <?php echo e($element->DelkomNo); ?><br>
										<?php echo e(\App\Http\Fnk::EnUstKategori($element->UrunKategori->first()->kategori_id)); ?> No: <?php echo e($element->UreticiSeriNo); ?> 
									</div>
								</div>
							</a>
						</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
			<?php /*kampanyalı ürünler sonu */ ?>
			<?php endif; ?>
		</div>
	</article>
</section>

<section>
	<div class="container">
		<div class="title-box">
			<h2 class="title"><?php echo e(\App\Http\Fnk::Ceviri('ihracat-Stok')); ?> </h2>
		</div>
		<div class="row">
			<div class="col-sm-6 col-md-6 bottom-padding">
				<p align="justify"><?php echo $ayarlar->AnaSayfaAltIcerikSutun1; ?></p>			
			</div>
			<div class="col-sm-6 col-md-6 bottom-padding">
				<p align="justify"><?php echo $ayarlar->AnaSayfaAltIcerikSutun2; ?></p>		
			</div>   
		</div>
	</div> 
</section>

<!--22.12.2016 günü anasayfadaki en alt kısma sadece fotoğraf eklenmek istendiği için iptal edilmiştir -->
<!-- <div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="home-promo">
				<div class="container" style="color:#a9a9a9; border-color:#ffffff; border-right-width:20px ;  padding-bottom:30px; background-image: url(<?php echo e(url('/images/uploads/'.$ayarlar->AnasayfaResim)); ?>); background-size: contain; background-repeat: no-repeat; background-color: black ">
					<div class="hidden-xs" style="position:absolute;bottom:5%;right:1%;"><img src="<?php echo e(url('/images/uploads/kartal-yazi.png')); ?>" /></div>

					<div class="col-md-12">
						<div class="col-md-6">
							<div class="panel-heading" style="border-bottom=1px; border-color=red" >
								<h5 class="title light text-center" style="padding-top: 45px;"><?php echo $ayarlar->AnaSayfaAltSliderBaslik; ?></h5>
								<hr style="border-top:1px solid rgb(17, 81, 117); margin:20px 10px">
							</div>
							<div class="row">
								<div class="col-md-4 "><?php echo $ayarlar->AnaSayfaAltSliderSutun1; ?></div>
								<div class="col-md-4 "><?php echo $ayarlar->AnaSayfaAltSliderSutun2; ?></div>
								<div class="col-md-4 "><?php echo $ayarlar->AnaSayfaAltSliderSutun3; ?></div>
							</div>
						</div>
						<div class="col-md-6 hidden-xs hidden-sm"></div>
					</div>
				</div>				
			</div>
		</div>
	</div>
</div> -->

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="home-promo">
                <div class="container" style="color:#a9a9a9; border-color:#ffffff; border-right-width:20px ;  padding-bottom:30px; background-size: contain; background-repeat: no-repeat; display: inline-flex; justify-content: center;">
                <?php if($ayarlar->AnasayfaResim!=''): ?>
                    <img src="<?php echo e(url('/images/uploads/'.$ayarlar->AnasayfaResim)); ?>" />
                <?php endif; ?>
                </div>              
            </div>
        </div>
    </div>
</div>


<?php /* 
	<section id="whoweare" class="full-width-box " style="height: auto;padding: 0;"><!--heght 460px paddingtp45px-->
		<?php if(empty($ayarlar->AnasayfaResim)): ?>
		<div style="background-image: url(<?php echo e(url('/images/uploads/'.\App\Http\Models\Ayarlar::where('DilId','1')->first()->AnasayfaResim)); ?>);" class="fwb-bg ">
			<div class="overlay"></div>
			<div class="hidden-xs" style="position:absolute;bottom:5%;right:1%;"><img src="<?php echo e(url('/images/uploads/kartal-yazi.png')); ?>" /></div>
		</div>
		<?php else: ?>
		<div style="background-image: url(<?php echo e(url('/images/uploads/'.$ayarlar->AnasayfaResim)); ?>);" class="fwb-bg "><div class="overlay"></div>
		<div class="hidden-xs" style="position:absolute;bottom:5%;right:2%;"><img src="<?php echo e(url('/images/uploads/kartal-yazi.png')); ?>" /></div>
	</div>
	
	<?php endif; ?>
	<div class="container" style="color:#a9a9a9;padding-bottom:30px;">
		<div class="col-md-12" >
			<div class="col-md-6">
				<div class="panel-heading" style="border-bottom=1px; border-color=red">
					<h3 class="title light text-center"><?php echo $ayarlar->AnaSayfaAltSliderBaslik; ?></h3>
					<hr style="border-top:1px solid rgb(17, 81, 117); margin:20px 10px">
				</div>
				<div class="row">
					<div class="col-md-4 "><?php echo $ayarlar->AnaSayfaAltSliderSutun1; ?></div>
					<div class="col-md-4 "><?php echo $ayarlar->AnaSayfaAltSliderSutun2; ?></div>
					<div class="col-md-4 "><?php echo $ayarlar->AnaSayfaAltSliderSutun3; ?></div>
				</div>
			</div>
			<div class="col-md-6 hidden-xs hidden-sm"></div>
		</div>
	</div>
</section> */ ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<style>
	.producttitle{
		text-decoration: none !important;
		font-weight: bold !important;
		text-align: left;
		text-indent: 10px;
		background-color: #643234;
		text-decoration: none;
		border-top-right-radius: 15px;
	}
	.producttitle a{
		color:white !important;
		text-decoration: none !important;
	}
	/* Shine */
	.hover14 figure {
		position: relative;
	}
	.hover14 figure::before {
		position: absolute;
		top: 0;
		left: -75%;
		z-index: 2;
		display: block;
		content: '';
		width: 50%;
		height: 100%;
		background: -webkit-linear-gradient(left, rgba(255,255,255,0) 0%, rgba(255,255,255,.3) 100%);
		background: linear-gradient(to right, rgba(255,255,255,0) 0%, rgba(255,255,255,.3) 100%);
		-webkit-transform: skewX(-25deg);
		transform: skewX(-25deg);
	}
	.hover14 figure:hover::before {
		-webkit-animation: shine .75s;
		animation: shine .75s;
	}
	@-webkit-keyframes shine {
		100% {
			left: 125%;
		}
	}
	@keyframes  shine {
		100% {
			left: 125%;
		}
	}
	figure {
		width: 100%;
		margin: 0;
		padding: 0;
		/* background: #d29292; */
		overflow: hidden;
		position: absolute;
	}
	figure:hover+span {
		bottom: -36px;
		opacity: 1;
	}
	.product-description{
		background-color: #f2f2f2;
		color: black;
		margin-top: -6px;
		text-decoration: none;
		padding-left: 10px;
		text-align: left;
	}
	.product-description:hover{
		text-decoration: none !important;
	}
	
	#main {
		padding: 0px 0;
	}


	.page-header {
		border-color: rgb(17, 83, 121) !important
	}
	.tp-bannertimer{
		background:rgb(0, 174, 239) !important;
	}
	.bottom-padding {
		margin-bottom: 30px;
	}	
</style>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>