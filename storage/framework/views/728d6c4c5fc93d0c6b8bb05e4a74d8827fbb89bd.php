<?php $__env->startSection('baslik', 'Ürün'); ?>

<?php 
use App\Http\Models\Menu as MN; 
$menu = MN::where('is_active',1)->where('MenuTipi', 'modul')->where('Link', 'LIKE','/tr/urunler%')->get(); 
 
 ?> 

<?php $__env->startSection('form'); ?>
<div class="form-group">
    <label for="Urun_UstKat" class="col-sm-2 control-label">Üst Kategori</label>
    <div class="col-sm-10">
        <select class="form-control" id="Urun_UstKat" required name="Urun_UstKat">
            <option value="">Üst Kategori Seçiniz</option>
            <?php foreach(@$menu as $td): ?>
                <option value="<?php echo e($td->id); ?>" <?php if(@$td->id == @$veri->Urun_UstKat): ?> selected <?php endif; ?>><?php echo e($td->Adi); ?></option>
            <?php endforeach; ?>
        </select>
    </div>
</div>

<div class="form-group">
    <label for="Adi" class="col-sm-2 control-label">Ürün Adı</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->Adi); ?>" name="Adi" id="Adi" placeholder="Ürün Adı">
    </div>
</div>

<div class="form-group">
    <label for="KategoriId" class="col-sm-2 control-label">Kategori</label>
    <div class="col-sm-10">
        <select class="form-control" id="KategoriId"  required name="KategoriId[]">
            <option value="">Kategori Seçiniz</option>
            <?php foreach(@$kategoriler as $kategori): ?>
                <option value="<?php echo e($kategori['id']); ?>"<?php if( \App\Http\Models\UrunKategori::where('urun_id', @$veri->id)->where('kategori_id', $kategori['id'])->first() ): ?> selected <?php endif; ?>>
                    <?php for($i = 0; $i < $kategori["derinlik"]; $i++): ?>
                    -
                    <?php endfor; ?>
                    <?php echo e($kategori["adi"]); ?></option>
            <?php endforeach; ?>
        </select>
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label">Ana Kategori Durumu </label>
    <div class="col-md-10">
        <div class="checkbox">
            <label>
                <input type="checkbox" id="UrunGizli" name="UrunGizli"  <?php if(@$veri->UrunGizli == 1): ?> checked <?php endif; ?> >Üst Kategoride Gösterilmesin
            </label>
        </div>
    </div>
</div>

<div class="form-group">
    <label for="Resim" class="col-sm-2 control-label">Resim</label>
    <div class="col-sm-10">
      <input type="file" class="form-control" name="Resim[]" id="Resim" multiple placeholder="Resim">
    </div>
    <?php if( @$veri->Resim ): ?>
        <div class="col-md-10 col-md-offset-2 container-fluid">
            <div class="checkbox">
            <?php foreach(json_decode(@$veri->Resim) as $key => $value ): ?>
                <a>
                    <img class="img" src="<?php echo e(url('images/uploads/Urun/resim/'.$value)); ?>" width="150" height="150" alt="">
                    <input type="checkbox" name="secili_belge_resim_kapak[]"   <?php if( @$value ): ?> checked value="<?php echo e($value); ?>" <?php else: ?> value="0" <?php endif; ?>>
                </a>
            <?php endforeach; ?>
            </div>
        </div>
    <?php endif; ?>
</div>

 
<div class="form-group">
    <label for="urun_gosterme_sekli" class="col-sm-2 control-label">Ürün Gösterimi</label>
    <div class="col-sm-10">
        <select class="form-control" id="urun_gosterme_sekli"  onchange="UrunGosterme()" required name="Gosterim">
            <option value="">Ürün Gösterme Tipini Seçin</option>
            
                <option  value="1" <?php if( @$veri->Gosterim == 1 ): ?> selected <?php endif; ?>>Katolog Resimleri</option>
                <option value="2" <?php if( @$veri->Gosterim == 2 ): ?> selected <?php endif; ?>>Ürün Özellikleri</option>
                <option value="3" <?php if(@$veri->Gosterim == 3 ): ?> selected  <?php endif; ?>>Alt Kategori Ekleme</option>
            
        </select>
    </div>
</div>

<div class="form-group" id="Katalog" <?php if ( ! ( @$veri->Gosterim == 1 )): ?> style="display: none" <?php endif; ?>>
    <label for="Resim2" class="col-sm-2 control-label">Katalog Resimleri</label>
    <div class="col-sm-10">
        <input type="file" class="form-control" name="Resim2[]" id="Resim2"  multiple >
    </div>
    <?php if( @$veri->Resim2 ): ?>
        <div class="col-md-10 col-md-offset-2 container container-fluid" >
            <div class="checkbox">
                <?php foreach(json_decode(@$veri->Resim2)  as $key=>$value): ?>
                <a>
                    <img class="img" src="<?php echo e(url('images/uploads/urun/resim/'.$value )); ?>" width="150" height="150" alt="">
                    <input type="checkbox"  name="secili_belge_resim[]"   <?php if( @$value ): ?> checked value="<?php echo e($value); ?>" <?php else: ?> value="0" <?php endif; ?>>
                </a>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endif; ?>
</div>
<div class="form-group" id="AltKategori"  <?php if ( ! ( @$veri->Gosterim == 3 )): ?>style="display:none"  <?php endif; ?> >
    <label for="Urun_UstKat" class="col-sm-2 control-label">Alt Kategori</label>
    <div class="col-sm-10">
        <select class="form-control" id="Urun_AltKat" required name="Urun_AltKat">
            <option value="">Alt Kategori Seçiniz</option>
            <?php foreach(@$kategoriler as $kategori): ?>
                <option value="<?php echo e($kategori["slug"]); ?>" <?php if( $kategori["slug"]==@$veri->Urun_AltKat ): ?> selected <?php endif; ?>>
                    <?php for($i = 0; $i < $kategori["derinlik"]; $i++): ?>
                    -
                    <?php endfor; ?>
                    <?php echo e($kategori["adi"]); ?></option>
            <?php endforeach; ?>
        </select>
    </div>
</div>
<div class="form-group" id="UrunOzellikleriBoyut"  <?php if ( ! ( @$veri->Gosterim == 2 )): ?>style="display:none"  <?php endif; ?> >
    <label for="Icerik" class="col-sm-2 control-label">Boyut</label>
    <div class="col-sm-10">
        <input type="text" name="Icerik" class="form-control" value="<?php echo e(@$veri->Icerik); ?>"></input>
    </div>
</div>
<div class="form-group" id="UrunOzellikleriAna" <?php if ( ! ( @$veri->Gosterim == 2 )): ?> style="display:none" <?php endif; ?>  >
    <label for="Icerik2" class="col-sm-2 control-label">Ana Maddesi</label>
    <div class="col-sm-10">
        <input type="text" name="Icerik2" class="form-control" value="<?php echo e(@$veri->Icerik2); ?>"></input>
    </div>
</div>
<div class="form-group" id="UrunOzellikleriRenk"  <?php if ( ! ( @$veri->Gosterim == 2 )): ?>style="display: none" <?php endif; ?>>
    <label for="Resim3" class="col-sm-2 control-label">Ürün Farklı Renkleri</label>
    <div class="col-sm-10">
        <input type="file" class="form-control" name="Resim3[]" id="Resim3"  multiple >
    </div>
    <?php if( @$veri->Resim3 ): ?>
        <div class="col-md-10 col-md-offset-2 container container-fluid" >
            <div class="checkbox">
                <?php foreach(json_decode(@$veri->Resim3)  as $key=>$value): ?>
                <a>
                    <img class="img" src="<?php echo e(url('images/uploads/urun/resim/renk/'.$value )); ?>" width="150" height="150" alt="">
                    <input type="checkbox"  name="secili_belge_resim2[]"   <?php if( @$value ): ?> checked value="<?php echo e($value); ?>" <?php else: ?> value="0" <?php endif; ?>>
                </a>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endif; ?>
</div>





<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('select>option:eq(1)').attr('selected', true);
        $('.gizli').hide();

         

    });
    function UrunGosterme() {
        var tip = $('#urun_gosterme_sekli').val();

        if (tip == "1") {//Katalog resimleri
            $('#Katalog').show('slow');
            $('#UrunOzellikleriBoyut').hide('slow');
            $('#UrunOzellikleriAna').hide('slow');           
            $('#UrunOzellikleriRenk').hide('slow');
            $('#AltKategori').hide('slow');

        } 
        else if (tip == "2") {          //Ürün Özellikleri
            $('#Katalog').hide('slow');
            $('#UrunOzellikleriBoyut').show('slow');
            $('#UrunOzellikleriAna').show('slow');           
            $('#UrunOzellikleriRenk').show('slow');
            $('#AltKategori').hide('slow');



        }else{
            $('#AltKategori').show('slow');
            $('#Katalog').hide('slow');
            $('#UrunOzellikleriBoyut').hide('slow');
            $('#UrunOzellikleriAna').hide('slow');           
            $('#UrunOzellikleriRenk').hide('slow');
        }
       
        
    }

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>