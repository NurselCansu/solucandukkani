<?php
	$url = Request::url();
	$kontrol = strpos($url,'/Duzenle');
	$mesaj = ($kontrol)?' Güncelleme':' Ekleme';
        if(!isset($formrefresh)){
            $formrefresh = true;
        }
?>
<?php $__env->startSection('title'); ?><?php echo $__env->yieldContent('baslik'); ?><?php echo e($mesaj); ?><?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="panel panel-default">
 	<div class="panel-heading">
  		<h3 class="panel-title"><?php echo $__env->yieldContent('title'); ?>
  			<div class="progress pull-right" style="width: 200px;display: none;">
				<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" id="uploadbar"></div>
			</div>
		</h3>
	</div>
 	<div class="panel-body">
		<form method="post" enctype="multipart/form-data" class="form-horizontal" action="javascript:;" id="form">
		<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
            <?php if(isset($Dil) && $Dil): ?>
			<div class="form-group gizli" >
			    <label for="Dil" class="col-sm-2 control-label">Dil</label>
			    <div class="col-sm-10">
			    	<select class="form-control" id="Dil" required name="Dil">
                       <option data-bayrak="flag.png">Lütfen Dil Seçiniz</option>
                    	<?php foreach($tumdiller as $td): ?>
                    	    <option <?php if($Dil && (@$veri->DilId == $td->id)): ?> selected <?php endif; ?> data-bayrak="<?php echo e(@$td->Resim); ?>" value="<?php echo e($td->id); ?>"><?php echo e($td->UzunAd); ?> </option>
                    	<?php endforeach; ?>
			    	</select>
			    </div>
			</div>
			<?php endif; ?>
			<?php echo $__env->yieldContent('form'); ?>
            <?php if(isset($MetaAlani) && $MetaAlani): ?>
 				<div class="form-group">
 				    <label class="col-sm-2 control-label">Meta Keywords</label>
 				    <div class="col-sm-10">
 				        <select type="text" class="form-control" required multiple value="<?php echo e(@$veri->MetaTag); ?>" name="MetaTag[]" id="MetaTag" placeholder="Meta Keywords">
 				        	<?php if(!empty(@$veri->MetaTag)): ?>
        						<?php foreach(json_decode(@$veri->MetaTag) as $tag): ?>
         						   <option value="<?php echo e($tag); ?>" selected="selected"><?php echo e($tag); ?></option>
						        <?php endforeach; ?>
						    <?php endif; ?>    
 				        </select>
 				    </div>
 				</div>
 				<div class="form-group">
 				    <label class="col-sm-2 control-label">Meta Title</label>
 				    <div class="col-sm-10">
 				        <input type="text" class="form-control" required value="<?php echo e(@$veri->MetaTitle); ?>" name="MetaTitle" id="MetaTitle" placeholder="Meta Title">
 				    </div>
 				</div>
 				<div class="form-group">
 				    <label class="col-sm-2 control-label">Meta Description</label>
 				    <div class="col-sm-10">
 				        <input type="text" class="form-control" required value="<?php echo e(@$veri->MetaDescription); ?>" name="MetaDescription" id="MetaDescription" placeholder="Meta Description">
 				    </div>
 				</div>
 			<?php endif; ?>
			<?php if(isset($is_active) && $is_active): ?>
			<div class="form-group is_active">
			    <label for="is_active" class="col-sm-2 control-label">Durum</label>
			    <div class="col-sm-10">
			    	<select class="form-control" id="is_active" required name="is_active">
			    		<?php echo Fnk::Option($AktifText,@$veri->is_active); ?>

			    	</select>
			    </div>
			</div>
			<?php endif; ?>
			
			<div class="form-group onizleme" style="display: none">
				<div class="col-md-8 col-md-offset-2">
				    <div onclick="onizleme()" class="btn btn-primary">Önizle</div>
				</div>
			</div>

			<div class="form-group">
			    <div class="col-sm-offset-2 col-sm-10">
			      <button type="submit" id="btn" class="btn btn-success">Kaydet</button>
			    </div>
			  </div>
		</form>
  </div>
</div>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('jsform'); ?>


<style>
    .validateerror{
        color:#a94442;
    }
</style>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('plugins/datetimepicker/bootstrap-datetimepicker.css')); ?>">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="<?php echo e(asset('plugins/datetimepicker/moment.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/datetimepicker/tr.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/datetimepicker/bootstrap-datetimepicker.min.js')); ?>"></script>


<script src="<?php echo e(asset('js/jquery.form.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/jqueryvalidate/jquery.validate.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/jqueryvalidate/messages_tr.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/jquery.inputmask/dist/inputmask/inputmask.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/jquery.inputmask/dist/inputmask/jquery.inputmask.js')); ?>"></script>

<script src="<?php echo e(asset('plugins/ckeditor/ckeditor.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/ckeditor/adapters/jquery.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/ckfinder/ckfinder.js')); ?>"></script>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('#MetaTag').select2({
			tags: true,
    		multiple: true,
    		tokenSeparators: [',',' '],
    		
    	});

    	function formatState (state) {
 		 	if (!state.id) { return state.text; }
		  	var $state = $(
		    	'<span><img src="<?php echo e(url('/')); ?>/flags/flags/'+$(state.element).data('bayrak')+ '" class="img-flag" style="height:30px ; border-radius: 100%; " /> ' + state.text + '</span>'
		  	);
		  	return $state;
		};


    	$('#Dil').select2({
    		 templateResult: formatState
    	});
		
	});
</script>

<script type="text/javascript">
    $( document ).ready( function() {
        $(":input").inputmask();
        if($( 'textarea.ck' ).length>0){
            $( 'textarea.ck' ).ckeditor();
            if ( typeof CKEDITOR == 'undefined' ){
            }else{
            	$('.ck').each(function (index){
					var editor = $(this).ckeditorGet();
					CKFinder.setupCKEditor( editor, '<?php echo e(dirname($_SERVER["SCRIPT_NAME"])); ?>/plugins/ckfinder/' ) ;
            	});	
            }
        }
    });
function uploadbartemizle(){
	$('#uploadbar').parent('div').hide();
	var bar = $('#uploadbar');
	var percentVal = '0%';
	bar.width(percentVal);
	bar.html(percentVal);
	bar.attr('aria-valuenow',0);
}


	
	$( document ).ready(function() {
		$('#form').validate({
			ignore: ':hidden:not([class~=selectized]),:hidden > .selectized, .selectize-control .selectize-input input',
			highlight: function(element) {
	            $(element).closest('.form-group').addClass('has-error');
	        },
	        unhighlight: function(element) {
	            $(element).closest('.form-group').removeClass('has-error');
	        },
	        errorElement: 'span',
	        errorClass: 'help-block validateerror',
	        errorPlacement: function(error, element) {
	            if(element.parent('.input-group').length) {
	                error.insertAfter(element.parent());
	            } else {
	                error.insertAfter(element);
	            }
	        },
	         submitHandler: function(form) {
	         	return true;
	         }
		});
		
		var bar = $('#uploadbar');
		$('#form').ajaxForm({
			url:'<?php echo e(Request::url()); ?>',
		    beforeSend: function() {
				bar.parent('div').show();
		        var percentVal = '0%';
		        bar.width(percentVal);
		        bar.html(percentVal);
		        bar.attr('aria-valuenow',0);
		        $('#btn').attr('disabled',true);
		    },
		    uploadProgress: function(event, position, total, percentComplete) {
		        var percentVal = percentComplete + '%';
		        bar.attr('aria-valuenow',percentComplete);
		        bar.width(percentVal);
		        bar.html(percentVal);
		    },
		    success: function() {
		        var percentVal = '100%';
		        bar.attr('aria-valuenow',100);
		        bar.width(percentVal);
		        bar.html(percentVal);
		        $('#btn').removeAttr('disabled');
		        
		    },
			complete: function(xhr) {
				var sonuc = JSON.parse(xhr.responseText);
				if(sonuc.analytics){
					swal({
						title : "Uyarı !",
						text : "Google Analytics Doğrulama Kodu Ayarlaması Yapıldı.Lütfen eski ayarı düzenleyin",
						type: "warning",
						  confirmButtonColor: "#DD6B55",
						  confirmButtonText: "Anladım",
						},
						function(isConfirm){
						  if (isConfirm) {
						    window.location.href="<?php echo e(url('Admin/GoogleAnalytics/Listele')); ?>";	
						  } 
					});
				}
				else if(sonuc.google){
					swal({
						title : "Uyarı !",
						text : "Google Doğrulama Kodu Ayarlaması Yapıldı.Lütfen eski ayarı düzenleyin",
						type: "warning",
						  confirmButtonColor: "#DD6B55",
						  confirmButtonText: "Anladım",
						},
						function(isConfirm){
						  if (isConfirm) {
						    window.location.href="<?php echo e(url('Admin/Google/Listele')); ?>";	
						  } 
					});
				}
				else if(sonuc.captcha){
					swal({
						title : "Uyarı !",
						text : "ReCaptcha Ayarlaması Yapıldı.Lütfen eski ayarı düzenleyin",
						type: "warning",
						  confirmButtonColor: "#DD6B55",
						  confirmButtonText: "Anladım",
						},
						function(isConfirm){
						  if (isConfirm) {
						    window.location.href="<?php echo e(url('Admin/Captcha/Listele')); ?>";	
						  } 
					});
				}
				else if(sonuc.popup){
					var dil=$("#Dil option:selected").text();
					swal({
  						  title: "Uyarı!",
						  text: dil+" diline ait popup ayarı bulunmaktadır . Lütfen eski ayarı düzenleyin . . .",
						  type: "warning",
						  confirmButtonColor: "#DD6B55",
						  confirmButtonText: "Anladım",
						},
						function(isConfirm){
						  if (isConfirm) {
						    location.reload();	
						  } 
						});
				}
				else if(sonuc.mail ){
					var dil=$("#Dil option:selected").text();
					swal({
  						  title: "Uyarı!",
						  text: dil+" diline ait mail ayarı bulunmaktadır . Lütfen eski ayarı düzenleyin . . .",
						  type: "warning",
						  confirmButtonColor: "#DD6B55",
						  confirmButtonText: "Anladım",
						},
						function(isConfirm){
						  if (isConfirm) {
						    location.reload();	
						  } 
						});
					
				}
				else if(sonuc.islem){
					
					new PNotify({
					    title: '<?php echo $__env->yieldContent("title"); ?>',
					    text: '<?php echo $__env->yieldContent("title"); ?> İşleminiz Başarıyla Yapılmıştır.',
					    type: 'success',
					    styling: 'bootstrap3'
					});
					
						if (typeof(submitafter) === "function") {
						 	submitafter(sonuc);
						 }
					<?php if($kontrol): ?>
					
					if(typeof sonuc.yeniresim !='undefined'){
						$("#anlikresim").attr("src","<?php echo url(''); ?>/"+sonuc.yeniresim);
					}
					
					<?php else: ?>
                        <?php if($formrefresh): ?>
                                                
                        $('.ck').val('');
						$("#form")[0].reset();
                                                
						<?php endif; ?>
						if($('.ck').length>0){
							var ckname = $('.ck').ckeditorGet().name.toString();
							var string = 'CKEDITOR.instances.'+ckname+'.setData()';
							eval(string);
						}
						if($('.selectize').length>0){
							$( ".selectize" ).each(function( index ) {
								if(index%3==0)this.selectize.clear()
							});
						}
						
					//	jQuery("a[data-toggle=collapse][href=#adim1]").click();
						
					<?php endif; ?>

				
					// <?php if(Request::segment(2)=='Kategori'): ?>
					// swal({
					// 	  title: "Kategori !",
					// 	  text: "Oluşturduğunuz kategoriye ürün eklemezseniz 404 hatasına maruz kalırsınız!!!",
					// 	  type: "warning",
					// 	  showCancelButton: false,
					// 	  confirmButtonColor: "#DD6B55",
					// 	  confirmButtonText: "Tamam",
					// 	  closeOnConfirm: false,
					// 	  closeOnCancel: false
					// 		},
					// 		function(isConfirm){
					// 		  if (isConfirm) {
					// 		    location.reload();
					// 		  } 

					// 		});
					// <?php else: ?>
					// 
					// <?php endif; ?>

				/*
					<?php if(Request::segment(2)=='Kategori'): ?>
					swal({
						  title: "Kategori !",
						  text: "Oluşturduğunuz kategoriye ürün eklemezseniz 404 hatasına maruz kalırsınız!!!",
						  type: "warning",
						  showCancelButton: false,
						  confirmButtonColor: "#DD6B55",
						  confirmButtonText: "Tamam",
						  closeOnConfirm: false,
						  closeOnCancel: false
							},
							function(isConfirm){
							  if (isConfirm) {
							    location.reload();
							  } 

							});
					<?php else: ?>
					
					<?php endif; ?>*/


					location.reload();
					
				}else{
					$.each(sonuc.error, function(index, value) {
					   new PNotify({
						    title: 'HATA',
						    text: value,
						    type: 'error',
						    styling: 'bootstrap3'
						});
					});
    				$('#btn').removeAttr('disabled');
				}
				jQuery(document).ready(function () {
				   setTimeout(uploadbartemizle(),1000 );
				});
			}
		});
		
	});
	
	
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>