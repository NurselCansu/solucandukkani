	<meta charset="utf-8">
	<title><?php echo $__env->yieldContent('Title'); ?></title>
	<meta name="keywords" content="<?php echo $__env->yieldContent('MetaTag'); ?>">
	<meta name="description" content="<?php echo $__env->yieldContent('MetaDescription'); ?>" /> 
	<meta name="robots" content="index, follow"> 
	<meta class="viewport" name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!-- Favicon -->
	<link rel="shortcut icon" href="<?php echo e(asset('img/favicon.ico')); ?>"></link>

	<!-- Font -->
	<link rel='stylesheet' href='<?php echo e(asset('http://fonts.googleapis.com/css?family=Arimo:400,700,400italic,700italic')); ?>'></link>

	<!-- Plugins CSS -->
	<link rel="stylesheet" href="<?php echo e(asset('css/bootstrap.css')); ?>"></link>
	<link rel="stylesheet" href="<?php echo e(asset('css/font-awesome.min.css')); ?>"></link>
	<link rel="stylesheet" href="<?php echo e(asset('css/jslider.css')); ?>"></link>
	<link rel="stylesheet" href="<?php echo e(asset('css/revslider/settings.css')); ?>"></link>
	<link rel="stylesheet" href="<?php echo e(asset('css/jquery.fancybox.css')); ?>"></link>
	<link rel="stylesheet" href="<?php echo e(asset('css/animate.css')); ?>"></link>
	<link rel="stylesheet" href="<?php echo e(asset('css/video-js.min.css')); ?>"></link>
	<link rel="stylesheet" href="<?php echo e(asset('css/morris.css')); ?>"></link>
	<link rel="stylesheet" href="<?php echo e(asset('css/royalslider/royalslider.css')); ?>"></link>
	<link rel="stylesheet" href="<?php echo e(asset('css/royalslider/skins/minimal-white/rs-minimal-white.css')); ?>"></link>
	<link rel="stylesheet" href="<?php echo e(asset('css/layerslider/css/layerslider.css')); ?>"></link>
	<link rel="stylesheet" href="<?php echo e(asset('css/ladda.min.css')); ?>"></link>
	<link rel="stylesheet" href="<?php echo e(asset('css/datepicker.css')); ?>"></link>
	<link rel="stylesheet" href="<?php echo e(asset('css/jquery.scrollbar.css')); ?>"></link>
	<link rel="stylesheet" href="<?php echo e(asset('css/flag-icon.min.css')); ?>"></link>

	<!-- Theme CSS -->
	<link rel="stylesheet" href="<?php echo e(asset('css/style.css')); ?>"></link>
	<link rel="stylesheet" href="<?php echo e(asset('css/main.css')); ?>"></link>


	<!-- Custom CSS -->
	<link rel="stylesheet" href="<?php echo e(asset('css/customizer/pages.css')); ?>"></link>
	<link rel="stylesheet" href="<?php echo e(asset('css/customizer/shop-pages-customizer.css')); ?>"></link>

	<!-- IE Styles-->
	<link rel='stylesheet' href="<?php echo e(asset('css/ie/ie.css')); ?>"></link>
	<!--SWEET ALERT DİALOG CSS AND JS-->
	<script src="<?php echo e(url('dist/sweetalert.min.js')); ?>"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('dist/sweetalert.css')); ?>">

	<!--[if lt IE 9]>
	  <script src="<?php echo e(asset('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')); ?>"></script>
	  <script src="<?php echo e(asset('https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')); ?>"></script>
	  <link rel='stylesheet' href="<?php echo e(asset('css/ie/ie8.css')); ?>">
	<![endif]-->
        
        <script src='https://www.google.com/recaptcha/api.js'></script>
	