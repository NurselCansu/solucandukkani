
	<meta charset="utf-8">
	<title><?php echo $__env->yieldContent('Title'); ?></title>
	<meta name="keywords" content="<?php echo $__env->yieldContent('MetaTag'); ?>"/>
	<meta name="description" content="<?php echo $__env->yieldContent('MetaDescription'); ?>" /> 
	<meta name="robots" content="index, follow"> 
    <meta name="author" content="Portakal Yazılım - www.portakalyazilim.com.tr">
	<meta class="viewport" name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">


	<!-- Favicon -->
		<link rel="shortcut icon" href="<?php echo e(url('img/favicon.ico')); ?>" type="image/x-icon" />
		<link rel="apple-touch-icon" href="<?php echo e(url('img/apple-touch-icon.png')); ?>">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo e(asset('vendor/bootstrap/css/bootstrap.min.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('vendor/font-awesome/css/font-awesome.min.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('vendor/magnific-popup/magnific-popup.min.css')); ?>">
		
		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo e(asset('css/theme.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('css/theme-elements.css')); ?>">

		<link rel="stylesheet" href="<?php echo e(asset('vendor/rs-plugin/css/settings.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('vendor/rs-plugin/css/layers.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('vendor/rs-plugin/css/navigation.css')); ?>">

		
	    <!-- Owl Carousel Assets -->
	    <link href="<?php echo e(asset('/plugins/owl.carousel/owl-carousel/owl.carousel.css')); ?>" rel="stylesheet">
	    <link href="<?php echo e(asset('/plugins/owl.carousel/owl-carousel/owl.theme.css')); ?>" rel="stylesheet">

		<!-- Demo CSS -->
		<link rel="stylesheet" href="<?php echo e(asset('css/demos/demo-digital-agency.css')); ?>">


		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo e(asset('css/custom.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('dist/sweetalert.css')); ?>">



		

		