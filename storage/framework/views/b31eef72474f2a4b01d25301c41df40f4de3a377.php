<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Yönetim Paneli">
    <meta name="author" content="Portakal Yazılım - www.portakalyazilim.com.tr">
    
    <title>Admin - <?php echo $__env->yieldContent('title'); ?></title>

    <link rel="shortcut icon" href="<?php echo e(url('images/uploads/'.$ayarlar->Favicon)); ?>" type="image/x-icon" />
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo e(asset('plugins/bootstrap/dist/css/bootstrap.min.css')); ?>" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo e(asset('plugins/metisMenu/dist/metisMenu.min.css')); ?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo e(asset('css/sb-admin-2.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/lightbox.min.css')); ?>" rel="stylesheet">


    <!-- Custom Fonts -->
    <link href="<?php echo e(asset('plugins/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo e(asset('css/pnotify.custom.min.css')); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo e(asset('/plugins/cropper-master/dist/cropper.css')); ?>" rel="stylesheet" type="text/css">

    <!--SWEET ALERT DİALOG CSS AND JS-->
    <script src="<?php echo e(url('dist/sweetalert.min.js')); ?>"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('dist/sweetalert.css')); ?>">
    <style type="text/css">
        li > a [ href="#" ]{
            font-size: 15px!important;
        }  
        .nav-second-level > li {
            font-size: 12px!important;
        }
    </style>
    
    <?php echo $__env->yieldContent('css'); ?>

</head>

<?php 
use App\Http\Models\Google;
use App\Http\Models\Mail;
use App\Http\Models\MailKullanici;
use App\Http\Models\Popup;
use App\Http\Models\Tawk;

@$google = Google::where('is_active',1)->first();
@$mail = Mail::where('is_active',1)->first();
@$kullanıcı = MailKullanici::where('is_active',1)->first();
@$popup = Popup::where('is_active',1)->first();
@$tawk = Tawk::where('is_active',1)->first();

?>

<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo e(url('Admin')); ?>"> <?php echo e($ayarlar->FirmaAdi); ?></a>
            </div>
            <!-- /.navbar-header -->
            <a class="navbar-brand" style="margin-left: 30%"><center>Portakal CMS <i style="font-size:15px">versiyon 2.1</i></center></a>
          <ul class="nav navbar-top-links navbar-right">
             <?php
                $iletisimIstekleriBadge=0;
                foreach ($iletisimistekleri as $value) {
                    if($value["is_active"]==0)
                        $iletisimIstekleriBadge++;
                }
                if(count($iletisimistekleri)==0){
                    $iletisimistekleri = App\Http\Models\IletisimIstekleri::orderby('id','desc')->limit(3)->get();
                }
               
                ?>
               
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" <?php if($iletisimIstekleriBadge > 0): ?> style="color:rgb(217, 83, 79)" <?php endif; ?> href="#">
                        <?php if($iletisimIstekleriBadge >0 ): ?> <span class="badge"><?php echo e($iletisimIstekleriBadge); ?></span><?php endif; ?>
                        <i class="fa fa-envelope-o"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                     
                    <?php foreach($iletisimistekleri as $ii): ?>
                        <li>
                            <a href="<?php echo e(url('Admin/IletisimIstekleri/Detay/'.$ii->id)); ?>">
                                <div>
                                    <strong><?php echo e($ii->AdSoyad); ?> <?php if($ii->is_active==0): ?> <span class="uyari">(Yeni)</span> <?php endif; ?> </strong>
                                    <span class="pull-right text-muted">
                                        <em><?php echo e(\App\Http\Fnk::TarihFark($ii->created_at)); ?></em>
                                    </span>
                                </div>
                                <div><?php echo e(\App\Http\Fnk::KelimeBol($ii->Konu,5)); ?></div>
                            </a>
                        </li>
                        <li class="divider"></li>
                    <?php endforeach; ?>
                        <li>
                            <a class="text-center" href="<?php echo e(url('Admin/IletisimIstekleri/Listele')); ?>">
                                <strong>Tüm Mesajlar</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                </li>
            
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li>
                            <a href="<?php echo e(url('Admin/Yonetici/Duzenle/'.$admin->id)); ?>"><i class="fa fa-user fa-fw"></i> Profil Düzenle</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?php echo e(url('Admin/logout')); ?>"><i class="fa fa-sign-out fa-fw"></i> Çıkış Yap</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <?php echo $__env->make('Admin.Layout.Menuler', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
                
                <div style="padding:10px;text-align: center;margin-top: 25px;" class="hidden-xs">
                <a href="http://www.portakalyazilim.com.tr" target="_blank"><img src="<?php echo e(url('images/portakal.png')); ?>" width="100px"></a>
                </div>
                
                
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    
                    <h1 class="page-header"><?php echo $__env->yieldContent('title'); ?></h1>

                    <div class="anasayfa" style="display: none">
<?php if( in_array($admin->role, [0]) ): ?>
<!-- EKLENEN MODÜL LİSTESİ -->
                    <div class="col-lg-6 col-md-6 yesil">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-check fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="eklenen_modüller" style="font-size: 20px"></div>
                                    <div style="font-size: 25px">Aktif Modüller</div>
                                </div>
                            </div>
                        </div>
                        <div class="olanlar">
                        <!--Recaptcha Kontrolü-->
                        <?php if(@$google->site_key && @$google->secret_key): ?>
                            <a href="#">
                                <div class="panel-footer">
                                        <span class="pull-left">ReCaptcha Kodu Eklendi</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                </div>
                            </a> 
                        <?php endif; ?>
                        <!--Analytics Kontrolü-->
                        <?php if(@$google->Analytics && @$google->AnalyticsAccountId): ?>
                            <a href="#">
                                <div class="panel-footer">
                                        <span class="pull-left">Google Analytics Kodu Ekli</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                </div>
                            </a> 
                        <?php endif; ?>
                        <!--Google Site Doğrulama Kontrolü-->
                        <?php if(@$google->content): ?>
                            <a href="#">
                                <div class="panel-footer">
                                        <span class="pull-left">Google Site Doğrulama Kodu Ekli</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                </div>
                            </a> 
                        <?php endif; ?>

                        <!--Mail Ayar Kontrolü-->

                        <?php if(@$mail): ?>
                           <a href="#">
                                <div class="panel-footer">
                                        <span class="pull-left">E-posta Ayarları Yapıldı</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                </div>
                            </a> 
                        <?php endif; ?>

                        <!-- Mail Kullanıcı Kontrolü -->

                        <?php if(@$kullanıcı): ?>
                            <a href="#">
                                <div class="panel-footer">
                                        <span class="pull-left">E-posta Kullanıcısı Var</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                </div>
                            </a> 
                        <?php endif; ?>

                        <!--Popup Kontrolü-->
                        <?php if(@$popup): ?>
                            <a href="#">
                                <div class="panel-footer">
                                        <span class="pull-left">Popup Eklendi</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                </div>
                            </a> 
                        <?php endif; ?>

                        <!--Tawk Kontrolü-->
                        <?php if(@$tawk): ?>
                            <a href="#">
                                <div class="panel-footer">
                                        <span class="pull-left">Online Destek Kodu (Tawk To) Eklendi</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                </div>
                            </a> 
                        <?php endif; ?>
                        </div>

                    </div>
                    </div>
<!-- EKLENMEYEN MODÜL LİSTESİ -->
                    <div class="col-lg-6 col-md-6 kirmizi">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-warning fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="eklenmeyen_modüller" style="font-size: 20px"></div>
                                    <div style="font-size: 25px">Pasif Modüller</div>
                                </div>
                            </div>
                        </div>
                        <div class="olmayanlar">
                        <!--Recaptcha Kontrolü-->
                        <?php if(@$google->site_key && @$google->secret_key): ?>
                        <?php elseif(@$google->id): ?>
                            <a href="<?php echo e(url('/Admin/Google/Duzenle/'.@$google->id)); ?>">
                                <div class="panel-footer">
                                        <span class="pull-left">ReCaptcha Kodu Eklenmedi</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                </div>
                            </a> 
                        <?php else: ?>
                            <a href="<?php echo e(url('/Admin/Google/Ekle')); ?>">
                                <div class="panel-footer">
                                        <span class="pull-left">ReCaptcha Kodu Eklenmedi</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                </div>
                            </a>   
                        <?php endif; ?>
                        <!--Analytics Kontrolü-->
                        <?php if(@$google->Analytics && @$google->AnalyticsAccountId): ?>
                        <?php elseif(@$google->id): ?>
                            <a href="<?php echo e(url('/Admin/Google/Duzenle/'.@$google->id)); ?>">
                                <div class="panel-footer">
                                        <span class="pull-left">Google Analytics Kodu Eklenmedi</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                </div>
                            </a> 
                        <?php else: ?>
                            <a href="<?php echo e(url('/Admin/Google/Ekle')); ?>">
                                <div class="panel-footer">
                                        <span class="pull-left">Google Analytics Kodu Eklenmedi</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                </div>
                            </a>   
                        <?php endif; ?>

                        <!--Google Site Doğrulama Kontrolü-->
                        <?php if(@$google->content): ?>
                        <?php elseif(@$google->id): ?>
                            <a href="<?php echo e(url('/Admin/Google/Duzenle/'.@$google->id)); ?>">
                                <div class="panel-footer">
                                        <span class="pull-left">Google Site Doğrulama Kodu Eklenmedi</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                </div>
                            </a> 
                        <?php else: ?>
                            <a href="<?php echo e(url('/Admin/Google/Ekle')); ?>">
                                <div class="panel-footer">
                                        <span class="pull-left">Google Site Doğrulama Kodu Eklenmedi</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                </div>
                            </a>   
                        <?php endif; ?>

                        <!--Mail Ayar Kontrolü-->

                        <?php if(empty(@$mail)): ?>
                           <a href="<?php echo e(url('/Admin/Mail/Ekle')); ?>">
                                <div class="panel-footer">
                                        <span class="pull-left">E-posta Ayarları Yapılmadı</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                </div>
                            </a> 
                        <?php endif; ?>

                        <!-- Mail Kullanıcı Kontrolü -->

                        <?php if(empty(@$kullanıcı)): ?>
                            <a href="<?php echo e(url('/Admin/MailKullanici/Ekle')); ?>">
                                <div class="panel-footer">
                                        <span class="pull-left">E-posta Kullanıcısı Yok</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                </div>
                            </a> 
                        <?php endif; ?>
                        <!--Popup Kontrolü-->
                        <?php if(empty(@$popup)): ?>
                            <a href="<?php echo e(url('/Admin/Popup/Ekle')); ?>">
                                <div class="panel-footer">
                                        <span class="pull-left">Popup Eklenmedi</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                </div>
                            </a> 
                        <?php endif; ?>

                        <!--Tawk Kontrolü-->
                        <?php if(empty(@$tawk)): ?>
                            <a href="<?php echo e(url('/Admin/Tawk/Ekle')); ?>">
                                <div class="panel-footer">
                                        <span class="pull-left">Tawk To Eklenmedi</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                </div>
                            </a> 
                        <?php endif; ?>
                        </div>
                       
                    </div>
                </div>
            </div>
    <?php endif; ?>

                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
              <?php echo $__env->yieldContent('content'); ?>

            </div>
      
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<div style="padding:10px;text-align: center;" class="visiable-xs hidden-sm hidden-md hidden-lg">
                <a href="http://www.portakalyazilim.com.tr" target="_blank"><img src="<?php echo e(url('images/portakal.png')); ?>" width="100px"></a>
			</div>
    <!-- jQuery -->
    <script src="<?php echo e(asset('plugins/jquery/dist/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/lightbox.min.js')); ?>"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo e(asset('plugins/bootstrap/dist/js/bootstrap.min.js')); ?>"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo e(asset('plugins/metisMenu/dist/metisMenu.min.js')); ?>"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo e(asset('js/sb-admin-2.js')); ?>"></script>
    <script src="<?php echo e(asset('js/pnotify.custom.min.js')); ?>"></script>
    <script src="<?php echo e(asset('plugins/cropper-master/dist/cropper.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('plugins/jquery-ui/jquery-ui.js')); ?>"></script>
	
	<script>
		function AjaxPost(url,post,callback){
			post+='&_token=<?php echo e(csrf_token()); ?>'
			jQuery.post("<?php echo e(url('Admin')); ?>/"+url, post, function (data) {
				if(callback && typeof(callback) === "function") {
					callback(data);
				}
			}, "json");
		}

        <?php if(session('islem')): ?>
            alert("<?php echo e(session('islem')); ?>");
        <?php endif; ?>
	</script>
    
	
	<?php echo $__env->yieldContent('jsform'); ?>
	<?php echo $__env->yieldContent('js'); ?>
	
</body>

<script type="text/javascript">
    
    jQuery(document).ready(function($) {
        <?php if(empty(Request::segment(2))): ?>
            $('.anasayfa').show();
        <?php endif; ?>

        var eklenen = $(".olanlar a").size();
        var eklenmeyen = $(".olmayanlar a").size();

        if(eklenen == 0){
            $('.yesil').hide();
            $('.kirmizi').removeClass('col-lg-6');
            $('.kirmizi').removeClass('col-md-6');
            $('.kirmizi').addClass('col-lg-12');
            $('.kirmizi').addClass('col-md-12');

        }
        if(eklenmeyen == 0){
            $('.kirmizi').hide();
            $('.yesil').removeClass('col-lg-6');
            $('.yesil').removeClass('col-md-6');
            $('.yesil').addClass('col-lg-12');
            $('.yesil').addClass('col-md-12');
        }

        $(".eklenen_modüller").text(eklenen);
        $(".eklenmeyen_modüller").text(eklenmeyen);
    });

</script>
</html>
