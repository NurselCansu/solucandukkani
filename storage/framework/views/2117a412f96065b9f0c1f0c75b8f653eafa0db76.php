

<?php $__env->startSection('Title',$menu->MetaTitle); ?>
<?php $__env->startSection('MetaTag',$menu->MetaTag); ?>
<?php $__env->startSection('MetaDescription',$menu->MetaDescription); ?>


<?php $__env->startSection('content'); ?>

<?php if($menu->Adi=='Drifter'): ?>
           <style type="text/css">
               
               hr{

                    height: 5px!important;
                    border: none!important;
                    color: #333!important;
                    background-color: #333!important;

               }

           </style>
<?php endif; ?> 

<div class="breadcrumb-box">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="/<?php echo e($dil); ?>/"><?php echo e(\App\Http\Fnk::Ceviri('anasayfa')); ?></a> </li>
            <li class="active"><?php echo e($menu->Adi); ?></li>
        </ul>	
    </div>
</div><!-- .breadcrumb-box -->

<section id="main" style="padding-top:0px; ">
    <header class="page-header">
        <div class="container" style=" padding-left: 15px; left: 16px;">
            
            
            <h5 class="title col-lg-3 col-md-3" style="padding-top: 3%;" <?php if($menu->Adi=='Drifter'): ?> style="padding-top:3%" <?php endif; ?>><?php echo e($menu->Adi); ?></h5>
            <?php if($menu->Adi=='Drifter'): ?>
            <img class="drif col-md-9 col-lg-9 col-xs-9" src="<?php echo e(url('/images/Drifter.png')); ?>" />
            <?php endif; ?>   
        </div>	
    </header>
    <div class="container">
        <div class="row">
            <article class="bottom-padding content col-sm-12 col-md-12" style="margin-bottom:0px;">
                
            <div id="<?php echo e($menu->Adi); ?>">
                <?php echo $menu->Icerik; ?>

            </div>    
                <div class="clearfix"></div>
            </article><!-- .content -->
        </div>
    </div><!-- .container -->
</article><!-- .content -->

<?php $__env->stopSection(); ?>

<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>