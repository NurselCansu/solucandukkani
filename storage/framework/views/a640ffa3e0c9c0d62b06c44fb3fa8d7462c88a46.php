<?php $__env->startSection('Title',$menu->MetaTitle); ?>
<?php $__env->startSection('MetaTag'); ?>
    <?php echo e(implode(',', json_decode($menu->MetaTag))); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('MetaDescription',$menu->MetaDescription); ?>



<?php $__env->startSection('content'); ?>


   
<div role="main" class="main">
    <section class="page-header page-header-custom-background parallax" style="background-color: rgba(57, 74, 9, 0.85);" data-plugin-parallax data-plugin-options='{"speed": 1.5}' data-image-src="<?php echo e(asset('img/parallax.jpg')); ?>">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumb breadcrumb-valign-mid">
                        <li><a href="<?php echo e(url('/'.$dil)); ?>"><?php echo e(\App\Http\Fnk::Ceviri('anasayfa')); ?></a></li>
                        <li class="active"><?php echo e(\App\Http\Fnk::Ceviri('iletisim')); ?></li>
                    </ul>
                    <h1><?php echo e(\App\Http\Fnk::Ceviri('iletisim')); ?></h1>
                </div>
            </div>
        </div>
    </section>

</div>

<section class="section section-no-border section-light ">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-7">
                <h4 class="heading-primary "><?php echo e(\App\Http\Fnk::Ceviri('iletisim-gec')); ?></h4>
                <?php foreach($subeler as $sube): ?>
                <div class="col-md-5">
                    <div class="custom-location">
                        <img src="<?php echo e(url('/img/pin.png')); ?>" alt class="" />
                        <h6 class="font-weight-bold text-color-quaternary text-lg mb-xs"><?php echo e($sube->Adres); ?></h6>
                            <p>
                            <h6 <?php if(empty($sube->Telefon)): ?> style="display: none" <?php endif; ?>><strong>Tel:</strong> <a href="tel:+123456789" class="text-color-dark"><?php echo e($sube->Telefon); ?></a></h6>
                            <h6 <?php if(empty($sube->Fax)): ?> style="display: none" <?php endif; ?>><strong>Fax:</strong> <a href="tel:+123456789" class="text-color-dark"><?php echo e($sube->Fax); ?></a></h6>
                            <h6 <?php if(empty($sube->Eposta)): ?> style="display: none" <?php endif; ?>><strong>Email:</strong> <a href="tel:+123456789" class="text-color-dark"><?php echo e($sube->Eposta); ?></a></h6>
                           </p>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
            <div class="col-md-3 col-sm-5">
                <div class="custom-join-now-form custom-position-style-1 background-color-primary p-xlg">
                    <h4 class="font-weight-bold text-color-light mb-none"><?php echo e(\App\Http\Fnk::Ceviri('iletisim-gec')); ?></h4>
                    <p></p>
                    <form id="contactform" class="form-box register-form contact-form" method="POST" action="<?php echo e(url($dil.'/'.\App\Http\Fnk::Ceviri('iletisim-url'))); ?>" id="form">
                    <input type='hidden' name='_token' value='<?php echo e(csrf_token()); ?>' ></input>
                    <input type="hidden" class="form-control" id="dilIlet" required name="dilIlet" value="<?php echo e($dil); ?>"></input>

                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="AdSoyad" id="AdSoyad" placeholder="<?php echo e(\App\Http\Fnk::Ceviri('ad')); ?>" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="email" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="Eposta" id="Eposta" placeholder="<?php echo e(\App\Http\Fnk::Ceviri('eposta')); ?>" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="text" value="" data-msg-required="Please enter your phone." maxlength="100" class="form-control" name="Konu" id="Konu" placeholder="<?php echo e(\App\Http\Fnk::Ceviri('konu')); ?>" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input rows="3" type="text" value="" data-msg-required="Please enter your phone." maxlength="100" class="form-control" name="Mesaj" id="Mesaj" placeholder="<?php echo e(\App\Http\Fnk::Ceviri('mesaj')); ?>" required>
                                </div>
                            </div>
                        </div>
                        <?php echo e(Fnk::captchaSet()); ?>

                        <?php echo app('captcha')->display();; ?>

                        <div class="row">
                            <div class="col-md-12">
                                <input type="submit" value="<?php echo e(\App\Http\Fnk::Ceviri('gonder')); ?>" class="btn btn-default custom-btn-style-2 font-weight-bold" data-loading-text="Loading...">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>                    
<!-- Google Maps - Go to the bottom of the page to change settings and map location. -->
<div id="googlemaps" class="google-map m-none"></div>
<br/>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQKVYMlwil4C_gTM-yE5VdAbRGeqt-MBo"></script>
<script>
var map =$("#googlemaps").gMap({
        controls: {
            draggable: (($.browser.mobile) ? false : true),
            panControl: true,
            zoomControl: true,
            mapTypeControl: true,
            scaleControl: true,
            streetViewControl: true,
            overviewMapControl: true
        },

        scrollwheel: false,
        markers: [

        //Bu alan dile gore adres çekmeli!!!!
        <?php foreach($subeler as $sube): ?>

           
            {
            address: "<?php echo e($sube->Adres); ?>",
            html:"<strong align=\"center\"><?php echo e($sube->SubeAdi); ?></strong><br/><strong>Adres:</strong><?php echo e($sube->Adres); ?><br/><strong>Telefon:</strong><?php echo e($sube->Telefon); ?><br/><strong>E-posta:</strong><?php echo e($sube->Eposta); ?>",
            
            icon: {
              
                image: "<?php echo e(url('img/pin.png')); ?>",
                iconsize: [57, 66],
                iconanchor: [57, 66]

            }

        },
           
        <?php endforeach; ?>
        ],
        
        
        zoom: 6
    });
        mapRef = $('#googlemaps').data('gMap.reference');

    // Map Center At
    var mapCenterAt = function(options, e) {
        e.preventDefault();
        $('#googlemaps').gMap("centerAt", options);
    }

    // Styles from https://snazzymaps.com/
    var styles = [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}];

    var styledMap = new google.maps.StyledMapType(styles, {
        name: 'Styled Map'
    });

    mapRef.mapTypes.set('map_style', styledMap);
    mapRef.setMapTypeId('map_style');

</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('Site.Layout.MasterIletisim', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>