<?php $__env->startSection('Title',Fnk::Ceviri('solucan')); ?>
<?php $__env->startSection('MetaTag'); ?>
	<?php foreach($solucanlar as $solucan): ?>
    <?php echo @implode(', ', json_decode(@$solucan->MetaTag ,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES)); ?>, 
    <?php endforeach; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('MetaDescription',Fnk::Ceviri('solucan')); ?>


<?php $__env->startSection('content'); ?>
<div role="main" class="main" >
	<center><h1 style="font-size: 30px;color: #880e0a;"><?php echo e(Fnk::Ceviri('solucan')); ?></h1></center></br>
<div class="">    
<div class="row col-md-12 "> 
<?php foreach($solucanlar as $solucan): ?>
<a href="<?php echo e(url($dil.'/'.Fnk::Ceviri('menu-url').'/'.$solucan->link)); ?>" style="color: #555;">
	<div class="col-md-3 kirmizi_halka">
		<center ><?php echo e($solucan->Adi); ?></center>
	</div>
</a>
<?php endforeach; ?>


</div>
</div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<script>
    $(document).ready(function() {
        $('.kirmizi_halka center').each(function(index, el) {
            var yazi_uzunluk = $(this).text().length;
            var span_height = $(this).height();
            var div_height = $(this).parent().height();
            $(this).parent().css('padding-top',(div_height - span_height) / 2);
            if(yazi_uzunluk > 58){
                $(this).css({
                    'font-size' : '15px',
                    'padding-top' : (div_height - span_height - 10),
                });
            }

        });
       
       
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>