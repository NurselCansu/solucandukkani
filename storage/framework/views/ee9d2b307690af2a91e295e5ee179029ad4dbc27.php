<?php $__env->startSection('baslik', 'Haberler'); ?>

<?php $__env->startSection('form'); ?>

<div class="form-group">
    <label for="Adi" class="col-sm-2 control-label">Haber Adı</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->Adi); ?>" name="Adi" id="Adi" placeholder="Haber Adı">
    </div>
</div>

<div class="form-group">
    <label for="Resim" class="col-sm-2 control-label">Resim</label>
    <div class="col-sm-10">
    <?php if( @$veri->Resim ): ?>
        <img src="<?php echo e(url('images/uploads/Haberler/'.$veri->Resim)); ?>" style="max-height: 70px;margin-bottom: 10px;" id="anlikresim" class="img-rounded" />
        <?php endif; ?>
      <input type="file" class="form-control" name="Resim" id="Resim" placeholder="Resim">
    </div>
</div>
<div class="form-group">
    <label  class="col-sm-2 control-label">Haber İçeriği</label>
    <div class="col-sm-10">
        <textarea name="Icerik" class="ck"><?php echo e(@$veri->Icerik); ?></textarea>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Meta Keywords</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->MetaTag); ?>" name="MetaTag" id="MetaTag" placeholder="Meta Keywords">
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Meta Title</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->MetaTitle); ?>" name="MetaTitle" id="MetaDescription" placeholder="Meta Description">
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Meta Description</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->MetaDescription); ?>" name="MetaDescription" id="MetaDescription" placeholder="Meta Description">
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>