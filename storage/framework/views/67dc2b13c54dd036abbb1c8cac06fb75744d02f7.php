


<?php $__env->startSection('Title',$icerik[0]->MetaTitle); ?>
<?php $__env->startSection('MetaTag',$icerik[0]->MetaTag); ?>
<?php $__env->startSection('MetaDescription',$icerik[0]->MetaDescription); ?>

<?php $__env->startSection('content'); ?>

<div class="breadcrumb-box">
	<div class="container">
		<ul class="breadcrumb">
			<li><a href="/<?php echo e($dil); ?>/"><?php echo e(\App\Http\Fnk::Ceviri('anasayfa')); ?></a> </li>
			<li class="active"><?php echo e(\App\Http\Fnk::Ceviri('Ekibimiz')); ?></li>
		</ul>   
	</div>
</div>

<div class="clearfix"></div>

<section id="main" style="padding-top: 30px;">
	<header class="page-header" >
		<div class="container">
			<h5 class="title"><?php echo e(\App\Http\Fnk::Ceviri('Ekibimiz')); ?></h5>
		</div>  
	</header>

	<div class="container">
		<article class="content">
			<div class="title-box">
				<h2 class="title"><?php echo e(\App\Http\Fnk::Ceviri('Kurucumuz')); ?></h2>
			</div>
			<div class="bottom-padding">
				<div class="employee employee-single">
					<div class="row">
						<div class="images-box col-xs-9 col-sm-6 col-md-6">
							<div class="image">

								<img  src="<?php echo e(url('/images/uploads/ekibimiz/'.@json_decode($kurucu[0]->Fotograf)[0] )); ?>" width="350" height="350">
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="employee-description col-sm-6 col-md-6">
							<h3 class="name"><?php echo e($kurucu[0]->Adi_soyadi); ?></h3>
							<div class="role"><?php echo e($kurucu[0]->Pozisyon); ?></div>
							<div>
								<p><?php echo $icerik[0]->Icerik; ?></p>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>

			<div class="bottom-padding bottom-padding-min">
				<div class="title-box">
				  <h2 class="title"><?php echo e(\App\Http\Fnk::Ceviri('Müdürlerimiz')); ?></h2>
				</div>
				<div class="clearfix"></div>	
				<div class="row ">
				 	<?php foreach( $mudur as $mudur): ?>
				 		<div class="col-sm-3 col-md-3">
							<div class="default">
							 	<div class="image">
									<img  src="<?php echo e(url('/images/uploads/ekibimiz/'.json_decode($mudur->Fotograf)[0])); ?>" width="250">
							  	</div>
							  	<div class="description">
									<div class="vertical employee">
								  		<h3 class="name" align="center"><?php echo e($mudur->Adi_soyadi); ?></h3>
								  		<div class="role" align="center"><?php echo e($mudur->Pozisyon); ?></div>	
								  		<div class="role" align="center"><?php echo e($mudur->Telefon); ?></div>	
								  		<div class="role" align="center"><a style="color: #494949" href="mailto:<?php echo e($mudur->Email); ?>"><?php echo e($mudur->Email); ?></a></div>	
									</div>
							  	</div>
							</div>
							<div class="clearfix"></div>
						</div>
					<?php endforeach; ?>
			  	</div>
			</div>
	
			<div class="bottom-padding bottom-padding-min">
				<div class="title-box">
				  <h2 class="title"><?php echo e(\App\Http\Fnk::Ceviri('Personelimiz')); ?></h2>
				</div>
				<div class="clearfix"></div>	
				<div class="row ">
				 	<?php foreach( $personel as $personel): ?>
				 		<div class="col-sm-3 col-md-3 ">
							<div class="default">
							 	<div class="image">
									<img  src="<?php echo e(url('/images/uploads/ekibimiz/'.json_decode($personel->Fotograf)[0])); ?>" width="250">
							  	</div>
							  	<div class="description">
									<div class="vertical employee">
								  		<h3 class="name" align="center"><?php echo e($personel->Adi_soyadi); ?></h3>
								  		<div class="role" align="center"><?php echo e($personel->Pozisyon); ?></div>	
								  		<div class="role" align="center"><?php echo e($personel->Telefon); ?></div>	
								  		<div class="role" align="center"><a style="color: #494949" href="mailto:<?php echo e($personel->Email); ?>"><?php echo e($personel->Email); ?></a></div>	
									</div>
							  	</div>
							</div>
							<div class="clearfix"></div>
						</div>
					<?php endforeach; ?>
			  	</div>
			</div>

		</article><!-- .content -->
	</div>
</section><!-- #main -->


<div class="clearfix"></div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>