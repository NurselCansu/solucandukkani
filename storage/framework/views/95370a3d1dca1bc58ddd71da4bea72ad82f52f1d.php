<?php $__env->startSection('baslik','Solucan'); ?>
<?php $__env->startSection('form'); ?>


<input type="hidden" name="id" value="<?php echo e(@$veri->id); ?>">


<div class="form-group">
<label class="col-sm-2 control-label">Ana Görsel Yazısı</label>
<div class="col-sm-10">
<input type="text" name="Adi" value="<?php echo e(@$veri->Adi); ?>" class="form-control">
</div>
</div>

<div class="form-group " >
    <label class="col-sm-2 control-label">Ana Görsel Link Verme</label>
    <div class="col-sm-10">
       <select name="link" class="form-control" required>
        
           <?php foreach($menuler as $menu): ?>
                <option value="<?php echo e($menu->Slug); ?>" <?php if(($menu->Sluq) == (@$veri->link)): ?> selected <?php endif; ?>><?php echo e($menu->Adi); ?></option>
           <?php endforeach; ?>
       </select>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>