<?php $__env->startSection('header'); ?>
<div id="top-box">
    <div class="top-box-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-9 col-sm-5 white">
                    <div class="navbar navbar-inverse top-navbar top-navbar-left" role="navigation">
                        <nav>
                            <ul class="nav navbar-nav navbar-left">
                                <li style="float:left;"><a class="headertoptext" href="#"><?php echo e($ayarlar->AnaSayfaBaslik); ?></a></li>
                            </ul>
                        </nav>
                    </div>
                </div>

                <div class="col-xs-3 col-sm-7">
                    <div class="navbar navbar-inverse top-navbar top-navbar-right" role="navigation">
                        <button style="padding-right: 24px;padding-top: 12px;" type="button" class="navbar-toggle btn-navbar collapsed" data-toggle="collapse" data-target=".top-navbar .navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <nav class="collapse collapsing navbar-collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <?php foreach($tumdiller as $element): ?>
                                <li><a href="<?php echo e(url($element->KisaAd.'/cevir/'.@$segment2.'/'.@$segment3.'/'.@$segment4.'/'.@$segment5)); ?>"><img src="<?php echo e(url($element->Resim)); ?>"></img><?php echo e($element->UzunAd); ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- #top-box -->
<header class="header">
    <div class="header-wrapper">
        <div class="container">
            <div class="row" style="padding-left: 0px;">

                <div class="col-sm-5 col-xs-5 col-md-2 col-lg-3 logo-box" >
                    <div class="logo">
                        <a href="<?php echo e(url($dil)); ?>">
                            <img src="<?php echo e(url('/images/uploads/'.$ayarlar->Logo)); ?>" class="logo-img" alt="">
                        </a>
                    </div>
                </div><!-- .logo-box -->
                
                <a href=<?php echo e(url('/'.$dil.'/'.\App\Http\Fnk::Ceviri('operator-kart-url-0'))); ?> class="operatorcardcontainer socialheadericon icon rounded operator">
                    <div class="operatorcard">
                        <span class="operatorkarttext"><?php echo e(\App\Http\Fnk::Ceviri('operator-kart')); ?></span>
                    </div>
                </a>
                
                <div class="col-sm-7 col-xs-7 col-md-10 col-lg-9 " style="padding-left:0px; padding-top: 8px;">
                    <div class="headerWrapTest">
                        <div class="primary">
                            <div class="navbar navbar-default" id="ustmenu" role="navigation">
                                <?php echo \App\Http\Fnk::Menuler(); ?>

                            </div>
                        </div><!-- .primary -->

                        <div class="header-icons">
                             <div class="socialheader social social-list">
                                <?php foreach($sosyal as $element): ?>
                                <a class="socialheadericon icon rounded icon-<?php echo e($element->Adi); ?>" <?php echo e($element->LinkAcilisTipi); ?> href="<?php echo e(url($element->Link)); ?>"><i class="fa fa-<?php echo e($element->Adi); ?>"></i></a>
                                <?php endforeach; ?>

                                 <div class="search-header">
                                    <a class="blackfont" href="#">
                                        <i class="verticaltop fa fa-search" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div><!-- .header-icons -->
                    </div>
                </div>

                <div class="phone-active col-sm-9 col-md-9">
                    <a href="#" class="close"><span><?php echo e(\App\Http\Fnk::Ceviri('kapat')); ?></span>×</a>
                    <span class="title"></span> <strong style="float: right; margin-right: 25px;"><?php echo e($iletisimbilgileri->Telefon); ?></strong>
                </div>
                <div class="mail-active col-sm-9 col-md-9">
                    <a href="#" class="close"><span><?php echo e(\App\Http\Fnk::Ceviri('kapat')); ?></span>×</a>
                    <span class="title"></span> <strong style="float: right; margin-right: 25px;"><?php echo e($iletisimbilgileri->Eposta); ?></strong>
                </div>
                <div class="search-active col-sm-9 col-md-9">
                    <a href="#" class="close"><span><?php echo e(\App\Http\Fnk::Ceviri('kapat')); ?></span>×</a>
                    <form name="search-form" class="search-form">
                        <input class="search-string form-control" type="search" placeholder="<?php echo e(\App\Http\Fnk::Ceviri('arayin')); ?>" name="search-string" id="search">
                        <button class="search-submit">
                            <svg x="0" y="0" width="16px" height="16px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve">
                            <path fill="#231F20" d="M12.001,10l-0.5,0.5l-0.79-0.79c0.806-1.021,1.29-2.308,1.29-3.71c0-3.313-2.687-6-6-6C2.687,0,0,2.687,0,6
                                  s2.687,6,6,6c1.402,0,2.688-0.484,3.71-1.29l0.79,0.79l-0.5,0.5l4,4l2-2L12.001,10z M6,10c-2.206,0-4-1.794-4-4s1.794-4,4-4
                                  s4,1.794,4,4S8.206,10,6,10z"></path>
                                <image src="img/png-icons/search-icon.png" alt="" width="16" height="16" style="vertical-align: top;">
                            </svg>
                        </button>
                    </form>
                </div>
            </div><!--.row -->
        </div>
    </div><!-- .header-wrapper -->
</header><!-- .header -->
<script src="<?php echo e(asset('js/jquery-3.0.0.min.js')); ?>"></script>

<script type="text/javascript">
$(document).ready(function(){
    $('#ustmenu ul.nav li a[href="<?php echo e(urldecode(Request::fullUrl())); ?>"]').closest('.sub').closest('li').addClass('xxx');
    $('#ustmenu ul.nav li a[href="<?php echo e(urldecode(Request::fullUrl())); ?>"]').closest('li').addClass('xxx');

//Rusça menu sorunu css çözümü
    <?php if($dil=='ru'): ?>
    $('.headerWrapTest').css('font-size','13px');
    <?php endif; ?>
});
</script>


<?php $__env->stopSection(); ?>
