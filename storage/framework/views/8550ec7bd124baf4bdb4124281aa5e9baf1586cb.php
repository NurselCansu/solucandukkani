<?php $__env->startSection('title', 'Yönetici Detay'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading"><?php echo $__env->yieldContent('title'); ?></div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
				<tr>
					<th style="width:200px;">Adı Soyadı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->name); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">E-Posta Adresi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->email); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Oluşturulma Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->created_at->format('d.m.Y H:i')); ?></td>
				</tr>
				<tr>
					<th style="width:200px;">Güncelleme Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><?php echo e($veri->updated_at->format('d.m.Y H:i')); ?></td>
				</tr>
				<tr>
					<td colspan="3"><a href="<?php echo e(URL::previous()); ?>" class="btn btn-primary pull-right">Geri Dön</a></td>
				</tr>
			</table>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>