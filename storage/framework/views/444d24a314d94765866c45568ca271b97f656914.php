<?php $__env->startSection('baslik', 'Mail Kullanıcı'); ?>

<?php $__env->startSection('form'); ?>

<div class="form-group">
    <label for="KullaniciAdi" class="col-sm-2 control-label">Kullanıcı Adı </label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->KullaniciAdi); ?>" name="KullaniciAdi" id="KullaniciAdi" placeholder="KullaniciAdi">
    </div>
</div>

<div class="form-group">
    <label for="Eposta" class="col-sm-2 control-label">Mail Adresi</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="<?php echo e(@$veri->Eposta); ?>" name="Eposta" id="Eposta" placeholder="Eposta">
    </div>
</div>



<?php $__env->stopSection(); ?>


<?php echo $__env->make('Admin.Layout.Form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>