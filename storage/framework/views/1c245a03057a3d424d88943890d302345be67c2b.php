<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"
></script>
<script type="text/javascript">
	var userLang = (navigator.language || navigator.userLanguage).substring(0,2); 
	var control = false;
    <?php if( $diller->count() > 1): ?>
		<?php foreach($diller as $dil): ?>
		
			if( userLang == "<?php echo e($dil->KisaAd); ?>" ){
				
				window.location = '<?php echo e(url('/')); ?>/'+userLang;
				control = true;
			}
			else{
				
				window.location = '<?php echo e(url('/'.$dil->first()->KisaAd)); ?>';
			}
		<?php endforeach; ?>	
		if(control){
			window.location = '<?php echo e(url('/')); ?>/'+userLang;		
		}

	<?php else: ?>	
		
		window.location = '<?php echo e(url('/'.$diller->first()->KisaAd)); ?>';
	<?php endif; ?>
</script>