<?php $__env->startSection('Title',@$ayarlar->MetaTitle); ?>
<?php $__env->startSection('MetaTag'); ?>
	<?php echo @implode(', ', json_decode(@$ayarlar->MetaTag ,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES)); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('MetaDescription',@$ayarlar->MetaDescription); ?>



<?php $__env->startSection('content'); ?>			
	
<?php $__env->stopSection(); ?>
<?php $__env->startSection('css'); ?>
<style type="text/css">	

	.sweet-alert {margin: auto; transform: translateX(-50%);background-size: inherit; }
	.sweet-alert { 
		width: <?php echo e(trim(@$popup->Genislik)); ?>px;
	 	background-image: url("<?php echo e(url('images/uploads/Popup/resim/kapak/'.json_decode(@$popup->ResimKapak)[0] )); ?>") 
	 	
	 }

</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<script type="text/javascript">
<?php if(@$popup): ?>
	jQuery(document).ready(function($) {
		swal({
  			  title: '<span style="color:<?php echo e(@$popup->BaslikRenk); ?>!important" ><?php echo e(@$popup->Baslik); ?></span>',
			  text: '<span style="color:<?php echo e(@$popup->IcerikRenk); ?>!important" ><?php echo e(@$popup->Icerik); ?></span>',
			  timer: <?php echo e(@$popup->Sure); ?>*1000,
			  animation: "slide-from-top",
			  allowEscapeKey:true,
              allowOutsideClick:true,
			  showConfirmButton: false,
			  imageUrl: "<?php echo e(url('images/uploads/Popup/resim/'.json_decode(@$popup->Resim)[0] )); ?>",
			  imageSize:"<?php echo e(@$popup->FotoGenislik); ?>x<?php echo e(@$popup->FotoUzunluk); ?>",
			  html: true,
		});
	});
<?php endif; ?>
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>