<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Request;
use View;


class DilServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
         View::composer('*',function($view){
            $view->with('dil',Request::segment(1)); // Tüm viewlere linkteki dil gönderildi.
        });
        \App::setLocale(Request::segment(1));
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
