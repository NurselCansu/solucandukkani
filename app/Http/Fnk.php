<?php

namespace App\Http;

use Request;
use DB;
use Image;
use Config;
class Fnk {

     public static function ayIsmiGetir($ay){

        switch ($ay) {
            case 1:
                return "Ocak";
                break;

            case 2:
                return "Şubat";
                break;

            case 3:
                return "Mart";
                break;
                
            case 4:
                return "Nisan";
                break;

            case 5:
                return "Mayıs";
                break;

            case 6:
                return "Haziran";
                break;

            case 7:
                return "Temmuz";
                break;
                
            case 8:
                return "Ağustos";
                break;

            case 9:
                return "Eylül";
                break;

            case 10:
                return "Ekim";
                break;

            case 11:
                return "Kasım";
                break;
                
            case 12:
                return "Aralık";
                break;
            
        }

    }
    public static function OnePageHeader($dil){
        @$dil_id = \App\Http\Models\Dil::where('KisaAd',$dil)->where('is_active',1)->first()->id;
        @$onepage = \App\Http\Models\OnePage::where('is_active',1)->where('DilId',$dil_id)->get();
        if(@$onepage){
        foreach ($onepage as $key => $value) {
            if($value->Tip == "baslangic"){
                echo '  <li>
                        <a href="#home" class="menu" data-hash data-hash-offset="300">
                        '. $value->Adi .'
                        </a>
                        </li>';
            }
            else if($value->Tip == "iletisim" ){
                echo '  <li>
                        <a href="#googlemaps" class="menu" data-hash data-hash-offset="142">
                        '. $value->Adi .'
                        </a>
                        </li>';
            }
            else{
                echo '  <li>
                        <a href="#'.$value->Tip.'-' . $value->id . '" class="menu" data-hash data-hash-offset="142">
                        '. $value->Adi .'
                        </a>
                        </li>';
                }           
            
            }
        }
    }
    public static function OnePageContent($dil){
        @$dil_id = \App\Http\Models\Dil::where('KisaAd',$dil)->where('is_active',1)->first()->id;
        @$onepage = \App\Http\Models\OnePage::where('DilId',$dil_id)->where('is_active',1)->get();
        @$subeler = \App\Http\Models\IletisimBilgileri::where('DilId',$dil_id)->where('is_active',1)->get();
        @$bloglar = \App\Http\Models\Bloglar::where('DilId',$dil_id)->where('is_active',1)->orderBy('id','Desc')->take(3)->get();
        if(@$onepage){
        foreach ($onepage as $key => $value) {
           
              if($value->Tip == "klinik"){
                echo ' <div class="container">
                            <div class="row">
                            <h2 class="font-weight-semibold mb-xs" id="'.$value->Tip.'-' . $value->id . '">' . Fnk::Ceviri("klinik") . '</h2></div></div>
                                <div class="parallaxBen">
                                 <div class="container">
                                    <div class="row">
                                </br>
                                </br>
                       ';
                foreach (json_decode($value->klinik_baslik) as $key2 => $value2) {
                    
                    echo '<div class="col-md-4 appear-animation" data-appear-animation="bounceInUp" data-appear-animation-delay="' . $key2 * 1000 . '">
                            <div class="feature-box feature-box-style-5">
                                <div class="feature-box-icon">
                                    <i class="fa '. json_decode($value->klinik_icons)[$key2] .'"></i>
                                </div>
                                <div class="feature-box-info">
                                    <h4 class="mb-sm text-uppercase" style="color:white"><strong>' . $value2 . '</strong></h4>
                                    <p class="mb-lg" style="color:white">'. json_decode($value->klinik_icerik)[$key2] .'</p>
                                </div>
                            </div>
                        </div>';
                }
                    echo '</div></div></div></br></br></br>';

              }
              elseif($value->Tip == "baslangic"){
                    echo '<section class="section section-custom-medical" style="margin-top:-3%">
                <div class="container">
                    <div class="row medical-schedules">
                        <div class="col-lg-3 box-one background-color-primary appear-animation" data-appear-animation="fadeInLeft" data-appear-animation-delay="0">
                            <div class="feature-box feature-box-style-2 p-lg">
                                <div class="feature-box-icon">
                                    <img src="img/medical-icon-heart.png" alt class="img-responsive pt-xs" />
                                </div>
                                <div class="feature-box-info ml-md">
                                    <h4 class="m-none">'. $value->kutucuk1 .'</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 box-two background-color-tertiary appear-animation" data-appear-animation="fadeInLeft" data-appear-animation-delay="600">
                            <h5 class="m-none">
                                <a href="demo-medical-doctors.html" title="">
                                    ' . $value->kutucuk2 . '
                                    <i class="icon-arrow-right-circle icons"></i>
                                </a>
                            </h5>
                        </div>
                        <div class="col-lg-3 box-three background-color-primary appear-animation" data-appear-animation="fadeInLeft" data-appear-animation-delay="1200">
                            <div class="expanded-info p-xlg background-color-primary">
                                <div class="info custom-info">
                                  ' . $value->kutucuk3altyazi . '
                                </div>
                            </div>
                            <h5 class="m-none">
                                ' . $value->kutucuk3 . '
                                <i class="icon-arrow-right-circle icons"></i>
                            </h5>
                        </div>
                        <div class="col-lg-3 box-four background-color-secondary p-none appear-animation" data-appear-animation="fadeInLeft" data-appear-animation-delay="1800">
                            <a href="tel:' . @$subeler->first()->Telefon . '" class="text-decoration-none">
                                <div class="feature-box feature-box-style-2 m-none">
                                    <div class="feature-box-icon">
                                        <i class="icon-call-out icons"></i>
                                    </div>
                                    <div class="feature-box-info ml-md">
                                        <label class="font-weight-light">' . $value->kutucuk4 . '</label>
                                        <strong class="font-weight-normal">' . @$subeler->first()->Telefon .'</strong>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="section row mt-xlg pt-xlg mb-xlg pb-xs">
                        <div class="col-sm-8 col-md-8">
                            <h2 class="font-weight-semibold mb-xs">' . $value->baslangic_baslik . '</h2>
                             ' . $value->baslangic_icerik . '
                        </div>
                        <div class="col-sm-4 col-md-4">
                            <a href="' . $dil . '/' . Fnk::Ceviri("menu-url") . '/' . $value->link . '">
                                <span class="thumb-info thumb-info-lighten thumb-info-centered-info thumb-info-no-borders mt-lg" style=" background-color: #f4f4f4;>
                                    <span class="thumb-info-wrapper" style="border-radius : 100% ">
                                        <img src="images/uploads/' . $value->Resim . '"  class="img-responsive" alt="">
                                        <span class="thumb-info-title">
                                            
                                            <span class="thumb-info-type">' . @\App\Http\Models\Menu::where('is_active',1)->where('Slug',$value->link)->first()->Adi .'</span>
                                        </span>
                                    </span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </section>';
              }
              elseif($value->Tip == "doktor"){

                echo '<div class="container" id="'.$value->Tip.'-' . $value->id . '">
                    <div class="row">
                        <div class="col-md-4">
                            <a href="' . $dil . '/' . Fnk::Ceviri("menu-url") . '/' . $value->link . '">
                                <span class="thumb-info thumb-info-lighten thumb-info-centered-info thumb-info-no-borders mt-lg">
                                    <span class="thumb-info-wrapper">
                                        <img src="images/uploads/' . $value->Resim . '"  class="img-responsive" alt="">
                                        <span class="thumb-info-title">
                                            <span class="thumb-info-inner">' . $value->doktor_isim . '</span>
                                            <span class="thumb-info-type">' . @\App\Http\Models\Menu::where('is_active',1)->where('Slug',$value->link)->first()->Adi . '</span>
                                        </span>
                                    </span>
                                </span>
                            </a>
                        </div>
                        <div class="col-md-8">

                            <h2 class="mb-none">' . $value->doktor_isim . '</strong></h2>
                            <h4 class="heading-primary">
                                <span class="word-rotate active" data-plugin-options="{"delay": 4000, "animDelay": 1000}">
                                    <span class="word-rotate-items" style="width: 63.0682px; top: -21.9886px;">
                                        <span>' . $value->unvan1 . '</span>
                                        <span>' . $value->unvan2 . '</span>
                                    
                                </span>
                            </h4>

                            <hr class="solid">

                            <p>' . $value->paragraf1 . '</p>
                            <p>' . $value->paragraf2 . '</p>


                        </div>
                    </div>
                    </div>
                    <hr class="solid" style="margin-left:17%;margin-right:17%">';

              }
              elseif($value->Tip == "tedaviler"){

              }
              elseif($value->Tip == "yazılar"){

                    echo '<section class="section" id="'.$value->Tip.'-' . $value->id . '">
                <div class="container">
                    <div class="row mt-md">
                        <div class="col-md-12">
                            <h2 class="font-weight-semibold m-none">' . Fnk::Ceviri("merak-ettikleriniz") . '</h2>
                            <p class="lead font-weight-normal"></p>
                        </div>
                    </div>
                    <div class="row">';
                if(@$bloglar){
                foreach($bloglar as $key2 => $value2){


                    echo    '<div class="col-sm-4 col-md-4">
                            <a href="' . $dil . '/'. Fnk::Ceviri("bloglar") .'/' . $value2->id . '" class="text-decoration-none">
                                <span class="kart thumb-info thumb-info-side-image thumb-info-side-image-custom thumb-info-no-zoom thumb-info-no-zoom thumb-info-side-image-custom-highlight">
                                    <span id="kart-' . $value2->id . '" class="kart_img thumb-info-side-image-wrapper">
                                        <img id="image-' . $value2->id . '" alt="' . $value2->Adi . '" class="kart_wrapper img-responsive" src="images/uploads/' . $value2->Resim . '" height="100" style="display:block;height:250px!important;margin:0px auto">
                                    </span>
                                    <span class="thumb-info-caption">
                                        <span class="thumb-info-caption-text p-xl">
                                            <h4 class="font-weight-semibold mb-xs">' . $value2->Adi . '</h4>
                                            <p>' .substr($value2->Icerik, 0, 30) .' . . .</p>
                                        </span>
                                    </span>
                                </span>
                            </a>
                        </div>';
                       }
                   }

                    echo '</div>
                    <div class="row pb-xlg">
                        <div class="col-md-12 center">
                            <a href="' . $dil .'/'. Fnk::Ceviri('bloglar') . '" class="btn btn-borders btn-quaternary custom-button text-uppercase font-weight-bold blog">' . Fnk::Ceviri("fazlasi") . '</a>
                        </div>
                    </div>
                </div>
            </section>';
                

              }

           
        }
    }
    }


    public static function optimizeAndReseizeImage( $dosyaAdi , $dosyaYolu , $sikistirma){

        if (Request::hasFile($dosyaAdi))
        { 

            $img = Image::make(Request::file($dosyaAdi));
            $ad = Request::file($dosyaAdi)->getClientOriginalName();
            $img->save(public_path( $dosyaYolu.'/'.$ad),$sikistirma);
            return $ad;
        }
        else{
            return null;
        }
    }

    public static function optimizeAndReseizeMoreImage($id,$request , $dosyaAdi , $dosyaYolu , $sikistirma){
      
        if ($id == ''){ 
            if(Request::hasFile($dosyaAdi)) {
                foreach (Request::file($dosyaAdi) as $key => $file) {
                    $img = Image::make($file);
                    $ad = $file->getClientOriginalName();
                    $img->save(public_path( $dosyaYolu.'/'.$ad),$sikistirma);
                    $resimArray[$key] = $ad;
                }
                return json_encode($resimArray);
            }    

        }
        else if($id){
            if(Request::hasFile($dosyaAdi)) {
                foreach (Request::file($dosyaAdi) as $key => $file) {
                    $img = Image::make($file);
                    $ad = $file->getClientOriginalName();
                    $img->save(public_path( $dosyaYolu.'/'.$ad),$sikistirma);
                    $resimArray[$key] = $ad;     
                }
                if( count($request)>0 ){
                    foreach ($request as $key_ => $value_) {
                        $resimArray[count($resimArray)] = $value_;
                    }
                }
                
                return json_encode($resimArray);    
                
            }elseif( !( Request::hasFile($dosyaAdi) ) ) {
                if(count($request) > 0) {
                    foreach ($request as $key_ => $value__) {
                        $resimArray[$key_] = $value__;
                    }

                    return json_encode($resimArray);
                    
                }else{
                    return "";
                }
            }    
        }
    }
    
    public static function mailSet($dil){
        $mail = \App\Http\Models\Mail::where('DilId',\App\Http\Models\Dil::where('KisaAd', $dil)->first()->id)->first();
        if($mail){
            $config = array(
                'driver' => $mail->Driver,
                'host' => $mail->Host,
                'port' => $mail->Port,
                'from' => array('address' => $mail->GonderilecekMailAdres, 'name' => $mail->GonderilecekMailAdresIsmi),
                'encryption' => $mail->Encryption,
                'username' => $mail->KullaniciAdi,
                'password' => $mail->Sifre,
                'sendmail' => '/usr/sbin/sendmail -bs',
            );
            Config::set('mail',$config);
        }
    }
     public static function captchaSet(){
        $captcha = \App\Http\Models\Google::where('is_active',1)->first();
        if($captcha){
            $config = array(
                'sitekey' => $captcha->site_key,
                'secret' => $captcha->secret_key
            );
            Config::set('captcha',$config);
        }
    }

    public static function googleDogrulamaKodu(){
        @$google = \App\Http\Models\Google::where('is_active',1)->first();
        echo '<meta name="google-site-verification" content="'.@$google->content.'"/>';
    }


    public static function EnUstKategori($BaslangicId) {
        $ustkategoriid = \App\Http\Models\Kategori::select('Slug', 'UstKategoriId')->where('id', $BaslangicId)->first()->UstKategoriId;
        if ($ustkategoriid == 0) {
            $slug = \App\Http\Models\Kategori::select('Slug', 'UstKategoriId')->where('id', $BaslangicId)->first()->Slug;
            return \App\Http\Fnk::Ceviri($slug);
        } else {
            $yeniid = \App\Http\Models\Kategori::select('Slug', 'UstKategoriId')->where('id', $BaslangicId)->first()->UstKategoriId;
            return \App\Http\Fnk::EnUstKategori($yeniid);
        }
    }
  
    public static function DilIdGetir($adi){
        $dil = \App\Http\Models\Dil::where('KisaAd', $adi)->get();
        return $dil[0]->id;

    }

    public static function DilGetir($id, $alan = '') {
        $dil = \App\Http\Models\Dil::find($id);
        if ($alan == '') {
            return $dil;
        } else {
            return $dil->$alan;
        }
    }

    public static function Ceviri($slug) {
        $dilKisaAd = \App::getLocale();
        $dil = \App\Http\Models\Dil::where('KisaAd', $dilKisaAd)->first();
        if( is_object(\App\Http\Models\Dil::where('KisaAd', Request::segment(1))->get()) && !empty(\App\Http\Models\Dil::where('KisaAd', Request::segment(1))->first()->id) ) {
            $tanim = \App\Http\Models\DilTanimlama::where('DilId', $dil->id)->where('Slug', $slug)->first();                
        } else {
            return ;
        }
        if (count($tanim) > 0) {
            return $tanim->Ceviri;
        } else {
            $defaultdil = \App\Http\Models\Dil::where('DefaultDil', 1)->first();
            $tanim = \App\Http\Models\DilTanimlama::where('DilId', $defaultdil->id)->where('Slug', $slug)->first();
            if (count($tanim) > 0) {
                return $tanim->Ceviri;
            } else {
                $sabit = \App\Http\Models\DilSabiti::where('Slug', $slug)->first();
                if (count($sabit) > 0 && $sabit->SabitAdi != '') {
                    return $sabit->SabitAdi;
                } else {
                    return $slug;
                }
            }
        }
    }

    public static function Ceviriadmin($slug, $diladi) {
        $dilKisaAd = $diladi;
        $dil = \App\Http\Models\Dil::where('KisaAd', $dilKisaAd)->first();
        if (empty($dil)) {
            $dil = \App\Http\Models\Dil::first();
        }
        $tanim = \App\Http\Models\DilTanimlama::where('DilId', $dil->id)->where('Slug', $slug)->first();
        if (count($tanim) > 0) {
            return $tanim->Ceviri;
        } else {
            $defaultdil = \App\Http\Models\Dil::where('DefaultDil', 1)->first();
            $tanim = \App\Http\Models\DilTanimlama::where('DilId', $defaultdil->id)->where('Slug', $slug)->first();
            if (count($tanim) > 0) {
                return $tanim->Ceviri;
            } else {
                $sabit = \App\Http\Models\DilSabiti::where('Slug', $slug)->first();
                if (count($sabit) > 0 && $sabit->SabitAdi != '') {
                    return $sabit->SabitAdi;
                } else {
                    return $slug;
                }
            }
        }
    }

    public static function SosyalAglar() {
        $sosyalmedyalar = \App\Http\Models\SosyalMedya::where('is_active', 1)->orderby('Sira')->get();
        echo '<ul class="sosyalmedya">';
        foreach ($sosyalmedyalar as $key => $sosyal) {
            echo '<li><a href="' . url($sosyal->Link) . '" target="' . $sosyal->LinkAcilisTipi . '"><img src="' . url('images/uploads/SosyalMedya/' . $sosyal->Resim) . '"></a></li>';
        }
        echo '</ul>';
    }

    public static function RasgeleKelime($minkelimesayisi, $maxkelimesayisi) {
        $kelimeler = 'Lorem ipsum dolor sit amet consectetur adipiscing elit Integer convallis nunc in dignissim laoreet Donec ut aliquam mauris Aenean ultrices convallis nisi ac porttitor eros interdum eget Vestibulum id elementum ipsum Sed lectus dui facilisis interdum nunc eu pellentesque pulvinar nisl Nam porta quis purus ut pretium Curabitur sit amet augue commodo sagittis lectus eget ultricies elit Integer venenatis dapibus mollis Praesent tempus augue justo sed tempus erat lacinia sed Ut ante diam cursus ut dui vel semper tincidunt metus In hac habitasse platea dictumst Quisque molestie ligula tortor sed tempus diam convallis ut Aenean ultrices mattis justo cursus rhoncus est Mauris tincidunt orci vel lacus scelerisque at auctor arcu convallis Curabitur id volutpat turpis ut faucibus lorem Ut non lectus justo Pellentesque vulputate pretium gravida Maecenas vehicula nulla orci Duis sit amet ullamcorper purus Maecenas placerat turpis vel lacinia iaculis Donec in lorem lacus Mauris placerat quam in lorem interdum facilisis Aenean eu lorem magna Phasellus nec ex quis sem elementum pellentesque Nunc vestibulum turpis nunc non dapibus nisi bibendum a Proin orci nulla efficitur at tincidunt sed consectetur eu leo Donec egestas tortor sodales fringilla quam non pretium felis Sed fringilla leo tellus vel ultricies lacus cursus nec Cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus Nam ornare vestibulum odio ut sodales Etiam gravida sapien id elementum imperdiet lectus turpis convallis nibh nec maximus enim sapien et nunc Vestibulum dignissim aliquam nisi sit amet faucibus Vivamus efficitur elit vel nisl bibendum a accumsan eros aliquam Duis aliquet nunc at nisl lacinia id rhoncus risus volutpat Etiam arcu nibh tincidunt dapibus faucibus quis tristique sed arcu Praesent pellentesque augue sed velit egestas at auctor dolor dictum Curabitur vulputate mauris pulvinar laoreet iaculis Proin tincidunt hendrerit tortor ut fermentum enim porttitor id Quisque condimentum odio vel purus lacinia sollicitudin Morbi gravida sollicitudin tellus vel pellentesque Quisque venenatis tincidunt justo sit amet ultricies orci efficitur ac Praesent lobortis lacinia augue vitae mattis enim mollis vitae Sed dapibus convallis finibus Nullam vitae ultrices ipsum sed efficitur tortor Vestibulum mattis ipsum in commodo viverra Sed mattis nulla eu hendrerit suscipit Suspendisse eleifend dignissim sapien venenatis iaculis Donec pharetra augue sit amet mollis fermentum felis neque consectetur magna ac rutrum tortor tellus a eros Etiam id sapien ac turpis finibus consectetur Morbi enim nisl tempus vitae lectus sit amet scelerisque viverra risus Sed fringilla odio ipsum eget ullamcorper tellus interdum nec Sed id ante ullamcorper tincidunt orci vitae semper nunc Suspendisse ac commodo mi nec auctor nisi Proin ut ligula consectetur tincidunt purus eu volutpat arcu Duis auctor nisi in maximus fringilla Cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus Aenean laoreet scelerisque dui et bibendum nunc vulputate vel Donec mattis suscipit justo at ornare enim lacinia ullamcorper Suspendisse et felis nec augue mollis volutpat consectetur vel diam Donec feugiat dui vel aliquam sagittis Nunc luctus dui eros eget interdum risus ultricies posuere Mauris vehicula pretium urna sit amet rutrum ipsum dictum nec Sed varius diam';
        $kelimeler = explode(' ', $kelimeler);
        $cikti = '';
        $kelimesayisi = rand($minkelimesayisi, $maxkelimesayisi);
        for ($x = 0; $x < $kelimesayisi; $x++) {
            if ($x != 0) {
                $cikti.=' ';
            }
            $cikti .= $kelimeler[rand(0, 499)];
        }
        return $cikti;
    }

    public static function AdKisalt($ad) {
        $ad = self::KelimeBasHarfBuyuk($ad);
        $bol = explode(' ', $ad);
        if (count($bol) > 1) {
            return $bol[0] . ' ' . mb_substr($bol[1], 0, 1) . '.';
        } else {
            return $ad;
        }
    }

    public static function AdSoyadBol($adsoyad, $istek = 1) {
        $adsoyad = self::KelimeBasHarfBuyuk($adsoyad);
        $bol = explode(' ', $adsoyad);
        $kelimesayisi = count($bol);
        if ($kelimesayisi > 1) {
            if ($istek == 1) {
                $sadecead = '';
                for ($x = 0; $x < $kelimesayisi - 1; $x++) {
                    if ($x > 0) {
                        $sadecead.=' ';
                    }
                    $sadecead.=$bol[$x];
                }
                return $sadecead;
            } else {
                return $bol[$kelimesayisi - 1];
            }
        } else {
            return $adsoyad;
        }
    }

    public static function TarihDuzenle($tarih, $saatyaz = true) {
        if ($tarih == "") {
            return $tarih;
        }

        $saatvarmi = explode(' ', $tarih);
        if (count($saatvarmi) == 2) {
            $tarih = $saatvarmi[0];
            $saatbol = explode(':', $saatvarmi[1]);
            $saat = ' ' . $saatbol[0] . ':' . $saatbol[1];
        } else {
            $saat = '';
        }

        $tarihbol = explode(".", $tarih);
        $ayrac = "-";
        if (count($tarihbol) == 1) {
            $tarihbol = explode("-", $tarih);
            $ayrac = ".";
        }
        $gun = $tarihbol[0];
        $ay = $tarihbol[1];
        $yil = $tarihbol[2];
        if ($saatyaz) {
            return $yil . $ayrac . $ay . $ayrac . $gun . $saat;
        } else {
            return $yil . $ayrac . $ay . $ayrac . $gun;
        }
    }

    public static function YaziylaSayiKontrol() {
        $sayilar = ['1', '01', '007', '01905', '16399', '01071550', '0.30', '0.07', '0.3', '578.90', '5.05'];
        $ciktilar = ['BİR TL', 'BİR TL', 'YEDİ TL', 'BİNDOKUZYÜZBEŞ TL', 'ONALTIBİNÜÇYÜZDOKSANDOKUZ TL', 'BİRMİLYONYETMİŞBİRBİNBEŞYÜZELLİ TL', 'OTUZ KURUŞ', 'YEDİ KURUŞ', 'OTUZ KURUŞ', 'BEŞYÜZYETMİŞSEKİZ TL DOKSAN KURUŞ', 'BEŞ TL BEŞ KURUŞ'];
        foreach ($sayilar as $key => $sayi) {
            if (self::YaziylaSayi($sayi) !== $ciktilar[$key]) {
                return 'Hatalı Çıktı : ' . $sayi . ' (' . $ciktilar[$key] . ') = "' . self::YaziylaSayi($sayi) . '"';
            }
        }
        return count($sayilar) . ' Adet Kontrol Başarıyla Çalıştırıldı.';
    }

    public static function TarihFark($tarih1, $tarih2 = "", $tipi = "", $ek = true) {

        $ekstring = '';


        if ($tarih1 == '0000-00-00 00:00:00') {
            return '***';
        }
        if ($tarih2 == "") {
            $tarih2 = date("Y-m-d H:i:s");
        }
        $t1_timestamp = strtotime($tarih1);
        $t2_timestamp = strtotime($tarih2);

        if ($ek == true) {
            if ($t1_timestamp > $t2_timestamp) {
                $ekstring = ' sonra';
            } else {
                $ekstring = ' önce';
            }
        }

        if ($tipi == "sn") {
            $bol = "1";
        } else if ($tipi == "d") {
            $bol = 60;
        } else if ($tipi == "s") {
            $bol = 3600;
        } else if ($tipi == "g") {
            $bol = 86400;
        } else if ($tipi == "a") {
            $bol = 86400 * 30;
        } else if ($tipi == "y") {
            $bol = 86400 * 30 * 12;
        } else if ($tipi == "") {
            $sn = self::TarihFark($tarih1, $tarih2, "sn");
            if ($sn > 60) {
                $dk = self::TarihFark($tarih1, $tarih2, "d");
                if ($dk > 60) {
                    $s = self::TarihFark($tarih1, $tarih2, "s");
                    if ($s > 24) {
                        $g = self::TarihFark($tarih1, $tarih2, "g");
                        if ($g > 30) {
                            $a = self::TarihFark($tarih1, $tarih2, "a");
                            if ($a > 12) {
                                $y = self::TarihFark($tarih1, $tarih2, "y");
                                return $y . " yil" . $ekstring;
                            } else {
                                return $a . " ay" . $ekstring;
                            }
                        } else {
                            return $g . " gün" . $ekstring;
                        }
                    } else {
                        return $s . " saat" . $ekstring;
                    }
                } else {
                    return $dk . " dk" . $ekstring;
                }
            } else {
                return $sn . " sn" . $ekstring;
            }
        }

        if ($t1_timestamp > $t2_timestamp) {
            $result = ($t1_timestamp - $t2_timestamp) / $bol;
        } else if ($t2_timestamp > $t1_timestamp) {
            $result = ($t2_timestamp - $t1_timestamp) / $bol;
        } else {
            return 0;
        }
        return floor($result);
    }

    public static function Aylar($ay = "") {
        $aylar = array('01' => 'Ocak', '02' => 'Şubat', '03' => 'Mart', '04' => 'Nisan', '05' => 'Mayıs', '06' => 'Haziran', '07' => 'Temmuz', '08' => 'Ağustos', '09' => 'Eylül', '10' => 'Ekim', '11' => 'Kasım', '12' => 'Aralık');
        if ($ay == "") {
            return $aylar;
        } else {
            return $aylar[$ay];
        }
    }

    public static function GunAtla($x, $artieksi = '+', $format = "Y-m-d H:i:s", $tip = 'days') {
        $suan = strtotime(date("Y-m-d H:i:s"));
        $gunatla = strtotime($artieksi . $x . " " . $tip, $suan);
        if ($format != "") {
            return date($format, $gunatla);
        }

        $gun = date("d", $gunatla);
        $ay = date("m", $gunatla);
        $yil = date("Y", $gunatla);
        $saat = date("H", $gunatla);
        $dakika = date("i", $gunatla);
        $saniye = date("s", $gunatla);
        if ($format == "") {
            return (object) array("gun" => $gun, "ay" => $ay, "yil" => $yil, "saat" => $saat, "dakika" => $dakika, "saniye" => $saniye);
        }
    }

    public static function ResimAdi($klasor = 'images/uploads/', $originalname, $x = 0) {
        $array = explode('.', $originalname);
        $key = count($array) - 1;
        $sonuc = $array[$key];
        $y = 0;
        $resimadim = '';
        foreach ($array as $a) {
            $y++;
            if (count($array) != $y) {
                $resimadim.=$a;
            }
        }
        $resimadim = self::Slug($resimadim);
        $duzeltilecekler = array("JPG", "JPEG", "GIF", "PNG");
        $degistir = array("jpg", "jpg", "gif", "png");
        $resimuzantisi = str_replace($duzeltilecekler, $degistir, $sonuc);
        $ad = $resimadim;
        if ($x != 0) {
            $resimadimkontrol = $ad . '_' . $x;
        } else {
            $resimadimkontrol = $ad;
        }
        if (is_file($klasor . "/" . $resimadimkontrol . '.' . $resimuzantisi)) {
            $x++;
            return self::ResimAdi($klasor, $originalname, $x);
        } else {
            return $resimadimkontrol . '.' . $resimuzantisi;
        }
    }

    private static function SlugTekrarTemizle($bul, $slug) {
        $newslug = str_replace($bul . $bul, $bul, $slug);
        if (strpos($newslug, $bul . $bul) !== FALSE) {
            return self::SlugTekrarTemizle($bul, $newslug);
        }
        return $newslug;
    }

    public static function Slug($temizlenecekmetin) {
        $bul = array("Â", "Ç", "ç", "Ğ", "ğ", "İ", "ı", "Ö", "ö", "Ş", "ş", "Ü", "ü", " ", "\"", "é", "!", "'", "^", "+", "%", "&", "/", "(", ")", "=", "?", "<", ">", "£", "#", "$", "½", "{", "[", "]", "}", "\\", "|", "@", "€", "ß", ":", ".", ",", ";", "*", '/', '_', '’', '‘', '“', '”', '…', '–', "--");
        $degistir = array("a", "c", "c", "g", "g", "i", "i", "o", "o", "s", "s", "u", "u", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-");
        $string = str_replace($bul, $degistir, $temizlenecekmetin);
        $string = self::SlugTekrarTemizle('-', $string);
        $string = ltrim($string, '-');
        $string = rtrim($string, '-');
        return strtolower($string);
    }

    private static $KHarf = array("i", "ı", "ş", "ğ", "ö", "ü", "ç");
    private static $BHarf = array("İ", "I", "Ş", "Ğ", "Ö", "Ü", "Ç");

    public static function KelimeBasHarfBuyuk($kelime) {
        return ltrim(mb_convert_case(str_replace(array(' I', ' ı', ' İ', ' i'), array(' I', ' I', ' İ', ' İ'), ' ' . self::KucukHarf($kelime)), MB_CASE_TITLE, "UTF-8"));
    }

    public static function BuyukHarf($kelime) {
        return strtoupper(str_replace(self::$KHarf, self::$BHarf, $kelime));
    }

    public static function KucukHarf($kelime) {
        return strtolower(str_replace(self::$BHarf, self::$KHarf, $kelime));
    }

    public static function KelimeBol($cumle, $kelimesayisi) {
        $cumle = strip_tags($cumle);
        $bol = explode(' ', $cumle);
        $yenicumle = '';
        if (count($bol) > 0) {
            foreach ($bol as $key => $value) {
                if ($key != 0) {
                    $yenicumle.=' ';
                }
                if ($key < $kelimesayisi) {
                    $yenicumle.=$value;
                } else {
                    $yenicumle.='...';
                    break;
                }
            }
        } else {
            return $cumle;
        }
        return $yenicumle;
    }

    public static function DilAlani($resim_kisaad_uzunad = 'resim', $ayrac = ' | ') {
        $diller = \App\Http\Models\Dil::where('is_active', 1)->get();
        if (count($diller) > 1) {

            if ($resim_kisaad_uzunad == 'resim') {
                echo '<ul class="dilalani">';
            }
            foreach ($diller as $key => $dil) {
                if ($resim_kisaad_uzunad == 'resim') {
                    echo '<li><a href="' . url(Fnk::KucukHarf($dil->KisaAd)) . '"><img src="' . url('images/uploads/Dil/' . $dil->Resim) . '"></a></li>';
                } elseif ($resim_kisaad_uzunad == 'kisaad') {
                    if ($key > 0) {
                        echo $ayrac;
                    }
                    echo '<a href="' . url(Fnk::KucukHarf($dil->KisaAd)) . '">' . $dil->KisaAd . '</a>';
                } else {
                    if ($key > 0) {
                        echo $ayrac;
                    }
                    echo '<a href="' . url(Fnk::KucukHarf($dil->KisaAd)) . '">' . $dil->UzunAd . '</a>';
                }
            }
            if ($resim_kisaad_uzunad == 'resim') {
                echo '</ul>';
            }
        }
    }

    public static function Slider($dilid = '', $title = true, $limit = 10) {
        $veri = \App\Http\Models\Slider::where('is_active', 1)->orderBy('Sira', 'Asc')->limit($limit);
        if ($dilid != '') {
            $veri->where('DilId', $dilid);
        }
        $sliderlar = $veri->get();

        echo '<div id="carousel-slider" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">';
        foreach ($sliderlar as $key => $value) {
            if ($key == 0) {
                $class = 'active';
            } else {
                $class = '';
            }
            echo '<li data-target="#carousel-slider" data-slide-to="' . $key . '" class="' . $class . '"></li>';
        }
        echo '</ol>
  <div class="carousel-inner" role="listbox">';
        foreach ($sliderlar as $key => $slider) {
            if ($slider->SliderTipi == 'link') {
                $link = $slider->Link;
            } else {
                $link = url(Self::DilGetir($slider->DilId, 'KisaAd') . '/Slider/' . $slider->Slug);
            }
            if ($key == 0) {
                $class = 'active';
            } else {
                $class = '';
            }
            $resim = explode('.', $slider->Resim);
            $resimthumb = '';
            foreach ($resim as $x => $r) {
                if (count($resim) == $x + 1) {
                    $resimthumb.='-thumb.' . $r;
                } else {
                    if ($x != 0) {
                        $resimthumb.='.';
                    }
                    $resimthumb.=$r;
                }
            }
            if (!(\File::exists('images/uploads/Slider/' . $resimthumb))) {
                $resimthumb = $slider->Resim;
            }
            echo '<div class="item ' . $class . '">
	      <a href="' . $link . '" ' . $slider->LinkAcilisTipi . '><img src="' . url('images/uploads/Slider/' . $resimthumb) . '" alt="' . $slider->Adi . '"></a>';
            if ($title) {
                echo '<div class="carousel-caption"><a href="' . $link . '" ' . $slider->LinkAcilisTipi . ' style="color:#fff">' . $slider->Adi . '</a></div>';
            }
            echo '</div>';
        }
        echo '</div>
  <a class="left carousel-control" href="#carousel-slider" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-slider" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>';
    }

   public static function Menuler($dilid = '', $ulclass = true, $veri = '', $x = 0) {
            $dil = \Request::segment(1);
            if ($dilid == '') {
                $dilid = \App\Http\Models\Dil::where('KisaAd', $dil)->first();
                $dilid = $dilid->id;
            }
            if ($veri == '') {
                $veri = \App\Http\Models\Menu::where('is_active', 1)->where('UstKatId', 0)->where('Tip',0)->orderby('Sira', 'ASC');
                if ($dilid != '') {
                    $veri->where('DilId', $dilid);
                }
                $veri = $veri->get();
            }
            if (count($veri) > 0) {
                $ulclass = ($ulclass === true) ? 'nav navbar-nav navbar-center' : $ulclass;
                $altulclass = 'dropdown-submenu';
                
                foreach ($veri as $value) {
                    if ($value->MenuTipi == 'link' || $value->MenuTipi == 'modul') {
                        if (substr($value->Link, 0, 4) == "http") {
                            $link = $value->Link;
                        } else {
                            $link = url($value->Link);
                        }
                    } elseif ($value->MenuTipi == 'icerik') {
                        $link = url($dil . '/'.\App\Http\Fnk::Ceviriadmin('menu-url',$dil).'/' . $value->Slug);
                    }
                    $altdongu = false;
                    $span = '';
                    $spanSon = '';
                    $drop='';

                    $liclass = 'dropdown-full-color dropdown-primary';
                    if ($value->children()->where('is_active', 1)->count() > 0) {
                        $altdongu = true;
                        $span = '<ul class="dropdown-menu">';
                        $spanSon='</ul>';
                        $drop='dropdown-toggle';
                        $liclass = 'dropdown dropdown-full-color dropdown-primary';

                    }

                   
                    if (!$ulclass && $altdongu) {
                        $liclass = 'dropdown-submenu';
                    }

                    echo   '<li class="' . $liclass . ' " ><a class="'. $drop .'" href="' . $link . '" ' . $value->LinkAcilisTipi . '">' . $value->Adi . '</a>'.$span ;
                    if ($altdongu) {
                        self::Menuler($dilid, false, $value->children()->where('is_active', 1)->orderBy('Sira')->get(), $x + 1);
                    }
                    echo '</li>';
                    echo $spanSon;
                }
                
                $x++;
            }
        }


    public static function Kategoriler($dilid = '', $ulclass = true, $veri = '', $x = 0) {
        $dil = \Request::segment(1);
        if ($dilid == '') {
            $dilid = \App\Http\Models\Dil::where('KisaAd', 'tr')->first();
            $dilid = $dilid->id;
        }
        if ($veri == '') {
            $veri = \App\Http\Models\Kategori::where('is_active', 1)->where('UstKategoriId', 0)->orderby('KategoriSirasi', 'ASC');
            if ($dilid != '') {
                $veri->where('DilId', $dilid);
            }
            $veri = $veri->get();

        }
        if (count($veri) > 0) {
            if ($x == 0) {
                echo '<ul class="nav nav-list mb-xl show-bg-active">';
            }
            $ulclass = ($ulclass === true) ? '' : $ulclass;
            $altulclass = 'sub';
            echo '<ul class="' . $ulclass . '">';
            foreach ($veri as $key => $value) {
                $altdongu = false;
                $span = '';
                if ($value->children()->where('is_active', 1)->count() > 0) {
                    $altdongu = true;
                    //$span = '<span class="caret"></span>';
                }
                $liclass = '';
                $opensub = '';
                if ($altdongu) {
                    $liclass = 'parent';
                    $opensub = '<span class="open-sub"></span>';
                }
                $urunKategoriSayac = \App\Http\Models\UrunKategori::where('kategori_id', $value->id)->count();
               
                if ($urunKategoriSayac > 0 || !empty($value->Icerik)) {

                    $href = url( $dil . '/' . \App\Http\Fnk::Ceviri('urunler-url') . '/'. \App\Http\Fnk::Ceviri('kategori') .'/' . $value->Slug );
                } else {
                    $href = "javascript:void(0);";
                }

                if ($href == Request::url()) {
                    $liclass .= " active";
                }
                echo '<li class="' . $liclass . '"><a href="' . $href . '">' . $opensub . Fnk::Ceviri($value->Slug) . ' ' . $span . '</a>';
                if ($altdongu) {
                    self::Kategoriler($dilid, $altulclass, $value->children()->where('is_active', 1)->orderBy('KategoriSirasi')->get(), $x + 1);
                }
                echo '</li>';
            }
            echo '</ul>';
            if ($x == 0) {
                echo '</ul>';
            }
            $x++;
        }
    }


    public static function KategorilerTop($dilid = '', $ulclass = true, $veri = '', $x = 0) {
        $dil = \Request::segment(1);
        if ($dilid == '') {
            $dilid = \App\Http\Models\Dil::where('KisaAd', $dil)->first();
            $dilid = $dilid->id;
        }
        if ($veri == '') {
            $veri = \App\Http\Models\Kategori::where('is_active', 1)->where('UstKategoriId', 0)->orderby('KategoriSirasi', 'ASC');
            if ($dilid != '') {
                $veri->where('DilId', $dilid);
            }
            $veri = $veri->get();
        }
        if (count($veri) > 0) {
            if ($x == 0) {
                echo '';
            }
            $ulclass = ($ulclass === true) ? '' : $ulclass;
            $altulclass = '';
            echo '';
            foreach ($veri as $value) {
                $altdongu = false;
                $span = '';
                if ($value->children()->where('is_active', 1)->count() > 0) {
                    $altdongu = true;
                    //$span = '<span class="caret"></span>';
                }
                $liclass = '';
                $opensub = '';
                if ($altdongu) {
                    $liclass = '';
                    $opensub = '';
                }
                if ($value->UstkategoriId == 0) {
                    echo '<a href="/' . $dil . '/Urunler/' . $value->Slug . '" class="logoh banner">'
                    . '<img style="display:none !important;" clase="kategoritext kategoriimage" class="replace-2x" src="/images/uploads/Kategori/' . $value->Resim . '" width="127" height="79" alt="">'
                    . '<h2  style="position: absolute;" class="title">' . $value->Adi . '</h2>'
                    . '</a>';
                }
            }
            echo '';
            if ($x == 0) {
                echo '';
            }
            $x++;
        }
    }

    public static function AltMenuler($dil = 'tr', $tip = 'bootstrap', $ulclass = true, $veri = '', $x = 0) {
        $dil = \App\Http\Models\Dil::where('KisaAd', $dil)->first()->id;
        if ($veri == '') {
            $veri = \App\Http\Models\AltMenu::where('is_active', 1)->where('DilId', $dil)->where('UstKatId', 0)->orderby('Sira', 'ASC')->get();
        }
        if (count($veri) > 0) {
            foreach ($veri as $value1) {
                echo '<div class="col-md-3 col-sm-6 col-xs-12 column"><div class="footer-widget links-widget"><h4><span style="border-bottom:1px solid #ccc;padding:0px 10px 5px 10px">' . $value1->Adi . '</span></h4>';
                foreach ($value1->children()->where('is_active', 1)->orderBy('Sira')->get() as $value) {
                    if ($value->MenuTipi == 'link' || $value->MenuTipi == 'modul') {
                        if (substr($value->Link, 0, 4) == "http") {
                            $link = $value->Link;
                        } else {
                            $link = url($value->Link);
                        }
                    } elseif ($value->MenuTipi == 'icerik') {
                        $link = url($dil . '/AltMenu/' . $value->Slug);
                    }
                    echo '<div>
                       <a style="color:#fff;" href="' . $link . '" ' . $value->LinkAcilisTipi . '>' . $value->Adi . '</a>';
                    echo '</div>';
                }
                echo '</div></div>';
            }
        }
    }

    public static function ChildrenHtml($veri, $baslik = 'Adi') {
        if (count($veri) > 0) {
            echo '<ul>';
            foreach ($veri as $value) {
                echo '<li>' . $value->$baslik;
                self::ChildrenHtml($value->children, $baslik);
                echo '</li>';
            }
            echo '</ul>';
        }
    }

    public static function ChildrenOption($veri, $selectedid = FALSE, $notinid = '', $baslik = 'Adi', $x = 0, $disabled = true) {

        if (count($veri) > 0 ) {
            $x++;
            $bosluk = '';
            for ($y = 1; $y < $x; $y++) {
                $bosluk.='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
            }
            $ontaki = ($x > 1) ? $bosluk . '>>>' : '';

            foreach ($veri as $value) {

                if ($value->id != $notinid) {
                    $selected = ($value->id == $selectedid) ? 'selected' : '';
                    if ($value->children->count() > 0 ) {
                        if ($disabled) {
                            echo '<option disabled="true" value="' . $value->id . '" ' . $selected . '>' . $ontaki . $value->$baslik . '</option>';
                        } else {
                            echo '<option value="' . $value->id . '" ' . $selected . '>' . $ontaki . $value->$baslik . '</option>';
                        }
                        self::ChildrenOption($value->children, $selectedid, $notinid, $baslik, $x, $disabled);
                    } else {
                        echo '<option value="' . $value->id . '" ' . $selected . '>' . $ontaki . $value->$baslik . '</option>';
                    }
                }
            }
        }
    }

    public static function Ulkeler(){
        $ülkeler = array
(
        "VI" => "ABD Virgin Adaları","AF" => "Afganistan","AX" => "Aland Adaları","DE" => "Almanya","US" => "Amerika Birleşik Devletleri","UM" => "Amerika Birleşik Devletleri Küçük Dış Adaları","AS" => "Amerikan Samoası","AD" => "Andora","AO" => "Angola","AI" => "Anguilla","AQ" => "Antarktika","AG" => "Antigua ve Barbuda","AR" => "Arjantin","AL" => "Arnavutluk","AW" => "Aruba","QU" => "Avrupa Birliği","AU" => "Avustralya","AT" => "Avusturya","AZ" => "Azerbaycan","BS" => "Bahamalar","BH" => "Bahreyn","BD" => "Bangladeş","BB" => "Barbados","EH" => "Batı Sahara","BZ" => "Belize","BE" => "Belçika","BJ" => "Benin","BM" => "Bermuda","BY" => "Beyaz Rusya","BT" => "Bhutan","ZZ" => "Bilinmeyen veya Geçersiz Bölge","AE" => "Birleşik Arap Emirlikleri","GB" => "Birleşik Krallık","BO" => "Bolivya","BA" => "Bosna Hersek","BW" => "Botsvana","BV" => "Bouvet Adası","BR" => "Brezilya","BN" => "Brunei","BG" => "Bulgaristan","BF" => "Burkina Faso","BI" => "Burundi","CV" => "Cape Verde","EN"=>"İngilizce","GI" => "Cebelitarık","DZ" => "Cezayir","CX" => "Christmas Adası","DJ" => "Cibuti","CC" => "Cocos Adaları","CK" => "Cook Adaları","TD" => "Çad","CZ" => "Çek Cumhuriyeti","CN" => "Çin","DK" => "Danimarka","DM" => "Dominik","DO" => "Dominik Cumhuriyeti","TL" => "Doğu Timor","EC" => "Ekvator","GQ" => "Ekvator Ginesi","SV" => "El Salvador","ID" => "Endonezya","ER" => "Eritre","AM" => "Ermenistan","EE" => "Estonya","ET" => "Etiyopya","FK" => "Falkland Adaları (Malvinalar)","FO" => "Faroe Adaları","MA" => "Fas","FJ" => "Fiji","CI" => "Fildişi Sahilleri","PH" => "Filipinler","PS" => "Filistin Bölgesi","FI" => "Finlandiya","FR" => "Fransa","GF" => "Fransız Guyanası","TF" => "Fransız Güney Bölgeleri","PF" => "Fransız Polinezyası","GA" => "Gabon","GM" => "Gambia","GH" => "Gana","GN" => "Gine","GW" => "Gine-Bissau","GD" => "Granada","GL" => "Grönland","GP" => "Guadeloupe","GU" => "Guam","GT" => "Guatemala","GG" => "Guernsey","GY" => "Guyana","ZA" => "Güney Afrika","GS" => "Güney Georgia ve Güney Sandwich Adaları","KR" => "Güney Kore","CY" => "Güney Kıbrıs Rum Kesimi","GE" => "Gürcistan","HT" => "Haiti","HM" => "Heard Adası ve McDonald Adaları","IN" => "Hindistan","IO" => "Hint Okyanusu İngiliz Bölgesi","NL" => "Hollanda","AN" => "Hollanda Antilleri","HN" => "Honduras","HK" => "Hong Kong SAR - Çin","HR" => "Hırvatistan","IQ" => "Irak","VG" => "İngiliz Virgin Adaları","IR" => "İran","IE" => "İrlanda","ES" => "İspanya","IL" => "İsrail","SE" => "İsveç","CH" => "İsviçre","IT" => "İtalya","IS" => "İzlanda","JM" => "Jamaika","JP" => "Japonya","JE" => "Jersey","KH" => "Kamboçya","CM" => "Kamerun","CA" => "Kanada","ME" => "Karadağ","QA" => "Katar","KY" => "Kayman Adaları","KZ" => "Kazakistan","KE" => "Kenya","KI" => "Kiribati","CO" => "Kolombiya","KM" => "Komorlar","CG" => "Kongo","CD" => "Kongo Demokratik Cumhuriyeti","CR" => "Kosta Rika","KW" => "Kuveyt","KP" => "Kuzey Kore","MP" => "Kuzey Mariana Adaları","CU" => "Küba","KG" => "Kırgızistan","LA" => "Laos","LS" => "Lesotho","LV" => "Letonya","LR" => "Liberya","LY" => "Libya","LI" => "Liechtenstein","LT" => "Litvanya","LB" => "Lübnan","LU" => "Lüksemburg","HU" => "Macaristan","MG" => "Madagaskar","MO" => "Makao S.A.R. Çin","MK" => "Makedonya","MW" => "Malavi","MV" => "Maldivler","MY" => "Malezya","ML" => "Mali","MT" => "Malta","IM" => "Man Adası","MH" => "Marshall Adaları","MQ" => "Martinik","MU" => "Mauritius","YT" => "Mayotte","MX" => "Meksika","FM" => "Mikronezya Federal Eyaletleri","MD" => "Moldovya Cumhuriyeti","MC" => "Monako","MS" => "Montserrat","MR" => "Moritanya","MZ" => "Mozambik","MN" => "Moğolistan","MM" => "Myanmar","EG" => "Mısır","NA" => "Namibya","NR" => "Nauru","NP" => "Nepal","NE" => "Nijer","NG" => "Nijerya","NI" => "Nikaragua","NU" => "Niue","NF" => "Norfolk Adası","NO" => "Norveç","CF" => "Orta Afrika Cumhuriyeti","UZ" => "Özbekistan","PK" => "Pakistan","PW" => "Palau","PA" => "Panama","PG" => "Papua Yeni Gine","PY" => "Paraguay","PE" => "Peru","PN" => "Pitcairn","PL" => "Polonya","PT" => "Portekiz","PR" => "Porto Riko","RE" => "Reunion","RO" => "Romanya","RW" => "Ruanda","RU" => "Rusya Federasyonu","SH" => "Saint Helena","KN" => "Saint Kitts ve Nevis","LC" => "Saint Lucia","PM" => "Saint Pierre ve Miquelon","VC" => "Saint Vincent ve Grenadinler","WS" => "Samoa","SM" => "San Marino","ST" => "Sao Tome ve Principe","SN" => "Senegal","SC" => "Seyşeller","SL" => "Sierra Leone","SG" => "Singapur","SK" => "Slovakya","SI" => "Slovenya","SB" => "Solomon Adaları","SO" => "Somali","LK" => "Sri Lanka","SD" => "Sudan","SR" => "Surinam","SY" => "Suriye","SA" => "Suudi Arabistan","SJ" => "Svalbard ve Jan Mayen","SZ" => "Svaziland","RS" => "Sırbistan","CS" => "Sırbistan-Karadağ","CL" => "Şili","TJ" => "Tacikistan","TZ" => "Tanzanya","TH" => "Tayland","TW" => "Tayvan","TG" => "Togo","TK" => "Tokelau","TO" => "Tonga","TT" => "Trinidad ve Tobago","TN" => "Tunus","TC" => "Turks ve Caicos Adaları","TV" => "Tuvalu","TR" => "Türkiye","TM" => "Türkmenistan","UG" => "Uganda","UA" => "Ukrayna","OM" => "Umman","UY" => "Uruguay","QO" => "Uzak Okyanusya","JO" => "Ürdün","VU" => "Vanuatu","VA" => "Vatikan","VE" => "Venezuela","VN" => "Vietnam","WF" => "Wallis ve Futuna","YE" => "Yemen","NC" => "Yeni Kaledonya","NZ" => "Yeni Zelanda","GR" => "Yunanistan","ZM" => "Zambiya","ZW" => "Zimbabve",
    );
            return array_change_key_case($ülkeler,CASE_LOWER);

    }

    public static function Sehirler($id = "") {
        $iller = array("1" => "Adana", "2" => "Adıyaman", "3" => "Afyonkarahisar", "4" => "Ağrı", "5" => "Amasya", "6" => "Ankara", "7" => "Antalya", "8" => "Artvin", "9" => "Aydın",
            "10" => "Balıkesir", "11" => "Bilecik", "12" => "Bingöl", "13" => "Bitlis", "14" => "Bolu", "15" => "Burdur", "16" => "Bursa", "17" => "Çanakkale", "18" => "Çankırı",
            "19" => "Çorum", "20" => "Denizli", "21" => "Diyarbakır", "22" => "Edirne", "23" => "Elazığ", "24" => "Erzincan", "25" => "Erzurum", "26" => "Eskişehir", "27" => "Gaziantep",
            "28" => "Giresun", "29" => "Gümüşhane", "30" => "Hakkari", "31" => "Hatay", "32" => "Isparta", "33" => "Mersin", "34" => "İstanbul", "35" => "İzmir", "36" => "Kars",
            "37" => "Kastamonu", "38" => "Kayseri", "39" => "Kırklareli", "40" => "Kırşehir", "41" => "Kocaeli", "42" => "Konya", "43" => "Kütahya", "44" => "Malatya", "45" => "Manisa",
            "46" => "Kahramanmaraş", "47" => "Mardin", "48" => "Muğla", "49" => "Muş", "50" => "Nevşehir", "51" => "Niğde", "52" => "Ordu", "53" => "Rize", "54" => "Sakarya",
            "55" => "Samsun", "56" => "Siirt", "57" => "Sinop", "58" => "Sivas", "59" => "Tekirdağ", "60" => "Tokat", "61" => "Trabzon", "62" => "Tunceli", "63" => "Şanlıurfa",
            "64" => "Uşak", "65" => "Van", "66" => "Yozgat", "67" => "Zonguldak", "68" => "Aksaray", "69" => "Bayburt", "70" => "Karaman", "71" => "Kırıkkale", "72" => "Batman",
            "73" => "Şırnak", "74" => "Bartın", "75" => "Ardahan", "76" => "Iğdır", "77" => "Yalova", "78" => "Karabük", "79" => "Kilis", "80" => "Osmaniye", "81" => "Düzce");
        if ($id != "") {
            if (isset($iller[$id])) {
                return $iller[$id];
            } else {
                $bul = array_search(self::KelimeBasHarfBuyuk($id), $iller);
                if ($bul) {
                    return $bul;
                } else {
                    foreach ($iller as $key => $value) {
                        if (self::Slug($value) == $id) {
                            return $key;
                        }
                    }
                    return false;
                }
            }
        }

        return $iller;
    }

    public static function Ilceler($il = '', $slug = '') {
        $ilceler = array('Adana' => array('Aladağ', 'Ceyhan', 'Çukurova', 'Feke', 'İmamoğlu', 'Karaisalı', 'Karataş', 'Kozan', 'Pozantı', 'Saimbeyli', 'Sarıçam', 'Seyhan', 'Tufanbeyli', 'Yumurtalık', 'Yüreğir'), 'Adıyaman' => array('Besni', 'Çelikhan', 'Gerger', 'Gölbaşı', 'Kahta', 'Merkez', 'Samsat', 'Sincik', 'Tut'), 'Afyonkarahisar' => array('Başmakçı', 'Bayat', 'Bolvadin', 'Çay', 'Çobanlar', 'Dazkırı', 'Dinar', 'Emirdağ', 'Evciler', 'Hocalar', 'İhsaniye', 'İscehisar', 'Kızılören', 'Merkez', 'Sandıklı', 'Sinanpaşa', 'Şuhut', 'Sultandağı'), 'Ağrı' => array('Diyadin', 'Doğubayazıt', 'Eleşkirt', 'Hamur', 'Merkez', 'Patnos', 'Taşlıçay', 'Tutak'), 'Aksaray' => array('Ağaçören', 'Eskil', 'Gülağaç', 'Güzelyurt', 'Merkez', 'Ortaköy', 'Sarıyahşi'), 'Amasya' => array('Göynücek', 'Gümüşhacıköy', 'Hamamözü', 'Merkez', 'Merzifon', 'Suluova', 'Taşova'), 'Ankara' => array('Akyurt', 'Altındağ', 'Ayaş', 'Bala', 'Beypazarı', 'Çamlıdere', 'Çankaya', 'Çubuk', 'Elmadağ', 'Etimesgut', 'Evren', 'Gölbaşı', 'Güdül', 'Haymana', 'Kalecik', 'Kazan', 'Keçiören', 'Kızılcahamam', 'Mamak', 'Nallıhan', 'Polatlı', 'Pursaklar', 'Şereflikoçhisar', 'Sincan', 'Yenimahalle'), 'Antalya' => array('Akseki', 'Aksu', 'Alanya', 'Demre', 'Döşemealtı', 'Elmalı', 'Finike', 'Gazipaşa', 'Gündoğmuş', 'İbradı', 'Kaş', 'Kemer', 'Kepez', 'Konyaaltı', 'Korkuteli', 'Kumluca', 'Manavgat', 'Muratpaşa', 'Serik'), 'Ardahan' => array('Çıldır', 'Damal', 'Göle', 'Hanak', 'Merkez', 'Posof'), 'Artvin' => array('Ardanuç', 'Arhavi', 'Borçka', 'Hopa', 'Merkez', 'Murgul', 'Şavşat', 'Yusufeli'), 'Aydın' => array('Bozdoğan', 'Buharkent', 'Çine', 'Didim', 'Efeler', 'Germencik', 'İncirliova', 'Karacasu', 'Karpuzlu', 'Koçarlı', 'Köşk', 'Kuşadası', 'Kuyucak', 'Nazilli', 'Söke', 'Sultanhisar', 'Yenipazar'), 'Balıkesir' => array('Altıeylül', 'Ayvalık', 'Balya', 'Bandırma', 'Bigadiç', 'Burhaniye', 'Dursunbey', 'Edremit', 'Erdek', 'Gömeç', 'Gönen', 'Havran', 'İvrindi', 'Karesi', 'Kepsut', 'Manyas', 'Marmara', 'Savaştepe', 'Sındırgı', 'Susurluk'), 'Bartın' => array('Amasra', 'Kurucaşile', 'Merkez', 'Ulus'), 'Batman' => array('Beşiri', 'Gercüş', 'Hasankeyf', 'Kozluk', 'Merkez', 'Sason'), 'Bayburt' => array('Aydıntepe', 'Demirözü', 'Merkez'), 'Bilecik' => array('Bozüyük', 'Gölpazarı', 'İnhisar', 'Merkez', 'Osmaneli', 'Pazaryeri', 'Söğüt', 'Yenipazar'), 'Bingöl' => array('Adaklı', 'Genç', 'Karlıova', 'Kiğı', 'Merkez', 'Solhan', 'Yayladere', 'Yedisu'), 'Bitlis' => array('Adilcevaz', 'Ahlat', 'Güroymak', 'Hizan', 'Merkez', 'Mutki', 'Tatvan'), 'Bolu' => array('Dörtdivan', 'Gerede', 'Göynük', 'Kıbrıscık', 'Mengen', 'Merkez', 'Mudurnu', 'Seben', 'Yeniçağa'), 'Burdur' => array('Ağlasun', 'Altınyayla', 'Bucak', 'Çavdır', 'Çeltikçi', 'Gölhisar', 'Karamanlı', 'Kemer', 'Merkez', 'Tefenni', 'Yeşilova'), 'Bursa' => array('Büyükorhan', 'Gemlik', 'Gürsu', 'Harmancık', 'İnegöl', 'İznik', 'Karacabey', 'Keles', 'Kestel', 'Mudanya', 'Mustafakemalpaşa', 'Nilüfer', 'Orhaneli', 'Orhangazi', 'Osmangazi', 'Yenişehir', 'Yıldırım'), 'Çanakkale' => array('Ayvacık', 'Bayramiç', 'Biga', 'Bozcaada', 'Çan', 'Eceabat', 'Ezine', 'Gelibolu', 'Gökçeada', 'Lapseki', 'Merkez', 'Yenice'), 'Çankırı' => array('Atkaracalar', 'Bayramören', 'Çerkeş', 'Eldivan', 'Ilgaz', 'Kızılırmak', 'Korgun', 'Kurşunlu', 'Merkez', 'Orta', 'Şabanözü', 'Yapraklı'), 'Çorum' => array('Alaca', 'Bayat', 'Boğazkale', 'Dodurga', 'İskilip', 'Kargı', 'Laçin', 'Mecitözü', 'Merkez', 'Oğuzlar', 'Ortaköy', 'Osmancık', 'Sungurlu', 'Uğurludağ'), 'Denizli' => array('Acıpayam', 'Babadağ', 'Baklan', 'Bekilli', 'Beyağaç', 'Bozkurt', 'Buldan', 'Çal', 'Çameli', 'Çardak', 'Çivril', 'Güney', 'Honaz', 'Kale', 'Merkezefendi', 'Pamukkale', 'Sarayköy', 'Serinhisar', 'Tavas'), 'Diyarbakır' => array('Bağlar', 'Bismil', 'Çermik', 'Çınar', 'Çüngüş', 'Dicle', 'Eğil', 'Ergani', 'Hani', 'Hazro', 'Kayapınar', 'Kocaköy', 'Kulp', 'Lice', 'Silvan', 'Sur', 'Yenişehir'), 'Düzce' => array('Akçakoca', 'Çilimli', 'Cumayeri', 'Gölyaka', 'Gümüşova', 'Kaynaşlı', 'Merkez', 'Yığılca'), 'Edirne' => array('Enez', 'Havsa', 'İpsala', 'Keşan', 'Lalapaşa', 'Meriç', 'Merkez', 'Süloğlu', 'Uzunköprü'), 'Elazığ' => array('Ağın', 'Alacakaya', 'Arıcak', 'Baskil', 'Karakoçan', 'Keban', 'Kovancılar', 'Maden', 'Merkez', 'Palu', 'Sivrice'), 'Erzincan' => array('Çayırlı', 'İliç', 'Kemah', 'Kemaliye', 'Merkez', 'Otlukbeli', 'Refahiye', 'Tercan', 'Üzümlü'), 'Erzurum' => array('Aşkale', 'Aziziye', 'Çat', 'Hınıs', 'Horasan', 'İspir', 'Karaçoban', 'Karayazı', 'Köprüköy', 'Narman', 'Oltu', 'Olur', 'Palandöken', 'Pasinler', 'Pazaryolu', 'Şenkaya', 'Tekman', 'Tortum', 'Uzundere', 'Yakutiye'), 'Eskişehir' => array('Alpu', 'Beylikova', 'Çifteler', 'Günyüzü', 'Han', 'İnönü', 'Mahmudiye', 'Mihalgazi', 'Mihalıççık', 'Odunpazarı', 'Sarıcakaya', 'Seyitgazi', 'Sivrihisar', 'Tepebaşı'), 'Gaziantep' => array('Araban', 'İslahiye', 'Karkamış', 'Nizip', 'Nurdağı', 'Oğuzeli', 'Şahinbey', 'Şehitkamil', 'Yavuzeli'), 'Giresun' => array('Alucra', 'Bulancak', 'Çamoluk', 'Çanakçı', 'Dereli', 'Doğankent', 'Espiye', 'Eynesil', 'Görele', 'Güce', 'Keşap', 'Merkez', 'Piraziz', 'Şebinkarahisar', 'Tirebolu', 'Yağlıdere'), 'Gümüşhane' => array('Kelkit', 'Köse', 'Kürtün', 'Merkez', 'Şiran', 'Torul'), 'Hakkari' => array('Çukurca', 'Merkez', 'Şemdinli', 'Yüksekova'), 'Hatay' => array('Altınözü', 'Antakya', 'Arsuz', 'Belen', 'Defne', 'Dörtyol', 'Erzin', 'Hassa', 'İskenderun', 'Kırıkhan', 'Kumlu', 'Payas', 'Reyhanlı', 'Samandağ', 'Yayladağı'), 'Iğdır' => array('Aralık', 'Karakoyunlu', 'Merkez', 'Tuzluca'), 'Isparta' => array('Aksu', 'Atabey', 'Eğirdir', 'Gelendost', 'Gönen', 'Keçiborlu', 'Merkez', 'Şarkikaraağaç', 'Senirkent', 'Sütçüler', 'Uluborlu', 'Yalvaç', 'Yenişarbademli'), 'İstanbul' => array('Adalar', 'Arnavutköy', 'Ataşehir', 'Avcılar', 'Bağcılar', 'Bahçelievler', 'Bakırköy', 'Başakşehir', 'Bayrampaşa', 'Beşiktaş', 'Beykoz', 'Beylikdüzü', 'Beyoğlu', 'Büyükçekmece', 'Çatalca', 'Çekmeköy', 'Esenler', 'Esenyurt', 'Eyüp', 'Fatih', 'Gaziosmanpaşa', 'Güngören', 'Kadıköy', 'Kağıthane', 'Kartal', 'Küçükçekmece', 'Maltepe', 'Pendik', 'Sancaktepe', 'Sarıyer', 'Şile', 'Silivri', 'Şişli', 'Sultanbeyli', 'Sultangazi', 'Tuzla', 'Ümraniye', 'Üsküdar', 'Zeytinburnu'), 'İzmir' => array('Aliağa', 'Balçova', 'Bayındır', 'Bayraklı', 'Bergama', 'Beydağ', 'Bornova', 'Buca', 'Çeşme', 'Çiğli', 'Dikili', 'Foça', 'Gaziemir', 'Güzelbahçe', 'Karabağlar', 'Karaburun', 'Karşıyaka', 'Kemalpaşa', 'Kınık', 'Kiraz', 'Konak', 'Menderes', 'Menemen', 'Narlıdere', 'Ödemiş', 'Seferihisar', 'Selçuk', 'Tire', 'Torbalı', 'Urla'), 'Kahramanmaraş' => array('Afşin', 'Andırın', 'Çağlayancerit', 'Dulkadiroğlu', 'Ekinözü', 'Elbistan', 'Göksun', 'Nurhak', 'Onikişubat', 'Pazarcık', 'Türkoğlu'), 'Karabük' => array('Eflani', 'Eskipazar', 'Merkez', 'Ovacık', 'Safranbolu', 'Yenice'), 'Karaman' => array('Ayrancı', 'Başyayla', 'Ermenek', 'Kazımkarabekir', 'Merkez', 'Sarıveliler'), 'Kars' => array('Akyaka', 'Arpaçay', 'Digor', 'Kağızman', 'Merkez', 'Sarıkamış', 'Selim', 'Susuz'), 'Kastamonu' => array('Abana', 'Ağlı', 'Araç', 'Azdavay', 'Bozkurt', 'Çatalzeytin', 'Cide', 'Daday', 'Devrekani', 'Doğanyurt', 'Hanönü', 'İhsangazi', 'İnebolu', 'Küre', 'Merkez', 'Pınarbaşı', 'Şenpazar', 'Seydiler', 'Taşköprü', 'Tosya'), 'Kayseri' => array('Akkışla', 'Bünyan', 'Develi', 'Felahiye', 'Hacılar', 'İncesu', 'Kocasinan', 'Melikgazi', 'Özvatan', 'Pınarbaşı', 'Sarıoğlan', 'Sarız', 'Talas', 'Tomarza', 'Yahyalı', 'Yeşilhisar'), 'Kilis' => array('Elbeyli', 'Merkez', 'Musabeyli', 'Polateli'), 'Kırıkkale' => array('Bahşili', 'Balışeyh', 'Çelebi', 'Delice', 'Karakeçili', 'Keskin', 'Merkez', 'Sulakyurt', 'Yahşihan'), 'Kırklareli' => array('Babaeski', 'Demirköy', 'Kofçaz', 'Lüleburgaz', 'Merkez', 'Pehlivanköy', 'Pınarhisar', 'Vize'), 'Kırşehir' => array('Akçakent', 'Akpınar', 'Boztepe', 'Çiçekdağı', 'Kaman', 'Merkez', 'Mucur'), 'Kocaeli' => array('Başiskele', 'Çayırova', 'Darıca', 'Derince', 'Dilovası', 'Gebze', 'Gölcük', 'İzmit', 'Kandıra', 'Karamürsel', 'Kartepe', 'Körfez'), 'Konya' => array('Ahırlı', 'Akören', 'Akşehir', 'Altınekin', 'Beyşehir', 'Bozkır', 'Çeltik', 'Cihanbeyli', 'Çumra', 'Derbent', 'Derebucak', 'Doğanhisar', 'Emirgazi', 'Ereğli', 'Güneysınır', 'Hadim', 'Halkapınar', 'Hüyük', 'Ilgın', 'Kadınhanı', 'Karapınar', 'Karatay', 'Kulu', 'Meram', 'Sarayönü', 'Selçuklu', 'Seydişehir', 'Taşkent', 'Tuzlukçu', 'Yalıhüyük', 'Yunak'), 'Kütahya' => array('Altıntaş', 'Aslanapa', 'Çavdarhisar', 'Domaniç', 'Dumlupınar', 'Emet', 'Gediz', 'Hisarcık', 'Merkez', 'Pazarlar', 'Şaphane', 'Simav', 'Tavşanlı'), 'Malatya' => array('Akçadağ', 'Arapgir', 'Arguvan', 'Battalgazi', 'Darende', 'Doğanşehir', 'Doğanyol', 'Hekimhan', 'Kale', 'Kuluncak', 'Pütürge', 'Yazıhan', 'Yeşilyurt'), 'Manisa' => array('Ahmetli', 'Akhisar', 'Alaşehir', 'Demirci', 'Gölmarmara', 'Gördes', 'Kırkağaç', 'Köprübaşı', 'Kula', 'Salihli', 'Sarıgöl', 'Saruhanlı', 'Şehzadeler', 'Selendi', 'Soma', 'Turgutlu', 'Yunusemre'), 'Mardin' => array('Artuklu', 'Dargeçit', 'Derik', 'Kızıltepe', 'Mazıdağı', 'Midyat', 'Nusaybin', 'Ömerli', 'Savur', 'Yeşilli'), 'Mersin' => array('Akdeniz', 'Anamur', 'Aydıncık', 'Bozyazı', 'Çamlıyayla', 'Erdemli', 'Gülnar', 'Mezitli', 'Mut', 'Silifke', 'Tarsus', 'Toroslar', 'Yenişehir'), 'Muğla' => array('Bodrum', 'Dalaman', 'Datça', 'Fethiye', 'Kavaklıdere', 'Köyceğiz', 'Marmaris', 'Menteşe', 'Milas', 'Ortaca', 'Seydikemer', 'Ula', 'Yatağan'), 'Muş' => array('Bulanık', 'Hasköy', 'Korkut', 'Malazgirt', 'Merkez', 'Varto'), 'Nevşehir' => array('Acıgöl', 'Avanos', 'Derinkuyu', 'Gülşehir', 'Hacıbektaş', 'Kozaklı', 'Merkez', 'Ürgüp'), 'Niğde' => array('Altunhisar', 'Bor', 'Çamardı', 'Çiftlik', 'Merkez', 'Ulukışla'), 'Ordu' => array('Akkuş', 'Altınordu', 'Aybastı', 'Çamaş', 'Çatalpınar', 'Çaybaşı', 'Fatsa', 'Gölköy', 'Gülyalı', 'Gürgentepe', 'İkizce', 'Kabadüz', 'Kabataş', 'Korgan', 'Kumru', 'Mesudiye', 'Perşembe', 'Ulubey', 'Ünye'), 'Osmaniye' => array('Bahçe', 'Düziçi', 'Hasanbeyli', 'Kadirli', 'Merkez', 'Sumbas', 'Toprakkale'), 'Rize' => array('Ardeşen', 'Çamlıhemşin', 'Çayeli', 'Derepazarı', 'Fındıklı', 'Güneysu', 'Hemşin', 'İkizdere', 'İyidere', 'Kalkandere', 'Merkez', 'Pazar'), 'Sakarya' => array('Adapazarı', 'Akyazı', 'Arifiye', 'Erenler', 'Ferizli', 'Geyve', 'Hendek', 'Karapürçek', 'Karasu', 'Kaynarca', 'Kocaali', 'Pamukova', 'Sapanca', 'Serdivan', 'Söğütlü', 'Taraklı'), 'Samsun' => array('19.may', 'Alaçam', 'Asarcık', 'Atakum', 'Ayvacık', 'Bafra', 'Canik', 'Çarşamba', 'Havza', 'İlkadım', 'Kavak', 'Ladik', 'Salıpazarı', 'Tekkeköy', 'Terme', 'Vezirköprü', 'Yakakent'), 'Şanlıurfa' => array('Akçakale', 'Birecik', 'Bozova', 'Ceylanpınar', 'Eyyübiye', 'Halfeti', 'Haliliye', 'Harran', 'Hilvan', 'Karaköprü', 'Siverek', 'Suruç', 'Viranşehir'), 'Siirt' => array('Baykan', 'Eruh', 'Kurtalan', 'Merkez', 'Pervari', 'Şirvan', 'Tillo'), 'Sinop' => array('Ayancık', 'Boyabat', 'Dikmen', 'Durağan', 'Erfelek', 'Gerze', 'Merkez', 'Saraydüzü', 'Türkeli'), 'Şırnak' => array('Beytüşşebap', 'Cizre', 'Güçlükonak', 'İdil', 'Merkez', 'Silopi', 'Uludere'), 'Sivas' => array('Akıncılar', 'Altınyayla', 'Divriği', 'Doğanşar', 'Gemerek', 'Gölova', 'Gürün', 'Hafik', 'İmranlı', 'Kangal', 'Koyulhisar', 'Merkez', 'Şarkışla', 'Suşehri', 'Ulaş', 'Yıldızeli', 'Zara'), 'Tekirdağ' => array('Çerkezköy', 'Çorlu', 'Ergene', 'Hayrabolu', 'Kapaklı', 'Malkara', 'Marmaraereğlisi', 'Muratlı', 'Saray', 'Şarköy', 'Süleymanpaşa'), 'Tokat' => array('Almus', 'Artova', 'Başçiftlik', 'Erbaa', 'Merkez', 'Niksar', 'Pazar', 'Reşadiye', 'Sulusaray', 'Turhal', 'Yeşilyurt', 'Zile'), 'Trabzon' => array('Akçaabat', 'Araklı', 'Arsin', 'Beşikdüzü', 'Çarşıbaşı', 'Çaykara', 'Dernekpazarı', 'Düzköy', 'Hayrat', 'Köprübaşı', 'Maçka', 'Of', 'Ortahisar', 'Şalpazarı', 'Sürmene', 'Tonya', 'Vakfıkebir', 'Yomra'), 'Tunceli' => array('Çemişgezek', 'Hozat', 'Mazgirt', 'Merkez', 'Nazımiye', 'Ovacık', 'Pertek', 'Pülümür'), 'Uşak' => array('Banaz', 'Eşme', 'Karahallı', 'Merkez', 'Sivaslı', 'Ulubey'), 'Van' => array('Bahçesaray', 'Başkale', 'Çaldıran', 'Çatak', 'Edremit', 'Erciş', 'Gevaş', 'Gürpınar', 'İpekyolu', 'Muradiye', 'Özalp', 'Saray', 'Tuşba'), 'Yalova' => array('Altınova', 'Armutlu', 'Çiftlikköy', 'Çınarcık', 'Merkez', 'Termal'), 'Yozgat' => array('Akdağmadeni', 'Aydıncık', 'Boğazlıyan', 'Çandır', 'Çayıralan', 'Çekerek', 'Kadışehri', 'Merkez', 'Saraykent', 'Sarıkaya', 'Şefaatli', 'Sorgun', 'Yenifakılı', 'Yerköy'), 'Zonguldak' => array('Alaplı', 'Çaycuma', 'Devrek', 'Ereğli', 'Gökçebey', 'Kilimli', 'Kozlu', 'Merkez'));
        $il = self::KelimeBasHarfBuyuk($il);
        if ($slug !== '') {
            $il = self::Sehirler($il);
            if (isset($ilceler[$il])) {
                if (isset($ilceler[$il][$slug])) {
                    return $ilceler[$il][$slug];
                }


                foreach ($ilceler[$il] as $key => $value) {
                    if (self::Slug($value) == $slug) {
                        return $key;
                    }
                }


                return false;
            } else {
                return false;
            }
        }

        if ($il != "") {
            if (isset($ilceler[$il])) {
                return $ilceler[$il];
            } else {
                $il = self::Sehirler($il);
                if (isset($ilceler[$il])) {
                    return $ilceler[$il];
                } else {
                    return false;
                }
            }
        }

        return $ilceler;
    }

    public static function SehirKordinatlari($il = '') {
        $kordinatlar = array('1' => array('lat' => '35.3213330000', 'lng' => '37.0000000000'), '2' => array('lat' => '38.2785610000', 'lng' => '37.7647510000'), '3' => array('lat' => '30.5403400000', 'lng' => '38.7637600000'), '4' => array('lat' => '43.0484270000', 'lng' => '39.7188320000'), '5' => array('lat' => '35.8353200000', 'lng' => '40.6499100000'), '6' => array('lat' => '32.8541100000', 'lng' => '39.9207700000'), '7' => array('lat' => '30.7056300000', 'lng' => '36.8841400000'), '8' => array('lat' => '41.8182910000', 'lng' => '41.1827700000'), '9' => array('lat' => '27.8780280000', 'lng' => '37.8521680000'), '10' => array('lat' => '27.8826100000', 'lng' => '39.6483690000'), '11' => array('lat' => '29.9830610000', 'lng' => '40.1501310000'), '12' => array('lat' => '40.4963890000', 'lng' => '38.8847220000'), '13' => array('lat' => '42.1231800000', 'lng' => '38.3937990000'), '14' => array('lat' => '31.6115610000', 'lng' => '40.7394790000'), '15' => array('lat' => '30.2888760000', 'lng' => '37.7269090000'), '16' => array('lat' => '29.0634480000', 'lng' => '40.2668640000'), '17' => array('lat' => '26.4141600000', 'lng' => '40.1553120000'), '18' => array('lat' => '33.6134210000', 'lng' => '40.6013430000'), '19' => array('lat' => '34.9555560000', 'lng' => '40.5505560000'), '20' => array('lat' => '29.0863900000', 'lng' => '37.7765200000'), '21' => array('lat' => '40.2306290000', 'lng' => '37.9144100000'), '22' => array('lat' => '26.5622690000', 'lng' => '41.6818080000'), '23' => array('lat' => '39.2263980000', 'lng' => '38.6809690000'), '24' => array('lat' => '39.5000000000', 'lng' => '39.7500000000'), '25' => array('lat' => '41.2700000000', 'lng' => '39.9000000000'), '26' => array('lat' => '30.5205560000', 'lng' => '39.7766670000'), '27' => array('lat' => '37.3833200000', 'lng' => '37.0662200000'), '28' => array('lat' => '38.3895300000', 'lng' => '40.9128110000'), '29' => array('lat' => '39.5085560000', 'lng' => '40.4385880000'), '30' => array('lat' => '37.5742780000', 'lng' => '43.7347920000'), '31' => array('lat' => '36.3498100000', 'lng' => '36.4018490000'), '32' => array('lat' => '30.5565610000', 'lng' => '37.7647710000'), '33' => array('lat' => '34.6333330000', 'lng' => '36.8000000000'), '34' => array('lat' => '28.9769600000', 'lng' => '41.0052700000'), '35' => array('lat' => '27.1287200000', 'lng' => '38.4188500000'), '36' => array('lat' => '43.1000000000', 'lng' => '40.6166670000'), '37' => array('lat' => '33.7827300000', 'lng' => '41.3887100000'), '38' => array('lat' => '35.4787290000', 'lng' => '38.7312200000'), '39' => array('lat' => '27.2166670000', 'lng' => '41.7333330000'), '40' => array('lat' => '34.1709100000', 'lng' => '39.1424900000'), '41' => array('lat' => '29.8815200000', 'lng' => '40.8532700000'), '42' => array('lat' => '32.4833330000', 'lng' => '37.8666670000'), '43' => array('lat' => '29.9833330000', 'lng' => '39.4166670000'), '44' => array('lat' => '38.3094600000', 'lng' => '38.3551900000'), '45' => array('lat' => '27.4289210000', 'lng' => '38.6190990000'), '46' => array('lat' => '36.9371490000', 'lng' => '37.5858310000'), '47' => array('lat' => '40.7244770000', 'lng' => '37.3211630000'), '48' => array('lat' => '28.3636110000', 'lng' => '37.2152780000'), '49' => array('lat' => '41.5064820000', 'lng' => '38.7432930000'), '50' => array('lat' => '34.7122220000', 'lng' => '38.6250000000'), '51' => array('lat' => '34.6833330000', 'lng' => '37.9666670000'), '52' => array('lat' => '37.8764110000', 'lng' => '40.9838790000'), '53' => array('lat' => '40.5234490000', 'lng' => '41.0200500000'), '54' => array('lat' => '30.4357630000', 'lng' => '40.6939970000'), '55' => array('lat' => '36.3312800000', 'lng' => '41.2927820000'), '56' => array('lat' => '41.9500000000', 'lng' => '37.9333330000'), '57' => array('lat' => '35.1530690000', 'lng' => '42.0231400000'), '58' => array('lat' => '37.0178790000', 'lng' => '39.7476620000'), '59' => array('lat' => '27.5166670000', 'lng' => '40.9833330000'), '60' => array('lat' => '36.5500000000', 'lng' => '40.3166670000'), '61' => array('lat' => '39.7178000000', 'lng' => '41.0014500000'), '62' => array('lat' => '39.5455170000', 'lng' => '39.1058250000'), '63' => array('lat' => '38.7969090000', 'lng' => '37.1591490000'), '64' => array('lat' => '29.4081900000', 'lng' => '38.6823010000'), '65' => array('lat' => '43.4088900000', 'lng' => '38.4891400000'), '66' => array('lat' => '34.8146900000', 'lng' => '39.8180810000'), '67' => array('lat' => '31.7987310000', 'lng' => '41.4564090000'), '68' => array('lat' => '34.0369800000', 'lng' => '38.3686900000'), '69' => array('lat' => '40.2248800000', 'lng' => '40.2551690000'), '70' => array('lat' => '33.2287480000', 'lng' => '37.1759300000'), '71' => array('lat' => '33.5152510000', 'lng' => '39.8468210000'), '72' => array('lat' => '41.1350900000', 'lng' => '37.8811680000'), '73' => array('lat' => '42.4697610000', 'lng' => '37.5187300000'), '74' => array('lat' => '32.3455810000', 'lng' => '41.6415210000'), '75' => array('lat' => '42.7021710000', 'lng' => '41.1104810000'), '76' => array('lat' => '44.0654980000', 'lng' => '39.9193000000'), '77' => array('lat' => '29.2666670000', 'lng' => '40.6500000000'), '78' => array('lat' => '32.6203500000', 'lng' => '41.2061000000'), '79' => array('lat' => '37.1212200000', 'lng' => '36.7183990000'), '80' => array('lat' => '36.2500000000', 'lng' => '37.0700000000'), '81' => array('lat' => '31.1565400000', 'lng' => '40.8438490000'));
        $il = self::KelimeBasHarfBuyuk($il);
        if ($il != "") {
            if (isset($kordinatlar[$il])) {
                return $kordinatlar[$il];
            } else {
                $il = self::Sehirler($il);
                if (isset($kordinatlar[$il])) {
                    return $kordinatlar[$il];
                } else {
                    return null;
                }
            }
        }

        return $kordinatlar;
    }

    public static function Option($basla, $bitir = "", $secim = "", $stringarray = "") {
        if (is_array($basla) or is_object($basla)) {
            $veri = '';
            foreach ($basla as $val => $key) {
                if ($val == $bitir) {
                    $sec = "selected";
                } else {
                    $sec = "";
                }
                $veri.='<option ' . $sec . ' value="' . $val . '">' . $key . '</option>';
            }
            if ($secim == 'return') {
                return $veri;
            } else {
                echo $veri;
            }
        } else {
            $don = $bitir - $basla + 1;
            for ($x = 0; $x < $don; $x++) {
                if ($basla == $secim) {
                    $sec = "selected";
                } else {
                    $sec = "";
                }
                if (is_array($stringarray)) {
                    echo '<option ' . $sec . ' value="' . $basla . '">' . $stringarray[$x] . '</option>';
                } else {
                    echo '<option ' . $sec . ' value="' . $basla . '">' . $basla . '</option>';
                }
                $basla++;
            }
        }
    }

    public static function ParentOption($veri, $selectedid = FALSE, $baslik = 'Adi', $x = 0) {
        if (count($veri) > 0) {
            $x++;
            $bosluk = '';
            for ($y = 1; $y < $x; $y++) {
                $bosluk.='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
            }
            $ontaki = ($x > 1) ? $bosluk . '>>>' : '';

            foreach ($veri as $value) {
                if (count($value->children) > 1) {
                    $selected = ($value->id == $selectedid) ? 'selected' : '';
                    echo '<option value="' . $value->id . '" ' . $selected . '>' . $ontaki . $value->$baslik . '</option>';
                    self::ParentOption($value->children, $selectedid, $baslik, $x);
                }
            }
        }
    }

    public static function ParentHtml($element, $class, $linkyapisi, $linkoperator) {
        if ($element->parents) {
            self::ParentHtml($element->parents, '', $linkyapisi, $linkoperator);
        }
        if ($class == '') {
            $link = self::KategoriLink($element, $linkyapisi, $linkoperator);
            echo '<li ' . $class . '><a href="' . url($link) . '">' . $element->Adi . '</a></li>';
        } else {
            echo '<li ' . $class . '>' . $element->Adi . '</li>';
        }
    }

    public static function ChildrenIds($veri) {
        $ids = array($veri->id);
        if (count($veri->children) > 0) {
            foreach ($veri->children as $value) {
                $altids = self::ChildrenIds($value);
                foreach ($altids as $altid) {
                    $ids[] = $altid;
                }
            }
        }
        return $ids;
    }

    public static function Console($e) {
        echo '<script>console.log("' . $e . '");</script>';
    }

    public static function hex2rgb($hex) {
        $hex = str_replace("#", "", $hex);

        if (strlen($hex) == 3) {
            $r = hexdec(substr($hex, 0, 1) . substr($hex, 0, 1));
            $g = hexdec(substr($hex, 1, 1) . substr($hex, 1, 1));
            $b = hexdec(substr($hex, 2, 1) . substr($hex, 2, 1));
        } else {
            $r = hexdec(substr($hex, 0, 2));
            $g = hexdec(substr($hex, 2, 2));
            $b = hexdec(substr($hex, 4, 2));
        }
        $rgb = array($r, $g, $b);
        //return implode(",", $rgb); // returns the rgb values separated by commas
        return $rgb; // returns an array with the rgb values
    }

    public static function ThumbnailYap($dosyayolu, $max_en, $max_boy, $resimolcususabitmi = false, $cerceve = true, $cerceverengihex = 'fff') {
        //Yazan: Buğra Güney & Ecehan Ece
        //$dosyayolu değişkeni resmin dosyayolunu
        //$max_en değişkeni resmin thumbnail eninin kaç piksel olacağını
        //$max_boy değişkeni resmin thumbnail boyunun kaç piksel olacağını
        //$resimolcususabitmi değişkeni false ise resim max_en ve max_boy'a göre oranlanır, true ise orijinal en ve boyuna göre oranlanır.
        //$cerceve değişkeni true ise resmin kalan kısımlarına arkaplan rengi ataması yapar, false ise yapılmaz.
        //$cerceverengihex değişkeni 3 ya da 6 karakterlik renk hex kodunu alır. # ile de kullanılabilir. burada verilen hex koduna sahip rengi resmin arkaplanına atar.

        $thumbnail_adi = "-thumb";
        $resim_detay = getimagesize("$dosyayolu");
        $orjinal_en = $resim_detay[0];
        $orjinal_boy = $resim_detay[1];
        $xoran = $max_en / $orjinal_en;
        $yoran = $max_boy / $orjinal_boy;

        $resim_en = $max_en;
        $resim_boy = $max_boy;

        if (!$resimolcususabitmi && $cerceve) {
            if ($orjinal_en <= $max_en && $orjinal_boy <= $max_boy) {
                $resim_en = $orjinal_en;
                $resim_boy = $orjinal_boy;
            } else if (($xoran * $orjinal_boy) < $max_boy) {
                $resim_en = $max_en;
                $resim_boy = ceil($xoran * $orjinal_boy);
            } else {
                $resim_en = ceil($yoran * $orjinal_en);
                $resim_boy = $max_boy;
            }
        }

        if ($cerceve == TRUE) {
            if ($orjinal_en <= $max_en && $orjinal_boy <= $max_boy) {
                $ic_resim_son_en = $orjinal_en;
                $ic_resim_son_boy = $orjinal_boy;
            } else if (($xoran * $orjinal_boy) < $max_boy) {
                $ic_resim_son_en = $max_en;
                $ic_resim_son_boy = ceil($xoran * $orjinal_boy);
            } else {
                $ic_resim_son_en = ceil($yoran * $orjinal_en);
                $ic_resim_son_boy = $max_boy;
            }
        } else {
            $ic_resim_son_en = $max_en;
            $ic_resim_son_boy = $max_boy;
        }

        $koor_x = 0;
        $koor_y = 0;

        if ($cerceve) {
            $koor_x = intval(($resim_en - $ic_resim_son_en) / 2);
            $koor_y = intval(($resim_boy - $ic_resim_son_boy) / 2);
        }

        $dosyayoluhangi = explode('.', $dosyayolu);
        if ($dosyayoluhangi[1] == 'gif') {
            $imgt = "ImageGIF";
            $imgcreatefrom = "ImageCreateFromGIF";
            $dosyayoluarray = explode('.gif', $dosyayolu);
            $dosyayoluson = $dosyayoluarray[0] . $thumbnail_adi . '.gif';
        }
        if ($dosyayoluhangi[1] == 'jpg') {
            $imgt = "ImageJPEG";
            $imgcreatefrom = "ImageCreateFromJPEG";
            $dosyayoluarray = explode('.jpg', $dosyayolu);
            $dosyayoluson = $dosyayoluarray[0] . $thumbnail_adi . '.jpg';
        }
        if ($dosyayoluhangi[1] == 'jpeg') {
            $imgt = "ImageJPEG";
            $imgcreatefrom = "ImageCreateFromJPEG";
            $dosyayoluarray = explode('.jpeg', $dosyayolu);
            $dosyayoluson = $dosyayoluarray[0] . $thumbnail_adi . '.jpeg';
        }
        if ($dosyayoluhangi[1] == 'png') {
            $imgt = "ImagePNG";
            $imgcreatefrom = "ImageCreateFromPNG";
            $dosyayoluarray = explode('.png', $dosyayolu);
            $dosyayoluson = $dosyayoluarray[0] . $thumbnail_adi . '.png';
        }
        if ($imgt) {
            $eski_resim = $imgcreatefrom("$dosyayolu");
            $yeni_resim = imagecreatetruecolor($resim_en, $resim_boy);
            if ($cerceve) {
                $rgb = self::hex2rgb($cerceverengihex);
                if (count($rgb) != 3) {
                    $rgb = [255, 255, 255];
                }
                $beyaz = imagecolorallocate($yeni_resim, $rgb[0], $rgb[1], $rgb[2]);
                imagefill($yeni_resim, 0, 0, $beyaz);
            }
            imagecopyresized($yeni_resim, $eski_resim, $koor_x, $koor_y, 0, 0, $ic_resim_son_en, $ic_resim_son_boy, $orjinal_en, $orjinal_boy);
            $imgt($yeni_resim, "$dosyayoluson");
        }
    }

}

?>
