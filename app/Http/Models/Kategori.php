<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model {

    protected $table = 'kategori';

    public function parents() {
        return $this->belongsTo('App\Http\Models\Kategori', 'UstKategoriId', 'id');
    }

    public function children() {
        return $this->hasMany('App\Http\Models\Kategori', 'UstKategoriId');
    }

}
