<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Urun;
use App\Http\Models\UrunKategori;
use App\Http\Models\Menu;
use App\Http\Models\Kategori;
use App\Http\Models\Dil;

class UrunController extends Controller {

 
    public function index(Request $request, $dil, $id) {

        $data["urunler"] = Urun::where('DilId', Dil::where('KisaAd', $dil)->first()->id)->where('is_active', 1)->where('Urun_UstKat', $id)->orderby('Sira','ASC')->paginate(20);
        if (empty($data["urunler"]->first())) {
            $data["urunler"] = Urun::where('DilId', Dil::where('KisaAd', 'tr')->first()->id)->where('is_active', 1)->where('Urun_UstKat', $id)->orderby('Sira','ASC')->paginate(20);
            
        }
        $data["menu"] = Menu::where('DilId', Dil::where('KisaAd', $dil)->first()->id)->where('id',$id)->first();
        
        
        return view('Site.Page.Urunler', $data);
    }

    
}
