<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Slider;
use App\Http\Models\Popup;
use App\Http\Models\Dil;
use App\Http\Models\Urun;
use App\Http\Models\Menu;
use App\Http\Models\IletisimBilgileri;
use App\Http\Models\Haber;
class DashboardController extends Controller {

    public function index($dil) {
         
       
    	$data["sliders"] = Slider::where('DilId', Dil::where('KisaAd', $dil)->first()->id)
                ->whereRaw('is_active = 1 and ((baslangic <="' . date('Y-m-d H:i:s') . '" AND bitis >= "' . date('Y-m-d H:i:s') . '") OR (baslangic = "0000-00-00 00:00:00" OR bitis = "0000-00-00 00:00:00"))')
                ->orderBy('sira', 'asc')
                ->get();
        if (empty($data["sliders"]->first())) {
            $data["sliders"] = Slider::where('DilId', Dil::where('KisaAd', 'tr')->first()->id)
                    ->whereRaw('is_active = 1 and ((baslangic <="' . date('Y-m-d H:i:s') . '" AND bitis >= "' . date('Y-m-d H:i:s') . '") OR (baslangic = "0000-00-00 00:00:00" OR bitis = "0000-00-00 00:00:00"))')
                    ->orderBy('sira', 'asc')
                    ->get();
        }
     
       $data["popup"] = Popup::where('DilId', Dil::where('KisaAd', $dil)->first()->id)
                ->where('is_active',1)
                ->whereRaw('is_active = 1 and ((baslangic <="' . date('Y-m-d H:i:s') . '" AND bitis >= "' . date('Y-m-d H:i:s') . '") OR (baslangic = "0000-00-00 00:00:00" OR bitis = "0000-00-00 00:00:00"))')
                ->first();
       
        return view('Site.Page.Anasayfa',$data);
    }

}
