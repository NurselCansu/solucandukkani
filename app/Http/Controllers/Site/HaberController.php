<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Slider;
use App\Http\Models\Dil;
use App\Http\Models\Urun;
use App\Http\Models\IletisimBilgileri;
use App\Http\Models\Haber;
class HaberController extends Controller {

    public function index(Request $reuest,$dil,$Slug) {


        $data["haber"] = Haber::where('DilId', Dil::where('KisaAd', $dil)->first()->id)
                ->where('is_active', 1)->where('Slug',$Slug)->first();

       
        return view('Site.Page.Haber',$data);
    }

}
