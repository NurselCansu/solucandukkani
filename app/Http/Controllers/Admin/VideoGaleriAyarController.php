<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Models\VideoGaleriAyar as VT;
use Validator;
use Input;
use Fnk;
use Auth;

class VideoGaleriAyarController extends AdminController{
	
    protected $Table = 'videogaleri_ayar'; // Database Alanları
    protected $ListelemeAlanlari = ['DilId','Fotograf','created_at','is_active']; // Database Alanları
    protected $ListelemeBasliklari = ['Dil','Fotoğraf','Eklenme Tarihi','Durum'];
    protected $WhereListele = ""; // listeleme sorgusuna where koşulu koymak istenirse örn : MenuTipi='icerik'
    protected $BirlesmelerText = ['DilId'=>[ 1=>'Türkçe', 2=>'İngilizce' ,3=>'İspanyolca' ,4=>'Rusça', 5=>'Fransızca']]; // Listelenecek Veride if atıp farklı veri göstermek içindir. örn : ['MenuTipi'=>['icerik'=>'İçerik','link'=>'Link']] veya ['Dil'=>['tr'=>'Türkçe','en'=>'İngilizce']]
    protected $Birlesmeler = []; // key kısmına ListelemeAlanlarindaki ilgili alan yazılıyor. Value Kısmı Array olacak. 0 eleman inner join yapılacak tabloadi.columnadi 1. eleman Kısmı tabloadi.goruntulenecekColumn örn: 'id'=>['ayarlar.id','ayarlar.Logo']
    protected $ListelemeSirala = 0; // Liste sıralaması desc olarak hangi kolondan olacağı belirtilir. 0 dan başlar
    protected $EkleButonu = true; // Listeleme sayfasının üzerindeki Yeni Ekle Butonu
    protected $SiralamaButonu = false; // Listeleme sayfasının üzerinde Yeni Ekle Butonu yanında Sırala Butonu
    protected $TumunuSilButonu = false; // Listeleme sayfasının üzerinde Tümünü Sil Butonu
    protected $KopyalaButonu = false; // Listeleme sayfasının veri satırlarında Kopyala butonu
    protected $KopyalaVeri = ''; // eval fonksiyonu için php yazılabilir
    protected $DuzenleButonu = true; // Listeleme sayfasının veri satırlarında Düzenle butonu
    protected $SilButonu = true; // Listeleme sayfasının veri satırlarında Sil butonu
    protected $DetayButonu = true; // Listeleme sayfasının veri satırlarında Detay butonu
    protected $Title = 'Video Galeri Ayar';
    protected $SilmeMesaji = ''; // Boş Bırakılırsa Direk Silme İşlemi Yapar Dolu İsede /// Silmek İstediğiniz "$silmemesaji" Bulunuyorsa Onlarda Silinecektir. <br>Devam etmek istediğinizden Emin misiniz? şeklinde yazar.
    protected $ResimKlasor = false; // Veritabanından Resim adında bir alan çekildiyse public/uploads baz alınır ek klasör varsa belirtilir.
    protected $AktifText = ['Pasif','Aktif']; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $ustkatkontrol = []; // inner join aynı tablo içinde ise ve bağlılıkları var ise array 0. eleman ListelemeAlanlari 1. eleman da column name örn: ['UstKatId','Adi'] // Detay sayfası ve Listeleme Sayfasında Kullanılır.

    protected $Degiskenler = []; //form viewlere gönderilecek veriler
    protected $is_activeButonu = true; //Form viewlerinde is_active selectbox durumu
    protected $DilTab = true; // Form viewlerde Dil Tabları olması istenirse
    protected $GaleriSiralaButonu = false;
    protected $Dil = true;
    
    public function update(Request $request,$id){
    	$rules = [
                'Fotograf' => 'required',
                'Dil' => 'required'
             ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return json_encode(['islem' => false, 'error' => $validator->errors()->all()]);
        }
        
        if($id==0){
        	$vt = new VT;
        }else{
         	$vt = VT::find($id);
        }    
        $vt->DilId = $request->Dil;
        
        $vt->is_active = $request->is_active;
        $save = $vt->save();
		
        if($request->hasFile('Fotograf')) {
            $klasor = 'images/uploads/FotoGaleri/sayfa';
            $ad = Fnk::ResimAdi($klasor, $request->file('Fotograf')[0]->getClientOriginalName());
            $ad = $vt->id . '_' . $ad;
            $request->file('Fotograf')[0]->move($klasor, $ad);
            $resim = $ad;
            
            $vt->Fotograf = @$resim;
            $save = $vt->save();
        }
        
        return json_encode(array('islem' => $save));
    }
    public function destroy($id) {
        $vt = VT::find($id);
        $silinecekler = Fnk::ChildrenIds($vt);
        Vt::whereIn('id', $silinecekler)->delete();
        return redirect()->back();
    } 
    
    
}
