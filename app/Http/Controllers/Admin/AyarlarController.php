<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Ayarlar as VT;
use App\Http\Models\Dil as VD;

use Validator;
use Image;
use File;
use Fnk;

class AyarlarController extends AdminController {

    protected $Table = 'ayarlar'; // Database Alanları
    protected $ListelemeAlanlari = ['DilID', 'FirmaAdi',  'AnaSayfaBaslik', 'updated_at']; // Database Alanları
    protected $ListelemeBasliklari = ['Dil', 'Firma Adi',  'AnaSayfa Başlık', 'Tarih'];
    protected $WhereListele = ""; // listeleme sorgusuna where koşulu koymak istenirse örn : HaberTipi='icerik'
    protected $BirlesmelerText = []; // Listelenecek Veride if atıp farklı veri göstermek içindir. örn : ['HaberTipi'=>['icerik'=>'İçerik','link'=>'Link']] veya ['Dil'=>['tr'=>'Türkçe','en'=>'İngilizce']]
    protected $Birlesmeler = ['DilID' => ['diller.id', 'diller.UzunAd']]; // key kısmına ListelemeAlanlarindaki ilgili alan yazılıyor. Value Kısmı Array olacak. 0 eleman inner join yapılacak tabloadi.columnadi 1. eleman Kısmı tabloadi.goruntulenecekColumn örn: 'id'=>['ayarlar.id','ayarlar.Logo']
    protected $ListelemeSirala = 0; // Liste sıralaması desc olarak hangi kolondan olacağı belirtilir. 0 dan başlar
    protected $EkleButonu = true; // Listeleme sayfasının üzerindeki Yeni Ekle Butonu
    protected $SiralamaButonu = false; // Listeleme sayfasının üzerinde Yeni Ekle Butonu yanında Sırala Butonu
    protected $TumunuSilButonu = false; // Listeleme sayfasının üzerinde Tümünü Sil Butonu
    protected $KopyalaButonu = false; // Listeleme sayfasının veri satırlarında Kopyala butonu
    protected $KopyalaVeri = ''; // eval fonksiyonu için php yazılabilir
    protected $DuzenleButonu = true; // Listeleme sayfasının veri satırlarında Düzenle butonu
    protected $SilButonu = true; // Listeleme sayfasının veri satırlarında Sil butonu
    protected $DetayButonu = true; // Listeleme sayfasının veri satırlarında Detay butonu
    protected $Title = 'Ayarlar';
    protected $SilmeMesaji = ''; // Boş Bırakılırsa Direk Silme İşlemi Yapar Dolu İsede /// Silmek İstediğiniz "$silmemesaji" Bulunuyorsa Onlarda Silinecektir. <br>Devam etmek istediğinizden Emin misiniz? şeklinde yazar.
    protected $ResimKlasor = true; // Veritabanından Resim adında bir alan çekildiyse public/uploads baz alınır ek klasör varsa belirtilir.
    protected $AktifText = ['Pasif', 'Aktif']; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $AktifTextOneCikarilmis = []; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $AktifTextKampanyaliUrun = [];
    protected $ustkatkontrol = []; // inner join aynı tablo içinde ise ve bağlılıkları var ise array 0. eleman ListelemeAlanlari 1. eleman da column name örn: ['UstKatId','Adi'] // Detay sayfası ve Listeleme Sayfasında Kullanılır.
    protected $Degiskenler = []; //form viewlere gönderilecek veriler
    protected $is_activeButonu = true; //Form viewlerinde is_active selectbox durumu
    protected $MetaAlani = false;


    protected function DegiskenAta() {
        $this->Degiskenler['diller'] = VD::select('id', 'UzunAd')->get();
    }
    
    public function update(Request $request, $id) {
       
        $rules = [
            'FirmaAdi' => 'required|max:255',
            'Dil' => 'required',
            'Logo'=>'mimes:jpeg,bmp,png,gif',
            'Favicon'=>'mimes:ico|dimensions:max_height=32,max_width=32',
            'MetaTag' => 'required',
            'MetaTitle' => 'required',
            'MetaDescription' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return json_encode(['islem' => false, 'error' => $validator->errors()->all()]);
        }

        if ($request->id == '') {
            $vt = new VT;
        } else {
            $vt = VT::find($request->id);
        }

        $mesaj = array();
        $vt->DilId = $request->Dil;
        $vt->FirmaAdi = $request->FirmaAdi;
        $vt->AnaSayfaBaslik = $request->AnaSayfaBaslik;
        $vt->AnaSayfaIcerikSutun1 = $request->AnaSayfaIcerikSutun1;
        $vt->AnaSayfaIcerikSutun2 = $request->AnaSayfaIcerikSutun2;
        $vt->AnaSayfaIcerikSutun3 = $request->AnaSayfaIcerikSutun3;
        $vt->slogan_link = $request->slogan_link;
        $vt->AnaSayfaAltIcerikSutun1 = $request->AnaSayfaAltIcerikSutun1;
        $vt->AnaSayfaAltIcerikSutun2 = $request->AnaSayfaAltIcerikSutun2;
        $vt->AnaSayfaAltIcerikSutun3 = $request->AnaSayfaAltIcerikSutun3;
        $vt->AnaSayfaAltSliderSutun1 = $request->AnaSayfaAltSliderSutun1;
        $vt->AnaSayfaAltSliderSutun2 = $request->AnaSayfaAltSliderSutun2;
        $vt->MetaTag = json_encode($request->MetaTag,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        $vt->MetaTitle = $request->MetaTitle;
        $vt->MetaDescription = $request->MetaDescription;
        $vt->is_active = $request->is_active;

        if(Fnk::optimizeAndReseizeImage('Logo','images/uploads',50)){
            $vt->Logo = Fnk::optimizeAndReseizeImage('Logo','images/uploads',50);
        }
        if ($request->hasFile('Favicon'))
        {
            $file = $request->file('Favicon');
            if($vt->Favicon == $file->getClientOriginalName()){
                File::delete('images/uploads/'.$vt->Favicon);
            }
            $klasor = 'images/uploads/';
            $ad = Fnk::ResimAdi($klasor,$file->getClientOriginalName());

            $file->move($klasor,$ad);
           
            $vt->Favicon = $ad;
            $mesaj['favicon'] = $klasor.'/'.$ad; //url('images/uploads') sabittir.
        }
        

        $mesaj['islem'] = $vt->save();
        return json_encode($mesaj);
    }

    public function destroy($id) {
        $vt = VT::find($id);
        $silinecekler = Fnk::ChildrenIds($vt);
        Vt::whereIn('id', $silinecekler)->update(['is_active' => -1]);
        return redirect()->back();
    }

}
