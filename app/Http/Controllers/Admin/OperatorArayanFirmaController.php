<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\OperatorArayanFirma as VT;
use Validator;
use Fnk;
use DB;

class OperatorArayanFirmaController extends AdminController {

    protected $Table = 'operatorarayanfirma'; // Database Alanları
    protected $ListelemeAlanlari = ['FirmaAdi', 'FirmaAdres','FirmaTelefon','FirmaEmail','FirmaYetkili','YetkiliGorevi','YetkiliTelefon','FirmaWebSitesi','CalismaTuru','CalismaTipi','GACalismaSuresi','PatlatmaBelgesi','OperatorEhliyeti','YagFiltreBakimBilgisi','DeliciDeneyimSuresi']; // Database Alanları
    protected $ListelemeBasliklari = ['Firma Adı', 'Firma Adresi','Firma Telefonu','Firma Emaili','Firma Yetkilisi','Yetkilinin Görevi','Yetkilinin Telefonu','Firma Web Sitesi','Çalışma Türü','Çalışma Tipi','Günlük/Aylık Çalışma Süresi','Patlatma Belgesi','Operator Ehliyeti','Yağ/Filtre Bakım Bilgisi','Delici Deneyim Süresi'];
    protected $WhereListele = ""; // listeleme sorgusuna where koşulu koymak istenirse örn : HaberTipi='icerik'
    protected $BirlesmelerText = []; // Listelenecek Veride if atıp farklı veri göstermek içindir. örn : ['HaberTipi'=>['icerik'=>'İçerik','link'=>'Link']] veya ['Dil'=>['tr'=>'Türkçe','en'=>'İngilizce']]
    protected $Birlesmeler = []; // key kısmına ListelemeAlanlarindaki ilgili alan yazılıyor. Value Kısmı Array olacak. 0 eleman inner join yapılacak tabloadi.columnadi 1. eleman Kısmı tabloadi.goruntulenecekColumn örn: 'id'=>['ayarlar.id','ayarlar.Logo']
    protected $ListelemeSirala = 0; // Liste sıralaması desc olarak hangi kolondan olacağı belirtilir. 0 dan başlar
    protected $EkleButonu = false; // Listeleme sayfasının üzerindeki Yeni Ekle Butonu
    protected $SiralamaButonu = false; // Listeleme sayfasının üzerinde Yeni Ekle Butonu yanında Sırala Butonu
    protected $TumunuSilButonu = false; // Listeleme sayfasının üzerinde Tümünü Sil Butonu
    protected $KopyalaButonu = false; // Listeleme sayfasının veri satırlarında Kopyala butonu
    protected $KopyalaVeri = ''; // eval fonksiyonu için php yazılabilir
    protected $DuzenleButonu = false; // Listeleme sayfasının veri satırlarında Düzenle butonu
    protected $SilButonu = true; // Listeleme sayfasının veri satırlarında Sil butonu
    protected $DetayButonu = true; // Listeleme sayfasının veri satırlarında Detay butonu
    protected $Title = 'Operatör Arayan Firma';
    protected $SilmeMesaji = ''; // Boş Bırakılırsa Direk Silme İşlemi Yapar Dolu İsede /// Silmek İstediğiniz "$silmemesaji" Bulunuyorsa Onlarda Silinecektir. <br>Devam etmek istediğinizden Emin misiniz? şeklinde yazar.
    protected $ResimKlasor = true; // Veritabanından Resim adında bir alan çekildiyse public/uploads baz alınır ek klasör varsa belirtilir.
    protected $AktifText = []; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $AktifTextOneCikarilmis = []; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $ustkatkontrol = []; // inner join aynı tablo içinde ise ve bağlılıkları var ise array 0. eleman ListelemeAlanlari 1. eleman da column name örn: ['UstKatId','Adi'] // Detay sayfası ve Listeleme Sayfasında Kullanılır.
    protected $Degiskenler = []; //form viewlere gönderilecek veriler
    protected $is_activeButonu = true; //Form viewlerinde is_active selectbox durumu
    protected $MetaAlani = false;

    public function show($id) {
        $veri = VT::find($id);
        $veri->is_active = 1;
        $veri->save();
        return view('Admin.OperatorArayanFirmaDetay', ['veri' => $veri]);
    }

}
