<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use GoogleAnalyticsAPI;
use App\Http\Models\Google;
use App\Http\Models\Analytics;
use Excel;
class AnalyticsController extends Controller
{
	protected $ga;
	public function __construct()
	{
		$start_date = Analytics::where('key', 'Ziyaret')->orderby('created_at', 'desc')->first();

		$defaults = array(
			'start-date' => (!empty($start_date) ? $start_date->created_at->format('Y-m-d') : date('Y-m-d', strtotime('-1 month'))),
			'end-date' => date('Y-m-d', strtotime('-1 day')),
		);
		if ($defaults["start-date"] == $defaults["end-date"]) {
			return;
		}

		$this->ga = new GoogleAnalyticsAPI('service');
		$this->ga->auth->setClientId('7687bd6480970b328aa8f16db38236916a7eed1c');
		$this->ga->auth->setEmail('serviceaccount@portakal-yazilim.iam.gserviceaccount.com');
		$this->ga->auth->setPrivateKey(base_path('resources/Portakal-Yazilim-7687bd648097.p12'));

		$auth = $this->ga->auth->getAccessToken();
		if ($auth['http_code'] == 200 && @Google::where('is_active',1)->first()->AnalyticsAccountId != 0) {
			$accessToken = $auth['access_token'];
			$tokenExpires = $auth['expires_in'];
			$tokenCreated = time();
			$this->ga->setAccessToken($accessToken);
			$this->ga->setAccountId('ga:'. @Google::where('is_active',1)->first()->AnalyticsAccountId );

			$this->ga->setDefaultQueryParams($defaults);

			$params = array(
				'metrics' => 'ga:visits,ga:pageviews',
				'dimensions' => 'ga:date',
			);
			$query = $this->ga->query($params);
	 

			$data["visits"] = $this->ga->getVisitsByDate();
			if ( !empty($data["visits"]["rows"]) ) {
				foreach ($data["visits"]["rows"] as $element) {
					$analytics = Analytics::where('key', 'Ziyaret')->where('created_at', date('Y-m-d', strtotime($element[0])))->first();
					if(empty($analytics)) {
						$analytics = new Analytics;
					}
					$analytics->key = "Ziyaret";
					$analytics->value = $element[1];
					$analytics->valueCogul = $element[2];
					$analytics->created_at = strtotime($element[0]);
					$analytics->save();
				}
			}

			$data["visitsByCountries"] = $this->ga->getVisitsByCountries(array('max-results' => 5));
			if ( !empty($data["visitsByCountries"]["rows"]) ) {
				foreach ($data["visitsByCountries"]["rows"] as $element) {
					$analytics = Analytics::where('key', 'ZiyaretUlke')->where('created_at', date('Y-m-d'))->first();
					if(empty($analytics)) {
						$analytics = new Analytics;
					}
					$analytics->key = "ZiyaretUlke";
					$analytics->value = json_encode($element, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
					$analytics->created_at = date('Y-m-d', strtotime('-1 day'));
					$analytics->save();
				}
			}

			$data["visitsByCities"] = $this->ga->getVisitsByCities(array('max-results' => 5));
			if ( !empty($data["visitsByCities"]["rows"]) ) {
				foreach ($data["visitsByCities"]["rows"] as $element) {
					$analytics = Analytics::where('key', 'ZiyaretSehir')->where('created_at', date('Y-m-d'))->first();
					if(empty($analytics)) {
						$analytics = new Analytics;
					}
					$analytics->key = "ZiyaretSehir";
					$analytics->value = json_encode($element, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
					$analytics->created_at = date('Y-m-d', strtotime('-1 day'));
					$analytics->save();
				}
			}

			$data["visitsByLanguages"] = $this->ga->getVisitsByLanguages(array('max-results' => 5));
			if ( !empty($data["visitsByLanguages"]["rows"]) ) {
				foreach ($data["visitsByLanguages"]["rows"] as $element) {
					$analytics = Analytics::where('key', 'ZiyaretDil')->where('created_at', date('Y-m-d'))->first();
					if(empty($analytics)) {
						$analytics = new Analytics;
					}
					$analytics->key = "ZiyaretDil";
					$analytics->value = json_encode($element, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
					$analytics->created_at = date('Y-m-d', strtotime('-1 day'));
					$analytics->save();
				}
			}

			$data["getKeywords"] = $this->ga->getKeywords();
			if ( !empty($data["getKeywords"]["rows"]) ) {
				foreach ($data["getKeywords"]["rows"] as $element) {
					$analytics = Analytics::where('key', 'Kelimeler')->where('created_at', date('Y-m-d'))->first();
					if(empty($analytics)) {
						$analytics = new Analytics;
					}
					$analytics->key = "Kelimeler";
					$analytics->value = json_encode($element, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
					$analytics->created_at = date('Y-m-d', strtotime('-1 day'));
					$analytics->save();
				}
			}

			$data["getReferralTraffic"] = $this->ga->getReferralTraffic();
			if ( !empty($data["getReferralTraffic"]["rows"]) ) {
				foreach ($data["getReferralTraffic"]["rows"] as $element) {
					$analytics = Analytics::where('key', 'KaynakSite')->where('created_at', date('Y-m-d'))->first();
					if(empty($analytics)) {
						$analytics = new Analytics;
					}
					$analytics->key = "KaynakSite";
					$analytics->value = json_encode($element, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
					$analytics->created_at = date('Y-m-d', strtotime('-1 day'));
					$analytics->save();
				}
			}

		} else {
			echo "Google Analytics Bağlanamadı! <br> Lütfen Ayarlar Google Account Id kısmını kontrol ediniz!";
			return;
		}
	}

	public function index()
	{
		$data["analytics"] = Analytics::where('key', 'Ziyaret')->whereBetween('created_at', [date('Y-m-d', strtotime('-1 month')), date('Y-m-d', strtotime('now'))])->get();
		$data["analyticsDiger"] = Analytics::whereIn('key', ['ZiyaretUlke', 'ZiyaretSehir', 'ZiyaretDil', 'Kelimeler', 'KaynakSite'])->where('created_at', date('Y-m-d', strtotime('-1 day')))->get();
		return view('Admin.Istatistik', $data);
	}

    public function excelYap(Request $request){

        $sayfa_adi = 'Ziyaret Sayısı';
        $dosya_adi = 'Ziyaret Sayısı';
        $excel_array[0] = ['Tarih' , 'Ziyaret Sayısı' ];
        $data_ = Analytics::where('key', 'Ziyaret')->get();
        foreach( $data_ as $element){
            $excel_array[ count($excel_array) ] = [ date( 'd/m/y' ,strtotime($element->created_at) ) , $element->value ];
        }
        $excel_array[count($excel_array)]=[' ',' '];
        $excel_say=count($excel_array);

        $excel_array[$excel_say] = ['Tarih' , 'Ziyaret Sayısı' ];
        $data_ = Analytics::where('key', 'Kelimeler')->get();
        foreach( $data_ as $element){
            $excel_say++;
            $excel_array[ $excel_say ] = [ json_decode($element->value)[0] , json_decode($element->value)[1]];
        }
        $excel_array[count($excel_array)]=[' ',' '];
        $excel_say=count($excel_array);

        $excel_array[$excel_say] = ['Kaynak' ,'Ziyaret Sayısı'];
        $data_ = Analytics::where('key', 'KaynakSite')->get();
        foreach( $data_ as $element){ 
            $excel_say++;
            $excel_array[ $excel_say ] = [ json_decode($element->value)[0] , json_decode($element->value)[1]];
        }
        $excel_array[count($excel_array)]=[' ',' '];
        $excel_say=count($excel_array);
        
        $excel_array[$excel_say] = ['Ülke' , 'Ziyaret Sayısı' ];
        $data_ = Analytics::where('key', 'ZiyaretUlke')->get();
        foreach( $data_ as $element){ 
            $excel_say++;
            $excel_array[ $excel_say ] = [ json_decode($element->value)[0] , json_decode($element->value)[1]  ];
        }
        $excel_array[count($excel_array)]=[' ',' '];
        $excel_say=count($excel_array);

        $excel_array[$excel_say] = ['Şehir' , 'Ziyaret Sayısı' ];
        $data_ = Analytics::where('key', 'ZiyaretSehir')->get();
        foreach( $data_ as $element){ 
            $excel_say++;
            $excel_array[ $excel_say ] = [ json_decode($element->value)[0] , json_decode($element->value)[1] ];
        }
        $excel_array[count($excel_array)]=[' ',' '];
        $excel_say=count($excel_array);


        $excel_array[$excel_say] = ['Dil' , 'Ziyaret Sayısı' ];
        $data_ = Analytics::where('key', 'ZiyaretDil')->get();
        foreach( $data_ as $element){ 
            $excel_say++;
            $excel_array[ $excel_say ] = [ json_decode($element->value)[0] , json_decode($element->value)[1]];
        }        

       	Excel::create( $dosya_adi , function($excel) use ($excel_array, $sayfa_adi , $dosya_adi) {
           	$excel->sheet( $sayfa_adi , function($sheet) use ($excel_array) {
       			$sheet->fromArray($excel_array , null , 'A1', false , false);
           	});
       	})->export('xls');
    }
}
