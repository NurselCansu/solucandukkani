<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Menu as VT;
use App\Http\Models\DilSabiti as DilSabitiVT;
use App\Http\Models\DilTanimlama as DilTanımlamaVT;
use Validator;
use Fnk;

class MenuController extends AdminController {

    protected $Table = 'menuler'; // Database Alanları
    protected $ListelemeAlanlari = ['Adi', 'DilId','MenuTipi', 'is_active']; // Database Alanları
    protected $ListelemeBasliklari = ['Adi','Dil' ,'Menu Tipi', 'Durum'];
    protected $WhereListele = ""; // listeleme sorgusuna where koşulu koymak istenirse örn : MenuTipi='icerik'
    protected $BirlesmelerText = []; // Listelenecek Veride if atıp farklı veri göstermek içindir. örn : ['MenuTipi'=>['icerik'=>'İçerik','link'=>'Link']] veya ['Dil'=>['tr'=>'Türkçe','en'=>'İngilizce']]
    protected $Birlesmeler = ['DilId'=>[ 'diller.id','diller.UzunAd']]; // key kısmına ListelemeAlanlarindaki ilgili alan yazılıyor. Value Kısmı Array olacak. 0 eleman inner join yapılacak tabloadi.columnadi 1. eleman Kısmı tabloadi.goruntulenecekColumn örn: 'id'=>['ayarlar.id','ayarlar.Logo']
    protected $ListelemeSirala = 0; // Liste sıralaması desc olarak hangi kolondan olacağı belirtilir. 0 dan başlar
    protected $EkleButonu = true; // Listeleme sayfasının üzerindeki Yeni Ekle Butonu
    protected $SiralamaButonu = true; // Listeleme sayfasının üzerinde Yeni Ekle Butonu yanında Sırala Butonu
    protected $TumunuSilButonu = false; // Listeleme sayfasının üzerinde Tümünü Sil Butonu
    protected $KopyalaButonu = false; // Listeleme sayfasının veri satırlarında Kopyala butonu
    protected $KopyalaVeri = ''; // eval fonksiyonu için php yazılabilir
    protected $DuzenleButonu = true; // Listeleme sayfasının veri satırlarında Düzenle butonu
    protected $SilButonu = true; // Listeleme sayfasının veri satırlarında Sil butonu
    protected $DetayButonu = true; // Listeleme sayfasının veri satırlarında Detay butonu
    protected $Title = 'Menu';
    protected $SilmeMesaji = 'Menüye Ait Alt Menü'; // Boş Bırakılırsa Direk Silme İşlemi Yapar Dolu İsede /// Silmek İstediğiniz "$silmemesaji" Bulunuyorsa Onlarda Silinecektir. <br>Devam etmek istediğinizden Emin misiniz? şeklinde yazar.
    protected $ResimKlasor = false; // Veritabanından Resim adında bir alan çekildiyse public/uploads baz alınır ek klasör varsa belirtilir.
    protected $AktifText = ['Pasif', 'Aktif']; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $ustkatkontrol = []; // inner join aynı tablo içinde ise ve bağlılıkları var ise array 0. eleman ListelemeAlanlari 1. eleman da column name örn: ['UstKatId','Adi'] // Detay sayfası ve Listeleme Sayfasında Kullanılır.
    protected $Degiskenler = []; //form viewlere gönderilecek veriler
    protected $is_activeButonu = true; //Form viewlerinde is_active selectbox durumu
    protected $MetaAlani = true;

    protected function DegiskenAta() {
        $this->Degiskenler['menuler'] = VT::where('UstKatId', 0)->orderBy('Sira')->where('is_active', 1)->get();

    }

    public function update(Request $request, $id) {
        $kayit = true;
        $rules = [
            'Adi' => 'required',
            'MenuTipi' => 'required',
            'is_active' => 'required',
            'MetaTag' => 'required',
            'MetaTitle' => 'required',
            'MetaDescription' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return json_encode(['islem' => false, 'error' => $validator->errors()->all()]);
        }

        $link = '#';
        if ($request->MenuTipi == 'link') {

            if (substr($request->Link, 0, 4) == "http") {
                $link = $request->Link;
            } 
            else {
                if ($request->Link != "#" && $request->Link != '') {
                    $link =  $request->Link;
                }
            }
        } 
        elseif ($request->MenuTipi == 'modul') {
            if($request->Modul=='urunler-url'){ 
                if($request->id==''){ 
                    $x=VT::orderBy('id', 'DESC')->first(); 
                    $sayı=$x->id; 
                    $sayı++; 
                    $link = url(Fnk::DilGetir($request->Dil, 'KisaAd') . '/' . \App\Http\Fnk::Ceviriadmin($request->Modul, Fnk::DilGetir($request->Dil, 'KisaAd')).'/' . $sayı); 
                } 
                else{ 
 
                    $id=VT::find($request->id); 
                    $sayı=$id->id; 
                    $link = url(Fnk::DilGetir($request->Dil, 'KisaAd') . '/' . \App\Http\Fnk::Ceviriadmin($request->Modul, Fnk::DilGetir($request->Dil, 'KisaAd')).'/' . $sayı); 
 
                } 
            } 
            

            else {
                $link = url(Fnk::DilGetir($request->Dil, 'KisaAd') . '/' . \App\Http\Fnk::Ceviriadmin($request->Modul, Fnk::DilGetir($request->Dil, 'KisaAd')));
            } 

        }

        if ($request->id == '') {
            $vt = new VT;
        } else {
            $vt = VT::find($request->id);
        }

        $vt->DilId = $request->Dil;
        $vt->Adi = $request->Adi;
        if(!$vt->Slug){
            if ($request->Dil == 1 && $request->MenuTipi == 'icerik') {
            $vt->Slug = $this->Slug($request->Adi, $request->id);
            $dilsabitivt = new DilSabitiVT;
            $dilsabitivt->SabitAdi = $vt->Slug;
            $dilsabitivt->Slug = $vt->Slug;
            $dilsabitivt->save();
            
            $diltanimlamavt = new DilTanımlamaVT;
            $diltanimlamavt->DilId = 1;
            $diltanimlamavt->SabitId = DilSabitiVT::where('Slug',$vt->Slug )->first()->id;
            $diltanimlamavt->Slug = $vt->Slug ;
            $diltanimlamavt->Ceviri = $vt->Slug;
            $diltanimlamavt->save();
            }else{
            $vt->Slug = $this->Slug($request->Adi, $request->id);
            }
        }    
        if($request->Tip){
        $vt->Tip = 1;
        }
        else{
        $vt->Tip = 0; 
        }
        $vt->MenuTipi = $request->MenuTipi;
        $vt->LinkAcilisTipi = $request->LinkAcilisTipi;
        $vt->Icerik = $request->Icerik;
        $vt->Link = str_replace( url('/') , '' , $link);
        $vt->UstKatId = $request->UstKatId;
        $vt->is_active = $request->is_active;
        $vt->MetaTag = json_encode($request->MetaTag,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        $vt->MetaTitle = $request->MetaTitle;
        $vt->MetaDescription = $request->MetaDescription;
        $save = $vt->save();

        //$vt->save();
       


        return json_encode(array('islem' => $save, 'menu' => @$vt));
    }

    public function destroy($id) {
        $vt = VT::find($id);
        $silinecekler = Fnk::ChildrenIds($vt);
        Vt::whereIn('id', $silinecekler)->update(['is_active' => -1]);
        return redirect()->back();
    }

    public function tagDuzenle($id=16){
          $aranan = $request->input('aranan');
          $firmaara = VT::where('id',16)->select('MetaTag')->get();
          return $firmaara;
    }

    public function Sirala($UstKatId = 0) {
        $baslik = $this->Title;
        $vtcolumnname = 'Adi';
        $anakategoriler = VT::where('is_active', '!=', '-1')->where('UstKatId', 0)->get();
        $veri = VT::where('is_active', '!=', '-1')->where('UstKatId', $UstKatId)->orderBy('Sira', 'ASC')->get();
        return view('Admin.Layout.Sirala', ['veri' => $veri, 'anakategoriler' => $anakategoriler, 'ustkatid' => $UstKatId, 'vtcolumnname' => $vtcolumnname, 'baslik' => $baslik]);
    }

}
