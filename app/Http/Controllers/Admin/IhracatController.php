<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Ihracat as VT;

use Validator;
use Fnk;

class IhracatController extends AdminController {

    protected $Table = 'ihracat'; // Database Alanları
    protected $ListelemeAlanlari = ['AfrikaKulAdUnvanTelEmail', 'AsiaKulAdUnvanTelEmail', 'AustraliaKulAdUnvanTelEmail', 'SAmericaKulAdUnvanTelEmail', 'EuropeKulAdUnvanTelEmail', 'NAmericaKulAdUnvanTelEmail', 'updated_at']; // Database Alanları
    protected $ListelemeBasliklari = ['Afrika Yetkili Adı, Telefon, Email ', 'Asya Yetkili Adı, Telefon, Email ','Avustralya Yetkili Adı, Güney America, Email ','Afrika Yetkili Adı, Telefon, Email ','Afrika Yetkili Adı, Telefon, Email ','Afrika Yetkili Adı, Telefon, Email ' ,'Tarih'];
    protected $WhereListele = ""; // listeleme sorgusuna where koşulu koymak istenirse örn : HaberTipi='icerik'
    protected $BirlesmelerText = []; // Listelenecek Veride if atıp farklı veri göstermek içindir. örn : ['HaberTipi'=>['icerik'=>'İçerik','link'=>'Link']] veya ['Dil'=>['tr'=>'Türkçe','en'=>'İngilizce']]
    protected $Birlesmeler = []; // key kısmına ListelemeAlanlarindaki ilgili alan yazılıyor. Value Kısmı Array olacak. 0 eleman inner join yapılacak tabloadi.columnadi 1. eleman Kısmı tabloadi.goruntulenecekColumn örn: 'id'=>['ayarlar.id','ayarlar.Logo']
    protected $ListelemeSirala = 0; // Liste sıralaması desc olarak hangi kolondan olacağı belirtilir. 0 dan başlar
    protected $EkleButonu = true; // Listeleme sayfasının üzerindeki Yeni Ekle Butonu
    protected $SiralamaButonu = false; // Listeleme sayfasının üzerinde Yeni Ekle Butonu yanında Sırala Butonu
    protected $TumunuSilButonu = false; // Listeleme sayfasının üzerinde Tümünü Sil Butonu
    protected $KopyalaButonu = false; // Listeleme sayfasının veri satırlarında Kopyala butonu
    protected $KopyalaVeri = ''; // eval fonksiyonu için php yazılabilir
    protected $DuzenleButonu = true; // Listeleme sayfasının veri satırlarında Düzenle butonu
    protected $SilButonu = true; // Listeleme sayfasının veri satırlarında Sil butonu
    protected $DetayButonu = false; // Listeleme sayfasının veri satırlarında Detay butonu
    protected $Title = 'Ihracat';
    protected $SilmeMesaji = ''; // Boş Bırakılırsa Direk Silme İşlemi Yapar Dolu İsede /// Silmek İstediğiniz "$silmemesaji" Bulunuyorsa Onlarda Silinecektir. <br>Devam etmek istediğinizden Emin misiniz? şeklinde yazar.
    protected $ResimKlasor = true; // Veritabanından Resim adında bir alan çekildiyse public/uploads baz alınır ek klasör varsa belirtilir.
    protected $AktifText = ['Pasif', 'Aktif']; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $AktifTextOneCikarilmis = ['Hayır', 'Evet']; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $AktifTextKampanyaliUrun = [];
    protected $ustkatkontrol = []; // inner join aynı tablo içinde ise ve bağlılıkları var ise array 0. eleman ListelemeAlanlari 1. eleman da column name örn: ['UstKatId','Adi'] // Detay sayfası ve Listeleme Sayfasında Kullanılır.
    protected $Degiskenler = []; //form viewlere gönderilecek veriler
    protected $is_activeButonu = true; //Form viewlerinde is_active selectbox durumu
    protected $MetaAlani = true;



    public function update(Request $request, $id) {
        
        $kayit = true;
        $rules = [

            'is_active' => 'required',
            'MetaTag' => 'required',
            'MetaTitle' => 'required',
            'MetaDescription' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return json_encode(['islem' => false, 'error' => $validator->errors()->all()]);
        }

        if ($request->id == '') {
            $vt = new VT;
        } else {
            $vt = VT::find($request->id);
        }
        $vt->DilId = $request->Dil;
        
        $vt->AfrikaKulAdUnvanTelEmail = $request->AfrikaKulAdUnvanTelEmail;
        $vt->AsiaKulAdUnvanTelEmail = $request->AsiaKulAdUnvanTelEmail;
        $vt->AustraliaKulAdUnvanTelEmail = $request->AustraliaKulAdUnvanTelEmail;
        $vt->SAmericaKulAdUnvanTelEmail = $request->SAmericaKulAdUnvanTelEmail;
        $vt->EuropeKulAdUnvanTelEmail = $request->EuropeKulAdUnvanTelEmail;
        $vt->NAmericaKulAdUnvanTelEmail = $request->NAmericaKulAdUnvanTelEmail;
        
        $vt->IhracatIcerik = $request->IhracatIcerik;
       
        $vt->is_active = $request->is_active;
        $vt->MetaTag = json_encode($request->MetaTag,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        $vt->MetaTitle = $request->MetaTitle;
        $vt->MetaDescription = $request->MetaDescription;
        $save = $vt->save();
        
        if ($request->id == ''){

            if ($request->hasFile('BuyukResim')) {
                $klasor = 'images/uploads/ihracat';
                $ad = Fnk::ResimAdi($klasor, $request->file('BuyukResim')->getClientOriginalName());
                $ad = $vt->id . '_' . $ad;
                $request->file('BuyukResim')->move($klasor, $ad);
                $resim = $ad;
            }
            $vt->BuyukResim = @$resim;

            if ($request->hasFile('BelgeResim')) {
                foreach ($request->file('BelgeResim') as $key => $file) {
                    $klasor = 'images/uploads/ihracat/belgeler';
                    $ad = Fnk::ResimAdi($klasor, $file->getClientOriginalName());
                    $ad = $vt->id . '_' . $key . '_' . $ad;
                    $resimArray[$key] = $ad;
                    $file->move($klasor, $ad);
                }
                $resim_ = json_encode($resimArray);
            }
            $vt->BelgeResim = @$resim_;
            $save = $vt->save();
            

        }elseif ( $request->id ) {
            if( $request->hasFile('BuyukResim') ){
                
                $klasor = 'images/uploads/ihracat';
                $ad = Fnk::ResimAdi($klasor, $request->file('BuyukResim')->getClientOriginalName());
                $ad = $vt->id . '_' . $ad;
                $request->file('BuyukResim')->move($klasor, $ad);
                $resim = $ad;
                $vt->BuyukResim = @$resim;
            }
            if( $request->hasFile('BelgeResim')) {
                foreach ($request->file('BelgeResim') as $key => $file) {
                    $klasor = 'images/uploads/ihracat/belgeler';
                    $ad = Fnk::ResimAdi($klasor, $file->getClientOriginalName());
                    $ad = $vt->id . '_' . $key . '_' . $ad;
                    $resimArray[$key] = $ad;
                    $file->move($klasor, $ad);
                }
                if( count($request->secili_belge_resim)>0 ){
                    foreach ($request->secili_belge_resim as $key_ => $value_) {
                        $resimArray[count($resimArray)] = $value_;
                    }
                }
                $resim__ = json_encode($resimArray);
                $vt->BelgeResim = @$resim__;

            }elseif( !( $request->hasFile('BelgeResim') ) ) {
                
                if(count($request->secili_belge_resim) > 0) {
                    foreach ($request->secili_belge_resim as $key_ => $value__) {
                        $resimArray[$key_] = $value__;
                    }
                    $resim__ = json_encode($resimArray);
                    $vt->BelgeResim = @$resim__;
                }else
                    $vt->BelgeResim = "";
            }           
        }
        $vt->save();
        return json_encode(array('islem' => $save, 'ihracat' => @$vt));
    }

    public function destroy($id) {
        $vt = VT::find($id);
        $silinecekler = Fnk::ChildrenIds($vt);
        Vt::whereIn('id', $silinecekler)->update(['is_active' => -1]);
        return redirect()->back();
    }

}
