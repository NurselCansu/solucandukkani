<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Tools as VT;

use Validator;
use Fnk;

class ToolsController extends AdminController {

    protected $Table = 'tools'; // Database Alanları
    protected $ListelemeAlanlari = ['Adi',  'is_active', 'updated_at']; // Database Alanları
    protected $ListelemeBasliklari = ['Adi',  'Durum', 'Tarih'];
    protected $WhereListele = ""; // listeleme sorgusuna where koşulu koymak istenirse örn : HaberTipi='icerik'
    protected $BirlesmelerText = []; // Listelenecek Veride if atıp farklı veri göstermek içindir. örn : ['HaberTipi'=>['icerik'=>'İçerik','link'=>'Link']] veya ['Dil'=>['tr'=>'Türkçe','en'=>'İngilizce']]
    protected $Birlesmeler = []; // key kısmına ListelemeAlanlarindaki ilgili alan yazılıyor. Value Kısmı Array olacak. 0 eleman inner join yapılacak tabloadi.columnadi 1. eleman Kısmı tabloadi.goruntulenecekColumn örn: 'id'=>['ayarlar.id','ayarlar.Logo']
    protected $ListelemeSirala = 0; // Liste sıralaması desc olarak hangi kolondan olacağı belirtilir. 0 dan başlar
    protected $EkleButonu = true; // Listeleme sayfasının üzerindeki Yeni Ekle Butonu
    protected $SiralamaButonu = true; // Listeleme sayfasının üzerinde Yeni Ekle Butonu yanında Sırala Butonu
    protected $TumunuSilButonu = false; // Listeleme sayfasının üzerinde Tümünü Sil Butonu
    protected $KopyalaButonu = false; // Listeleme sayfasının veri satırlarında Kopyala butonu
    protected $KopyalaVeri = ''; // eval fonksiyonu için php yazılabilir
    protected $DuzenleButonu = true; // Listeleme sayfasının veri satırlarında Düzenle butonu
    protected $SilButonu = true; // Listeleme sayfasının veri satırlarında Sil butonu
    protected $DetayButonu = true; // Listeleme sayfasının veri satırlarında Detay butonu
    protected $Title = 'Tools';
    protected $SilmeMesaji = ''; // Boş Bırakılırsa Direk Silme İşlemi Yapar Dolu İsede /// Silmek İstediğiniz "$silmemesaji" Bulunuyorsa Onlarda Silinecektir. <br>Devam etmek istediğinizden Emin misiniz? şeklinde yazar.
    protected $ResimKlasor = true; // Veritabanından Resim adında bir alan çekildiyse public/uploads baz alınır ek klasör varsa belirtilir.
    protected $AktifText = ['Pasif', 'Aktif']; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $AktifTextOneCikarilmis = ['Hayır', 'Evet']; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $AktifTextKampanyaliUrun = [];
    protected $ustkatkontrol = []; // inner join aynı tablo içinde ise ve bağlılıkları var ise array 0. eleman ListelemeAlanlari 1. eleman da column name örn: ['UstKatId','Adi'] // Detay sayfası ve Listeleme Sayfasında Kullanılır.
    protected $Degiskenler = ['id', 'Adi']; //form viewlere gönderilecek veriler
    protected $is_activeButonu = true; //Form viewlerinde is_active selectbox durumu
    protected $MetaAlani = true;



    public function update(Request $request, $id) {
        $kayit = true;
        $rules = [
            'Adi' => 'required',
            'is_active' => 'required',
            'MetaTag' => 'required',
            'MetaTitle' => 'required',
            'MetaDescription' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return json_encode(['islem' => false, 'error' => $validator->errors()->all()]);
        }

        if ($request->id == '') {
            $vt = new VT;
        } else {
            $vt = VT::find($request->id);
        }
        $vt->DilId = $request->Dil;

        $vt->Adi = $request->Adi;
        $vt->Slug = $this->Slug($request->Adi, $request->id);
        $vt->ToolDurum = $request->has('ToolDurum');
        $vt->is_active = $request->is_active;
        $vt->MetaTag = json_encode($request->MetaTag,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        $vt->MetaTitle = $request->MetaTitle;
        $vt->MetaDescription = $request->MetaDescription;
        $save = $vt->save();

        if ($request->hasFile('KapakResim')) {
            foreach ($request->file('KapakResim') as $key => $file) {
                $klasor = 'images/uploads/Tools/KapakResim';
                $ad = Fnk::ResimAdi($klasor, $file->getClientOriginalName());
                $ad = $vt->id . '_' . $key . '_' . $ad;
                $resimArray = $ad;
                $file->move($klasor, $ad);
            }
            $resim_ = $resimArray;
        }
        $vt->KapakResim = @$resim_;

        if ($request->hasFile('Pdf')) {
            foreach ($request->file('Pdf') as $key => $file) {
                $klasor = 'images/uploads/Tools/Pdf';
                $ad = Fnk::ResimAdi($klasor, $file->getClientOriginalName());
                $ad = $vt->id . '_' . $key . '_' . $ad;
                $resimArray = $ad;
                $file->move($klasor, $ad);
            }
            $pdf = $resimArray;
        }
        $vt->Pdf = @$pdf;

        $save = $vt->save();
        $vt->save();


        return json_encode(array('islem' => $save, 'tools' => @$vt));
    }

    public function Sirala($UstKatId = 0) {
        $baslik = $this->Title;
        $vtcolumnname = 'Adi';
            $anakategoriler = VT::where('is_active','!=','-1')->get();
            $veri = VT::where('is_active','!=','-1')->orderBy('Sira','ASC')->get();
        return view('Admin.Layout.Sirala', ['veri' => $veri, 'anakategoriler' => $anakategoriler, 'ustkatid' => $UstKatId, 'vtcolumnname' => $vtcolumnname, 'baslik' => $baslik]);
    }

    public function destroy($id) {
        $vt = VT::find($id);
        $silinecekler = Fnk::ChildrenIds($vt);
        Vt::whereIn('id', $silinecekler)->update(['is_active' => -1]);
        return redirect()->back();
    }

}
