<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;


use App\Http\Models\SosyalMedya as VT;
use Validator;
use Fnk;



class SosyalMedyaController extends AdminController{
    
    protected $Table = 'sosyalmedya'; // Database Alanları
    protected $ListelemeAlanlari = ['Adi', 'is_active']; // Database Alanları
    protected $ListelemeBasliklari = ['Adi', 'Durum'];
    protected $WhereListele = ""; // listeleme sorgusuna where koşulu koymak istenirse örn : SosyalMedyaTipi='icerik'
    protected $BirlesmelerText = []; // Listelenecek Veride if atıp farklı veri göstermek içindir. örn : ['SosyalMedyaTipi'=>['icerik'=>'İçerik','link'=>'Link']] veya ['Dil'=>['tr'=>'Türkçe','en'=>'İngilizce']]
    protected $Birlesmeler = []; // key kısmına ListelemeAlanlarindaki ilgili alan yazılıyor. Value Kısmı Array olacak. 0 eleman inner join yapılacak tabloadi.columnadi 1. eleman Kısmı tabloadi.goruntulenecekColumn örn: 'id'=>['ayarlar.id','ayarlar.Logo']
    protected $ListelemeSirala = 0; // Liste sıralaması desc olarak hangi kolondan olacağı belirtilir. 0 dan başlar
    protected $EkleButonu = true; // Listeleme sayfasının üzerindeki Yeni Ekle Butonu
    protected $SiralamaButonu = true; // Listeleme sayfasının üzerinde Yeni Ekle Butonu yanında Sırala Butonu
    protected $TumunuSilButonu = false; // Listeleme sayfasının üzerinde Tümünü Sil Butonu
    protected $KopyalaButonu = false; // Listeleme sayfasının veri satırlarında Kopyala butonu
    protected $KopyalaVeri = ''; // eval fonksiyonu için php yazılabilir
    protected $DuzenleButonu = true; // Listeleme sayfasının veri satırlarında Düzenle butonu
    protected $SilButonu = true; // Listeleme sayfasının veri satırlarında Sil butonu
    protected $DetayButonu = true; // Listeleme sayfasının veri satırlarında Detay butonu
    protected $Title = 'Sosyal Medya';
    protected $SilmeMesaji = ''; // Boş Bırakılırsa Direk Silme İşlemi Yapar Dolu İsede /// Silmek İstediğiniz "$silmemesaji" Bulunuyorsa Onlarda Silinecektir. <br>Devam etmek istediğinizden Emin misiniz? şeklinde yazar.
    protected $ResimKlasor = true; // Veritabanından Resim adında bir alan çekildiyse public/uploads baz alınır ek klasör varsa belirtilir.
    protected $AktifText = ['Pasif','Aktif']; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $ustkatkontrol = []; // inner join aynı tablo içinde ise ve bağlılıkları var ise array 0. eleman ListelemeAlanlari 1. eleman da column name örn: ['UstKatId','Adi'] // Detay sayfası ve Listeleme Sayfasında Kullanılır.
    protected $Dil = false;
    protected $Degiskenler = []; //form viewlere gönderilecek veriler
    protected $is_activeButonu = true; //Form viewlerinde is_active selectbox durumu
  
  
    public function update(Request $request,$id){
        $kayit = true;
        $rules = [
            'Adi' => 'required',
            'Link' => 'required',
            'is_active' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return json_encode(['islem' => false, 'error' => $validator->errors()->all()]);
        }
        
        $link = '#';
        if(substr($request->Link, 0, 4) == "http"){
            $link = $request->Link;
        }else{
            if($request->Link != "#" && $request->Link != ''){
                $link = 'http://' . $request->Link;
            }
        }


        if($request->id==''){
            $vt = new VT;
        }else{
            $vt = VT::find($request->id);
        }
        $vt->Adi = $request->Adi;
        $vt->LinkAcilisTipi = $request->LinkAcilisTipi;
        $vt->Link = $link;
        $vt->is_active = $request->is_active;
        
        if ($request->hasFile('Resim')) {
            $file = $request->file('Resim');
            $klasor = 'images/uploads/SosyalMedya';
            $ad = Fnk::ResimAdi($klasor,$file->getClientOriginalName());
            $file->move($klasor,$ad);
            $vt->Resim = $ad;
        }
        
        $save = $vt->save();
        
        $vt->save();
      
        return json_encode(array('islem' => $save, 'SosyalMedya' => @$vt));
    }
    
    public function destroy($id) {
        $vt = VT::find($id);
        $silinecekler = Fnk::ChildrenIds($vt);
        Vt::whereIn('id',$silinecekler)->update(['is_active'=>-1]);
        return redirect()->back();
    }

    public function Sirala($UstKatId = 0) {
        $baslik = $this->Title;
        $vtcolumnname = 'Adi';
            $anakategoriler = VT::where('is_active','!=','-1')->get();
            $veri = VT::where('is_active','!=','-1')->orderBy('Sira','ASC')->get();
        return view('Admin.Layout.Sirala', ['veri' => $veri, 'anakategoriler' => $anakategoriler, 'ustkatid' => $UstKatId, 'vtcolumnname' => $vtcolumnname, 'baslik' => $baslik]);
    }

}
