<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Models\Ik as VT;
use Validator;
use Fnk;
use DB;
class InsanKaynaklariController extends AdminController{
	
	protected $Table = 'ik'; // Database Alanları
	protected $ListelemeAlanlari = ['Adi','Soyadi','DogumYeriIl','TelefonCep','TcNo','BasvuruYapilanPozisyon']; // Database Alanları
	protected $ListelemeBasliklari = ['Adı','Soyadı','Doğum Yeri','Telefon','TC Kimlik Numarası','Başvuru Yapılan Pozisyon'];
	protected $WhereListele = ""; // listeleme sorgusuna where koşulu koymak istenirse örn : HaberTipi='icerik'
	protected $BirlesmelerText = []; // Listelenecek Veride if atıp farklı veri göstermek içindir. örn : ['HaberTipi'=>['icerik'=>'İçerik','link'=>'Link']] veya ['Dil'=>['tr'=>'Türkçe','en'=>'İngilizce']]
	protected $Birlesmeler = []; // key kısmına ListelemeAlanlarindaki ilgili alan yazılıyor. Value Kısmı Array olacak. 0 eleman inner join yapılacak tabloadi.columnadi 1. eleman Kısmı tabloadi.goruntulenecekColumn örn: 'id'=>['ayarlar.id','ayarlar.Logo']
	protected $ListelemeSirala = 0; // Liste sıralaması desc olarak hangi kolondan olacağı belirtilir. 0 dan başlar
	protected $EkleButonu = false; // Listeleme sayfasının üzerindeki Yeni Ekle Butonu
	protected $SiralamaButonu = false; // Listeleme sayfasının üzerinde Yeni Ekle Butonu yanında Sırala Butonu
	protected $TumunuSilButonu = false; // Listeleme sayfasının üzerinde Tümünü Sil Butonu
	protected $KopyalaButonu = false; // Listeleme sayfasının veri satırlarında Kopyala butonu
	protected $KopyalaVeri = ''; // eval fonksiyonu için php yazılabilir
	protected $DuzenleButonu = false; // Listeleme sayfasının veri satırlarında Düzenle butonu
	protected $SilButonu = true; // Listeleme sayfasının veri satırlarında Sil butonu
	protected $DetayButonu = true; // Listeleme sayfasının veri satırlarında Detay butonu
	protected $Title = 'Başvuru';
	protected $SilmeMesaji = ''; // Boş Bırakılırsa Direk Silme İşlemi Yapar Dolu İsede /// Silmek İstediğiniz "$silmemesaji" Bulunuyorsa Onlarda Silinecektir. <br>Devam etmek istediğinizden Emin misiniz? şeklinde yazar.
	protected $ResimKlasor = true; // Veritabanından Resim adında bir alan çekildiyse public/uploads baz alınır ek klasör varsa belirtilir.
	protected $AktifText = []; // key value mantığındadır. is_active alanı için geçerlidir.
	protected $AktifTextOneCikarilmis = []; // key value mantığındadır. is_active alanı için geçerlidir.
	protected $ustkatkontrol = []; // inner join aynı tablo içinde ise ve bağlılıkları var ise array 0. eleman ListelemeAlanlari 1. eleman da column name örn: ['UstKatId','Adi'] // Detay sayfası ve Listeleme Sayfasında Kullanılır.

	protected $Degiskenler = []; //form viewlere gönderilecek veriler
	protected $is_activeButonu = true; //Form viewlerinde is_active selectbox durumu
	protected $MetaAlani = true;

    public function show($id) {
		$sql = "select * from ".$this->Table." where id='".$id."'";
    	$veri = DB::select(DB::raw($sql))[0];
    	$veriVT = VT::find($id);
    	$veriVT->is_active = 1;
    	$veriVT->save();
    	$ustkat = '-Yok-';
    	 if (count($this->ustkatkontrol) > 0) {
            $ustbilgiler = DB::select(DB::raw('select id,is_active,'.implode(',', $this->ustkatkontrol).' from '.$this->Table." where id='".$veri->{$this->ustkatkontrol[0]}."'"));
            if(count($ustbilgiler)>0){
				$ustkat = $ustbilgiler[0]->{$this->ustkatkontrol[1]};
			}
        }
        $degiskenler = [];
        if(count($this->Birlesmeler)>0){
        	foreach($this->Birlesmeler as $key => $value){
				$jointable = explode('.',$value[1])[0];
				//'id'=>['ayarlar.id','ayarlar.Logo']
				$sql = "select ".$value[1]." as Adi from ".$this->Table.",".$jointable." where ".$this->Table.".id='".$id."' and  ".$this->Table.".".$key."=".$value[0];
				$sor = DB::select(DB::raw($sql));
				if(count($sor)>0){
					$degiskenler[$key] = $sor[0]->Adi;
				}else{
					$degiskenler[$key] = '-Yok-';					
				}
			}
		}
        return view('Admin.'.$this->GetControllerAdi().'Detay', ['veri' => $veri,'ustkat'=>$ustkat])->with($degiskenler);
    }



}