<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

use App\Http\Models\YetkiTanimlari;
use App\Http\Models\Admin;
use App\Http\Models\YoneticiYetkileri;
use App\Http\Models\YetkiGrup;

class YoneticiController extends Controller{
	
	public function index(){
		$veriler = Admin::all();
		return view('Admin.YoneticiListele', compact('veriler'));
	}
	
	public function create(){
		return view('Admin.YoneticiForm',['is_active'=>false]);
	}
	
	public function store(Request $request){
		$rules = [
		'name' => 'required|max:255',
		'email' => 'required|email|max:255|unique:admin',
		'password' => 'required|confirmed|min:6',
		];

		$validator = Validator::make($request->all(), $rules);
		if ($validator->fails()){
			return json_encode(['islem'=>false,'error'=>$validator->errors()->all()]);
		}

		$vt = new Admin;
		$vt->name = $request->input('name');
		$vt->email = $request->input('email');
		$vt->role = $request->input('role');
		$vt->password = bcrypt($request->input('password'));
		$save = $vt->save();

		return json_encode(array('islem'=>$save,'id'=>$vt->id));
	}

	public function show($id){
		$veri = Admin::find($id);
		return view('Admin.YoneticiDetay',['veri'=>$veri]);
	}

	public function edit($id){
		$veri = Admin::find($id); // $veri olarak göndermek zorunlu is_active alanı için :)
		return view('Admin.YoneticiForm',['veri'=>$veri,'is_active'=>false]);
	}

	public function update(Request $request,$id){
		$rules = [
		'name' => 'required|max:255',
		'email' => 'required|email|max:255|unique:admin,email,'.$id,
		'password' => 'confirmed|min:6',
		];
		$validator = Validator::make($request->all(), $rules);
		if ($validator->fails()){
			return json_encode(['islem'=>false,'error'=>$validator->errors()->all()]);
		}
		
		
		$mesaj = array();
		$vt = Admin::find($id);
		$vt->name = $request->input('name');
		$vt->email = $request->input('email');
		$vt->role = $request->input('role');
		if($request->input('password')!=''){
			$vt->password = bcrypt($request->input('password'));
		}
		$mesaj['islem'] = $vt->save();
		return json_encode($mesaj);
	}

	public function destroy($id){
		Admin::destroy($id);
		return redirect()->back();
	}

	public function ActiveUpdate($id,$deger){
		$vt = Admin::find($id);
		$vt->is_active=$deger;
		$vt->save();
		return redirect()->back();
	}


}