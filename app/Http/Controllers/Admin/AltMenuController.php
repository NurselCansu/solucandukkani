<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Models\AltMenu as VT;
use Validator;
use Input;
use Fnk;
use Auth;

class AltMenuController extends AdminController{
	
    protected $Table = 'altmenuler'; // Database Alanları
    protected $ListelemeAlanlari = ['Adi','MenuTipi','UstKatId','DilId','is_active']; // Database Alanları
    protected $ListelemeBasliklari = ['Adi','Menu Tipi','Üst Menü','DilId','Durum'];
    protected $WhereListele = ""; // listeleme sorgusuna where koşulu koymak istenirse örn : MenuTipi='icerik'
    protected $BirlesmelerText = ['Dil'=>['tr'=>'Türkçe','en'=>'İngilizce']]; // Listelenecek Veride if atıp farklı veri göstermek içindir. örn : ['MenuTipi'=>['icerik'=>'İçerik','link'=>'Link']] veya ['Dil'=>['tr'=>'Türkçe','en'=>'İngilizce']]
    protected $Birlesmeler = []; // key kısmına ListelemeAlanlarindaki ilgili alan yazılıyor. Value Kısmı Array olacak. 0 eleman inner join yapılacak tabloadi.columnadi 1. eleman Kısmı tabloadi.goruntulenecekColumn örn: 'id'=>['ayarlar.id','ayarlar.Logo']
    protected $ListelemeSirala = 0; // Liste sıralaması desc olarak hangi kolondan olacağı belirtilir. 0 dan başlar
    protected $EkleButonu = true; // Listeleme sayfasının üzerindeki Yeni Ekle Butonu
    protected $SiralamaButonu = true; // Listeleme sayfasının üzerinde Yeni Ekle Butonu yanında Sırala Butonu
    protected $TumunuSilButonu = false; // Listeleme sayfasının üzerinde Tümünü Sil Butonu
    protected $KopyalaButonu = false; // Listeleme sayfasının veri satırlarında Kopyala butonu
    protected $KopyalaVeri = ''; // eval fonksiyonu için php yazılabilir
    protected $DuzenleButonu = true; // Listeleme sayfasının veri satırlarında Düzenle butonu
    protected $SilButonu = true; // Listeleme sayfasının veri satırlarında Sil butonu
    protected $DetayButonu = false; // Listeleme sayfasının veri satırlarında Detay butonu
    protected $Title = 'Alt Menü';
    protected $SilmeMesaji = 'Menüye Ait Alt Menü'; // Boş Bırakılırsa Direk Silme İşlemi Yapar Dolu İsede /// Silmek İstediğiniz "$silmemesaji" Bulunuyorsa Onlarda Silinecektir. <br>Devam etmek istediğinizden Emin misiniz? şeklinde yazar.
    protected $ResimKlasor = false; // Veritabanından Resim adında bir alan çekildiyse public/uploads baz alınır ek klasör varsa belirtilir.
    protected $AktifText = ['Pasif','Aktif']; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $ustkatkontrol = ['UstKatId','Adi']; // inner join aynı tablo içinde ise ve bağlılıkları var ise array 0. eleman ListelemeAlanlari 1. eleman da column name örn: ['UstKatId','Adi'] // Detay sayfası ve Listeleme Sayfasında Kullanılır.

    protected $Degiskenler = []; //form viewlere gönderilecek veriler
    protected $is_activeButonu = true; //Form viewlerinde is_active selectbox durumu
 
    protected function DegiskenAta(){ // Create ve Update te otomatik çalışan fonksiyondur.
        $this->Degiskenler['menuler'] = VT::where('UstKatId',0)->orderBy('Sira')->get();
    }
  
    public function update(Request $request,$id){
    	$kayit = true;
	        $rules = [
	            'Adi' => 'required',
	            'MenuTipi' => 'required',
	            'is_active' => 'required'
	        ];
	        $validator = Validator::make($request->all(), $rules);
	        if ($validator->fails()) {
	        	return json_encode(['islem' => false, 'error' => $validator->errors()->all()]);
	        }
	        
        	$link = '#';
			if($request->MenuTipi == 'link'){
	            if(substr($request->Link, 0, 4) == "http"){
	                $link = $request->Link;
	            }else{
	                if($request->Link != "#" && $request->Link != ''){
	                    $link = 'http://' . $request->Link;
	                }
	            }
	        }elseif($request->MenuTipi == 'modul'){
	            $link = url(Fnk::DilGetir($request->Dil, 'KisaAd').'/'.$request->Modul);
	        }

	       if($request->id==''){
				$vt = new VT;
			}else{
            	$vt = VT::find($request->id);
			}
		        $vt->DilId = $request->Dil;
		        $vt->Adi = $request->Adi;
		        $vt->Slug = $this->Slug($request->Adi,$request->id);
		        $vt->MenuTipi = $request->MenuTipi;
		        $vt->LinkAcilisTipi = $request->LinkAcilisTipi;
		        $vt->Icerik = $request->Icerik;
		        $vt->Link =  str_replace(url('/'),'',$link);
		        $vt->UstKatId = $request->UstKatId;
		        $vt->is_active = $request->is_active;
	        $save = $vt->save();
			
			$vt->save();
		
		
        return json_encode(array('islem' => $save, 'menu' => @$vt));
    }
    
	 public function destroy($id) {
        $vt = VT::find($id);
        $silinecekler = Fnk::ChildrenIds($vt);
        Vt::whereIn('id',$silinecekler)->update(['is_active'=>-1]);
        return redirect()->back();
    }

    public function Sirala($UstKatId = 0) {
        $baslik = $this->Title;
        $vtcolumnname = 'Adi';
        	$anakategoriler = VT::where('is_active','!=','-1')->where('UstKatId', 0)->get();
        	$veri = VT::where('is_active','!=','-1')->where('UstKatId', $UstKatId)->orderBy('Sira','ASC')->get();
        return view('Admin.Layout.Sirala', ['veri' => $veri, 'anakategoriler' => $anakategoriler, 'ustkatid' => $UstKatId, 'vtcolumnname' => $vtcolumnname, 'baslik' => $baslik]);
    }

}
