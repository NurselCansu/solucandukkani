<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Ekibimiz as VT;

use Validator;
use Fnk;

class EkibimizController extends AdminController {

    protected $Table = 'ekibimiz'; // Database Alanları
    protected $ListelemeAlanlari = ['DilId','Adi_soyadi', 'Pozisyon' , 'is_active', 'updated_at']; // Database Alanları
    protected $ListelemeBasliklari = ['Dil','Adi', 'Pozisyon','Aktif' ,'Tarih'];
    protected $WhereListele = ""; // listeleme sorgusuna where koşulu koymak istenirse örn : HaberTipi='icerik'
    protected $BirlesmelerText = ['DilId'=>[ 1=>'Türkçe',2 =>'İngilizce' ,3 =>'İspanyolca' ,4 =>'Rusça' ,5 =>'Fransızca']]; // Listelenecek Veride if atıp farklı veri göstermek içindir. örn : ['MenuTipi'=>['icerik'=>'İçerik','link'=>'Link']] veya ['Dil'=>['tr'=>'Türkçe','en'=>'İngilizce']]
    protected $Birlesmeler = []; // key kısmına ListelemeAlanlarindaki ilgili alan yazılıyor. Value Kısmı Array olacak. 0 eleman inner join yapılacak tabloadi.columnadi 1. eleman Kısmı tabloadi.goruntulenecekColumn örn: 'id'=>['ayarlar.id','ayarlar.Logo']
    protected $ListelemeSirala = 0; // Liste sıralaması desc olarak hangi kolondan olacağı belirtilir. 0 dan başlar
    protected $EkleButonu = true; // Listeleme sayfasının üzerindeki Yeni Ekle Butonu
    protected $SiralamaButonu = true; // Listeleme sayfasının üzerinde Yeni Ekle Butonu yanında Sırala Butonu
    protected $TumunuSilButonu = false; // Listeleme sayfasının üzerinde Tümünü Sil Butonu
    protected $KopyalaButonu = false; // Listeleme sayfasının veri satırlarında Kopyala butonu
    protected $KopyalaVeri = ''; // eval fonksiyonu için php yazılabilir
    protected $DuzenleButonu = true; // Listeleme sayfasının veri satırlarında Düzenle butonu
    protected $SilButonu = true; // Listeleme sayfasının veri satırlarında Sil butonu
    protected $DetayButonu = true; // Listeleme sayfasının veri satırlarında Detay butonu
    protected $Title = 'Ekibimiz';
    protected $SilmeMesaji = ''; // Boş Bırakılırsa Direk Silme İşlemi Yapar Dolu İsede /// Silmek İstediğiniz "$silmemesaji" Bulunuyorsa Onlarda Silinecektir. <br>Devam etmek istediğinizden Emin misiniz? şeklinde yazar.
    protected $ResimKlasor = true; // Veritabanından Resim adında bir alan çekildiyse public/uploads baz alınır ek klasör varsa belirtilir.
    protected $AktifText = ['Pasif', 'Aktif']; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $AktifTextOneCikarilmis = ['Hayır', 'Evet']; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $AktifTextKampanyaliUrun = [];
    protected $ustkatkontrol = []; // inner join aynı tablo içinde ise ve bağlılıkları var ise array 0. eleman ListelemeAlanlari 1. eleman da column name örn: ['UstKatId','Adi'] // Detay sayfası ve Listeleme Sayfasında Kullanılır.
    protected $Degiskenler = ['id', 'Adi']; //form viewlere gönderilecek veriler
    protected $is_activeButonu = true; //Form viewlerinde is_active selectbox durumu
    protected $MetaAlani = true;



    public function update(Request $request, $id) {
        $kayit = true;
        $rules = [
            // 'is_active' => 'required',
            // 'MetaTag' => 'required',
            // 'MetaTitle' => 'required',
            // 'MetaDescription' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return json_encode(['islem' => false, 'error' => $validator->errors()->all()]);
        }
        if ($request->id == '') {
            $vt = new VT;
        } else {
            $vt = VT::find($request->id);
        }
        $vt->DilId = $request->Dil;
        if( $request->Tip =="I"){
            $vt->Tip ="I";
            $vt->Icerik = $request->Icerik;
            $vt->Adi_soyadi = "Kurucu İçerik";
            $vt->MetaTag = json_encode($request->MetaTag);
            $vt->MetaTitle = $request->MetaTitle;
            $vt->MetaDescription = $request->MetaDescription;
        }elseif( $request->Tip == "M" or $request->Tip =="P" or $request->Tip =="K" ){
            $vt->Adi_soyadi = $request->Adi_soyadi;
            $vt->Telefon = $request->Telefon;
            $vt->Email = $request->Email;
            $vt->Pozisyon = $request->Pozisyon;
            $vt->Tip = $request->Tip;
        }
        // $vt->Slug = $this->Slug($request->Adi, $request->id);
        $vt->is_active = $request->is_active;

        $save = $vt->save();

        if( $request->Tip == "M" or $request->Tip =="P" or $request->Tip =="K" ){
        
            if ($request->id == ''){
                if ($request->hasFile('Fotograf')) {
                    foreach ($request->file('Fotograf') as $key => $file) {
                        $klasor = 'images/uploads/ekibimiz/';
                        $ad = Fnk::ResimAdi($klasor, $file->getClientOriginalName());
                        $ad = $vt->id . '_' . $key . '_' . $ad;
                        $resimArray[$key] = $ad;
                        $file->move($klasor, $ad);
                    }
                    $resim_ = json_encode($resimArray);
                }
                $vt->Fotograf = @$resim_;
                $save = $vt->save();
            }elseif ( $request->id ) {
                if( $request->hasFile('Fotograf')) {
                    foreach ($request->file('Fotograf') as $key => $file) {
                        $klasor = 'images/uploads/ekibimiz/';
                        $ad = Fnk::ResimAdi($klasor, $file->getClientOriginalName());
                        $ad = $vt->id . '_' . $key . '_' . $ad;
                        $resimArray[$key] = $ad;
                        $file->move($klasor, $ad);
                    }
                    if( count($request->secili_belge_resim)>0 ){
                        foreach ($request->secili_belge_resim as $key_ => $value_) {
                            $resimArray[count($resimArray)] = $value_;
                        }
                    }
                    $resim__ = json_encode($resimArray);
                    $vt->Fotograf = @$resim__;
                }elseif( !( $request->hasFile('Fotograf') ) ) {
                    if(count($request->secili_belge_resim) > 0) {
                        foreach ($request->secili_belge_resim as $key_ => $value__) {
                            $resimArray[$key_] = $value__;
                        }
                        $resim__ = json_encode($resimArray);
                        $vt->Fotograf = @$resim__;
                    }else
                        $vt->Fotograf = "";
                }           
            }
        }
        $vt->save();


        return json_encode(array('islem' => $save, 'ekibimiz' => @$vt));
    }



    public function destroy($id) {
        $vt = VT::find($id);
        $silinecekler = Fnk::ChildrenIds($vt);
        Vt::whereIn('id', $silinecekler)->update(['is_active' => -1]);
        return redirect()->back();
    }

    public function EkibimizSirala() {
        $baslik = $this->Title;
        $vtcolumnname = 'Adi_soyadi';
        $vtDilId='DilId';
        $veri = VT::where('is_active', '!=', '-1')->where('Tip', 'P')->where('DilId','1')->orderBy('Sira', 'ASC')->get();
         
        return view('Admin.EkibimizSirala', ['veri'=>$veri  ,'vtcolumnname' => $vtcolumnname, 'vtDilId' => $vtDilId, 'baslik' => $baslik]);
    }

     public function EkibimizSiralaKaydet(Request $request){
        foreach($request->id as $sira=>$id){
            $sira++;
            $personel = VT::find($id);
            VT::where('Adi_soyadi',$personel->Adi_soyadi)->update(['Sira'=>$sira]);
        }
    }

}
