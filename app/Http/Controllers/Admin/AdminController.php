<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Request as RequestUrl;
use \App\Http\Fnk;
use File;

class AdminController extends Controller {
    
    private $link = '';
    private function LinkAta(){
        $this->link = str_replace(array('/Getir'), '', RequestUrl::url());
    }
    
    protected function GetControllerAdi(){
        return RequestUrl::segment(2);
    }
    protected $Table = 'users'; // Database Alanları
    protected $ListelemeAlanlari = ['id']; // Database Alanları
    protected $ListelemeBasliklari = ['id'];
    protected $WhereListele = ''; // listeleme sorgusuna where koşulu koymak istenirse örn : MenuTipi='icerik'
    protected $BirlesmelerText = []; // Listelenecek Veride if atıp farklı veri göstermek içindir. örn : ['MenuTipi'=>['icerik'=>'İçerik','link'=>'Link']] veya ['Dil'=>['tr'=>'Türkçe','en'=>'İngilizce']]
    protected $Birlesmeler = []; // key kısmına ListelemeAlanlarindaki ilgili alan yazılıyor. Value Kısmı Array olacak. 0 eleman inner join yapılacak tabloadi.columnadi 1. eleman Kısmı tabloadi.goruntulenecekColumn örn: 'id'=>['ayarlar.id','ayarlar.Logo']
    protected $ListelemeSirala = 0; // Liste sıralaması desc olarak hangi kolondan olacağı belirtilir. 0 dan başlar
    protected $EkleButonu = true; // Listeleme sayfasının üzerindeki Yeni Ekle Butonu
    protected $SiralamaButonu = false; // Listeleme sayfasının üzerinde Yeni Ekle Butonu yanında Sırala Butonu
    protected $TumunuSilButonu = false; // Listeleme sayfasının üzerinde Tümünü Sil Butonu
    protected $KopyalaButonu = false; // Listeleme sayfasının veri satırlarında Kopyala butonu
    protected $KopyalaVeri = ''; // eval fonksiyonu için php yazılabilir
    protected $DuzenleButonu = true; // Listeleme sayfasının veri satırlarında Düzenle butonu
    protected $SilButonu = true; // Listeleme sayfasının veri satırlarında Sil butonu
    protected $DetayButonu = true; // Listeleme sayfasının veri satırlarında Detay butonu
    protected $Title = 'Menü';
    protected $SilmeMesaji = ''; // Boş Bırakılırsa Direk Silme İşlemi Yapar Dolu İsede /// Silmek İstediğiniz "$silmemesaji" Bulunuyorsa Onlarda Silinecektir. <br>Devam etmek istediğinizden Emin misiniz? şeklinde yazar.
    protected $ResimKlasor = false; // Veritabanından Resim adında bir alan çekildiyse public/uploads baz alınır ek klasör varsa belirtilir.
    protected $AktifText = ['Pasif','Aktif']; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $AktifText2 = ['Pasif','Aktif']; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $AktifTextOneCikarilmis= ['Hayır','Evet']; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $AktifTextKampanyaliUrun= ['Hayır','Evet']; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $ustkatkontrol = []; // inner join aynı tablo içinde ise ve bağlılıkları var ise array 0. eleman ListelemeAlanlari 1. eleman da column name örn: ['UstKatId','Adi']
    protected $Degiskenler = []; //form viewlere gönderilecek veriler
    protected $is_activeButonu = true; //Form viewlerinde is_active selectbox durumu
    protected $DilTanimlamaButonu = false;
    protected $Dil = true;
    protected $FotoButonu = false;
    protected $FotoSiralaButonu = false;
    protected $GaleriSiralaButonu = false;
    protected $MetaAlani = false;
	
    public function index() { // Listeleme
        $degerler = [
            'ListelemeBasliklari' => $this->ListelemeBasliklari,
            'ListelemeSirala' => $this->ListelemeSirala,
            'EkleButonu' => $this->EkleButonu,
            'SiralamaButonu' => $this->SiralamaButonu,
            'TumunuSilButonu' => $this->TumunuSilButonu,
            'KopyalaButonu' => $this->KopyalaButonu,
            'Title' => $this->Title,
            'SilmeMesaji' => $this->SilmeMesaji,
            'Birlesmeler' => $this->Birlesmeler,
            'BirlesmelerText' => $this->BirlesmelerText,
            'DilTanimlamaButonu' => $this->DilTanimlamaButonu,
            'GaleriSiralaButonu' => $this->GaleriSiralaButonu,
        ];
        
        return view('Admin.Layout.Listele')->with($degerler);
    }

    public function Getir(Request $request) { // Listeleme Ajax Verileri Getirme

       
        $this->LinkAta();
        $newlist = ['id'];
        foreach ($this->ListelemeAlanlari as $list){
            $newlist[] = $list;
        }
        $this->ListelemeAlanlari = $newlist;
        $alanlar = $this->ListelemeAlanlari;
        $birlesmeler = $this->Birlesmeler;
        $birlesmelertext = $this->BirlesmelerText;

        $Get = $request;
        $link = $this->link;
    
        $sqlsorgu = 'select ';
        
        $secim = $this->Table . '.id';
        $x = 0;
        foreach ($alanlar as $alan) {
            $secim.=',';
            if (isset($birlesmeler[$alan])) {
                $secim.=$birlesmeler[$alan][1] . ' as ' . $alan . $x;
            } else {
                $alankontrol = explode('(', $alan);
                if (count($alankontrol) > 1) {
                    $secim.=$alankontrol[0] . '(' . $this->Table . '.' . $alankontrol[1];
                } else {
                    $secim.=$this->Table . '.' . $alan;
                }
            }
            $x++;
        }
        
        $sqlsorgu.=$secim.' from '.$this->Table;
        if (count($birlesmeler) > 0) {
            foreach ($birlesmeler as $key => $value) {
                $ad = $value[0];
                $bol = explode('.', $ad);
            	$sqlsorgu.= ' LEFT JOIN '.$bol[0].' ON '.$this->Table.'.'.$key.' = '.$ad;
            }
        }

        $limit = '';

        if (isset($Get->start) && $Get->length != -1) {
            $limit = " LIMIT " . intval($Get->start) . ", " . intval($Get->length);
        }

        $order = '';


        if (isset($Get->order) and count($Get->order)) {
            if (isset($birlesmeler[$alanlar[$Get->order[0]['column']]])) {
                $ord = $birlesmeler[$alanlar[$Get->order[0]['column']]][1];
            } else {
                $ord = $alanlar[$Get->order[0]['column']];
                $bol = explode(' as ', $ord);
                if (count($bol) > 1) {
                    $ord = $bol[1];
                }
            }
            $order .= ' order by ' . $ord . ' ' . $Get->order[0]['dir'];
        }

        if ($this->is_activeButonu) {
            $where = " where ".$this->Table.".is_active!='-1'";
        } else {
            $where = "";
        } 



        if (isset($Get->search['value']) and $Get->search['value'] != '') {
            if ($where == '') {
                $where.= ' where ';
            } else {
                $where.=' and ';
            }
            $where.=' (';
            $x = 0;
            foreach ($alanlar as $alan) {
                $yaz = true;
                if (isset($birlesmeler[$alan])) {
                    $alan = $birlesmeler[$alan][1];
                } else {
                    $alan = $this->Table . '.' . $alan;
                    $bol = explode('(', $alan);
                    if (count($bol) > 1) {
                        $yaz = false;
                    }
                    $bol = explode(' as ', $alan);
                    if (count($bol) > 1) {
                        $alan = $bol[1];
                    }
                }
                if ($yaz) {
                    if ($x != 0) {
                        $where.=' or';
                    }
                    $where.=' ' . $alan . " like '%" . $Get->search['value'] . "%'";
                    $x++;
                }
            }
            $where.=')';
        }
        if ($this->WhereListele != "") {
            if ($where == "") {
                $where = ' WHERE ';
            } else {
                $where.=' and ';
            }
            $where.=' ' . $this->WhereListele;
        }
		
		$sqlsorgusayisi = $sqlsorgu.' '.$where;
        $sqlsorgu.=' '.$where.$order.$limit;
        $sonuc = DB::select(DB::raw($sqlsorgu));;

     //   $sonuc = DB::select(DB::raw('select ' . $secim . ' from ' . implode(',', $tables) . $where . $order . $limit));
		$sqlsorgusayisibol = explode(' from ',$sqlsorgusayisi);
		$sqlsorgusayisi = 'select count('.$this->Table.'.id) as adet from '.$sqlsorgusayisibol[1];
        $alansor = DB::select(DB::raw($sqlsorgusayisi));
        $recordsFiltered = $alansor[0]->adet;
        $recordsTotal = DB::select(DB::raw("select count(id) as adet from " . $this->Table));
        $recordsTotal = $recordsTotal[0]->adet;
		
        $data = array();

        if (isset($this->ustkatkontrol) and count($this->ustkatkontrol) > 0) {

            $ustbilgiler = DB::select(DB::raw('select id,is_active,' . implode(',', $this->ustkatkontrol) . ' from ' . $this->Table));
            $kontrolust = array();
            foreach ($ustbilgiler as $ust) {
                $kontrolust[$ust->id] = [
                    'is_active' => $ust->is_active,
                    'ustkat' => $ust->{$this->ustkatkontrol[0]},
                    $this->ustkatkontrol[1] => $ust->{$this->ustkatkontrol[1]}
                ];
            }

            function pasifvarmi($ustkatid, $ustkategoriler) {
                if ($ustkatid == 0) {
                    return false;
                }
                if (isset($ustkategoriler[$ustkatid]['is_active']) and $ustkategoriler[$ustkatid]['is_active'] == 0) {
                    return TRUE;
                } elseif (isset($ustkategoriler[$ustkatid]["ustkat"])) {
                    return pasifvarmi($ustkategoriler[$ustkatid]["ustkat"], $ustkategoriler);
                } else {
                    return false;
                }
            }

            function adgetir($id, $kontrolust, $alan) {
                if (isset($kontrolust[$id])) {
                    return $kontrolust[$id][$alan];
                } else {
                    return '-Yok-';
                }
            }

        }

        foreach ($sonuc as $s) {
            $yenikayit = array();
            $x = 0;
            foreach ($alanlar as $alan) {
                if (isset($birlesmeler[$alan])) {
                    $yenikayit[] = $s->{$alan . $x};
                } elseif (isset($birlesmelertext[$alan])) {
                    $yenikayit[] = $birlesmelertext[$alan][$s->{$alan}];
                } elseif ($alan == 'Tarih' or $alan == 'created') {
                    $yenikayit[] = Fnk::TarihDuzenle($s->{$alan}) . " ( " . Fnk::TarihFark($s->{$alan}) . ")";
                } elseif ($alan == 'SonGiris') {
                    $yenikayit[] = Fnk::TarihFark($s->{$alan});
                } elseif ($alan == 'Resim') {
                    $yenikayit[] = '<img src="' . url('images/uploads/' . $this->ResimKlasor . '/' . $s->{$alan}) . '" height="50">';
                } elseif ($alan == 'is_active') {

                    foreach ($this->AktifText as $key => $active) {
                        $newkey = $key + 1;
                        if ($newkey > (count($this->AktifText) - 1)) {
                            $newkey = 0;
                        }
                        if ($key == $s->is_active) {
                            $btn = '<a href="' . $link . '/ActiveUpdate/' . $s->id . '/' . $newkey . '">' . $active . '</a>';

                            if ($key == 1) {
                                if (isset($this->ustkatkontrol) and count($this->ustkatkontrol) > 0 and pasifvarmi($s->id, $kontrolust)) {
                                    $btn.= ' <i class="fa fa-bell fa-fw" title="Üst Kategori Kapalı Olduğu İçin Gönmeyecektir." alt="Üst Kategori Kapalı Olduğu İçin Gönmeyecektir."></i>';
                                }
                            }
                        }
                    }
                    $yenikayit[] = $btn;
                } elseif ($alan == 'DefaultDil') {

                    foreach ($this->AktifText2 as $key => $active) {
                        $newkey = $key + 1;
                        if ($newkey > (count($this->AktifText2) - 1)) {
                            $newkey = 0;
                        }
                        if ($key == $s->DefaultDil && $active == 'Pasif') {
                            $btn = '<a href="' . $link . '/DefaultDilUpdate/' . $s->id . '/' . $newkey . '">' . $active . '</a>';
                        }else if($key==$s->DefaultDil && $active == 'Aktif'){
                            $btn = '<a href="#" style="color:black;text-decoration:none;">' . $active . '</a>';
                        }
                    }
                    $yenikayit[] = $btn;
                } elseif ($alan == 'OneCikarilmis') {

                    foreach ($this->AktifTextOneCikarilmis as $key => $active) {
                        $newkey = $key + 1;
                        if ($newkey > (count($this->AktifTextOneCikarilmis) - 1)) {
                            $newkey = 0;
                        }
                        if ($key == $s->OneCikarilmis && $active == 'Hayır') {
                            $btn = '<a href="' . $link . '/ActiveUpdateOneCikar/' . $s->id . '/' . $newkey . '">' . $active . '</a>';
                        }else if( $key == $s->OneCikarilmis && $active == 'Evet'){
                            $btn = '<a href="' . $link . '/ActiveUpdateOneCikar/' . $s->id . '/' . $newkey . '">' . $active . '</a>';
                        }
                    }
                    $yenikayit[] = $btn;
                }elseif ($alan == 'OneCikarilmis2') {

                    foreach ($this->AktifTextOneCikarilmis as $key => $active) {
                        $newkey = $key + 1;
                        if ($newkey > (count($this->AktifTextOneCikarilmis) - 1)) {
                            $newkey = 0;
                        }
                        if ($key == $s->OneCikarilmis2 && $active == 'Hayır') {
                            $btn = '<a href="' . $link . '/ActiveUpdateOneCikar/' . $s->id . '/' . $newkey . '">' . $active . '</a>';
                        }else if( $key == $s->OneCikarilmis2 && $active == 'Evet'){
                            $btn = '<a href="' . $link . '/ActiveUpdateOneCikar/' . $s->id . '/' . $newkey . '">' . $active . '</a>';
                        }
                    }
                    $yenikayit[] = $btn;
                }elseif ($alan == 'Kampanya') {

                    foreach ($this->AktifTextKampanyaliUrun as $key => $active) {
                        $newkey = $key + 1;
                        if ($newkey > (count($this->AktifTextKampanyaliUrun) - 1)) {
                            $newkey = 0;
                        }
                        if ($key == $s->Kampanya && $active == 'Hayır') {
                            $btn = '<a href="' . $link . '/ActiveUpdateKampanyaliYap/' . $s->id . '/' . $newkey . '">' . $active . '</a>';
                        }else if( $key == $s->Kampanya && $active == 'Evet'){
                            $btn = '<a href="' . $link . '/ActiveUpdateKampanyaliYap/' . $s->id . '/' . $newkey . '">' . $active . '</a>';
                        }
                    }
                    $yenikayit[] = $btn;
                }elseif ($alan == 'UrunGizli') {

                    foreach ($this->AktifTextOneCikarilmis as $key => $active) {
                        $newkey = $key + 1;
                        if ($newkey > (count($this->AktifTextOneCikarilmis) - 1)) {
                            $newkey = 0;
                        }
                        if ($key == $s->UrunGizli && $active == 'Hayır') {
                            $btn = '<a href="' . $link . '/ActiveUpdateOneCikar/' . $s->id . '/' . $newkey . '">' . $active . '</a>';
                        }else if( $key == $s->UrunGizli && $active == 'Evet'){
                            $btn = '<a href="' . $link . '/ActiveUpdateOneCikar/' . $s->id . '/' . $newkey . '">' . $active . '</a>';
                        }
                    }
                    $yenikayit[] = $btn;    
                }else {
                    if (isset($this->ustkatkontrol) and count($this->ustkatkontrol) > 0 and $alan == $this->ustkatkontrol[0]) {
                        $yenikayit[] = adgetir($s->$alan, $kontrolust, $this->ustkatkontrol[1]);
                    } else {
                        $bol = explode(' as ', $alan);
                        if (count($bol) > 1) {
                            $yenikayit[] = $s->{$bol[1]};
                        } else {
                            $yenikayit[] = $s->$alan;
                        }
                    }
                }
                $x++;
            }


            $kb = '';
            if ($this->KopyalaButonu) {
                $kb = '<button id="d_clip_button" class="kopyala btn btn-default btn-sm" data-clipboard-text="';
                $kb.=eval($this->KopyalaVeri);
                $kb.='"><b>Kopyala</b></button>';
            }

            if ($this->DetayButonu) {
                $kb .= ' <a class="btn btn-default btn-sm" href="'.$link.'/Detay/'.$s->id.'">Detay</a>';
            }

            if ($this->DuzenleButonu) {
                $kb.=' <a href="'.$link.'/Duzenle/'.$s->id.'" class="btn btn-sm btn-info">Düzenle</a>';
            }

            if ($this->SilButonu){
                if ($this->SilmeMesaji!='') {
                    $kb.= ' <a onclick="uyariveri('.$s->id.')" class="btn btn-sm btn-danger">Sil</a>';
                } else {
                    $kb.= ' <a href="'.$link.'/Sil/'.$s->id.'" class="btn btn-sm btn-danger">Sil</a>';
                }
            }
            
            if ($this->DilTanimlamaButonu) {
            $defaultdil = \App\Http\Models\Dil::where('DefaultDil',1)->first();
                $kb.=' <a href="'.$link.'/Tanimla/'.$defaultdil->id.'/'.$s->id.'" class="btn btn-sm btn-warning">Dil Tanımla</a>';
            }
            
            if($this->FotoButonu){
                $url = url('/');
                $kb.=' <a href="'.$url.'/Admin/Foto/Ekle/'.$s->id.'" class="btn btn-sm btn-warning">Fotograf Ekle</a>';
            }
            if($this->FotoSiralaButonu){
                $url = url('/');
                $kb.=' <a href="'.$url.'/Admin/Foto/Sirala/'.$s->id.'" class="btn btn-sm btn-success">Sırala</a>';
            }
            
            $yenikayit[] = $kb;


            $data[] = $yenikayit;
        }


        echo json_encode(array(
            "recordsTotal" => intval($recordsTotal),
            "recordsFiltered" => intval($recordsFiltered),
            "data" => $data
        ));
    }

    public function create(){ // Ekleme Formu
        $this->DegiskenAta();
        $this->Degiskenler['Dil'] = $this->Dil;
        $this->Degiskenler['is_active'] = $this->is_activeButonu;
        $this->Degiskenler['AktifText'] = $this->AktifText;
        $this->Degiskenler['MetaAlani'] = $this->MetaAlani;
        return view('Admin.'.$this->GetControllerAdi().'Form')->with($this->Degiskenler)->with('Title',$this->Title);
    }

    protected function DegiskenAta(){} // Ekle ve Düzenle Formuna Veri Gönderilmek istenirse

    public function store(Request $request) {
        return $this->update($request,0);
    }

    public function show($id) {
		$sql = "select * from ".$this->Table." where id='".$id."'";
    	$veri = DB::select(DB::raw($sql))[0];
    	$ustkat = '-Yok-';
    	 if (count($this->ustkatkontrol) > 0) {
            $ustbilgiler = DB::select(DB::raw('select id,is_active,'.implode(',', $this->ustkatkontrol).' from '.$this->Table." where id='".$veri->{$this->ustkatkontrol[0]}."'"));
            if(count($ustbilgiler)>0){
				$ustkat = $ustbilgiler[0]->{$this->ustkatkontrol[1]};
			}
        }
        $degiskenler = [];
        if(count($this->Birlesmeler)>0){
        	foreach($this->Birlesmeler as $key => $value){
				$jointable = explode('.',$value[1])[0];
				//'id'=>['ayarlar.id','ayarlar.Logo']
				$sql = "select ".$value[1]." as Adi from ".$this->Table.",".$jointable." where ".$this->Table.".id='".$id."' and  ".$this->Table.".".$key."=".$value[0];
				$sor = DB::select(DB::raw($sql));
				if(count($sor)>0){
					$degiskenler[$key] = $sor[0]->Adi;
				}else{
					$degiskenler[$key] = '-Yok-';					
				}
			}
		}
        return view('Admin.'.$this->GetControllerAdi().'Detay', ['veri' => $veri,'ustkat'=>$ustkat])->with($degiskenler);
    }
    
    public function edit($id) {
        $this->DegiskenAta();
        $this->Degiskenler['Dil'] = $this->Dil;
        $this->Degiskenler['is_active'] = $this->is_activeButonu;
        $this->Degiskenler['AktifText'] = $this->AktifText;
        $this->Degiskenler['MetaAlani'] = $this->MetaAlani;
        
		//	$this->Degiskenler['veri'] = VT::find($id);
			$this->Degiskenler['veri'] = DB::select("select * from ".$this->Table." where id = ?",[$id])[0];
		
        return view('Admin.'.$this->GetControllerAdi().'Form')->with($this->Degiskenler)->with('Title',$this->Title);
    }

    public function update(Request $request, $id) {}

    protected function Slug($baslik, $id = 0, $x = 0){
        $slugmetin = Fnk::Slug($baslik);
        if ($x != 0) {
            $slugmetin.='-' . $x;
        }

        if ($id == 0) {
        	$slugsor = DB::select(DB::raw("select * from ".$this->Table." where Slug='".$slugmetin."'"));
        }else{
            $slugsor = DB::select(DB::raw("select * from ".$this->Table." where Slug='".$slugmetin."' and id!='".$id."'"));
        }
        
        if (count($slugsor) > 0) {
            return $this->Slug($baslik, $id,  ++$x);
        } else {
            return $slugmetin;
        }
    }
    
	public function destroy($id) {
		/*if($this->ResimKlasor!==FALSE){
			$resimsor = DB::table($this->Table)->where('id',$id)->select('Resim')->get();
			$resimsor = $resimsor[0];
			$klasor = 'images/uploads';
            	if($this->ResimKlasor===TRUE){
            		$klasor.='/'.$this->GetControllerAdi();
				}else{
            		if($this->ResimKlasor!=''){
						$klasor.='/'.$this->ResimKlasor;
					}
				}
			if($resimsor->Resim!='../portakalyazilim.gif'){
					File::delete($klasor.'/'.$resimsor->Resim);
				}
			DB::table($this->Table)->where('id',$id)->delete();
		}else{
			DB::table($this->Table)->where('id',$id)->delete();
		}*/
            DB::table($this->Table)->where('id',$id)->update(['is_active'=>-1]);
        return redirect()->back();
    }
    
    public function ActiveUpdate($id, $deger) {
    	DB::table($this->Table)->where('id',$id)->update(['is_active'=>$deger]);
        return redirect()->back();
    }
    public function ActiveUpdateOneCikar($id, $deger) {
    	DB::table($this->Table)->where('id',$id)->update(['OneCikarilmis'=>$deger]);
        return redirect()->back();
    }
    public function ActiveUpdateKampanyaliYap($id, $deger) {
    	DB::table($this->Table)->where('id',$id)->update(['Kampanya'=>$deger]);
        return redirect()->back();
    }
    public function SiraKaydet(Request $request){
		foreach($request->id as $Sira => $id){
			$Sira++;
			DB::table($this->Table)->where('id',$id)->update(['Sira'=>$Sira]);
		}
	}
}
