<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;


use App\Http\Models\DilSabiti as VT;
use App\Http\Models\DilTanimlama;
use App\Http\Models\Dil;
use Validator;
use Fnk;
use Image;
use File;



class DilSabitiController extends AdminController{
	
    protected $Table = 'dilsabitleri'; // Database Alanları
    protected $ListelemeAlanlari = ['SabitAdi','Slug']; // Database Alanları
    protected $ListelemeBasliklari = ['Sabit Adi','Slug'];
    protected $WhereListele = ""; // listeleme sorgusuna where koşulu koymak istenirse örn : MenuTipi='icerik'
    protected $BirlesmelerText = []; // Listelenecek Veride if atıp farklı veri göstermek içindir. örn : ['MenuTipi'=>['icerik'=>'İçerik','link'=>'Link']] veya ['Dil'=>['tr'=>'Türkçe','en'=>'İngilizce']]
    protected $Birlesmeler = []; // key kısmına ListelemeAlanlarindaki ilgili alan yazılıyor. Value Kısmı Array olacak. 0 eleman inner join yapılacak tabloadi.columnadi 1. eleman Kısmı tabloadi.goruntulenecekColumn örn: 'id'=>['ayarlar.id','ayarlar.Logo']
    protected $ListelemeSirala = 0; // Liste sıralaması desc olarak hangi kolondan olacağı belirtilir. 0 dan başlar
    protected $EkleButonu = true; // Listeleme sayfasının üzerindeki Yeni Ekle Butonu
    protected $SiralamaButonu = false; // Listeleme sayfasının üzerinde Yeni Ekle Butonu yanında Sırala Butonu
    protected $TumunuSilButonu = false; // Listeleme sayfasının üzerinde Tümünü Sil Butonu
    protected $KopyalaButonu = false; // Listeleme sayfasının veri satırlarında Kopyala butonu
    protected $KopyalaVeri = ''; // eval fonksiyonu için php yazılabilir
    protected $DuzenleButonu = true; // Listeleme sayfasının veri satırlarında Düzenle butonu
    protected $SilButonu = true; // Listeleme sayfasının veri satırlarında Sil butonu
    protected $DetayButonu = true; // Listeleme sayfasının veri satırlarında Detay butonu
    protected $Title = 'Dil Sabiti';
    protected $SilmeMesaji = ''; // Boş Bırakılırsa Direk Silme İşlemi Yapar Dolu İsede /// Silmek İstediğiniz "$silmemesaji" Bulunuyorsa Onlarda Silinecektir. <br>Devam etmek istediğinizden Emin misiniz? şeklinde yazar.
    protected $ResimKlasor = false; // Veritabanından Resim adında bir alan çekildiyse public/uploads baz alınır ek klasör varsa belirtilir.
    protected $AktifText = ['Pasif','Aktif']; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $ustkatkontrol = []; // inner join aynı tablo içinde ise ve bağlılıkları var ise array 0. eleman ListelemeAlanlari 1. eleman da column name örn: ['UstKatId','Adi'] // Detay sayfası ve Listeleme Sayfasında Kullanılır.

    protected $Degiskenler = []; //form viewlere gönderilecek veriler
    protected $is_activeButonu = false; //Form viewlerinde is_active selectbox durumu
    protected $Dil = false;
  
  
    public function update(Request $request,$id){
	        $rules = [
	            'SabitAdi' => 'required',
	            'Slug' => 'required',
	        ];
	        $validator = Validator::make($request->all(), $rules);
	        if ($validator->fails()) {
	        	return json_encode(['islem' => false, 'error' => $validator->errors()->all()]);
	        }
	        
	       if($request->id==''){
                    $vt = new VT;
			}else{
            	$vt = VT::find($request->id);
			}
			
		        $vt->SabitAdi = $request->SabitAdi;
		        $vt->Slug = $request->Slug;
	        $save = $vt->save();
			
                $defaultdil = Dil::where('DefaultDil',1)->first();
                $diltanimlama = new DilTanimlama;
                $diltanimlama->DilId = $defaultdil->id;
                $diltanimlama->SabitId = $vt->id;
                $diltanimlama->Slug = $vt->Slug;
                $diltanimlama->Ceviri = $vt->SabitAdi;
                $diltanimlama->save();
                
            if($request->id==''){
				$path = app_path('../database/seeds/DilSabitleriSeeder.php');
				$yeniicerik = '';
				$contents = File::get($path);
				$bol = explode(']);',$contents);
				foreach($bol as $key => $value){
					if($key==count($bol)-1){
		$yeniicerik.='
		DilSabiti::create([\'SabitAdi\' => \''.$vt->SabitAdi.'\',\'Slug\' => \''.$vt->Slug.'\',\'is_active\'=>1]);';
						$yeniicerik.=$value;
					}else{
						$yeniicerik.=$value;
						$yeniicerik.=']);';
					}
				}
				File::put($path, $yeniicerik);
			}
		
        return json_encode(array('islem' => $save));
    }
    
    public function destroy($id){
		$dilsabiti = VT::find($id);
			
			$path = app_path('../database/seeds/DilSabitleriSeeder.php');
				$yeniicerik = '';
				$contents = File::get($path);
				$bol = explode(']);',$contents);
				foreach($bol as $key => $value){
		$bul='
		DilSabiti::create([\'SabitAdi\' => \''.$dilsabiti->SabitAdi.'\',\'Slug\' => \''.$dilsabiti->Slug.'\',\'is_active\'=>1';
					
					if($value==$bul){
							
					}elseif($key==count($bol)-1){
						$yeniicerik.=$value;
					}else{
						$yeniicerik.=$value;
						$yeniicerik.=']);';
					}
				}
				File::put($path, $yeniicerik);
		VT::where('id',$dilsabiti->id)->delete();
        return redirect()->back();
	}
}
