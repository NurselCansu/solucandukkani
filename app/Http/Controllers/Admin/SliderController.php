<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Models\Slider as VT;
use Validator;
use Input;
use Image;
use Fnk;
use File;

class SliderController extends AdminController{
	
    protected $Table = 'sliderlar'; // Database Alanları
    protected $ListelemeAlanlari = ['Adi','Resim','is_active']; // Database Alanları
    protected $ListelemeBasliklari = ['Adi','Resim','Durum'];
    protected $WhereListele = ""; // listeleme sorgusuna where koşulu koymak istenirse örn : MenuTipi='icerik'
    protected $BirlesmelerText = []; // Listelenecek Veride if atıp farklı veri göstermek içindir. örn : ['MenuTipi'=>['icerik'=>'İçerik','link'=>'Link']] veya ['Dil'=>['tr'=>'Türkçe','en'=>'İngilizce']]
    protected $Birlesmeler = []; // key kısmına ListelemeAlanlarindaki ilgili alan yazılıyor. Value Kısmı Array olacak. 0 eleman inner join yapılacak tabloadi.columnadi 1. eleman Kısmı tabloadi.goruntulenecekColumn örn: 'id'=>['ayarlar.id','ayarlar.Logo']
    protected $ListelemeSirala = 0; // Liste sıralaması desc olarak hangi kolondan olacağı belirtilir. 0 dan başlar
    protected $EkleButonu = true; // Listeleme sayfasının üzerindeki Yeni Ekle Butonu
    protected $SiralamaButonu = true; // Listeleme sayfasının üzerinde Yeni Ekle Butonu yanında Sırala Butonu
    protected $TumunuSilButonu = false; // Listeleme sayfasının üzerinde Tümünü Sil Butonu
    protected $KopyalaButonu = false; // Listeleme sayfasının veri satırlarında Kopyala butonu
    protected $KopyalaVeri = ''; // eval fonksiyonu için php yazılabilir
    protected $DuzenleButonu = true; // Listeleme sayfasının veri satırlarında Düzenle butonu
    protected $SilButonu = true; // Listeleme sayfasının veri satırlarında Sil butonu
    protected $DetayButonu = true; // Listeleme sayfasının veri satırlarında Detay butonu
    protected $Title = 'Slider';
    protected $SilmeMesaji = ''; // Boş Bırakılırsa Direk Silme İşlemi Yapar Dolu İsede /// Silmek İstediğiniz "$silmemesaji" Bulunuyorsa Onlarda Silinecektir. <br>Devam etmek istediğinizden Emin misiniz? şeklinde yazar.
    protected $ResimKlasor = "Slider"; // Veritabanından Resim adında bir alan çekildiyse public/uploads baz alınır ek klasör varsa belirtilir.
    protected $AktifText = ['Pasif','Aktif']; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $ustkatkontrol = []; // inner join aynı tablo içinde ise ve bağlılıkları var ise array 0. eleman ListelemeAlanlari 1. eleman da column name örn: ['UstKatId','Adi'] // Detay sayfası ve Listeleme Sayfasında Kullanılır.

    protected $Degiskenler = []; //form viewlere gönderilecek veriler
    protected $is_activeButonu = true; //Form viewlerinde is_active selectbox durumu
    protected $DilTab = false; // Form viewlerde Dil Tabları olması istenirse
    protected $MetaAlani = true;
    
    public function update(Request $request,$id){
    	$kayit = true;
	    $rules = [
	       
			'Adi' => 'required',
			'Resim'=>'mimes:jpeg,bmp,png,gif',
	        'is_active' => 'required',
	        'MetaTag' => 'required',
	        'MetaTitle' => 'required',
	        'MetaDescription' => 'required'
	    ];
	    $validator = Validator::make($request->all(), $rules);
	    if ($validator->fails()) {
	    	$kayit = false;
	    }
	    if($kayit===false){
			return json_encode(['islem' => false, 'error' => $validator->errors()->all()]);
		}
			
		if($kayit){

	       	if($request->id==''){
				$vt = new VT;
			}else{
            	$vt = VT::find($request->id);
			}
		    $vt->DilId = 1;
		    $vt->Adi = $request->Adi;
		    $vt->Slug = $this->Slug($request->Adi,$request->id);
            
            if ($request->hasFile('Resim')){

            	$file = $request->file('Resim');
            	$klasor = 'images/uploads/Slider';
				$ad = Fnk::ResimAdi($klasor,$file->getClientOriginalName());	    
				$file->move($klasor,$ad);
				if($vt->Logo!='../portakalyazilim.gif'){
					File::delete('images/uploads/Slider/'.$vt->Resim);
				}
             	$vt->Resim = $ad;
             	$mesaj['yeniresim'] = $klasor.'/'.$ad; //url('images/uploads') sabittir.
            }	    
		    /*$vt->SliderTipi = $request->SliderTipi;
		    $vt->LinkAcilisTipi = $request->LinkAcilisTipi;
            if($request->SliderTipi == 'icerik'){
				$vt->Icerik = $request->Icerik;
            }
            $link = '#';
			if($request->SliderTipi == 'link'){
	            if(substr($request->Link, 0, 4) == "http"){
	                $link = $request->Link;
	            }else{
	                if($request->Link != "#" && $request->Link != ''){
	                    $link = 'http://' . $request->Link;
	                }
	            }
	        }
			*/
    //         if ($request->hasFile('Katman')){
    //         	$file = $request->file('Katman');
    //         	$klasor = 'images/uploads/Slider';
				// $ad = Fnk::ResimAdi($klasor,$file->getClientOriginalName());	    
				// $file->move($klasor,$ad);
				// if($vt->Logo!='../portakalyazilim.gif'){
				// 	File::delete('images/uploads/Slider/'.$vt->Katman);
				// }
    //          	$vt->Katman = $ad;
    //          	$mesaj['yenikatman'] = $klasor.'/'.$ad; //url('images/uploads') sabittir.
    //         }
			$vt->KisaIcerik = $request->KisaIcerik;

			if($request->hasFile('Resim')){
				Fnk::ThumbnailYap($klasor.'/'.$ad, 1920, 590,true,true,'ccc');
			}
			//$vt->Link = $link;

			$vt->baslangic = Fnk::TarihDuzenle($request->baslangic);
			$vt->bitis = Fnk::TarihDuzenle($request->bitis);
			$vt->is_active = $request->is_active;
        	$vt->MetaTag = json_encode($request->MetaTag,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
			$vt->MetaTitle = $request->MetaTitle;
			$vt->MetaDescription = $request->MetaDescription;
	        $save = $vt->save();
		}
		
		if($kayit){
			$vt->save();
		}
        return json_encode(array('islem' => $save));
    }
    
    public function Sirala($UstKatId = 0) {
        $baslik = $this->Title;
        $vtcolumnname = 'Adi';
            $anakategoriler = VT::where('is_active','!=','-1')->get();
            $veri = VT::where('is_active','!=','-1')->orderBy('Sira','ASC')->get();
        return view('Admin.Layout.Sirala', ['veri' => $veri, 'anakategoriler' => $anakategoriler, 'ustkatid' => $UstKatId, 'vtcolumnname' => $vtcolumnname, 'baslik' => $baslik]);
    }
    

}
