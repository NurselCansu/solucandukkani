<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Fnk;
use App\Http\Models\Haber;
use App\Http\Models\Menu;

class DashboardController extends Controller
{
    public function index(){
       return view('Admin.Dashboard');
    }
    public function degistir(){
        $veriler = Haber::all();
        if(count($veriler)>0){
            foreach ($veriler as $veri) {
                $veri->Icerik = str_replace('http://esenaydogdu.portakalyazilim.com.tr/public/','http://esenaydogdu.com/',$veri->Icerik);
                $veri->save();
            }
        }

        $veriler = Menu::all();
        if(count($veriler)>0){
            foreach ($veriler as $veri) {
                $veri->Icerik = str_replace('http://esenaydogdu.portakalyazilim.com.tr/public/','http://esenaydogdu.com/',$veri->Icerik);
                $veri->save();
            }
        }


    }
}
