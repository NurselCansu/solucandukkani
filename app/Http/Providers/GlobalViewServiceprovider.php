<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Models\Ayarlar;
use App\Http\Models\IletisimIstekleri;
use App\Http\Models\IletisimBilgileri;
use App\Http\Models\Dil;
use App\Http\Models\Google;
use App\Http\Models\Tawk;
use App\Http\Models\Urun;
use App\Http\Models\Bayilik;
use App\Http\Models\AltMenu;
use App\Http\Models\SosyalMedya;

use View;
use Request;
use Auth;

class GlobalViewServiceprovider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {
        View::composer('*', function($view) {
            if (Request::segment(1) == 'Admin') {
                $view->with('ayarlar', Ayarlar::findOrFail(1))
                        ->with('iletisimistekleri', IletisimIstekleri::orderBy('is_active', 'desc')->get())
                        ->with('tumdiller', Dil::where('is_active',1)->orderBy('Sira','ASC')->get())
                        ->with('subeler', IletisimBilgileri::where('is_active', 1)->get())
                         
                        
                        ->with('admin', Auth::guard('admin')->user());
                        
               
            } else {
                if (!empty(Request::segment(1))) {


                    $view->with('ayarlar', Ayarlar::where('DilId', Dil::where('KisaAd', Request::segment(1))->first()->id)->where('is_active', 1)->first())
                            ->with('google_analytics',Google::where('is_active',1)->first())
                            ->with('tawk',Tawk::where('is_active',1)->first())
                            ->with('tumdiller', Dil::where('is_active',1)->orderBy('Sira','ASC')->get())
                            ->with('segment2', Request::segment(2))
                            ->with('segment3', Request::segment(3))
                            ->with('segment4', Request::segment(4))
                            ->with('varsayilandil', Dil::where('DefaultDil', 1)->first())
                            ->with('altmenuler', AltMenu::where('DilId', Dil::where('KisaAd', Request::segment(1))->first()->id)->where('is_active', 1)->get())
                            ->with('iletisimbilgileri', IletisimBilgileri::where('is_active', 1)->first())
                            ->with('bayilerYI', Bayilik::where('DilId', 1)->where('is_active',1)->where('bayilik_faliyet_alani',1)->get())
                            ->with('bayilerYD', Bayilik::where('DilId', 1)->where('is_active',1)->where('bayilik_faliyet_alani',0)->get())
                            ->with('subeler', IletisimBilgileri::where('DilId', Dil::where('KisaAd', Request::segment(1))->first()->id)->where('is_active',1)->get())
                            ->with('sosyal', SosyalMedya::where('is_active', 1)->get());
                } else {
                    $view->with('ayarlar', Ayarlar::findOrFail(1))
                          
                            ->with('tumdiller', Dil::where('is_active',1)->orderBy('Sira','ASC')->get())
                            ->with('iletisimbilgileri', IletisimBilgileri::where('is_active', 1)->first())
                            ->with('bayilerYI', Bayilik::where('DilId', 1)->where('is_active',1)->where('bayilik_faliyet_alani',1)->get())
                            ->with('bayilerYD', Bayilik::where('DilId', 1)->where('is_active',1)->where('bayilik_faliyet_alani',0)->get())
                            ->with('varsayilandil', Dil::where('DefaultDil', 1)->first());
                }
            }
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        //
    }

}
