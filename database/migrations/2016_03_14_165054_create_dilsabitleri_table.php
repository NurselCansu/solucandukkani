<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDilsabitleriTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('dilsabitleri', function (Blueprint $table) {
            $table->engine = "MyISAM";
            $table->increments('id');
            $table->string('SabitAdi');
            $table->string('Slug');
            $table->tinyInteger('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('dilsabitleri');
    }

}
