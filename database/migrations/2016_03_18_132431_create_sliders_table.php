<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('sliderlar', function (Blueprint $table) {
            $table->engine = "MyISAM";
            $table->increments('id');
            $table->tinyInteger('DilId');
            $table->string('Adi');
            $table->string('Slug');
            $table->string('Resim');
            $table->string('SliderTipi');
            $table->string('LinkAcilisTipi');
            $table->longText('Icerik');
            $table->string('Link');
            $table->integer('Sira');

            $table->string('KisaIcerik');  
            $table->string('MetaTag');
            $table->string('MetaTitle');
            $table->string('MetaDescription');
            $table->tinyInteger('is_active');
            $table->dateTime('baslangic');
            $table->dateTime('bitis');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('sliderlar');
    }

}
