<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAyarlarsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('ayarlar', function (Blueprint $table) {
            $table->engine = "MyISAM";
            $table->increments('id');
            $table->tinyInteger('DilId');           
            $table->string('FirmaAdi');
            $table->string('AnasayfaResim');
            $table->string('Logo');
            $table->string('AnaSayfaBaslik');
            $table->longText('AnaSayfaIcerikSutun1');
            $table->longText('AnaSayfaIcerikSutun2');
            $table->longText('AnaSayfaIcerikSutun3');
            $table->longText('AnaSayfaAltIcerikSutun1');
            $table->longText('AnaSayfaAltIcerikSutun2');
            $table->longText('AnaSayfaAltIcerikSutun3');
            $table->longText('AnaSayfaAltSliderSutun1');
            $table->longText('AnaSayfaAltSliderSutun2');
            $table->longText('AnaSayfaAltSliderSutun3');
            $table->tinyInteger('is_Kampanyali_Urunler');
            $table->string('MetaTag');
            $table->string('MetaTitle');
            $table->string('MetaDescription');
            $table->tinyInteger('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('ayarlar');
    }

}
