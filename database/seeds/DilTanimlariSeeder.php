<?php

use Illuminate\Database\Seeder;
use App\Http\Models\DilTanimlama;

class DilTanimlariSeeder extends Seeder {

    public function run() {
        DilTanimlama::truncate();
        DilTanimlama::create(['id' => 1, 'DilId' => '1', 'SabitId' => '1', 'Slug' => 'Iletisim', 'Ceviri' => 'İletişim', 'is_active' => 1]);
        DilTanimlama::create(['id' => 2, 'DilId' => '2', 'SabitId' => '1', 'Slug' => 'Iletisim', 'Ceviri' => 'Contact', 'is_active' => 1]);
    }

}
