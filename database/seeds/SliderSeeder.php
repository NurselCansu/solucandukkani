<?php

use Illuminate\Database\Seeder;
use App\Http\Models\Slider;
use App\Http\Fnk;

class SliderSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        Slider::truncate();
        
    }

}
