<?php

use Illuminate\Database\Seeder;
use App\Http\Models\Urun;

class UrunSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Urun::truncate();
        
    }

}
