<?php

use Illuminate\Database\Seeder;
use App\Http\Models\Dil;

class DilSeeder extends Seeder {

    public function run() {
        Dil::truncate();
        Dil::create([
            'id' => 1,
            'KisaAd' => 'tr',
            'UzunAd' => 'Türkçe',
            'Resim' => 'tr.svg',
            'DefaultDil' => 1,
            'is_active' => 1,
        ]);
    }

}
