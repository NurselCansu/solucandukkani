<?php

use Illuminate\Database\Seeder;
use App\Http\Models\User;

class UserTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        User::truncate();
        User::create(['id' => 1, 'name' => 'Portakal Yazılım', 'email' => 'info@portakalyazilim.com', 'password' => Hash::make('123456')]);
        User::create(['id' => 2, 'name' => 'Nursel Cansu Özkan', 'email' => 'nursel@portakalyazilim.com.tr', 'password' => Hash::make('123456')]);
        User::create(['id' => 3, 'name' => 'Osman Aşar', 'email' => 'osman@portakalyazilim.com', 'password' => Hash::make('123456')]);
    }

}
