<?php

use Illuminate\Database\Seeder;
use App\Http\Models\VideoGaleri;

class VideoGaleriSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        VideoGaleri::truncate();
        VideoGaleri::create([
            'id' => 1,
            'Adi' => 'Video Adı',
            'Link' => 'LV58fxXOQuE',
            'Slug' => 'Video-Adi',
            'is_active' => 1
        ]);
    }

}
